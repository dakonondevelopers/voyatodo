<?php

namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use \yii\web\UploadedFile;
use app\models\User;
use app\models\FormAdvertiser;
use app\models\VTTcategoryproduct;
use app\models\VTTconfig;
use app\models\VTTuserbank;
use app\models\VTTcreditcard;
use app\models\VTTbank;
use app\models\VTTproduct;
use app\models\VTTimage;

class ProductoController extends \yii\web\Controller
{
	public $layout="";

    public function actionIndex()
    {
        return $this->redirect(['/site']);
    }

    public function actionV($id)
    {
		$product = VTTproduct::find()
				->where("pkproduct =:pkproduct", [":pkproduct" => $id])
	            ->one();
	    $galery = VTTimage::find()->where("fkproduct =:pkproduct", [":pkproduct" => $product->pkproduct])->all();
	    $categories = VTTcategoryproduct::find()->all();
        if(\Yii::$app->user->isGuest)
        {
			$this->layout = 'layoutnologin';
			return $this->render('viewUser',['product' => $product,'categories' => $categories, 'galery' => $galery]);
        }
        else
			if (User::isUserSponsor(Yii::$app->user->identity->id))
			{
				$this->layout = 'layoutSponsor';
				return $this->render('viewUser',['product' => $product,'categories' => $categories, 'galery' => $galery]);
			}
			else 
				if (User::isUserAdvertiser(Yii::$app->user->identity->id) && 
					Yii::$app->user->identity->id == $product->getFkuser0()->one()->id)
			   	{
					$this->layout = 'layoutAdvertiser';
					$model = new FormAdvertiser;
			    	$message = null;
					if($model->validate() && $model->load(Yii::$app->request->post()))
					{
						$img_product = \yii\web\UploadedFile::getInstance($model, 'event_image');
						if($img_product)
						{
							$new_product = new VTTproduct;
							$rnd = $this->randKey("abcdef0123456789", 50);
			                $fileName = $rnd . str_replace(' ','_',$img_product->name);
			                $img_product->saveAs(Yii::getAlias('@webroot') . '/images/system_imgs/' . $fileName);
			                if($this->validateImages(600,600,Yii::getAlias('@webroot') . '/images/system_imgs/' .$fileName))
			                {
			                    $new_product->product_image = '/images/system_imgs/' . $fileName;
			                    $new_product->product_name = $model->product_name;
			                    $new_product->product_review = $model->product_review;
			                    $new_product->product_description = $model->product_description;
			                    $new_product->fkcategory = $model->fkcategory;
			                    $new_product->product_terms = $model->product_terms;
			                    $new_product->product_value = $model->product_value;
			                    $new_product->product_qty = $model->product_qty;
			                    $new_product->fkuser = Yii::$app->user->identity->id;
			                    if($new_product->insert())
			                    {
			                        $pkproduct = $new_product->pkproduct;
			                        $pkavailability = explode(",", $_POST['FormAdvertiser']['dates']);
			                        foreach ($pkavailability as $val)
			                        {
			                            if($val != '')
			                            {
			                                $availability = VTTavailability::find()
			                                            ->where('pkavailability = :pkavailability', [':pkavailability' => $val])
			                                            ->one();
			                                $availability->fkproduct = $pkproduct;
			                                $availability->availability_code = "";
			                                $availability->update();
			                            }
			                        }
			                        $message = 2;
			                    }
			                }
			                
						}
					}
			    	$creditcard = VTTcreditcard::find()-> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])->all();
			    	$listbanks = VTTbank::find()->all();
			    	$userbank = VTTuserbank::find()
						                -> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
						                ->all();
					$products = VTTproduct::find()
							-> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
			                ->all();
			        return $this->render('viewAdvertiser',['model' => $model, 
			        							  'categories' => $categories, 
			        							  'creditcard' => $creditcard, 
			        							  'listbanks' => $listbanks,
			        							  'userbank' => $userbank,
			        							  'product' => $product,
			        							  'message' => $message,
			        							  'products' => $products,
			        							  'galery' => $galery
			        							  ]);
			   	}
			   	else
			   		if(User::isUserCustomer(Yii::$app->user->identity->id))
				   	{
				   		$this->layout = 'layoutUser';
						return $this->render('viewUser',['product' => $product,'categories' => $categories, 'galery' => $galery]);
				   	}
				   	else
				   		if(User::isUserAdvertiser(Yii::$app->user->identity->id))
					   	{
					   		$this->layout = 'layoutAdvertiser';
							return $this->render('viewUser',['product' => $product,'categories' => $categories, 'galery' => $galery]);
					   	}
					   	else
					   	{
					   		$this->layout = 'layoutAdmin';
							return $this->render('viewUser',['product' => $product,'categories' => $categories, 'galery' => $galery]);
					   	}
    }

    public function actionCategoria($id)
    {
    	if(\Yii::$app->user->isGuest)
			$this->layout = 'layoutnologin';
        else
			if (User::isUserSponsor(Yii::$app->user->identity->id))
				$this->layout = 'layoutSponsor';
			else 
				if (User::isUserCustomer(Yii::$app->user->identity->id))
					$this->layout = 'layoutUser';
                else
                    if(User::isUserAdvertiser(Yii::$app->user->identity->id))
                        $this->layout = 'layoutAdvertiser';
                    else
                    	$this->layout = 'layoutAdmin';
        $category = VTTcategoryproduct::find()
            ->where("categoryproduct_name =:categoryproduct_name", [":categoryproduct_name" => $id])
            ->one();
        if($category)
        {
	    	$products = VTTproduct::find()
	    			->where("fkcategory =:fkcategory", [":fkcategory" => $category->pkcategoryproduct])
	            	->all();
	        $nameCategory = $category->categoryproduct_name;
        }
        else
        {
        	$products = null;
        	$nameCategory = 'NO EXISTE';
        }
        return $this->render('category',['products' => $products, 'nameCategory' => $nameCategory]); 
    }

    public function beforeAction($action) 
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionSetproduct()
    {
    	$data = Yii::$app->session->get('products');
    	if(is_null($data))
    	{
    		$data .= $_POST['id'].','.$_POST['date'].','.$_POST['cant'].';';
    		Yii::$app->session->set('products', $data);
    	}
    	else
    	{
    		$validateData = explode(';', $data);
	    	$tmp = "";

	    	var_dump($validateData);
	    	die();

	    	foreach ($validateData as $value) 
	    	{
	    		if($value != '')
	    		{
		    		$products = explode(',', $value);
		    		if($products[0] == $_POST['id'] && $products[1] == $_POST['date'])
		    		{
		    			$products[2] = $_POST['cant'];
		    			$tmp .= $products[0].",".$products[1].",".$products[2].";";
		    		}
		    		else
		    			$tmp .= $products[0].",".$products[1].",".$products[2].";";
	    		}
	    	}
	    	Yii::$app->session->set('products', $tmp);
    	}
    	$cantData = explode(';', $data);
    	return count($cantData)-1;
    }

}