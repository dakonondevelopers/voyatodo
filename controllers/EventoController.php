<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use \yii\web\UploadedFile;
use app\models\User;
use app\models\Users;
use app\models\Event;
use app\models\VTTcountry;
use app\models\VTTcity;
use app\models\VTTdepartament;
use app\models\VTTbank;
use app\models\VTTcategory;
use app\models\VTTimage;
use app\models\VTTuserbank;
use app\models\VTTcreditcard;
use app\models\VTTticket;
use app\models\VTTformeventgeneric;
use app\models\VTTbuy;
use app\models\FormSponsor;
use app\models\FormCreateEvent;
use app\models\VTTproposal;
use app\models\VTTmessage;
use app\models\VTTproposaldetails;
use app\models\VTTformgenericresponse;
use app\models\VTTpublicity;
use dosamigos\qrcode\QrCode;
use dosamigos\qrcode\lib\Enum;
use yii\db\Query;

class EventoController extends \yii\web\Controller
{

	public $layout="";

    public function actionIndex()
    {
        return $this->redirect(['/site']);
    }

    private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }

    /* Obtiene los datos de la opción asistentes */
    private function getDatasist($pkevent)
    {
        $query = new Query;
        $query->select(['vt_tbuy.pkbuy','vt_tbuy.buy_date','vt_tbuy.buy_value','vt_tticket.fktipetickect','vt_tformeventgeneric.formeventgeneric','vt_tformgenericresponse.formgenericresponse_response'])
        ->from('vt_tbuy')
        ->join('join',
            'vt_tticket',
            'vt_tbuy.fkticket=vt_tticket.pkticket')
        ->join('join',
            'vt_tformeventgeneric',
            'vt_tticket.fkevent=vt_tformeventgeneric.fkevent')
        ->join('join',
            'vt_tformgenericresponse',
            'vt_tformeventgeneric.pkformeventgeneric=vt_tformgenericresponse.fkformeventgeneric')
        ->where('vt_tticket.fkevent=:pkevent', [':pkevent' => $pkevent])
        ->groupBy('vt_tbuy.pkbuy','vt_tformeventgeneric.formeventgeneric')
        ->orderBy('vt_tformgenericresponse.fkformeventgeneric');
        $command = $query->createCommand();
        $data = $command->queryAll();
        return $data;
    }

    public function actionV($id)
    {
    	$event = Event::find()->
            where("event_url =:event_url", [":event_url" => $id])
            ->one();
        $galery = VTTimage::find()->where("fkevent =:pkevent", [":pkevent" => $event->pkevent])->all();
        $buy = new VTTbuy;
        $event_proposals = VTTproposal::find()
			                -> where("fkevent =:fkevent", [":fkevent" => $event->pkevent])
			                -> andWhere("fkstatus =:fkstatus", [":fkstatus" => 5])
			                ->all();
        if(\Yii::$app->user->isGuest)
        {
			$this->layout = 'layoutnologin';
			return $this->render('viewUser',['event' => $event, 
											 'galery'=>$galery,
											 'buy' => $buy,
											 'event_proposals' => $event_proposals,
											]);
        }
        else
			if (User::isUserSponsor(Yii::$app->user->identity->id))
			{
				$creditcard = VTTcreditcard::find()-> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])->all();
				$model = new FormSponsor;
				$this->layout = 'layoutSponsor';
				$proposals = VTTproposal::find()
			                -> where("fkevent =:fkevent", [":fkevent" => $event->pkevent])
			                -> andWhere("fkuser =:fkuser", [":fkuser" => Yii::$app->user->identity->id])
			                ->one();
			    if(!empty($proposals))
			    {
					$messages = VTTmessage::find()
	                        ->where("fkproposal =:fkproposal", [":fkproposal" => $proposals->pkproposal])
	                        ->andWhere("fkstatus =:fkstatus", [":fkstatus" => 7])
	                        ->orderBy('pkmessage ASC')
	                        ->all();
			    }
			    else
			    	$messages = '';
			   	$image = \yii\web\UploadedFile::getInstance($model, 'sponsor_banner');
			    if($image)
			   	{
			   		$rnd = $this->randKey("abcdef0123456789", 50);
                	$fileName = $rnd . str_replace(' ','_',$image->name);
                	$image->saveAs(Yii::getAlias('@webroot') . '/images/system_imgs/' . $fileName);
                	$validate_details = $proposals->getFkproposaldetails0()->one();                	
				   	if(!empty($validate_details))
				   	{
				   		if(!empty($validate_details->proposaldetails_image))
				   		{
					   		unlink(Yii::getAlias('@webroot') .$validate_details->proposaldetails_image);
					   		$validate_details->proposaldetails_image = '/images/system_imgs/' . $fileName;
					   		$validate_details->proposaldetails_url = $_POST['FormSponsor']['sponsor_url'];
		                    $validate_details->update();
				   		}
				   	}
				   	else
				   	{
				   		$details = new VTTproposaldetails;
	                    $details->proposaldetails_image = '/images/system_imgs/' . $fileName;
	                    $details->proposaldetails_url = $_POST['FormSponsor']['sponsor_url'];
	                    if($details->insert())
	                    {
	                    	$proposals->fkproposaldetails = $details->pkproposaldetails;
	                    	$proposals->update();
	                    }
				   	}	
			   	}
				return $this->render('viewSponsor',['event' => $event, 
													'galery'=>$galery, 
													'creditcard' => $creditcard, 
													'model' => $model,
													'buy' => $buy,
													'proposals' => $proposals,
													'messages' => $messages,
													'event_proposals' => $event_proposals,
													]);
			}
			else 
				if (User::isUserCustomer(Yii::$app->user->identity->id) && 
					Yii::$app->user->identity->id == $event->getFkuser0()->one()->id)
			   	{
					$model = new FormCreateEvent;
                    $message = 0;
					$creditcard = VTTcreditcard::find()-> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])->all();
					$countries = VTTcountry::find()->all();
			        $categories = VTTcategory::find()->all();
			        $listbanks = VTTbank::find()->all();
			        $userbank = VTTuserbank::find()
			                    -> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
			                    ->all();
			        $proposals = VTTproposal::find()
			                    -> where("fkevent =:fkevent", [":fkevent" => $event->pkevent])
			                    ->all();
			        $tickets = VTTticket::find()
                        -> where("fkevent =:fkevent", [":fkevent" => $event->pkevent])
                        ->all();
                    $formdinamic = VTTformeventgeneric::find()
                            ->where("fkevent =:fkevent", [":fkevent" => $event->pkevent])
                            ->all();
                    $asist = $this->getDatasist($event->pkevent);
                    $publicities = VTTpublicity::find()
                            -> where("fkevent =:fkevent", [":fkevent" => $event->pkevent])
                            ->all();
					$this->layout = 'layoutUser';
					return $this->render('viewAccount',['event' => $event, 
													'galery'=>$galery, 
													'creditcard' => $creditcard, 
													'model' => $model,
													'buy' => $buy,
                                                    'fkstatus' => $event->fkstatus,
													'countries' => $countries,
										            'categories' => $categories,
										            'listbanks' => $listbanks,
										            'userbank' => $userbank,
										            'message' => $message,
										            'tickets' => $tickets,
										            'proposals' => $proposals,
										            'event_proposals' => $event_proposals,
                                                    'formdinamic' => $formdinamic,
                                                    'asist' => $asist,
                                                    'publicities' => $publicities
													]);
			   	}
                else
                    if(User::isUserAdvertiser(Yii::$app->user->identity->id)){
                        $this->layout = 'layoutAdvertiser';
                        return $this->render('viewUser',['event' => $event, 
                                                 'galery'=>$galery,
                                                 'buy' => $buy,
                                                 'event_proposals' => $event_proposals,
                                                ]);
                    }
                    else
                        if(User::isUserAdmin(Yii::$app->user->identity->id))
                        {
                            $this->layout = 'layoutAdmin';
                            return $this->render('viewAdmin',['event' => $event, 
                                                 'galery'=>$galery,
                                                 'buy' => $buy,
                                                 'event_proposals' => $event_proposals,
                                                ]);
                        }
        			   	else
        			   	{
        			   		$this->layout = 'layoutUser';
        					return $this->render('viewUser',['event' => $event, 
        											 'galery'=>$galery,
        											 'buy' => $buy,
        											 'event_proposals' => $event_proposals,
        											]);
        			   	}
    }

    public function actionCategoria($id)
    {
    	if(\Yii::$app->user->isGuest)
			$this->layout = 'layoutnologin';
        else
			if (User::isUserSponsor(Yii::$app->user->identity->id))
				$this->layout = 'layoutSponsor';
			else 
				if (User::isUserCustomer(Yii::$app->user->identity->id))
					$this->layout = 'layoutUser';
                else
                    if(User::isUserAdvertiser(Yii::$app->user->identity->id))
                        $this->layout = 'layoutAdvertiser';
        $category = VTTcategory::find()
            ->where("category_route =:category_route", [":category_route" => $id])
            ->one();
        if($category)
        {
        	$event = Event::find()
        		->where("fkcategory1 =:fkcategory1", [":fkcategory1" => $category->pkcategory])
        		->orWhere("fkcategory2 =:fkcategory1",[":fkcategory1" => $category->pkcategory])
                ->all();
            $nameCategory = $category->category_name;
        }
        else
        {
            $event = null;
            $nameCategory = 'NO EXISTE';
        }
        return $this->render('category',['event' => $event, 'nameCategory' => $nameCategory]); 
    }

    public function actionGeneratecalendar()
    {
        $event = Event::find()
            ->where("pkevent =:pkevent", [":pkevent" => $_POST['id']])
            ->one();
        if($event)
        {
            $start_date = $event->event_stardate . ' ' . $event->event_starthour;
            $end_date = $event->event_enddate . ' ' . $event->event_endhour;
            $ical = "BEGIN:VCALENDAR,".
                    "VERSION:2.0,".
                    "PRODID:-//Voy A Todo Inc.//API VoyATodo//EN,".
                    "CALSCALE:GREGORIAN,".
                    "BEGIN:VEVENT,".
                    "UID:" . md5(uniqid(mt_rand(), true)).".voyatodo.evento,".
                    "DTSTAMP:". date('Ymd\TGis\Z').",".
                    "DTSTART:" . $this->dateCal($start_date).",".
                    "DTEND:" . $this->dateCal($end_date).",".
                    "URL:" . "http://www.voyatodo.com/evento/v/".$event->event_url.",".
                    "SUMMARY:" . "Evento ".$event->event_name.",".
                    "DESCRIPTION:" . "Más detalles en http://www.voyatodo.com/evento/v/".$event->event_url.",".
                    "END:VEVENT".",".
                    "END:VCALENDAR";
            return $ical;
        }
        else
            return '';
    }

    private function dateCal($date) 
    {
        return date('Ymd\TGis\Z',strtotime($date));
    }

    public function beforeAction($action) 
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionGetform()
    {
        $form = VTTformeventgeneric::find()->asArray()
            ->where("fkevent=:fkevent", [":fkevent" => $_POST['id']])
            ->all();
        if($form)
            return json_encode($form);
        else
            return '';
    }

    public function actionGetticket()
    {
        $ticket = VTTticket::find()->asArray()
                ->where("pkticket =:pkticket", [":pkticket" => $_POST['id']])
                ->one();
        if($ticket)
            return json_encode($ticket);
        else
            return '';
    }

    //SE REALIZA LA INTEGRACIÓN CON LA PLATAFORMA DE PAGO
    public function actionRegistro()
    {
        $r='';
        $validateBank = VTTuserbank::find()
                -> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
                ->one();
        $validateCard = VTTcreditcard::find()
            ->where("fkuser =:pkuser",[":pkuser" => Yii::$app->user->identity->id])
            ->one();
        if(empty($validateBank) && empty($validateCard))
            $r='2';
        else
        {
            $arrayinformation = json_decode($_POST['json_tickets']);
            $arrayform = json_decode($_POST['json_form']);
            foreach ($arrayinformation as $val) 
            {
                $ticket = VTTticket::findOne($val->pkticket);
                $codes = array();
                $quantity = $val->ticket_value;
                for ($i=0; $i < $quantity; $i++) 
                { 
                    $error = false;
                    $code_random = rand(1, 999999);
                    $cont = 0;
                    foreach ($arrayform as $formVal) 
                    {
                        $form = new VTTformgenericresponse;
                        $form->formgenericresponse_response = $formVal->value;
                        $form->formgenericresponse_code = "'".$code_random."'";
                        $form->formgenericresponse_sequence = $cont;
                        $form->fkformeventgeneric = $formVal->pkformeventgeneric;
                        $form->fkuser = Yii::$app->user->identity->id;
                        if(!$form->insert())
                        {
                            $error = true;
                            break(2);
                        }
                        $cont++;
                    }
                    if(!$error)
                    {
                        if(!is_null($ticket->ticket_qty))
                        {
                            $ticket->ticket_qty = $ticket->ticket_qty - ($i+1);
                            $ticket->update();
                        }
                        $buy = new VTTbuy;
                        $buy->buy_value = $ticket->ticket_value;
                        $buy->fkticket = $ticket->pkticket;
                        $buy->fkuser = Yii::$app->user->identity->id;
                        $code = $this->randKey("ABCDEFGHIJKLMNÑOPQRSTWXYZ123456789", 10);
                        $buy->buy_code = $code;
                        $buy->buy_status = '0';
                        if($buy->insert())
                            $codes[$i] = $code;
                    }
                }
                if(count($codes) > 0)
                {
                    $this->sendEmailtickets($ticket,$codes);
                    $r = '1';
                }
                else
                    $r = '0';
            }
        }
        return $r;
    }

    private function sendEmailtickets($ticket,$codes)
    {
        $subject = "Voy A Todo, Registro al evento";
        $body = "<h1>Gracias, ".Yii::$app->user->identity->username." ".Yii::$app->user->identity->last_name."</h1>";
        $body .= "<br>Confirmación de compra al evento: ".$ticket->getFkevent0()->one()->event_name;
        $body .= "<br>Boleta: ".$ticket->ticket_name;
        if($ticket->fktipetickect == 1)
            $body .= "<br>De tipo: Gratis";
        else
            if($ticket->fktipetickect == 2)
                $body .= "<br>De tipo: Pago";
            else
                if($ticket->fktipetickect == 3)
                    $body .= "<br>De tipo: Crowfunding";
        $size = count($codes);
        for ($i=0; $i < $size; $i++) 
            $body .= "<br>Tu código de verificación es: ".$codes[$i];
        $body .= "<br>Cantidad de entradas compradas: ".$size;
        $body .= "<br>Muchas gracias por hacer parte de la comunidad Voy A Todo.";
        Yii::$app->mailer->compose()
            ->setTo(Yii::$app->user->identity->email)
            ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
            ->setSubject($subject)
            ->setHtmlBody($body)
            ->send();
        $this->sendNotification($ticket->getFkevent0()->one()->fkuser, $ticket->getFkevent0()->one()->event_name);
    }

    private function sendNotification($pkuser,$nameEvent)
    {
        $user = Users::findOne($pkuser);
        $subject = "Voy A Todo, Registro a tu evento";
        $body = "<h1>Hola, ".$user->username." ".$user->last_name."</h1>";
        $body .= "<br>Notificación de compra a tu evento: ".$nameEvent;
        $body .= "<br>";
        Yii::$app->mailer->compose()
            ->setTo($user->email)
            ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
            ->setSubject($subject)
            ->setHtmlBody($body)
            ->send();
    }

    /*
    public function actionGenerateqr()
    {
        include(Yii::getAlias('@webroot').'/phpqrcode/qrlib.php');
        $route = Yii::getAlias('@webroot') . '/images/system_imgs/';
        $filename = $route.'codigo.png';
        $matrixPointSize = 10;
        $level = 'H';
        $data = 'Hola Mundo';
        $rnd = $this->randKey("abcdefghijklmnñopqrstxwyz0123456789", 50);
        $filename = $route.$rnd.$filename;
        QrCode::png($data); 
    }
    */

    public function actionValidatepin()
    {
        $r = '';
        $query = new Query;
        $query->select(['vt_tbuy.pkbuy']) 
        ->from('vt_tbuy')
        ->join('join',
                'vt_tticket',
                'vt_tbuy.fkticket=vt_tticket.pkticket')
        ->where("vt_tticket.fkevent=:fkevent", [":fkevent" => $_POST['pkevent']])
        ->andWhere("vt_tbuy.buy_code=:code", [":code" => $_POST['code']]);
        $command = $query->createCommand();
        $validate = $command->queryAll();
        if($validate)
        {
            foreach ($validate as $value) 
            {
                $buy = VTTbuy::findOne($value['pkbuy']);
                if($buy->buy_status == '0')
                {
                    $buy->buy_status = '1';
                    $buy->update();
                    $r = '1';
                }
            }
        }
        return $r;
    }

    private function validateImages($widthImg,$heightImg,$fileName)
    {
        list($width, $height, $type, $attributes) = getimagesize($fileName);
        if($width < $widthImg && $height < $heightImg)
        {
            unlink($fileName);
            return false;
        }
        else
            return true;
    }

    public function actionCreatepublicity()
    {
        $request = '';
        $publicity = new VTTpublicity;
        $publicity->publicity_name = $_POST['name'];
        $publicity->publicity_value = 0;
        $publicity->fkevent = $_POST['pkevent'];
        $publicity->fkcreditcard = $_POST['pkcreditcard'];
        if($publicity->insert())
        {
            $buy = new VTTbuy;
            $buy->buy_value = $_POST['value'];
            $buy->fkuser = Yii::$app->user->identity->id;
            $buy->fkpublicity = $publicity->pkpublicity;
            if($buy->insert())
            {
                $publicities = VTTpublicity::find()->asArray()
                    -> where("pkpublicity =:pkpublicity", [":pkpublicity" => $publicity->pkpublicity])
                    ->one();
                $buy_publicity = VTTbuy::find()->asArray()
                    ->where("fkpublicity =:fkpublicity", [":fkpublicity" => $publicity->pkpublicity])
                    ->one();
                $array = array_merge($publicities,$buy_publicity);
                $request = json_encode($array);
            }
            else
                $publicity->delete();
        }
        return $request;
    }

    public function actionUpdatepublicity()
    {
        $request = '';
        $publicity = VTTpublicity::findOne($_POST['pkpublicity']);
        $publicity->publicity_name = $_POST['name'];
        $publicity->fkcreditcard = $_POST['pkcreditcard'];
        if($publicity->update())
        {
            $buy = VTTbuy::find()
                ->where("fkpublicity =:fkpublicity", [":fkpublicity" => $publicity->pkpublicity])
                ->one();
            $buy->buy_value = $_POST['value'];
            if($buy->update())
            {
                $query = new Query;
                $query->select(['vt_tbuy.buy_value','vt_tpublicity.publicity_name','vt_tpublicity.publicity_value','vt_tpublicity.pkpublicity'])
                ->from('vt_tbuy')
                ->join('join',
                    'vt_tpublicity',
                    'vt_tbuy.fkpublicity=vt_tpublicity.pkpublicity')
                ->join('join',
                    'vt_tevent',
                    'vt_tpublicity.fkevent = vt_tevent.pkevent')
                ->where('vt_tevent.pkevent=:pkevent', [':pkevent' => $_POST['pkevent']]);
                $command = $query->createCommand();
                $data = $command->queryAll();
                $request = json_encode($data);
            }
        }
        return $request;
    }

    public function actionGetpublicity()
    {
        $response = "";
        $publicity = VTTpublicity::find()->asArray()
                ->where("pkpublicity =:pkpublicity", [":pkpublicity" => $_POST['id']])
                ->one();
        $buy = VTTbuy::find()->asArray()
            ->where("fkpublicity =:fkpublicity", [":fkpublicity" => $_POST['id']])
            ->one();
        if($publicity && $buy)
        {
            $array = array_merge($publicity,$buy);
            $response = json_encode($array);            
        }
        return $response;
    }

    public function actionHiddenevent()
    {
        $response = "";
        $event = Event::findOne($_POST['id']);
        $event->fkstatus = 1;
        //SE CREA LA NOTIFICACIÓN AL USUARIO DUEÑO DEL EVENTO
        if($event->update())
            $response="1";
        else
            print_r($event->getErrors());
        return $response;
    }

    public function actionShowevent()
    {
        $response = "";
        $event = Event::findOne($_POST['id']);
        $event->fkstatus = 2;
        //SE CREA LA NOTIFICACIÓN AL USUARIO DUEÑO DEL EVENTO
        if($event->update())
            $response="1";
        else
            print_r($event->getErrors());
        return $response;
    }

}
