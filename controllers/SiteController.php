<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Session;
use app\models\User;
use app\models\FormUserRegister;
use app\models\FormSponsorRegister;
use app\models\LoginForm;
use app\models\Users;
use app\models\FormRecoverPass;
use app\models\FormResetPass;
use app\models\VTTcategory;
use app\models\Event;
use app\models\VTTproduct;
use app\models\Contact;
use app\models\VTTcategoryproduct;
use yii\db\Query;

class SiteController extends Controller
{
    public $layout = 'layoutnologin';

    public function actionIndex()
    {
        $query = new Query;
        $query->select(['vt_tevent.pkevent', 'vt_tevent.*'])
        ->from('vt_tevent')
        ->distinct('vt_tevent.pkevent')
        ->join('join',
            'vt_tpublicity',
            'vt_tevent.pkevent = vt_tpublicity.fkevent')
        ->join('join',
            'vt_tbuy',
            'vt_tpublicity.pkpublicity = vt_tbuy.fkpublicity')
        ->where("vt_tevent.event_sponsorship =:sponsorship", [":sponsorship" => 1])
        ->andWhere("vt_tevent.fkstatus =:status", [":status" => 2])
        ->andWhere("vt_tevent.event_visible =:visible", [":visible" => 0])
        ->orderBy('vt_tbuy.buy_value desc');
        $command = $query->createCommand();
        $events_publicity = $command->queryAll();
        $query2 = new Query;
        $query2->select(['vt_tevent.*'])
        ->from('vt_tevent')
        ->where("vt_tevent.event_sponsorship =:sponsorship", [":sponsorship" => 1])
        ->andWhere("vt_tevent.fkstatus =:status", [":status" => 2])
        ->andWhere("vt_tevent.event_visible =:visible", [":visible" => 0])
        ->andWhere("vt_tevent.pkevent not in (select vt_tevent.pkevent
            from vt_tevent join vt_tpublicity on vt_tevent.pkevent = vt_tpublicity.fkevent
            join vt_tbuy on vt_tpublicity.pkpublicity = vt_tbuy.fkpublicity)");
        $command2 = $query2->createCommand();
        $events = $command2->queryAll();
        $img_category = VTTcategory::find()->all();
        return $this->render('index',['category' => $img_category, 
                'events'=>$events,
                'events_publicity' => $events_publicity]);
    }

    public function actionEventos()
    {
        $events = Event::find()
            ->where("event_sponsorship =:sponsorship", [":sponsorship" => 1])
            ->andWhere("fkstatus =:status", [":status" => 2])
            ->andWhere("event_visible =:visible", [":visible" => 0])
            ->all();
        return $this->render('events',['myevent'=>$events]);
    }

    public function actionTienda()
    {
        $categories = VTTcategoryproduct::find()->all();
        $products = VTTproduct::find()->all();
        
        return $this->render('tienda',['products' => $products,'categories' => $categories]);
    }

    private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }
  
    public function actionConfirm()
    {
        $table = new Users;
        if (Yii::$app->request->get())
        {
            $id = Html::encode($_GET["id"]);
            $authKey = $_GET["authKey"];
            if ((int) $id)
            {
                $model = $table
                ->find()
                ->where("id=:id", [":id" => $id])
                ->andWhere("authKey=:authKey", [":authKey" => $authKey]);
                if ($model->count() == 1)
                {
                    $activar = Users::findOne($id);
                    $activar->activate = 1;
                    if ($activar->update())
                    {
                        $subject = "VoyATodo - Bienvenido";
                        Yii::$app->mailer->compose('welcome')
                         ->setTo($activar->email)
                         ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                         ->setSubject($subject)
                         ->send();
                        echo "<meta http-equiv='refresh' content='0; ".Url::toRoute("site/login")."'>";
                    }
                    else
                        echo "<meta http-equiv='refresh' content='0; ".Url::toRoute("site/login")."'>";
                }
                else
                {
                    return $this->redirect(["site/login"]);
                }
            }
            else
            {
                return $this->redirect(["site/login"]);
            }
        }
    }
     
    public function actionRegistro()
    {
        if (!\Yii::$app->user->isGuest)
            return $this->redirect([User::validateRol()]);
        $model = new FormUserRegister;
        $message = null;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()))
        {
            if($model->validate())
            {   
                if($_POST['FormUserRegister']['type'] == 1)
                {
                    $table = new Users;
                    $table->username = $model->username;
                    $table->last_name = $model->last_name;
                    $table->email = $model->email;
                    $table->password = crypt($_POST['FormUserRegister']['password'], Yii::$app->params["salt"]);
                    $table->authkey = $this->randKey("abcdef0123456789", 200);
                    $table->accessToken = $this->randKey("abcdef0123456789", 200);         
                    $table->rol = 3;     
                    if ($table->insert())
                    {
                        $id = urlencode($table->id);
                        $authKey = urlencode($table->authkey);
                        $subject = "VoyATodo - Confirmaci贸n registro.";
                        Yii::$app->mailer->compose('register', ['url'=> "http://voyatodo.com".Yii::getAlias('@web')."/site/confirm?id=".$id."&authKey=".$authKey."'"])
                         ->setTo($model->email)
                         ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                         ->setSubject($subject)
                         ->send();
                        $model->username = null;
                        $model->last_name = null;
                        $model->email = null;
                        $model->password = null;
                        $model->password_repeat = null;
                        $message = "1";
                    }
                    else
                        $message = "2";
                }
                else
                {
                    $table = new Users;
                    $table->username = $model->username;
                    $table->email = $model->email;
                    $table->user_phone1 = $_POST['FormUserRegister']['user_phone1'];
                    $table->user_phone2 = $_POST['FormUserRegister']['user_phone2'];
                    $table->activate = 0;
                    $table->rol = 3;     
                    if ($table->insert())
                    {
                        $model->username = null;
                        $model->email = null;
                        $model->user_phone1 = null;
                        $model->user_phone2 = null;
                        $message = "3";
                    }
                    else
                    {
                        $message = "2";
                    }
                }
            }
            else
                $model->getErrors();
        }
        return $this->render("register", ["model" => $model, "message" => $message]);
    }

    public function actionPatrocinador()
    {
        if (!\Yii::$app->user->isGuest)
            return $this->redirect([User::validateRol()]);
        $model = new FormSponsorRegister;
        $message = null;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()))
        {
            if($model->validate())
            {
                $table = new Users;
                $table->username = $model->username;
                $table->last_name = $model->last_name;
                $table->email = $model->email;
                $table->user_phone1 = $model->user_phone1;
                $table->user_phone2 = $model->user_phone2;
                $table->activate = 0;
                $table->rol = 2;     
                if ($table->insert())
                {
                    $model->username = null;
                    $model->last_name = null;
                    $model->email = null;
                    $model->user_phone1 = null;
                    $model->user_phone2 = null;
                    $message = "1";
                }
                else
                {
                    $message = "2";
                }
            }
            else
            {
                $model->getErrors();
            }
        }
        return $this->render("registerSponsor", ["model" => $model, "message" => $message]);
    }

    public function actionProveedor()
    {
        if (!\Yii::$app->user->isGuest)
            return $this->redirect([User::validateRol()]);
        $model = new FormSponsorRegister;
        $message = null;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()))
        {
            if($model->validate())
            {
                $table = new Users;
                $table->username = $model->username;
                $table->last_name = $model->last_name;
                $table->email = $model->email;
                $table->user_phone1 = $model->user_phone1;
                $table->user_phone2 = $model->user_phone2;
                $table->activate = 0;
                $table->rol = 4;     
                if ($table->insert())
                {
                    $model->username = null;
                    $model->last_name = null;
                    $model->email = null;
                    $model->user_phone1 = null;
                    $model->user_phone2 = null;
                    $message = "1";
                }
                else
                {
                    $message = "2";
                }
            }
            else
            {
                $model->getErrors();
            }
        }
        return $this->render("registerAdvertiser", ["model" => $model, "message" => $message]);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }
    
    public function actions(){
        return [
                'auth' => [
                        'class' => 'yii\authclient\AuthAction',
                        'successCallback' => [$this, 'oAuthSuccess'],
                ],
                'error' => [
                        'class' => 'yii\web\ErrorAction',
                ],
        ];
    }

    public function oAuthSuccess($client){
  //get facebok-id
    $attributes = $client->getUserAttributes();
    $img = file_get_contents('https://graph.facebook.com/'.$attributes['id'].'/picture?type=large');
    $file = Yii::getAlias('@webroot') . '/images/system_imgs/' . $attributes['id'] .'.jpg';
    file_put_contents($file, $img);

    $exist = Users::find()
    ->where('email = :email',[':email'=> $attributes['email']])
    ->one();
    if($exist){
      $exist->user_image = '/images/system_imgs/' . $attributes['id'] .'.jpg';
      $exist->username = $attributes['name'];
      if(!$exist->save())
        var_dump($exist->getErrors());
    }else{
      $user = new Users;
      $user->username = $attributes['name'];
      $user->email    = $attributes['email'];
      $user->authkey = $this->randKey("abcdef0123456789", 200);
      $user->accessToken = $this->randKey("abcdef0123456789", 200);
      $user->rol = 3;
      $user->activate = 1;
      $user->user_image = '/images/system_imgs/' . $attributes['id'] .'.jpg';
      $user->password = crypt($attributes["id"], Yii::$app->params["salt"]);
      if(!$user->insert()){
        var_dump($user->getErrors());
      }
    }
    $this->login($attributes['email'], $attributes['id']);
  }

    private function login($email, $password){
        $model = new LoginForm();
        $model->username = $email;
        $model->password = $password;
        if($model->login()){
            return $this->redirect([User::validateRol()]);
        }else{
            var_dump($model->getErrors());
            die();
        }
    }

    public function actionRecuperar()
    {
        $model = new FormRecoverPass;     
        $message = null;
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                $table = Users::find()->where("email=:email", [":email" => $model->username]);
                if ($table->count() == 1)
                {
                    $table = Users::find()->where("email=:email", [":email" => $model->username])->one();
                    $verification_code = $this->randKey("abcdef0123456789", 200);
                    $table->verification_code = $verification_code;
                    $table->save();
                    $subject = "VoyATodo - Recuperaci贸n Contrase帽a.";
                    Yii::$app->mailer->compose('reset_password', ['code'=> "http://voyatodo.com".Yii::getAlias('@web').'/site/restaurar?key=' . $verification_code])
                     ->setTo($model->username)
                     ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                     ->setSubject($subject)
                     ->send();
                    $model->username = null;
                    $message = "1";
                }
                else //El usuario no existe
                    $message = "2";
            }
            else
                $model->getErrors();
        }
        return $this->render("recoverPass", ["model" => $model, "message" => $message]);
    }

    public function actionRestaurar()
    {
        if(isset($_GET["key"]))
        {
            $user = Users::find()
            ->where("verification_code=:code",[":code" => $_GET["key"]])
            ->One();
            if($user)
            {
                $model = new FormResetPass;
                $message = null;
                if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
                {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
                if ($model->load(Yii::$app->request->post()))
                {
                    if ($model->validate())
                    {
                        $user->password = crypt($model->password, Yii::$app->params["salt"]);
                        $user->verification_code = "";
                        $user->save();
                        $message = "<meta http-equiv='refresh' content='0; ".Url::toRoute("site/login")."'>";
                        $model->password = null;
                        $model->password_repeat= null;
                    }
                }
                return $this->render("resetPass", ["model" => $model, "message" => $message]);
            }
            else
                return $this->redirect("recuperar");
        }
        else
            return $this->redirect("recuperar");
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest)
            return $this->redirect([User::validateRol()]);
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()))
        {
            if($model->validate() && $model->login())
                return $this->redirect([User::validateRol()]);
        }
        return $this->render('login',['model'=>$model]);
    }
    
  public function actionContact()
    {   $model= new Contact();
        $message =  null;
    
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())){
            if($model->validate()){
                 $body = $model->name;
                 $body .= $model->email;
                 $body .= $model->body;
                 Yii::$app->mailer->compose()
                    ->setTo("martinez.crizz@gmail.com")
                    ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                    ->setSubject("solicitud de contacto")
                    ->setHtmlBody($body)
                    ->send();
                $message = 2;
                return $this->render("contact", ["model" => $model, "message" => $message]);
            }else
                $message = 1;
        }
        return $this->render("contact", ["model" => $model, "message" => $message]);
    }
    

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
   
}