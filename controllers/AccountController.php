<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\UploadedFile;
use app\models\User;
use app\models\Event;
use app\models\FormCreateEvent;
use app\models\VTTcountry;
use app\models\VTTcity;
use app\models\VTTdepartament;
use app\models\VTTbank;
use app\models\VTTcategory;
use app\models\VTTimage;
use app\models\VTTuserbank;
use app\models\VTTcreditcard;
use app\models\VTTticket;
use app\models\VTTformeventgeneric;
use app\models\VTTconfig;
use app\models\VTTproposal;
use app\models\VTTmessage;
use app\models\FormUserRegister;
use app\models\VTTbuy;
use yii\db\Query;
use app\models\VTTproduct;
use app\models\VTTcategoryproduct;
use app\models\FormUpdateInformation;
use app\models\Users;
use app\models\VTTfavorites;
use app\models\MailSend;
use app\models\FormUpdateBank;
use app\models\Contact;

class AccountController extends \yii\web\Controller
{
    public $layout = 'layoutUser';

    public function beforeAction($action) 
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $query = new Query;
        $query->select(['vt_tevent.pkevent', 'vt_tevent.*'])
        ->from('vt_tevent')
        ->distinct('vt_tevent.pkevent')
        ->join('join',
            'vt_tpublicity',
            'vt_tevent.pkevent = vt_tpublicity.fkevent')
        ->join('join',
            'vt_tbuy',
            'vt_tpublicity.pkpublicity = vt_tbuy.fkpublicity')
        ->where("vt_tevent.event_sponsorship =:sponsorship", [":sponsorship" => 1])
        ->andWhere("vt_tevent.fkstatus =:status", [":status" => 2])
        ->andWhere("vt_tevent.event_visible =:visible", [":visible" => 0])
        ->orderBy('vt_tbuy.buy_value desc');
        $command = $query->createCommand();
        $events_publicity = $command->queryAll();
        $query2 = new Query;
        $query2->select(['vt_tevent.*'])
        ->from('vt_tevent')
        ->where("vt_tevent.event_sponsorship =:sponsorship", [":sponsorship" => 1])
        ->andWhere("vt_tevent.fkstatus =:status", [":status" => 2])
        ->andWhere("vt_tevent.event_visible =:visible", [":visible" => 0])
        ->andWhere("vt_tevent.pkevent not in (select vt_tevent.pkevent
            from vt_tevent join vt_tpublicity on vt_tevent.pkevent = vt_tpublicity.fkevent
            join vt_tbuy on vt_tpublicity.pkpublicity = vt_tbuy.fkpublicity)");
        $command2 = $query2->createCommand();
        $events = $command2->queryAll();
        $img_category = VTTcategory::find()->all();
        $favorites = VTTfavorites::find()
            ->where("fkuser =:fkuser", [":fkuser" => Yii::$app->user->identity->id])
            ->all();
        return $this->render('index',['category' => $img_category, 
                'events'=>$events,
                'events_publicity' => $events_publicity,
                'favorites' => $favorites]);
    }

    public function actionTienda()
    {
        $categories = VTTcategoryproduct::find()->all();
        $products = VTTproduct::find()->all();
        return $this->render('tienda',['products' => $products,'categories' => $categories]);
    }

    public function actionPerfil()
    {
        $model = new FormUpdateInformation;
        $model2 = new FormUpdateBank;
        $information = Users::findOne(Yii::$app->user->identity->id);
        $message = null;
        $message2 = null;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model2->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model2);
        }
        if ($model2->load(Yii::$app->request->post()) && $model2->validate())
        {
            $userbank = VTTuserbank::find()
            -> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
            ->one();
            if(!$userbank)
                $userbank = new VTTuserbank;
            $userbank->fkbank = $model2->fkbank;
            $userbank->userbank_titularname = $model2->userbank_titularname;
            $userbank->userbank_identification = $model2->userbank_identification;
            $userbank->userbank_numberacount = $model2->userbank_numberacount;
            $userbank->userbank_tipeacount = $model2->userbank_tipeacount;
            $userbank->fkuser = Yii::$app->user->identity->id;
            if ($userbank->save())
                $message2 = 2;
            else
                $message2 = 1;
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $photo = \yii\web\UploadedFile::getInstance($model, 'user_photo');
            if($photo)
            {
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$photo->name);
                $photo->saveAs(Yii::getAlias('@webroot') . '/images/system_imgs/' . $fileName);
                $rnd = $this->randKey("abcdef0123456789", 50);
                if(!is_null($information->user_image))
                    unlink(Yii::getAlias('@webroot') . $information->user_image);
                $information->user_image = '/images/system_imgs/' . $fileName;
            }
            $information->username = $model->username;
            $information->last_name = $model->last_name;
            $information->user_phone1 = $model->user_phone1;
            $information->user_phone2 = $model->user_phone2;
            if ($information->update())
                $message = 2;
            else
                $message = 1;
        }
        $listbanks = VTTbank::find()->all();
        $userbank = VTTuserbank::find()
                    -> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
                    ->one();
        return $this->render('perfil',['model' => $model,
                            'model2' => $model2,
                            'information' => $information,
                            'message' => $message,
                            'message2' => $message2,
                            'listbanks' => $listbanks,
                            'userbank' => $userbank]);
    }

    public function actionFavoritos()
    {
        $favorites = VTTfavorites::find()
            ->where("fkuser =:fkuser", [":fkuser" => Yii::$app->user->identity->id])
            ->all();
        return $this->render('favorites',['favorites' => $favorites]);
    }

    public function actionMistickets()
    {
        $mytickets = VTTbuy::find()
                ->where("fkuser=:fkuser", [":fkuser" => Yii::$app->user->identity->id])
                ->andWhere("fkticket != null")
                ->groupBy('fkticket')
                ->all();
        return $this->render('tickets',['mytickets' => $mytickets]);
    }

    public function actionEventos()
    {
        $events = Event::find()
            ->where("event_sponsorship =:sponsorship", [":sponsorship" => 1])
            ->andWhere("fkstatus =:status", [":status" => 2])
            ->andWhere("event_visible =:visible", [":visible" => 0])
            ->all();
        $favorites = VTTfavorites::find()
            ->where("fkuser =:fkuser", [":fkuser" => Yii::$app->user->identity->id])
            ->all();
        return $this->render('events',['myevent'=>$events,'favorites' => $favorites]);
    }

    public function actionMiseventos()
    {
        $myevent = Event::find()->where("fkuser=:fkuser", [":fkuser" => Yii::$app->user->identity->id])->all();
        return $this->render('myevents',['myevent' => $myevent]);
    }

    private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }

    private function validateImages($widthImg,$heightImg,$fileName)
    {
        list($width, $height, $type, $attributes) = getimagesize($fileName);
        if($width < $widthImg && $height < $heightImg)
        {
            unlink($fileName);
            return false;
        }
        else
            return true;
    }

    public function actionContact()
    {  
       $this->layout = "layoutUserContact";
        $model= new Contact();
        $message =  null;
    
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())){
            if($model->validate()){
                 $body = $model->name;
                 $body .= $model->email;
                 $body .= $model->body;
                 Yii::$app->mailer->compose()
                    ->setTo("martinez.crizz@gmail.com")
                    ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                    ->setSubject("solicitud de contacto")
                    ->setHtmlBody($body)
                    ->send();
                $message = 2;
                return $this->render("contact", ["model" => $model, "message" => $message]);
            }else
                $message = 1;
        }
        return $this->render("contact", ["model" => $model, "message" => $message]);
    }

    public function actionCreatecredit()
    {
        $table = new VTTcreditcard;
        $table->creditcard_type = $_POST["creditcard_type"];
        $table->creditcard_titularname = $_POST["creditcard_titularname"];
        $table->creditcard_numbercard = $_POST["creditcard_numbercard"];
        $table->fkuser = Yii::$app->user->identity->id;
        if($table->insert())
        {
            $creditcard = VTTcreditcard::find()->asArray()
                    -> where("pkcreditcard =:pkcreditcard", [":pkcreditcard" => $table->pkcreditcard])
                    ->one();
            $r = json_encode($creditcard);
        }
        else
            $r = "0";
        return $r;
    }

    public function actionUpdatecredit()
    {
        $credit = VTTcreditcard::find()
                -> where("pkcreditcard =:pkcreditcard", [":pkcreditcard" => $_POST["id"]])
                ->one();
        $credit->creditcard_type = $_POST["creditcard_type"];
        $credit->creditcard_titularname = $_POST["creditcard_titularname"];
        $credit->creditcard_numbercard = $_POST["creditcard_numbercard"];
        $credit->fkuser = Yii::$app->user->identity->id;
        if($credit->update())
        {
            $creditcard = VTTcreditcard::find()->asArray()
                    -> where("fkuser =:fkuser", [":fkuser" => Yii::$app->user->identity->id])
                    ->all();
            $r = json_encode($creditcard);
        }
        else
            $r = "0";
        return $r;
    }

    public function actionUpdatebank()
    {
        $userbank = VTTuserbank::find()
                -> where("pkuserbank =:pkuserbank", [":pkuserbank" => $_POST["id"]])
                ->one();
        $userbank->fkbank = $_POST["fkbank"];
        $userbank->userbank_tipeacount = $_POST["userbank_tipeacount"];
        $userbank->userbank_identification = $_POST["userbank_identification"];
        $userbank->userbank_numberacount = $_POST["userbank_numberacount"];
        $userbank->userbank_titularname = $_POST["userbank_titularname"];
        $userbank->fkuser = Yii::$app->user->identity->id;
        if($userbank->update())
        {
            $bank = VTTuserbank::find()->asArray()
                    -> where("fkuser =:fkuser", [":fkuser" => Yii::$app->user->identity->id])
                    ->one();
            $r = json_encode($bank);
        }
        else
            $r = "0";
        return $r;
    }

    public function actionDeletebank()
    {
        $userbank = VTTuserbank::find()
                -> where("pkuserbank =:pkuserbank", [":pkuserbank" => $_POST["id"]])
                ->one();
        if($userbank->delete())
            return '1';
        else
            return '';
    }

    public function actionCreatebank()
    {
        $bank = new VTTuserbank;
        $bank->fkbank = $_POST["fkbank"];
        $bank->userbank_tipeacount = $_POST["userbank_tipeacount"];
        $bank->userbank_identification = $_POST["userbank_identification"];
        $bank->userbank_numberacount = $_POST["userbank_numberacount"];
        $bank->userbank_titularname = $_POST["userbank_titularname"];
        $bank->fkuser = Yii::$app->user->identity->id;
        if($bank->insert())
        {
            $banks = VTTuserbank::find()->asArray()
                    -> where("pkuserbank =:pkuserbank", [":pkuserbank" => $bank->pkuserbank])
                    ->one();
            $r = json_encode($banks);
        }
        else
            $r = "0";
        return $r;
    }

    private function convertDate($date)
    {
        if(!empty($date))
        {
            $time = strtotime($date);
            $newformat = date('Y-m-d',$time);
            return $newformat;
        }
        else
            return "";
    }

    public function actionCreateticket()
    {
        $ticket = new VTTticket;
        $ticket->ticket_name = $_POST["ticket_name"];
        $ticket->ticket_description = $_POST["ticket_description"];
        $ticket->ticket_value = $_POST["ticket_value"];
        $ticket->ticket_comision = $_POST["ticket_comision"];
        $ticket->ticket_insentive = $_POST["ticket_insentive"];
        $ticket->ticket_qty = $_POST["ticket_qty"];
        $ticket->ticket_seeclaim = $_POST["ticket_seeclaim"];
        $ticket->fktipetickect = $_POST["typeticket_name"];
        $ticket->ticket_start = $_POST["ticket_start"]; //$this->convertDate($_POST["ticket_start"]);
        $ticket->ticket_finish = $_POST["ticket_finish"];//$this->convertDate($_POST["ticket_finish"]);
        $ticket->ticket_enddate = $this->convertDate($_POST["ticket_enddate"]);
        $ticket->ticket_code = $_POST["ticket_code"];
        if($ticket->insert())
        {
            $tickets = VTTticket::find()->asArray()
                    -> where("pkticket =:pkticket", [":pkticket" => $ticket->pkticket])
                    ->one();
            $r = json_encode($tickets);
        }
        else
            $r = "0";
        return $r;
    }

    public function actionUpdateticket()
    {
        $ticket = VTTticket::find()
                -> where("pkticket =:pkticket", [":pkticket" => $_POST["id"]])
                ->one();
        $ticket->ticket_name = $_POST["ticket_name"];
        $ticket->ticket_description = $_POST["ticket_description"];
        $ticket->ticket_value = $_POST["ticket_value"];
        $ticket->ticket_comision = $_POST["ticket_comision"];
        $ticket->ticket_insentive = $_POST["ticket_insentive"];
        $ticket->ticket_qty = $_POST["ticket_qty"];
        $ticket->ticket_seeclaim = $_POST["ticket_seeclaim"];
        $ticket->fktipetickect = $_POST["typeticket_name"];
        $ticket->ticket_start = $this->convertDate($_POST["ticket_start"]);
        $ticket->ticket_finish = $this->convertDate($_POST["ticket_finish"]);
        $ticket->ticket_enddate = $this->convertDate($_POST["ticket_enddate"]);
        if($ticket->update())
        {
            $tickets = VTTticket::find()->asArray()
                    ->where("ticket_code =:ticket_code", [":ticket_code" => $ticket->ticket_code])
                    ->all();
            $r = json_encode($tickets);
        }
        else
            $r = "0";
        return $r;
    }

    public function actionDeleteticket()
    {
        $ticket_del = VTTticket::findOne($_POST["id"]);
        if($ticket_del->delete())
        {
            $tickets = VTTticket::find()->asArray()
                    ->where("ticket_code =:ticket_code", [":ticket_code" => $_POST["ticket_code"]])
                    ->all();
            $r = json_encode($tickets);
        }
        else
            $r = '0';
        return $r;
    }

    public function actionEvento()
    {
        $model = new FormCreateEvent;
        $modelMail = new MailSend;
        $message = 0; //Por defecto es 0
        $status = 1;
        $new = true;
        $eventid = null;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()))
        {
            $banner = \yii\web\UploadedFile::getInstance($model, 'event_image');
            if($model->validate() && $banner)
            {
                if(empty($_POST['FormCreateEvent']['eventid'])){
                    $new = true;
                    $event = new Event;
                }
                else
                {
                    $new = false;
                    $event = Event::findOne($_POST['FormCreateEvent']['eventid']);
                }
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileNameBanner = $rnd . str_replace(' ','_',$banner->name);
                $banner->saveAs(Yii::getAlias('@webroot') . '/images/system_imgs/' . $fileNameBanner);
                if($this->validateImages(1400,600,Yii::getAlias('@webroot') . '/images/system_imgs/' .$fileNameBanner))
                {
                    $event->event_image = '/images/system_imgs/' . $fileNameBanner;
                    $event->fkuser = Yii::$app->user->identity->id;
                    $event->fkstatus = 2;
                    $attrs = $model->getAtributes();
                    foreach ($attrs as $attr) 
                    {
                        if (strrpos($attr, "event") !== false)
                        {
                            if($attr=='event_country' || $attr=='event_galery' || $attr == 'event_image' || $attr == 'eventid')
                            {
                                /* Se hace el tratamiento con esos datos */
                            }
                            else
                                if($attr == 'event_city')
                                    $event->fkcity = $model->$attr;
                                else
                                    if($attr == 'event_enddate' || $attr == 'event_stardate')
                                    {
                                        $time = strtotime($model->$attr);
                                        $newformat = date('Y-m-d',$time);
                                        $event->$attr = $newformat;
                                    }
                                    else
                                        if($attr=="event_category_principal")
                                            $event->fkcategory1 = $model->$attr;
                                        else
                                            if($attr=="event_category_secundary")
                                                $event->fkcategory2 = $model->$attr;
                                            else
                                                if ($attr == 'event_endhour' || $attr == 'event_starthour') 
                                                {
                                                    $hour = strtotime($model->$attr);
                                                    $newhour = date("H:i",$hour);
                                                    $event->$attr = $newhour;
                                                }
                                                else
                                                    $event->$attr = $_POST['FormCreateEvent'][$attr];
                        }
                    }
                    $result = false;
                    if($new)
                    {
                        if($event->insert())
                            $result = true;
                    } 
                    else
                    {
                        if($event->update())
                        {
                            $result = true;
                            $up_ticket = VTTticket::find()
                                            ->where("fkevent=:pkevent", [":pkevent" => $event->pkevent])
                                            ->all();
                            foreach ($up_ticket as $val) 
                            {
                                $val->fkevent = $event->pkevent;
                                $val->update();
                            }
                            $this->deleteImage($event->pkevent);
                            VTTimage::deleteAll("fkevent=:pkevent", [":pkevent" => $event->pkevent]);
                            VTTformeventgeneric::deleteAll("fkevent=:pkevent", [":pkevent" => $event->pkevent]);
                        }
                    }
                    if ($result)
                    {
                        $error = false;
                        $pkevent = $event->pkevent;                         
                        if(isset($_FILES["FormCreateEvent"]['name']['event_galery']))
                        {
                            $format = array('image/jpg', 'image/jpeg', 'image/png','image/pjpeg');
                            $cant = count($_FILES["FormCreateEvent"]['name']['event_galery']);
                            if($cant <= 3)
                            {
                                for($i=0;$i<$cant;$i++)
                                {
                                    if( !in_array( $_FILES["FormCreateEvent"]['type']['event_galery'][$i], $format ) )
                                        $error = true;
                                    if( $_FILES["FormCreateEvent"]['size']['event_galery'][$i] > 2097152 ) 
                                        $error = true;
                                    if(!$error)
                                    {
                                        $rnd = $this->randKey("abcdef0123456789", 50);
                                        $nameImage= $rnd . str_replace(' ','_',$_FILES["FormCreateEvent"]['name']['event_galery'][$i]);
                                        $fileName = Yii::getAlias('@webroot') . '/images/system_imgs/' .$nameImage ;
                                        if (move_uploaded_file($_FILES["FormCreateEvent"]['tmp_name']['event_galery'][$i], $fileName))
                                        {
                                            if($this->validateImages(200,200,$fileName))
                                            {
                                                $images = new VTTimage;
                                                $images->image_path = '/images/system_imgs/' . $nameImage;
                                                $images->fkevent = $pkevent;
                                                $images->insert();
                                            }
                                            else
                                                $error = true;
                                        }
                                        else
                                            $error = true;
                                    }
                                }
                            }
                            else
                                $error = true;
                        }
                        if(!$error)
                        {
                            $pktickets = explode(",", $_POST['FormCreateEvent']['tickets']);
                            foreach ($pktickets as $val)
                            {
                                if($val != '')
                                {
                                    $ticket = VTTticket::find()
                                                ->where('pkticket = :pkticket', [':pkticket' => $val])
                                                ->one();
                                    $ticket->fkevent = $pkevent;
                                    $ticket->ticket_code = "";
                                    $ticket->update();
                                }
                            }
                            foreach ($_POST['generic'] as $formgeneric) 
                            {
                                if($formgeneric != '')
                                {
                                    $formdinamic = new VTTformeventgeneric;
                                    $formdinamic->formeventgeneric = $formgeneric;
                                    $formdinamic->fkevent = $pkevent;
                                    $formdinamic->insert();
                                }
                            }
                            $eventid = $event->pkevent;
                            $message = 2; //Fue publicado
                            $model->event_name = null;
                            $model->event_stardate = null;
                            $model->event_starthour = null;
                            $model->event_enddate = null;
                            $model->event_endhour = null;
                            $model->event_review = null;
                            $model->event_description = null;
                            $model->event_crowfunding = null;
                            $status = 2;
                            $new = false;
                        }
                        else
                        {
                            $event->deleteAll('pkevent=:pkevent',[':pkevent' => $pkevent]);
                            $message = 1;
                        }
                    }
                    else
                        $message = 1;
                }
                else
                    $message = 1;
            }
            else
            {
                $message = 1;
                $model->getErrors();
            }
        }

        //**************** Email ******************
        if ($modelMail->load(Yii::$app->request->post()) && $modelMail->validate())
        {
            $file = \yii\web\UploadedFile::getInstance($modelMail, 'file');
            if($file || $modelMail->email)
            {
                if($file)
                {                    
                    $rnd = $this->randKey("abcdef0123456789", 50);
                    $fileName = $rnd . str_replace(' ','_',$file->name);
                    $file->saveAs(Yii::getAlias('@webroot') . '/images/system_imgs/' . $fileName);
                    $data = \moonland\phpexcel\Excel::import(Yii::getAlias('@webroot') . '/images/system_imgs/' . $fileName);
                    $address = array();
                    foreach ($data as $row) 
                    {
                        foreach($row as $d)
                            $address[] = $d;
                    }
                    unlink(Yii::getAlias('@webroot') . '/images/system_imgs/' . $fileName);
                    $image = \yii\web\UploadedFile::getInstance($modelMail, 'image');
                    if($image)
                    {
                        $rnd = $this->randKey("abcdef0123456789", 50);
                        $fileName = $rnd . str_replace(' ','_',$image->name);
                        $image->saveAs(Yii::getAlias('@webroot') . '/images/system_imgs/' . $fileName);
                        Yii::$app->mailer->compose()
                         ->setBcc($address)
                         ->setFrom([Yii::$app->params["adminEmail"] => $modelMail->subject])
                         ->setSubject($modelMail->subject)
                         ->setTextBody($modelMail->message)
                         ->attach(Yii::getAlias('@webroot') . '/img/uploads/' . $fileName)
                         ->send();
                        $message = 6;
                    }
                    else
                    {
                        Yii::$app->mailer->compose()
                         ->setBcc($address)
                         ->setFrom([Yii::$app->params["adminEmail"] => $modelMail->subject])
                         ->setSubject($modelMail->subject)
                         ->setTextBody($modelMail->message)
                         ->send();
                        $message = 6;
                    }
                    $message = 6;
                }
                else
                {
                    $filea = \yii\web\UploadedFile::getInstance($modelMail, 'file2');
                    if($filea){
                        $rnd = $this->randKey("abcdef0123456789", 50);
                        $fileName = $rnd . str_replace(' ','_',$filea->name);
                        $filea->saveAs(Yii::getAlias('@webroot') . '/img/uploads/' . $fileName);
                         Yii::$app->mailer->compose()
                         ->setTo($modelMail->email)
                         ->setFrom([Yii::$app->params["adminEmail"] => $modelMail->subject])
                         ->setSubject($modelMail->subject)
                         ->setTextBody($modelMail->message)
                         ->attach(Yii::getAlias('@webroot') . '/img/uploads/' . $fileName)
                         ->send();
                        $message = 6;
                    }
                    else
                    {
                        Yii::$app->mailer->compose()
                         ->setTo($modelMail->email)
                         ->setFrom([Yii::$app->params["adminEmail"] => $modelMail->subject])
                         ->setSubject($modelMail->subject)
                         ->setTextBody($modelMail->message)
                         ->send();
                        $message = 6;
                    }
                }
                $modelMail->message = null;
            }
            else
                $message = 5;
        }
        //************** End Email ******************


        $countries = VTTcountry::find()->all();
        $categories = VTTcategory::find()->all();
        $listbanks = VTTbank::find()->all();
        $userbank = VTTuserbank::find()
                    -> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
                    ->all();
        $creditcard = VTTcreditcard::find()
                        -> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
                        ->all();
        if(!is_null($eventid))
            $tickets = VTTticket::find()
                        -> where("fkevent =:fkevent", [":fkevent" => $eventid])
                        ->all();
        else
            $tickets='';
        $this->layout='layoutEvento';
        return $this->render('createEvent',
            ['model'=>$model,
             'modelMail'=>$modelMail, 
            'countries' => $countries,
            'categories' => $categories,
            'listbanks' => $listbanks,
            'message'=>$message, 
            'new' => $new,
            'eventid'=> $eventid,
            'userbank' => $userbank,
            'creditcard' => $creditcard,
            'tickets' => $tickets,
            'fkstatus'=>$status
            ]);
    }

    public function actionGuardar()
    {
        $model = new FormCreateEvent;
        $new;
        $status = 1;
        $attrs = $model->getAtributes();
        $message = 3; //Por defecto es 3 (no fue guardado);
        $errorbanner = false;
        $errorgalery = false;
        if(empty($_POST['FormCreateEvent']['eventid'])){
            $new = true;
            $event = new Event;
        }
        else{
            $new = false;
            $event = Event::findOne($_POST['FormCreateEvent']['eventid']);
        }
        if(isset($_FILES["FormCreateEvent"]['name']['event_image']) && $_FILES["FormCreateEvent"]['name']['event_image'] != "")
        {
            $format = array('image/jpg', 'image/jpeg', 'image/png','image/pjpeg');
            if(!in_array( $_FILES["FormCreateEvent"]['type']['event_image'], $format ) )
                $errorbanner = true;
            if( $_FILES["FormCreateEvent"]['size']['event_image'] > 2097152 )
                $errorbanner = true;
            if(!$errorbanner)
            {
                $rnd = $this->randKey("abcdef0123456789", 50);
                $nameImage = $rnd . str_replace(' ','_',$_FILES["FormCreateEvent"]['name']['event_image']);
                $fileName = Yii::getAlias('@webroot') . '/images/system_imgs/' . $nameImage;
                if (move_uploaded_file($_FILES["FormCreateEvent"]['tmp_name']['event_image'], $fileName))
                {
                    $errorbanner = !$this->validateImages(1400,600,$fileName);
                    if(!$errorbanner)
                        $event->event_image = '/images/system_imgs/' . $nameImage;
                }
                else
                    $errorbanner = true;
            }
        }
        foreach ($attrs as $attr) 
        {
            if(isset($_POST["FormCreateEvent"][$attr]) && $_POST["FormCreateEvent"][$attr])
            {
                if (strrpos($attr, "event") !== false)
                {
                    if($attr=='event_country' || $attr=='event_galery' || $attr == 'event_image' || $attr == 'eventid')
                    {
                        /* Se hace el tratamiento con esos datos */
                    }
                    else
                        if($attr == 'event_city')
                            $event->fkcity = $_POST["FormCreateEvent"][$attr];
                        else
                            if($attr == 'event_enddate' || $attr == 'event_stardate')
                            {
                                $time = strtotime($_POST["FormCreateEvent"][$attr]);
                                $newformat = date('Y-m-d',$time);
                                $event->$attr = $newformat;
                            }
                            else
                                if ($attr == 'event_endhour' || $attr == 'event_starthour') 
                                {
                                    $hour = strtotime($model->$attr);
                                    $newhour = date("H:i",$hour);
                                    $event->$attr = $newhour;
                                }
                                else
                                    if($attr=="event_category_principal")
                                        $event->fkcategory1 = $_POST["FormCreateEvent"][$attr];
                                    else
                                        if($attr=="event_category_secundary")
                                            $event->fkcategory2 = $_POST["FormCreateEvent"][$attr];
                                        else
                                            $event->$attr = $_POST["FormCreateEvent"][$attr];
                }
            }
        }
        if(!$errorbanner)
        {
            $event->fkuser = Yii::$app->user->identity->id;
            $event->fkstatus = $_POST['FormCreateEvent']['fkstatus'];
            $result = false;
            if($new)
            {
                if($event->insert())
                    $result = true;
            } 
            else
            {
                if($event->update())
                {
                    $result = true;
                    $this->deleteImage($event->pkevent);
                    VTTimage::deleteAll("fkevent=:pkevent", [":pkevent" => $event->pkevent]);
                    VTTformeventgeneric::deleteAll("fkevent=:pkevent", [":pkevent" => $event->pkevent]);
                }
            }    
            if($result)
            {
                $pkevent = $event->pkevent;
                //Subida de las imagenes de la galeria
                if($_FILES["FormCreateEvent"]['name']['event_galery'][0])
                {
                    $format = array('image/jpg', 'image/jpeg', 'image/png','image/pjpeg');
                    $cant = count($_FILES["FormCreateEvent"]['name']['event_galery']);
                    if($cant <= 3)
                    {
                        for($i=0;$i<$cant;$i++)
                        {
                            if( !in_array( $_FILES["FormCreateEvent"]['type']['event_galery'][$i], $format ) )
                                $errorgalery = true;
                            if( $_FILES["FormCreateEvent"]['size']['event_galery'][$i] > 2097152 ) 
                                $errorgalery = true;
                            if($errorgalery)
                            {
                                $rnd = $this->randKey("abcdef0123456789", 50);
                                $nameImage= $rnd . str_replace(' ','_',$_FILES["FormCreateEvent"]['name']['event_galery'][$i]);
                                $fileName = Yii::getAlias('@webroot') . '/images/system_imgs/' .$nameImage ;
                                if (move_uploaded_file($_FILES["FormCreateEvent"]['tmp_name']['event_galery'][$i], $fileName))
                                {
                                    $errorgalery = !$this->validateImages(200,200,$fileName);
                                    if(!$errorgalery)
                                    {
                                        $images = new VTTimage;
                                        $images->image_path = '/images/system_imgs/' . $nameImage;
                                        $images->fkevent = $pkevent;
                                        $images->insert();
                                    }
                                }
                                else
                                    $errorgalery = true;
                            }
                        }
                    }
                    else
                        $errorgalery = true;
                }
                if(!$errorgalery)
                {
                    $pktickets = explode(",", $_POST['FormCreateEvent']['tickets']);
                    foreach ($pktickets as $val)
                    {
                        if($val != '')
                        {
                            $ticket = VTTticket::find()
                                    ->where('pkticket = :pkticket', [':pkticket' => $val])
                                    ->one();
                            $ticket->fkevent = $pkevent;
                            $ticket->ticket_code = "";
                            $ticket->update();
                        }
                    }
                    foreach ($_POST['generic'] as $formgeneric) 
                    {
                        if($formgeneric != '')
                        {
                            $formdinamic = new VTTformeventgeneric;
                            $formdinamic->formeventgeneric = $formgeneric;
                            $formdinamic->fkevent = $pkevent;
                            $formdinamic->insert();
                        }
                    }
                    $eventid =  $pkevent;
                    $message = 4; //4 (fue guardado);
                    $new = false;
                }
                else
                    $event->deleteAll('pkevent=:pkevent',[':pkevent' => $pkevent]);
            }
        }
        $countries = VTTcountry::find()->all();
        $categories = VTTcategory::find()->all();
        $listbanks = VTTbank::find()->all();
        $userbank = VTTuserbank::find()
                -> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
                ->all();
        $creditcard = VTTcreditcard::find()
                    -> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
                    ->all();
        return $this->render('createEvent',
            ['model'=>$model, 
            'countries' => $countries,
            'categories' => $categories,
            'listbanks' => $listbanks,
            'message'=> $message, 
            'new' => $new,
            'eventid'=> $_POST['FormCreateEvent']['eventid'],
            'userbank' => $userbank,
            'creditcard' => $creditcard,
            'fkstatus'=> $status
            ]);
    }

    private function deleteImage($pkevent)
    {
        $images = VTTimage::find()
                    -> where("fkevent=:pkevent", [":pkevent" => $pkevent])
                    ->all();
        foreach ($images as $img) 
            unlink(Yii::getAlias('@webroot') . $img->image_path);
    }

    public function actionGetcities()
    {
        $response = "";
        $departaments = VTTdepartament::find()
            ->where('fkcountry = :id', [':id' => $_GET["id"]])
            ->all();
        if(count($departaments)>0){
            $response .= "<option value=''> Selecciona Ciudad </option>";
            foreach($departaments as $dep)
            {
                $cities = $dep->getVTTcities()->all();
                foreach ($cities as $city) {
                    $response .= "<option value='".$city->pkcity."'>".$city->city_name."</option>";
                }
            }
        }
        else
            $response = "<option value=''>No hay subcategorias</option>";
        return $response;
    }

    public function actionGetcategories()
    {
        $response = "";
        $categories_sec = VTTcategory::find()
            ->where('pkcategory != :id', [':id' => $_GET["id"]])
            ->all();
        if(count($categories_sec)>0){
            foreach($categories_sec as $categories)
            {
                $response .= "<option value='".$categories->pkcategory."'>".$categories->category_name."</option>";
            }
        }
        else
            $response = "<option value=''>No hay Categorias</option>";
        return $response;
    }

    public function actionGetcredit()
    {
        $creditcard = VTTcreditcard::find()->asArray()
              ->where('pkcreditcard = :pkcreditcard', [':pkcreditcard' => $_POST["id"]])
              ->one();
        $r = json_encode($creditcard);
        return $r;
    }

    public function actionGetbank()
    {
        $userbank = VTTuserbank::find()->asArray()
              ->where('pkuserbank = :pkuserbank', [':pkuserbank' => $_POST["id"]])
              ->one();
        $bank = VTTbank::find()->asArray()
              ->where('pkbank = :pkbank', [':pkbank' => $userbank["fkbank"]])
              ->one();
        $result = array_merge($userbank, $bank);
        $r = json_encode($result);
        return $r;
    }

    public function actionGetticket()
    {
        $ticket = VTTticket::find()->asArray()
              ->where('pkticket = :pkticket', [':pkticket' => $_POST["id"]])
              ->one();
        if(!empty($ticket))
            $r = json_encode($ticket);
        else
            $r = '0';
        return $r;
    }

    public function actionGetcomision()
    {
        $comison = VTTconfig::find()->one();
        return $comison->config_comision1;
    }

    public function actionGetcrowfunding()
    {
        $comison = VTTconfig::find()->asArray()->one();
        return json_encode($comison);
    }

    public function actionGetproposal()
    {
        $proposal = VTTproposal::find()->asArray()
                        ->where("pkproposal =:pkproposal", [":pkproposal" => $_POST["pkproposal"]])
                        ->one();
        if(!empty($proposal))
            return json_encode($proposal);
        else
            return '';
    }

    public function actionSetmessage()
    {
        $message = new VTTmessage;
        $message->message_message = $_POST['message'];
        $message->message_senduser = "1";
        $message->fkstatus = 6;
        $message->fkproposal = $_POST["pkproposal"];
        $message->fkuser = Yii::$app->user->identity->id;
        if($message->insert())
            return "1";
        else
            return "0";
    }

    public function actionGetmessage()
    {
        $messages = VTTmessage::find()->asArray()
                    ->where("fkproposal =:fkproposal", [":fkproposal" => $_POST["pkproposal"]])
                    ->andWhere("fkstatus =:fkstatus", [":fkstatus" => 7])
                    ->orderBy('pkmessage ASC')
                    ->all();
        if(!empty($messages))
            return json_encode($messages);
        else
            return '';
    }

    public function actionAcceptproposal()
    {
        $request = '0';
        $validate_proposal = VTTproposal::find()
                        ->where("fkstatus =:fkstatus", [":fkstatus" => 5])
                        ->andWhere("fkevent =:fkevent", [":fkevent" => $_POST["pkevent"]])
                        ->count();
        if($validate_proposal < 3)
        {
            $proposal = VTTproposal::findOne($_POST["pkproposal"]);
            if($proposal)
            {
                $proposal->fkstatus = '5';
                if($proposal->update())
                    $request = '1';
            }
        }
        return $request;
    }

    public function actionGetcomisionpublicity()
    {
        $comison = VTTconfig::find()->one();
        return $comison->config_comision3;
    }

    public function actionSetfavorites()
    {
        $response = '';
        $favorites = VTTfavorites::find()
            ->where("fkevent =:fkevent", [":fkevent" =>$_POST["id"]])
            ->one();
        if($favorites)
            $favorites->delete();
        else
        {
            $addFavorite = new VTTfavorites;
            $addFavorite->fkevent = $_POST["id"];
            $addFavorite->fkuser = Yii::$app->user->identity->id;
            if($addFavorite->insert())
                $response = '1';
        }
        return $response;
    }

    public function actionSetpassword()
    {
        $information = Users::findOne(Yii::$app->user->identity->id);
        $information->password = crypt($_POST['data'], Yii::$app->params["salt"]);
        if($information->update())
            return '1';
        else
            return '0';
    }

    public function actionSetsocial()
    {
        $information = Users::findOne(Yii::$app->user->identity->id);
        $information->user_facebook = $_POST['facebook'];
        $information->user_twitter = $_POST['twitter'];
        $information->user_google = $_POST['google'];
        $information->user_youtube = $_POST['youtube'];           
        if($information->update())
            return '1';
        else
            return '0';
    }

    public function behaviors()
    {
        return [
            'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['index','contact','evento','guardar', 'getcities', 'getcategories','miseventos','mistickets',
                            'createbank', 'updatebank', 'createcredit', 'updatecredit', 'getcredit', 'getbank', 'createticket', 'getticket', 'updateticket', 'deleteticket', 'getcomision',
                            'getproposal', 'setmessage', 'getmessage', 'acceptproposal', 'perfil', 'miseventos','eventos','tienda','getcomisionpublicity','favoritos','setfavorites', 'deletebank','setpassword','setsocial','getcrowfunding'
                                 ],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return User::isUserCustomer(Yii::$app->user->identity->id);
                    },
                ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }
}