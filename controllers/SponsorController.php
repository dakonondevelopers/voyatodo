<?php
namespace app\controllers;

use Yii;
use app\models\Event;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\helpers\Html;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Users;
use app\models\FormSponsor;
use app\models\VTTcreditcard;
use app\models\VTTproposal;
use app\models\VTTconfig;
use app\models\VTTmessage;
use app\models\FormSponsorRegister;
use app\models\FormUpdateInformation;

class SponsorController extends \yii\web\Controller
{
	public $layout = 'layoutSponsor';

    public function actionIndex()
    {
        $myevent = Event::find()->
            where("event_sponsorship =:sponsorship", [":sponsorship" => 1])
            ->andWhere("fkstatus =:status", [":status" => 2])
            ->all();
        return $this->render('index',['myevent' => $myevent]);
    }

    public function actionPerfil()
    {
        $model = new FormUpdateInformation;
        $information = Users::findOne(Yii::$app->user->identity->id);
        $proposals = VTTproposal::find()
                ->where("fkuser =:fkuser", [":fkuser" => Yii::$app->user->identity->id])
                ->andWhere("fkstatus =:fkstatus", [":fkstatus" => 5])
                ->all();
        $message = null;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $photo = \yii\web\UploadedFile::getInstance($model, 'user_photo');
            if($photo)
            {
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$photo->name);
                $photo->saveAs(Yii::getAlias('@webroot') . '/images/system_imgs/' . $fileName);
                $rnd = $this->randKey("abcdef0123456789", 50);
                if(!is_null($information->user_image))
                    unlink(Yii::getAlias('@webroot') . $information->user_image);
                $information->user_image = '/images/system_imgs/' . $fileName;
            }
            $information->username = $model->username;
            $information->last_name = $model->last_name;
            $information->user_phone1 = $model->user_phone1;
            $information->user_phone2 = $model->user_phone2;
            if ($information->update())
                $message = 2;
            else
                $message = 1;
        }                
        return $this->render('perfil',['model' => $model,
                            'information' => $information,
                            'proposals' => $proposals,
                            'message' => $message]);
    }

    public function actionConversaciones()
    {
        $messages = VTTmessage::find()
                -> where("fkuser =:fkuser", [":fkuser" => Yii::$app->user->identity->id])
                -> andWhere("fkstatus =:fkstatus", [":fkstatus" => 7])
                ->groupBy('fkproposal')
                ->all();
        return $this->render('conversations',['messages' => $messages]);
    }

    public function actionGetcredit()
    {
        $creditcard = VTTcreditcard::find()->asArray()
              ->where('pkcreditcard = :pkcreditcard', [':pkcreditcard' => $_POST["id"]])
              ->one();
        $r = json_encode($creditcard);
        return $r;
    }

    public function actionCreatecredit()
    {
        $table = new VTTcreditcard;
        $table->creditcard_type = $_POST["creditcard_type"];
        $table->creditcard_titularname = $_POST["creditcard_titularname"];
        $table->creditcard_numbercard = $_POST["creditcard_numbercard"];
        $table->fkuser = Yii::$app->user->identity->id;
        if($table->insert())
        {
            $creditcard = VTTcreditcard::find()->asArray()
                    -> where("pkcreditcard =:pkcreditcard", [":pkcreditcard" => $table->pkcreditcard])
                    ->one();
            $r = json_encode($creditcard);
        }
        else
            $r = "0";
        return $r;
    }

    public function actionUpdatecredit()
    {
        $credit = VTTcreditcard::findOne($_POST["id"]);
        $credit->creditcard_type = $_POST["creditcard_type"];
        $credit->creditcard_titularname = $_POST["creditcard_titularname"];
        $credit->creditcard_numbercard = $_POST["creditcard_numbercard"];
        $credit->fkuser = Yii::$app->user->identity->id;
        if($credit->update())
        {
            $creditcard = VTTcreditcard::find()->asArray()
                    -> where("fkuser =:fkuser", [":fkuser" => Yii::$app->user->identity->id])
                    ->all();
            $r = json_encode($creditcard);
        }
        else
            $r = "0";
        return $r;
    }

    public function actionSendproposal()
    {
        $request = '0';
        $validate_proposal = VTTproposal::find()
                        ->where("fkstatus =:fkstatus", [":fkstatus" => 5])
                        ->andWhere("fkevent =:fkevent", [":fkevent" => $_POST["pkevent"]])
                        ->count();
        if($validate_proposal < 3)
        {
            $validate_sponsor = VTTproposal::find()
                        ->where("fkuser =:fkuser", [":fkuser" => Yii::$app->user->identity->id])
                        ->andWhere("fkevent =:fkevent", [":fkevent" => $_POST["pkevent"]])
                        ->one();
            if(!empty($validate_sponsor))
            {
                $validate_sponsor->proposal_neto1 = $_POST["proposal_neto1"];
                $validate_sponsor->proposal_total1 = $_POST["proposal_total1"];
                $validate_sponsor->proposal_contributions = $_POST["proposal_contributions"];
                $validate_sponsor->fkevent = $_POST["pkevent"];
                $validate_sponsor->fkuser = Yii::$app->user->identity->id;
                $validate_sponsor->fkstatus = '4';
                $validate_sponsor->proposal_type = $_POST["proposal_type"];
                if($validate_sponsor->update())
                    $request = '1';
            }
            else
            {
                $proposal = new VTTproposal;
                $proposal->proposal_neto1 = $_POST["proposal_neto1"];
                $proposal->proposal_total1 = $_POST["proposal_total1"];
                $proposal->proposal_contributions = $_POST["proposal_contributions"];
                $proposal->fkevent = $_POST["pkevent"];
                $proposal->fkuser = Yii::$app->user->identity->id;
                $proposal->fkstatus = '4';
                $proposal->proposal_type = $_POST["proposal_type"];
                if($proposal->insert())
                    $request = '1';
            }

        }
        return $request;
    }

    public function actionGetcomision()
    {
        $comison = VTTconfig::find()->one();
        return $comison->config_comision2;
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionSetmessage()
    {
        $request = '0';
        $proposal = VTTproposal::find()
                    ->where("fkuser =:fkuser", [":fkuser" => Yii::$app->user->identity->id])
                    ->andWhere("fkevent =:fkevent", [":fkevent" => $_POST["pkevent"]])
                    ->one();
        if(!empty($proposal))
        {
            $message = new VTTmessage;
            $message->message_message = $_POST['message'];
            $message->message_senduser = "2";
            $message->fkstatus = 6;
            $message->fkproposal = $proposal->pkproposal;
            $message->fkuser = Yii::$app->user->identity->id;
            if($message->insert())
                $request = "1";
        }
        return $request;
    }

    public function actionSetpassword()
    {
        $information = Users::findOne(Yii::$app->user->identity->id);
        $information->password = crypt($_POST['data'], Yii::$app->params["salt"]);
        if($information->update())
            return '1';
        else
            return '0';
    }

    public function actionSetsocial()
    {
        $information = Users::findOne(Yii::$app->user->identity->id);
        $information->user_facebook = $_POST['facebook'];
        $information->user_twitter = $_POST['twitter'];
        $information->user_google = $_POST['google'];
        $information->user_youtube = $_POST['youtube'];           
        if($information->update())
            return '1';
        else
            return '0';
    }

    public function behaviors()
    {
        return [
            'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['index','getcredit','updatecredit','createcredit','sendproposal', 'getcomision','setmessage', 'conversaciones', 'perfil','setpassword','setsocial'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return User::isUserSponsor(Yii::$app->user->identity->id);
                    },
                ]
                ],
            ],
            'verbs' => [
            	'class' => VerbFilter::className(),
            	'actions' => [
            		'logout' => ['get'],
            	],
            ],
        ];
    }

}
