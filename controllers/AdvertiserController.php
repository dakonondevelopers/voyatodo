<?php

namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use \yii\web\UploadedFile;
use yii\web\Response;
use app\models\User;
use app\models\FormAdvertiser;
use app\models\VTTcategoryproduct;
use app\models\VTTconfig;
use app\models\VTTuserbank;
use app\models\VTTcreditcard;
use app\models\VTTbank;
use app\models\VTTproduct;
use app\models\VTTimage;
use app\models\VTTavailability;
use app\models\FormUpdateInformation;
use app\models\Users;

class AdvertiserController extends \yii\web\Controller
{

	public $layout = 'layoutAdvertiser';

	private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }

    private function validateImages($widthImg,$heightImg,$fileName)
    {
        list($width, $height, $type, $attributes) = getimagesize($fileName);
        if($width < $widthImg && $height < $heightImg)
        {
            unlink($fileName);
            return false;
        }
        else
            return true;
    }

    public function actionIndex()
    {
    	$model = new FormAdvertiser;
    	$message = null;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
		if($model->validate() && $model->load(Yii::$app->request->post()))
		{
            if(isset($_FILES["FormAdvertiser"]['name']['product_image']))
            {
                $new_product = new VTTproduct;
                $new_product->product_name = $model->product_name;
                $new_product->product_review = $model->product_review;
                $new_product->product_description = $model->product_description;
                $new_product->fkcategory = $model->fkcategory;
                $new_product->product_terms = $model->product_terms;
                $new_product->product_qty = $_POST['FormAdvertiser']['product_qty'];
                $new_product->product_value = $model->product_value;
                $new_product->fkuser = Yii::$app->user->identity->id;
                if($new_product->insert())
                {
                    $pkproduct = $new_product->pkproduct;
                    $pkavailability = explode(",", $_POST['FormAdvertiser']['dates']);
                    foreach ($pkavailability as $val)
                    {
                        if($val != '')
                        {
                            $availability = VTTavailability::find()
                                        ->where('pkavailability = :pkavailability', [':pkavailability' => $val])
                                        ->one();
                            $availability->fkproduct = $pkproduct;
                            $availability->availability_code = "";
                            $availability->update();
                        }
                    }
                    $error = false;
                    $format = array('image/jpg', 'image/jpeg', 'image/png','image/pjpeg');
                    $cant = count($_FILES["FormAdvertiser"]['name']['product_image']);
                    if($cant <= 3)
                    {
                        for($i=0;$i<$cant;$i++)
                        {
                            if( !in_array( $_FILES["FormAdvertiser"]['type']['product_image'][$i], $format ) )
                                $error = true;
                            if( $_FILES["FormAdvertiser"]['size']['product_image'][$i] > 2097152 ) 
                                $error = true;
                            if(!$error)
                            {
                                $rnd = $this->randKey("abcdef0123456789", 50);
                                $nameImage= $rnd . str_replace(' ','_',$_FILES["FormAdvertiser"]['name']['product_image'][$i]);
                                $fileName = Yii::getAlias('@webroot') . '/images/system_imgs/' .$nameImage ;
                                if (move_uploaded_file($_FILES["FormAdvertiser"]['tmp_name']['product_image'][$i], $fileName))
                                {
                                    if($this->validateImages(600,600,$fileName))
                                    {
                                        $images = new VTTimage;
                                        $images->image_path = '/images/system_imgs/' . $nameImage;
                                        $images->fkproduct = $pkproduct;
                                        $images->insert();
                                    }
                                    else
                                        $error = true;
                                }
                                else
                                    $error = true;
                            }
                        }
                    }
                    else
                        $error = true;
                    if(!$error)
                    {
                        $message = 2;
                        $model->product_name = null;
                        $model->product_review = null;
                        $model->product_description = null;
                        $model->fkcategory = null;
                        $model->product_terms = null;
                        $model->product_qty  = null;
                    }
                    else
                    {
                        $new_product->delete();
                        $message = 1;
                    }
                }
                else
                    $message = 1;
			}
		}
    	$categories = VTTcategoryproduct::find()->all();
    	$creditcard = VTTcreditcard::find()-> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])->all();
    	$listbanks = VTTbank::find()->all();
    	$userbank = VTTuserbank::find()
			                -> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
			                ->all();
		$products = VTTproduct::find()
							-> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
			                ->all();
        return $this->render('index',['model' => $model, 
        							  'categories' => $categories, 
        							  'creditcard' => $creditcard, 
        							  'listbanks' => $listbanks,
        							  'userbank' => $userbank,
        							  'products' => $products,
        							  'message' => $message
        							  ]);
    }
    
    public function actionPerfil()
    {
        $model = new FormUpdateInformation;
        $information = Users::findOne(Yii::$app->user->identity->id);
        $message = null;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $photo = \yii\web\UploadedFile::getInstance($model, 'user_photo');
            if($photo)
            {
                $rnd = $this->randKey("abcdef0123456789", 50);
                $fileName = $rnd . str_replace(' ','_',$photo->name);
                $photo->saveAs(Yii::getAlias('@webroot') . '/images/system_imgs/' . $fileName);
                $rnd = $this->randKey("abcdef0123456789", 50);
                if(!is_null($information->user_image))
                    unlink(Yii::getAlias('@webroot') . $information->user_image);
                $information->user_image = '/images/system_imgs/' . $fileName;
            }
            $information->username = $model->username;
            $information->last_name = $model->last_name;
            $information->user_phone1 = $model->user_phone1;
            $information->user_phone2 = $model->user_phone2;
            if ($information->update())
                $message = 2;
            else
                $message = 1;
        }
        $listbanks = VTTbank::find()->all();
        $userbank = VTTuserbank::find()
                    -> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
                    ->one();
        return $this->render('perfil',['model' => $model,
                            'information' => $information,
                            'message' => $message,
                            'listbanks' => $listbanks,
                            'userbank' => $userbank]);
    }

    public function actionMisproductos()
    {
        $products = VTTproduct::find()
                -> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
                ->all();
        return $this->render('myproducts',['products' => $products]);
    }

    public function actionGetcomision()
    {
        $comison = VTTconfig::find()->one();
        return $comison->config_comision4;
    }

    public function actionCreatecredit()
    {
        $table = new VTTcreditcard;
        $table->creditcard_type = $_POST["creditcard_type"];
        $table->creditcard_titularname = $_POST["creditcard_titularname"];
        $table->creditcard_numbercard = $_POST["creditcard_numbercard"];
        $table->fkuser = Yii::$app->user->identity->id;
        if($table->insert())
        {
            $creditcard = VTTcreditcard::find()->asArray()
                    -> where("pkcreditcard =:pkcreditcard", [":pkcreditcard" => $table->pkcreditcard])
                    ->one();
            $r = json_encode($creditcard);
        }
        else
            $r = "0";
        return $r;
    }

    public function actionUpdatecredit()
    {
        $credit = VTTcreditcard::find()
                -> where("pkcreditcard =:pkcreditcard", [":pkcreditcard" => $_POST["id"]])
                ->one();
        $credit->creditcard_type = $_POST["creditcard_type"];
        $credit->creditcard_titularname = $_POST["creditcard_titularname"];
        $credit->creditcard_numbercard = $_POST["creditcard_numbercard"];
        $credit->fkuser = Yii::$app->user->identity->id;
        if($credit->update())
        {
            $creditcard = VTTcreditcard::find()->asArray()
                    -> where("fkuser =:fkuser", [":fkuser" => Yii::$app->user->identity->id])
                    ->all();
            $r = json_encode($creditcard);
        }
        else
            $r = "0";
        return $r;
    }

    public function actionUpdatebank()
    {
        $userbank = VTTuserbank::find()
                    -> where("pkuserbank =:pkuserbank", [":pkuserbank" => $_POST["id"]])
                    ->one();
        $userbank->fkbank = $_POST["fkbank"];
        $userbank->userbank_tipeacount = $_POST["userbank_tipeacount"];
        $userbank->userbank_identification = $_POST["userbank_identification"];
        $userbank->userbank_numberacount = $_POST["userbank_numberacount"];
        $userbank->userbank_titularname = $_POST["userbank_titularname"];
        $userbank->fkuser = Yii::$app->user->identity->id;
        if($userbank->update())
        {
            $bank = VTTuserbank::find()->asArray()
                    -> where("fkuser =:fkuser", [":fkuser" => Yii::$app->user->identity->id])
                    ->one();
            $r = json_encode($bank);
        }
        else
            $r = "0";
        return $r;
    }

    public function actionCreatebank()
    {
        $bank = new VTTuserbank;
        $bank->fkbank = $_POST["fkbank"];
        $bank->userbank_tipeacount = $_POST["userbank_tipeacount"];
        $bank->userbank_identification = $_POST["userbank_identification"];
        $bank->userbank_numberacount = $_POST["userbank_numberacount"];
        $bank->userbank_titularname = $_POST["userbank_titularname"];
        $bank->fkuser = Yii::$app->user->identity->id;
        if($bank->insert())
        {
            $banks = VTTuserbank::find()->asArray()
                    -> where("pkuserbank =:pkuserbank", [":pkuserbank" => $bank->pkuserbank])
                    ->one();
            $r = json_encode($banks);
        }
        else
            $r = "0";
        return $r;
    }

    public function actionDeletebank()
    {
        $userbank = VTTuserbank::find()
                -> where("pkuserbank =:pkuserbank", [":pkuserbank" => $_POST["id"]])
                ->one();
        if($userbank->delete())
            return '1';
        else
            return '';
    }

    public function actionGetcredit()
    {
        $creditcard = VTTcreditcard::find()->asArray()
              ->where('pkcreditcard = :pkcreditcard', [':pkcreditcard' => $_POST["id"]])
              ->one();
        $r = json_encode($creditcard);
        return $r;
    }

    public function actionGetbank()
    {
        $userbank = VTTuserbank::find()->asArray()
              ->where('pkuserbank = :pkuserbank', [':pkuserbank' => $_POST["id"]])
              ->one();
        $bank = VTTbank::find()->asArray()
              ->where('pkbank = :pkbank', [':pkbank' => $userbank["fkbank"]])
              ->one();
        $result = array_merge($userbank, $bank);
        $r = json_encode($result);
        return $r;
    }

    public function actionGetproduct()
    {
    	$r = '';
		$product = VTTproduct::find()->asArray()
              ->where('pkproduct = :pkproduct', [':pkproduct' => $_POST["id"]])
              ->one();
        $r = json_encode($product);
        return $r;
    }

    public function actionDeleteproduct()
    {
        $r = '';
        $product_del = VTTproduct::findOne($_POST["id"]);
        if($product_del)
        {
            $dates = $product_del->getVtTavailabilities()->all();
            foreach ($dates as $date) 
                $date->delete();
            if($product_del->delete())
            {
    			$products = VTTproduct::find()->asArray()
    						-> where("fkuser =:user", [":user" => Yii::$app->user->identity->id])
    		                ->all();
                $r = json_encode($products);
            }
        }
        return $r;
    }

    public function actionGetdays()
    {
        $product = VTTproduct::find()
            ->where("pkproduct =:pkproduct", [":pkproduct" => $_GET['id']])
            ->one();
        if(!empty($product))
        {
            $days_return = "";
            $days = $product->getVtTavailabilities()->all();
            foreach ($days as $day) 
            {
                $time_day = date('d',strtotime($day->availability_date));
                $days_return .= $time_day.',';
            }
            echo $days_return;
        }
        else
            echo "";
    }

    public function actionCreatedate()
    {
        $availability = new VTTavailability;
        $availability->availability_date = $_POST['date'];
        $availability->availability_code = $_POST['code'];
        $availability->availability_status = $_POST['status'];
        if($availability->insert())
            return $availability->pkavailability;
        else
            return '';
    }

    public function actionDeletedate()
    {
        $availability_del = VTTavailability::find()
                        ->where("availability_date =:date", [":date" => $_POST['date']])
                        ->andWhere("availability_code =:code", [":code" => $_POST['code']])
                        ->one();
        if($availability_del->delete())
        {
            $availability = VTTavailability::find()->asArray()
                    ->where("availability_code =:code", [":code" => $_POST['code']])
                    ->all();
            $r = json_encode($availability);
            return $r;
        }
        else
            return '';
    }

    public function beforeAction($action) 
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionSetpassword()
    {
        $information = Users::findOne(Yii::$app->user->identity->id);
        $information->password = crypt($_POST['data'], Yii::$app->params["salt"]);
        if($information->update())
            return '1';
        else
            return '0';
    }

    public function actionSetsocial()
    {
        $information = Users::findOne(Yii::$app->user->identity->id);
        $information->user_facebook = $_POST['facebook'];
        $information->user_twitter = $_POST['twitter'];
        $information->user_google = $_POST['google'];
        $information->user_youtube = $_POST['youtube'];           
        if($information->update())
            return '1';
        else
            return '0';
    }

    public function actionGetcomisionpublicity()
    {
        $comison = VTTconfig::find()->one();
        return $comison->config_comision3;
    }

    public function behaviors()
    {
        return [
            'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['index','getcomision','createbank', 'updatebank', 'createcredit', 'updatecredit', 'getcredit', 'getbank', 'getproduct','deleteproduct','getdays','createdate','deletedate','misproductos','perfil','deletebank','setsocial','setpassword','getcomisionpublicity'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return User::isUserAdvertiser(Yii::$app->user->identity->id);
                    },
                ]
                ],
            ],
            'verbs' => [
            	'class' => VerbFilter::className(),
            	'actions' => [
            		'logout' => ['get'],
            	],
            ],
        ];
    }

}
