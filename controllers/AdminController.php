<?php

namespace app\controllers;
use Yii;
use app\models\Event;
use app\models\User;
use app\models\Users;
use app\models\VTTmessage;
use app\models\VTTconfig;


class AdminController extends \yii\web\Controller
{
	public $layout = 'layoutAdmin';

    public function actionIndex()
    {
    	$events = Event::find()->all();
        return $this->render('index',['events' => $events]);
    }

    public function actionSolicitudes()
    {
    	$request = Users::find()
    			->where("activate =:activate", [":activate" => 0])
    			->all();
    	return $this->render('request',['request' => $request]);
    }

    public function actionUsuarios()
    {
        $users = Users::find()
                ->where("activate =:activate", [":activate" => 1])
                ->all();
        return $this->render('users',['users' => $users]);
    }

    public function actionParametros()
    {
        $comisions = VTTconfig::find()->all();
        return $this->render('params',['comisions' => $comisions]);
    }

    public function actionSetcomisions()
    {
        $r = '';
        $comisions = VTTconfig::find()->all();
        foreach ($comisions as $value) 
        {
            $value['config_comision1'] = $_POST['comision_1'];
            $value['config_comision2'] = $_POST['comision_2'];
            $value['config_comision3'] = $_POST['comision_3'];
            $value['config_comision4'] = $_POST['comision_4'];
            $value['config_days'] = $_POST['days'];
            $value['config_discount_crowfunding'] = $_POST['comision_5'];
            if($value->update())
            {
                $up_comisions = VTTconfig::find()->asArray()->all();
                $r = json_encode($up_comisions);
            }
        }
        return $r; 
    }

    public function actionGetuser()
    {
        if($_POST['rol'] == 5)
        {
            $users = Users::find()->asArray()
                    ->where("activate =:activate", [":activate" => 1])
                    ->all();   
        }
        else
        {
            $users = Users::find()->asArray()
                    ->where("activate =:activate", [":activate" => 1])
                    ->andWhere("rol =:rol",[":rol" => $_POST['rol']])
                    ->all();            
        }
        $request = json_encode($users);
        return $request;
    }

    public function actionAccept()
    {
    	$request = '';
    	$user_req = Users::findOne($_POST['id']);
		$password = $this->randKey("abcdef0123456789", 10);
    	$user_req->password = crypt($password, Yii::$app->params["salt"]);
    	$user_req->activate = 1;
        $user_req->user_date = date("Y-m-d");
    	if($user_req->update())
    	{
    		$subject = "Voy A Todo, Bienvenido";
            $body = "<h1>Bienvenido, ".$user_req->username." ".$user_req->last_name."</h1>";
            if($user_req->rol == 2)
                $body .= "<br>Ya puedes empezar a patrocinar eventos.";
            else
                if($user_req->rol == 4)
                    $body .= "<br>Ya puedes empezar a vender tus productos en la tienda.";
            $body .= "<br>Tu contraseña es: ". $password;
            $body .= "<br>La puedes cambiar cuando lo desees, accediendo a la opción '¿Olvidaste tu contraseña?'.";
            Yii::$app->mailer->compose()
                ->setTo($user_req->email)
                ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                ->setSubject($subject)
                ->setHtmlBody($body)
                ->send();
            $req = Users::find()->asArray()
    			->where("activate =:activate", [":activate" => 0])
                ->all();
    		if($req)
    			$request = json_encode($req);
    	}
    	return $request;
    }

    public function actionRefuse()
    {
    	$request = '';
    	$user_req = Users::findOne($_POST['id']);
    	if($user_req->delete())
    	{
    		$req = Users::find()->asArray()
    			->where("activate =:activate", [":activate" => 0])
                ->all();
    		if($req)
    			$request = json_encode($req);
    	}
    	return $request;
    }

    public function actionConversaciones()
    {
        $messages = VTTmessage::find()
                -> where("fkstatus =:fkstatus", [":fkstatus" => 6])
                ->groupBy('fkproposal')
                ->all();
        return $this->render('conversations',['messages' => $messages]);
    }

    public function actionGetmessage()
    {
        $r = '';
        $messages = VTTmessage::find()->asArray()
                -> where("fkproposal =:fkproposal", [":fkproposal" => $_POST['id']])
                ->andWhere("fkstatus =:fkstatus", [":fkstatus" => 6])
                ->all();
        if($messages)
            $r = json_encode($messages);
        return $r;
    }

    public function actionAcceptmessage()
    {
        $r = '';
        $message = VTTmessage::find()
                ->where("pkmessage =:pkmessage", [":pkmessage" => $_POST['id']])
                ->one();
        if($message)
        {
            $message->fkstatus = 7;
            if($message->update())
                $r = $this->Getjsonmessage();
        }
        return $r;
    }

    private function Getjsonmessage()
    {
        $messages = VTTmessage::find()
                    ->where("fkstatus =:fkstatus", [":fkstatus" => 6])
                    ->groupBy('fkproposal')
                    ->all();
        $msg_return = "";                
        foreach ($messages as $msg) 
        {
            $msg_return .= '{';
            $msg_return .= '"fkproposal":'.$msg->fkproposal.',';
            $msg_return .= '"pkmessage":'.$msg->pkmessage.',';
            $msg_return .= '"event_name":"'.$msg->getFkproposal0()->one()->getFkevent0()->one()->event_name.'",';
            $user = $msg->getFkproposal0()->one()->getFkevent0()->one()->getFkuser0()->one();
            $msg_return .= '"user_name":"'.$user->username.' '.$user->last_name.'",';
            $sponsor = $msg->getFkproposal0()->one()->getFkuser0()->one();
            $msg_return .= '"sponsor_name":"'.$sponsor->username.' '.$sponsor->last_name.'",';
            $msg_return .= '"cant_message":'.$this->getCountmessage($msg->fkproposal);
            $msg_return .= '},';
        }
        $rest = substr($msg_return, -1); 
        $r = "[".$msg_return."]";
        return $r;
    }

    public function actionEditmessage()
    {
        $r = '';
        $message = VTTmessage::find()
                ->where("pkmessage =:pkmessage", [":pkmessage" => $_POST['id']])
                ->one();
        if($message)
        {
            $message->message_message = $_POST['message'];
            $message->fkstatus = 7;
            if($message->update())
                $r = $this->Getjsonmessage();
        }
        return $r;
    }

    public function actionGetmessagevalue()
    {
        $r = '';
        $message = VTTmessage::find()->asArray()
                ->where("pkmessage =:pkmessage", [":pkmessage" => $_POST['id']])
                ->one();
        if($message)
            $r = json_encode($message);
        return $r;   
    }

    private function getCountmessage($fkproposal)
    {
        $cant_message = VTTmessage::find()
            ->where("fkstatus =:fkstatus", [":fkstatus" => 6])
            ->andWhere("fkproposal =:fkproposal", [":fkproposal" => $fkproposal])
            ->all();
        return count($cant_message); 
    }

    private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }

    public function beforeAction($action) 
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

}
