<?php

$this->title = 'Voy A Todo | Mis Productos';

?>

<div id="container">
  
  <!-- Start Home Page Slider -->
  <section id="inner01"> 
    <!-- Carousel -->
    <div id="main-slide" class="carousel00 ver-evento slide" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
      </div>
    </div>
    <!-- /carousel --> 
  </section>
  <!-- End Home Page Slider -->
    
  <div class="section destacados" style="background:#f2f2f2;">
    <div class="container container-update"> 
      
      <!-- Start Team Members -->
      <div class="row">
      <?php
        if(empty($products))
        { ?>
          <div class="big-title text-top text-center">
            <h1>A&Uacute;N NO TIENES <span>PRODUCTOS Y SERVICIOS</span></h1>
          </div>
          <img class="img-face-sad" src="<?php echo Yii::getAlias('@web')?>/images/face-sad.png" alt="face-sad">
          <?php  
        }
        else
        { ?>
          <div class="big-title text-center">
            <h1>esta es toda la lista de tus <span>productos</span></h1>
          </div>
          <?php
          foreach ($products as $product) 
          {
            ?>
            <div class="col-md-3 col-sm-6 col-lg-3">
              <div class="team-member modern">
                <a href="<?php echo Yii::getAlias('@web').'/producto/v/'.$product->pkproduct; ?>">
                  <div class="member-photo imgevent" style="background-image: url('<?php echo Yii::getAlias('@web').$product->getVtTimages()->one()->image_path; ?>');background-size: 100% 100%;">
                    <div class="member-name color0<?php echo $product->getFkcategory0()->one()->pkcategoryproduct; ?>"><span><?php echo $product->getFkcategory0()->one()->categoryproduct_name; ?></span> </div>
                  </div>
                </a>
                <div class="member-info">
                  <h2><a href="<?php echo Yii::getAlias('@web').'/producto/v/'.$product->pkproduct; ?>"> <?php echo $product->product_name; ?></a></h2>
                </div>  
                <div class="member-socail"> <p class="txt_money"><?php echo number_format($product->product_value); ?> COP</p> <a class="link-button" href="<?php echo Yii::getAlias('@web').'/producto/v/'.$product->pkproduct; ?>">ver m&aacute;s</a> </div>
              </div>
            </div>
            <?php
          }
        }
        ?>
      </div>
      <!-- End Team Members --> 
    </div>
    <!-- .container --> 
  </div>
</div>