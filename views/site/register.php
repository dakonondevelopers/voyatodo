<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Voy A Todo | Registro';

?>
<div id="container-login">
  <!-- Start Home Page Slider -->
  <section id="login-pages"> 
    <!-- Carousel -->
    <div id="main-slide0" class="carousel00 slide">
      <div class="item01">
        <div class="slider-content0">
          <div class="col-md-12 text-center">
            <h2 class="animated2"> <span>Comienza Ahora!</span> </h2>
            <h3 class="animated3"> <span>publica y gestiona tus eventos en minutos</span> </h3>
            <div class="col-md-4 hidden-xs"> </div>
            <div class="col-md-4">
              <div class="logi-box">
              <h3>REGISTRARME</h3>
              <?php $form = ActiveForm::begin([
                  'method' => 'post',
                  'id' => 'contact-form',
                  'enableClientValidation' => true,
                  'enableAjaxValidation' => true,
                  'class'=> 'contact-form',
                ]);
              ?>
              <div class="col-md-6">
                <div class="checkbox" style="z-index: 9;">
                  <input type="radio" name="user_type" value="1" class="inputradio type_user" checked> <span class="inputfalso"> <label class="lbl_radio">PERSONA</label> </span>
                </div>
              </div>
              <div class="col-md-6">
                <label class="checkbox" style="z-index: 9;"><input class="inputradio type_user" name="user_type" type="radio" value="2"><span class="inputfalso"></span><span class="lbl_radio">EMPRESA</span></label>
              </div>
              <input type="hidden" value="1" name="FormUserRegister[type]" class="user_type">
              <div class="form-group">
                <div class="controls">
                <label>NOMBRE</label>    
                <?= $form->field($model, "username")->input("text", ['placeholder' => 'Jhon', 'class' => 'email requiredField'])->label(false) ?>
                </div>
              </div>
              <div class="opt_people">
                <div class="form-group">
                  <div class="controls">
                    <label>APELLIDO</label>
                    <?= $form->field($model, "last_name")->input("text", ['placeholder' => 'Smith', 'class' => 'email requiredField last_name'])->label(false) ?>
                    <div id="error1" style="color:#A94452;"></div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <label>E-MAIL</label>    
                  <?= $form->field($model, "email")->input("email", ['placeholder' => 'johnsmith@mymail.com', 'class' => 'email requiredField'])->label(false); ?>
                </div>
              </div>
              <div class="opt_people">
                <div class="form-group">
                  <div class="controls">
                    <label>CONTRASE&Ntilde;A</label>
                    <?= $form->field($model, "password")->input("password", ['placeholder' => '•••••••••••••••••••••', 'class' => 'email password'])->label(false); ?>
                    <div id="error2" style="color:#A94452;"></div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="controls">
                    <label> CONFIRMAR CONTRASE&Ntilde;A</label>
                    <?= $form->field($model, "password_repeat")->input("password", ['placeholder' => '•••••••••••••••••••••', 'class' => 'email password_repeat'])->label(false); ?>
                    <div id="error3" style="color:#A94452;"></div>
                  </div>
                </div>
                <a type="submit" id="submit" class="btn-system01">CREAR CUENTA</a>
                <div id="success" style="color:#A94452;"></div>
              </div>
              <div class="opt_company hidden">
                <div class="form-group">
                  <div class="controls">
                    <label>TEL&Eacute;FONO 1</label>
                    <?= $form->field($model, "user_phone1")->input("text", ['placeholder' => '312 338 3515', 'class' => 'email user_phone1'])->label(false); ?>
                    <div id="error4" style="color:#A94452;"></div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="controls">
                    <label>TEL&Eacute;FONO 2</label>
                    <?= $form->field($model, "user_phone2")->input("text", ['placeholder' => 'Opcional', 'class' => 'email user_phone2'])->label(false); ?>
                    <div id="error5" style="color:#A94452;"></div>
                  </div>
                </div>
                <a type="submit" id="submit" class="btn-system01">SOLICITAR CUENTA</a>
                <div id="success_1" style="color:#A94452;"></div>
              </div>
              <?php $form->end() ?>
              </div>
              <p>
                <span style="color:#FFFFFF"> 
                  Al hacer click en "Crear cuenta" confirmo que he le&iacute;do y he aceptado los
                  <a class="text-register"  href="#">T&eacute;rminos y condiciones</a> y la 
                  <a class="text-register" href="#">Pol&iacute;tica de privacidad</a> de VOY A TODO    
                </span>
              </p>
              
              <div class="descarga-box">
                <p>Descarga la app:</p>
                <p class="animated5"><a href="#" class="app">app</a> &nbsp; <a href="#" class="an">an</a> </p>
              </div>
            </div>
            <div class="col-md-4 hidden-xs"> </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- /carousel --> 
  </section>
  <div class="clearfix"></div>
</div>

<?php
  if($message == 1)
  { ?>
    <script>
      swal("Muy bien", "Muy bien, ahora sólo falta que confirmes tu registro en tu cuenta de correo", "success")
    </script>
    <?php
  }
  else
    if($message == 2)
    {
      ?>
      <script>
        swal({
          title: "Oh! no",
          text: "Ha ocurrido un error al llevar a cabo tu registro",
          type: "error"
        });
      </script>
      <?php
    }
    else
      if($message == 3)
      {
        ?>
        <script>
          swal("Muy bien", "Tu solitud ha sido enviada correctamente, pronto nos pondremos en contacto contigo", "success")
        </script>
        <?php
      }
?>

<script type="text/javascript">
  $(document).ready(function() {  
      $(".btn-system01").click(function(){
        if($(".user_type").val() == 1 && $(".last_name").val() != '' && $(".password").val() != ''
          && $(".password_repeat").val() != ''){
          $("#success").html('');
          $("#success_1").html('');
          var dimension = $(".password").val().length;
          if(dimension > 5 && dimension < 17){
            $("#error2").html('');
            $("#error3").html('');
            if($(".password").val() == $(".password_repeat").val()){
              $("#error3").html('');
              var dim_last = $(".last_name").val().length;
              if( dim_last > 3 && dim_last < 100)
                $("#contact-form").submit();
              else
                $("#error1").html('Mínimo 3 y máximo 100 caracteres.');
            }
            else
              $("#error3").html('Las contraseñas no coinciden.');
          }
          else{
            $("#error2").html('Mínimo 6 y máximo 16 caracteres.');
            $("#error3").html('Mínimo 6 y máximo 16 caracteres.');
          }
        }
        else
          if($(".user_type").val() == 2 && $(".user_phone1").val() != ''){
            $("#success").html('');
            $("#success_1").html('');
            var dimension1 = $(".user_phone1").val().length;
            var dimension2 = $(".user_phone2").val().length;
            if(dimension1 > 7 && dimension1 < 45 || dimension2 > 7 && dimension2 < 45){
              $("#error4").html('');
              $("#error5").html('');
              if($(".user_phone1").val() == $(".user_phone2").val())
                $("#error5").html('Los télefonos son iguales.');
              else{
                if(!isNaN(parseFloat($(".user_phone1").val())) && isFinite($(".user_phone1").val())){
                  if(!isNaN(parseFloat($(".user_phone2").val())) || isFinite($(".user_phone2").val()))
                    $("#contact-form").submit();  
                  else
                    $("#error5").html('Sólo se aceptan números.');
                }
                else
                  $("#error4").html('Sólo se aceptan números.');
              }
            }
            else{
              $("#error4").html('Mínimo 5 y máximo 45 caracteres.');
            }
        }
        else{
          $("#success").html('Completa todos los datos.');
        }
    });
  });
</script>