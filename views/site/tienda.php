<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Voy A Todo | Tienda';

?>

<!-- Full Body Container -->
<div id="container">
  
  <!-- Start Home Page Slider -->
  <section id="inner01"> 
    <!-- Carousel -->
    <div id="main-slide" class="carousel00 ver-evento slide" data-ride="carousel">
      <div class="item active"> 
        <img class="img-responsive2 tienda1" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
        <div class="slider-content">
          
          <div class="container-fluid">
              <div class="col-md-12 text-center xs1">
                <h2 style="margin-top: 0px !important;">¿Qu&eacute; necesitas para tu evento?</h2>
                <h3 class="help-block">ENCUENTRA LO QUE NECESITAS EN EL BUSCADOR</h3>
              </div>
              <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 col-lg-offset-1 top_10">
                <select class="form-control">
                  <?php
                    echo "<option>Seleccione Categor&iacute;a </option>";
                    foreach ($categories as $category) 
                    {
                      echo "<option value=".$category->pkcategoryproduct.">".$category->categoryproduct_name."</option>";
                    }
                  ?>
                </select>
              </div>

              <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 top_10">
                <select class="form-control">
                  <option>FILTRAR POR</option>
                </select>
              </div>

              <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 top_10">
                <input type="text" placeholder="CIUDAD" class="form-control">
              </div>

              <div class="col-lg-1 col-md-1 col-sm-6 col-xs-12 top_10">
                <a class="btn cargar_ btn_search_products hidden-xs hidden-sm">Buscar</a>
                <a class="btn cargar_ btn-block btn_search_products hidden-md hidden-lg ">Buscar</a>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-lg-offset-1 top_10">
                <?php 
                  if(\Yii::$app->user->isGuest)
                  {
                  ?>
                    <a href="<?php echo Yii::getAlias('@web') ?>/site/login" class="btn cargar_ btn-block"><i class="fa fa-shopping-basket"></i> Mi carrito</a>
                  <?php
                  }
                  else
                  {
                  ?>
                    <a class="btn cargar_ btn-block"><i class="fa fa-shopping-basket"></i> Mi carrito</a>
                  <?php
                  }
                ?>
              </div>
          </div><!--row-->

        </div>
      </div>
    </div>
    <!-- /carousel --> 
  </section>
  <!-- End Home Page Slider -->
    
  <div class="section destacados" style="background:#f2f2f2;">
    <div class="container container-update"> 
      
      <!-- Start Team Members -->
      <div class="row">
      <?php
        if(empty($products))
        { ?>
          <div class="big-title text-top text-center">
            <h1>A&Uacute;N NO HAY <span>PRODUCTOS Y SERVICIOS</span></h1>
          </div>
          <img class="img-face-sad" src="<?php echo Yii::getAlias('@web')?>/images/face-sad.png" alt="face-sad">
          <?php  
        }
        else
        { ?>
          <div class="big-title text-center">
            <h1>mira algunos productos <span>destacados</span></h1>
            <p class="text-center">Estos son los productos y servicios destacados</p>
          </div>
          <?php
          foreach ($products as $product) 
          {
            ?>
            <div class="col-md-4 col-sm-6 col-lg-3">
              <div class="team-member modern">
                <a href="<?php echo Yii::getAlias('@web').'/producto/v/'.$product->pkproduct; ?>">
                  <div class="member-photo imgevent" style="background-image: url('<?php echo Yii::getAlias('@web').$product->getVtTimages()->one()->image_path; ?>');background-size: 100% 100%;">
                    <div class="member-name color0<?php echo $product->getFkcategory0()->one()->pkcategoryproduct; ?>"><span><?php echo $product->getFkcategory0()->one()->categoryproduct_name; ?></span> </div>
                  </div>
                </a>
                <div class="member-info">
                  <h2><a href="<?php echo Yii::getAlias('@web').'/producto/v/'.$product->pkproduct; ?>"> <?php echo $product->product_name; ?></a></h2>
                </div>  
                <div class="member-socail"> <p class="txt_money"><?php echo number_format($product->product_value); ?> COP</p> <a class="link-button" href="<?php echo Yii::getAlias('@web').'/producto/v/'.$product->pkproduct; ?>">ver m&aacute;s</a> </div>
              </div>
            </div>
            <?php
          }
        }
        ?>
      </div>
      <!-- End Team Members --> 
    </div>
    <!-- .container --> 
  </div>
    
  <div class="section purchase crowdfunding">
    <div class="container"> 
      <div class="section-video-content text-center"> 
        <!-- Start Animations Text -->
        <h3 class="h31">CREA TU PROPIO <span>EVENTO</span></h3>
        <p class="help-block">Voy A Todo te d&aacute; las herramientas necesarias para que puedas hacer toda<br />
         la gesti&oacute;n de tu eventos en su s&oacute;lo lugar</p>
        <!-- End Animations Text -->
        <div class="eventos-list eventos-list00"> <a class="eventos" href="<?php echo Yii::getAlias('@web') ?>/account/evento">crear mi evento</a> </div>
      </div>
    </div>
    <!-- .container --> 
  </div>
</div>