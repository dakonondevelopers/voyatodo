<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Voy A Todo | Recuperar Contrase&ntilde;a';

?>
<div id="container-login">
  <!-- Start Home Page Slider -->
  <section id="login-pages"> 
    <!-- Carousel -->
    <div id="main-slide0" class="carousel00 slide">
      <div class="item01">
        <div class="slider-content0">
          <div class="col-md-12 text-center">
            <h2 class="animated2"> <span>Comienza Ahora!</span> </h2>
            <h3 class="animated3"> <span>publica y gestiona tus eventos en minutos</span> </h3>
            <div class="col-md-4 hidden-xs"> </div>
            <div class="col-md-4">
              <div class="logi-box">
                <h3>CAMBIA TU CONTRASE&Ntilde;A</h3>
                <?php $form = ActiveForm::begin([
                  'method' => 'post',
                  'id' => 'contact-form',
                  'enableClientValidation' => true,
                  'enableAjaxValidation' => true,
                  'class'=> 'contact-form',
                  ]);
                ?>
                  <div class="form-group">
                    <div class="controls">
                      <label>E-MAIL</label>    
                      <?= $form->field($model, "username")->input("email", ['placeholder' => 'johnsmith@mymail.com', 'class' => 'email requiredField'])->label(false); ?>
                    </div>
                  </div>
                  <button type="submit" id="submit" class="btn-system01">CAMBIAR CONTRASE&Ntilde;A</button>
              </div>
              <div class="descarga-box">
                <p>Descarga la app:</p>
                <p class="animated5"><a href="#" class="app">app</a> &nbsp; <a href="#" class="an">an</a> </p>
              </div>
            </div>
            <div class="col-md-4 hidden-xs"> </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- /carousel --> 
  </section>
  <div class="clearfix"></div>
</div> 

<?php
  if($message == 1)
  { ?>
    <script>
      swal("Muy bien", "A tu email enviaremos las instrucciones para que cambies tu contraseña", "success")
    </script>
    <?php
  }
  else
    if($message == 2)
    {
      ?>
      <script>
        swal({
          title: "Oh! no",
          text: "Ha ocurrido un error",
          type: "error"
        });
      </script>
      <?php
    }
?>

<script type="text/javascript">
  $(document).ready(function() {  
      $(".btn-system01").click(function(){
      $("#contact-form").submit();
    });
  });
</script>