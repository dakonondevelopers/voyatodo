<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Voy A Todo | Proveedor';

?>
<div id="container-login">
  <!-- Start Home Page Slider -->
  <section id="login-pages"> 
    <!-- Carousel -->
    <div id="main-slide0" class="carousel00 slide">
      <div class="item01">
        <div class="slider-content0">
          <div class="col-md-12 text-center">
            <h2 class="animated2"> <span>¿Quieres Ser Proveedor?</span> </h2>
            <h3 class="animated3"> <span>¡REGISTRATE Y TE CONTACTAREMOS PARA QUE EMPIECES!</span> </h3>
            <div class="col-md-4 hidden-xs"> </div>
            <div class="col-md-4">
              <div class="logi-box">
              <h3>REGISTRARME</h3>
              <?php $form = ActiveForm::begin([
                  'method' => 'post',
                  'id' => 'contact-form',
                  'enableClientValidation' => true,
                  'enableAjaxValidation' => true,
                  'class'=> 'contact-form',
                ]);
              ?>
              <div class="form-group">
                <div class="controls">
                <label>NOMBRE</label>    
                <?= $form->field($model, "username")->input("text", ['placeholder' => 'Jhon', 'class' => 'email requiredField'])->label(false) ?>
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <label>APELLIDO</label>
                  <?= $form->field($model, "last_name")->input("text", ['placeholder' => 'Smith', 'class' => 'email requiredField'])->label(false) ?>
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <label>E-MAIL</label>    
                  <?= $form->field($model, "email")->input("email", ['placeholder' => 'johnsmith@mymail.com', 'class' => 'email requiredField'])->label(false); ?>
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <label>TEL&Eacute;FONO 1</label>
                  <?= $form->field($model, "user_phone1")->input("text", ['placeholder' => '312 338 3515', 'class' => 'email'])->label(false); ?>
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <label>TEL&Eacute;FONO 2</label>
                  <?= $form->field($model, "user_phone2")->input("text", ['placeholder' => '320 20 98', 'class' => 'email'])->label(false); ?>
                </div>
              </div>
              <button type="submit" id="submit" class="btn-system01">SOLICITAR CUENTA</button>
              <?php $form->end() ?>
              </div>
              <p>
                <span style="color:#FFFFFF"> 
                  Al hacer click en "Crear cuenta" confirmo que he le&iacute;do y he aceptado los
                  <a class="text-register"  href="#">T&eacute;rminos y condiciones</a> y la 
                  <a class="text-register" href="#">Pol&iacute;tica de privacidad</a> de VOY A TODO    
                </span>
              </p>
              
              <div class="descarga-box">
                <p>Descarga la app:</p>
                <p class="animated5"><a href="#" class="app">app</a> &nbsp; <a href="#" class="an">an</a> </p>
              </div>
            </div>
            <div class="col-md-4 hidden-xs"> </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- /carousel --> 
  </section>
  <div class="clearfix"></div>
</div>

<?php
  if($message == 1)
  { ?>
    <script>
      swal("Muy bien", "Tu solitud ha sido enviada correctamente, pronto nos pondremos en contacto contigo", "success")
    </script>
    <?php
  }
  else
    if($message == 2)
    {
      ?>
      <script>
        swal({
          title: "Oh! no",
          text: "Ha ocurrido un error al llevar a cabo tu registro",
          type: "error"
        });
      </script>
      <?php
    }
?>
 
<script type="text/javascript">
  $(document).ready(function() {  
      $(".btn-system01").click(function(){
      $("#contact-form").submit();
    });
  });
</script>