<?php

use app\models\VTTcategory;
use app\models\VTTticket;

$this->title = 'Voy A Todo | Home';

?>
  
  <!-- Start Home Page Slider -->
  <section id="home"> 
    <!-- Carousel -->
    <div id="main-slide" class="carousel slide" data-ride="carousel"> 
      
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#main-slide" data-slide-to="0" class="active"></li>
        <li data-target="#main-slide" data-slide-to="1"></li>
        <li data-target="#main-slide" data-slide-to="2"></li>
      </ol>
      <!--/ Indicators end--> 
      
      <!-- Carousel inner -->
      <div class="carousel-inner">
        <div class="item active"> <img class="img-responsive" src="<?php echo Yii::getAlias('@web')?>/images/slider/bg1.jpg" alt="slider">
          <div class="slider-content">
          <div class="container container-update">
            <div class="col-md-7 col-sm-6 text-left text-content-xs text-content">
              <h2 class="animated2">La forma m&aacute;s f&aacute;cil de crear, reservar<br />
                  y hacer dinero con tus eventos
              </h2>
              <h3 class="animated3"> <span>pruebala tambi&eacute;n en tu smartphone</span> </h3>
              <p class="animated4"><a href="<?php echo Yii::getAlias('@web') ?>/site/login"  class="eventos" >CREAR EVENTO</a> </p>
              <p class="animated5 animated5-top100"><a href="#" class="app">app</a> &nbsp; <a href="#" class="an">an</a> </p>
              
            </div>            
            <div class="col-md-5 col-sm-6 hidden-xs">
              <img class="img-responsive01" src="<?php echo Yii::getAlias('@web')?>/images/banner-1.png" alt="slider">
            </div>            
            <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <!--/ Carousel item end -->
        <div class="item"> <img class="img-responsive" src="<?php echo Yii::getAlias('@web')?>/images/slider/bg1.jpg" alt="slider">
          <div class="slider-content">
          <div class="container container-update">
            <div class="col-md-7 col-sm-6 text-left text-content-xs text-content">
              <h2 class="animated2">Dale un vistazo a la Tienda donde<br />
                encontrar&aacute;s servicios para tu Evento
              </h2>
              <h3 class="animated3"> <span>pruebala tambi&eacute;n en tu smartphone</span> </h3>
              <p class="animated4"><a href="<?php echo Yii::getAlias('@web') ?>/site/login"  class="eventos" >CREAR EVENTO</a> </p>
              <p class="animated5 animated5-top100 hideden-xs"><a href="#" class="app">app</a> &nbsp; <a href="#" class="an">an</a> </p>              
            </div>            
            <div class="col-md-5 col-sm-6 hidden-xs">
              <img class="img-responsive01" src="<?php echo Yii::getAlias('@web')?>/images/banner-2.png" alt="slider">
            </div>            
            <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <!--/ Carousel item end -->
        <div class="item"> <img class="img-responsive" src="<?php echo Yii::getAlias('@web')?>/images/slider/bg1.jpg" alt="slider">
          <div class="slider-content">
          <div class="container container-update">
            <div class="col-md-7 col-sm-6 text-left text-content-xs text-content">
              <h2 class="animated2">
                Crea tus Eventos con Preventa<br />
                o financiamiento colectivo. ¡Hazlo Hoy!
              </h2>
              <h3 class="animated3"> <span>pruebala también en tu smartphone</span> </h3>
              <p class="animated4"><a href="<?php echo Yii::getAlias('@web') ?>/site/login"  class="eventos" >CREAR EVENTO</a> </p>
              <p class="animated5 animated5-top100"><a href="#" class="app">app</a> &nbsp; <a href="#" class="an">an</a> </p>
            </div>            
            <div class="col-md-5 col-sm-6 hidden-xs">
              <img class="img-responsive01" src="<?php echo Yii::getAlias('@web')?>/images/banner-3.png" alt="slider">
            </div>            
            <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <!--/ Carousel item end --> 
      </div>
    </div>
    <!-- /carousel --> 
  </section>
  <!-- End Home Page Slider -->
  
  <div class="section destacados" style="background:#f2f2f2;">
    <div class="container container-update"> 
      
      <!-- Start Big Heading -->
      <div class="big-title text-center">
        <h1>mira algunos eventos <span>destacados</span></h1>
        <p class="text-center">Estos son los eventos más populares en este momento</p>
      </div>
      <!-- End Big Heading --> 
            
      <!-- Start Team Members -->
      <div class="row">
        <?php
          $cont = 0;
          foreach ($events_publicity as $event) 
          {
            ?>
            <div class="col-lg-3 col-md-4 col-sm-6">
              <div class="team-member modern">
                <a href="<?php echo Yii::getAlias('@web').'/evento/v/'.$event['event_url'] ?>"> 
                  <div class="member-photo imgevent" style="background-image: url('<?php echo Yii::getAlias('@web').$event['event_image']; ?>');background-size: 100% 100% !important;">
                    <?php
                      $type = VTTticket::find()
                        ->where("fkevent =:pkevent", [":pkevent" => $event['pkevent']])
                        ->one();
                      if($type)
                      {
                        ?>
                          <div class="member-name color0<?php echo $type->getFktipetickect0()->one()->pktypetickect; ?>">
                            <span><?php echo $type->getFktipetickect0()->one()->typeticket_name; ?></span> 
                          </div>
                        <?php
                      }
                      else
                      {
                        ?>
                          <div class="member-name color04">
                            <span>ENTRADA LIBRE</span> 
                          </div>
                        <?php
                      }
                    ?>
                  </div>
                </a>
                <div class="member-info">
                  <div class="post-date">
                    <?php
                      date_default_timezone_set('America/Bogota');
                      echo date('d M, Y. H:i',strtotime($event['event_stardate'].$event['event_starthour']));
                    ?>
                  </div>

                  <h2><a href="<?php echo Yii::getAlias('@web').'/evento/v/'.$event['event_url']; ?>"> <?php echo $event['event_name']; ?></a></h2>
                  <div class="post-date"><?php echo $event['event_address']; ?> </div>
                </div>  
                <div class="member-socail">
                  <?php
                    $query = VTTcategory::find()
                      ->where("pkcategory =:fkcategory", [":fkcategory" => $event['fkcategory1']])
                      ->one();
                    $icon = $query->category_pathicon;
                    $name = $query->category_name;
                  ?> 
                  <a class="icon05" style="background: url('<?php echo Yii::getAlias('@web').$icon; ?>') center no-repeat;" data-toggle="tooltip" title="<?php echo $name; ?>" data-placement="bottom" onmouseover="hover(this);" onmouseout="unhover(this);"></a>
                  <a class="icon04" data-toggle="tooltip" title="Favoritos" data-placement="bottom" href="<?php echo Yii::getAlias('@web') ?>/site/login"></a>
                  <a class="icon03" data-popover="<?php echo 'http://voyatodo.com/web/evento/v/'.$event['event_url']; ?>" data-toggle="popover" data-html="true" data-container="body"></a>
                  <a class="link-button" href="<?php echo Yii::getAlias('@web').'/evento/v/'.$event['event_url']; ?>">ver m&aacute;s</a> 
                </div>
              </div>
            </div>
            <?php
            $cont++;
            if($cont > 15)
              break;
          }
          if($cont < 15)
          {
            foreach ($events as $event) 
            {
              ?>
              <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="team-member modern">
                  <a href="<?php echo Yii::getAlias('@web').'/evento/v/'.$event['event_url'] ?>"> 
                    <div class="member-photo imgevent" style="background-image: url('<?php echo Yii::getAlias('@web').$event['event_image']; ?>');background-size: 100% 100% !important;">
                      <?php
                        $type = VTTticket::find()
                          ->where("fkevent =:pkevent", [":pkevent" => $event['pkevent']])
                          ->one();
                        if($type)
                        {
                          ?>
                            <div class="member-name color0<?php echo $type->getFktipetickect0()->one()->pktypetickect; ?>">
                              <span><?php echo $type->getFktipetickect0()->one()->typeticket_name; ?></span> 
                            </div>
                          <?php
                        }
                        else
                        {
                          ?>
                            <div class="member-name color04">
                              <span>ENTRADA LIBRE</span> 
                            </div>
                          <?php
                        }
                      ?>
                    </div>
                  </a>
                  <div class="member-info">
                    <div class="post-date">
                      <?php
                        date_default_timezone_set('America/Bogota');
                        echo date('d M, Y. H:i',strtotime($event['event_stardate'].$event['event_starthour']));
                      ?>
                    </div>

                    <h2><a href="<?php echo Yii::getAlias('@web').'/evento/v/'.$event['event_url']; ?>"> <?php echo $event['event_name']; ?></a></h2>
                    <div class="post-date"><?php echo $event['event_address']; ?> </div>
                  </div>  
                  <div class="member-socail">
                    <?php
                      $query = VTTcategory::find()
                        ->where("pkcategory =:fkcategory", [":fkcategory" => $event['fkcategory1']])
                        ->one();
                      $icon = $query->category_pathicon;
                      $name = $query->category_name;
                    ?> 
                    <a class="icon05" style="background: url('<?php echo Yii::getAlias('@web').$icon; ?>') center no-repeat;" data-toggle="tooltip" title="<?php echo $name; ?>" data-placement="bottom"></a> 
                    <a class="icon04" data-toggle="tooltip" title="Favoritos" data-placement="bottom" href="<?php echo Yii::getAlias('@web') ?>/site/login"></a>
                    <a class="icon03" data-popover="<?php echo 'http://voyatodo.com/web/evento/v/'.$event['event_url']; ?>" data-toggle="popover" data-html="true" data-container="body"></a>
                    <a class="link-button" href="<?php echo Yii::getAlias('@web').'/evento/v/'.$event['event_url']; ?>">ver m&aacute;s</a> 
                  </div>
                </div>
              </div>
              <?php
              $cont++;
              if($cont > 15)
                break;
            }
          }
        ?>
        <div class="clearfix"></div>
        <div class="eventos-list"> <a class="eventos" href="<?php echo Yii::getAlias('@web').'/site/eventos/'; ?>">ver más eventos</a> </div>
      </div>
      <!-- End Team Members -->       
    </div>
    <!-- .container --> 
  </div>
  <div class="section purchase crowdfunding">
    <div class="container"> 
      <div class="section-video-content text-center"> 
        <!-- Start Animations Text -->
        <h4>eventos de <span>preventa</span></h4>
        <p>Ahora en Voy a Todo puedes crear tus eventos de forma colaborativa.<br />
          Crealo, dale incentivos a tus patrocinadores y Hazlo tuyo!</p>
        <!-- End Animations Text -->
        <div class="eventos-list eventos-list00"> <a class="eventos" href="<?php echo Yii::getAlias('@web') ?>/site/login">crear mi evento</a> </div>
      </div>
    </div>
    <!-- .container --> 
  </div>
  <div class="section destacados" style="background:#f2f2f2;">
    <div class="container"> 
      <!-- Start Big Heading -->
      <div class="big-title text-center">
        <h1>¿buscas un <span>evento?</span></h1>
        <p class="text-center">Revisa el listado de categorías</p>
      </div>
      <!-- End Big Heading --> 
      <!-- Start Team Members -->
     <div class="row">
       <?php
          foreach ($category as $cate) { ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="evento-img">
                <figure>
                  <a href="<?php echo Yii::getAlias('@web').'/evento/categoria/'.$cate->category_route; ?>">
                    <img alt="<?php echo $cate->category_name ?>" src="<?php echo Yii::getAlias('@web').$cate->category_pathImage?>" /> 
                  </a>                   
                </figure>
              </div>
            </div> 
        <?php  }
       ?>
      </div>
      <!-- End Team Members --> 
      
    </div>
    <!-- .container --> 
  </div>
  <div class="section purchase crowdfunding">
    <div class="container"> 
      
      <!-- Start Video Section Content -->
      <div class="section-video-content text-center"> 
        
        <!-- Start Animations Text -->
        <h4>CREA TU PROPIO <span>evento</span></h4>
        <p>Voy a Todo te d&aacute; las herramientas necesarias para que puedas hacer toda<br />
          la gestión de tus eventos en un s&oacute;lo lugar</p>
        <!-- End Animations Text -->
        <div class="eventos-list eventos-list00"> <a class="eventos" href="<?php echo Yii::getAlias('@web') ?>/site/login">crear mi evento</a> </div>
      </div>
    </div>
    <!-- .container --> 
  </div>

  <!-- Popop -->
  <div id="popover-content" class="hide">
      <span>
        <a class="text-facebook ico share_facebook_2"> <i class="fa fa-facebook"></i> </a>
        <a class="text-twitter ico share_twitter_2"> <i class="fa fa-twitter"></i> </a>
        <a class="text-linkedin ico share_linkedin_2"> <i class="fa fa-linkedin"></i> </a>
        <a class="text-google share_google_2"> <i class="fa fa-google-plus"></i> </a>
      </span>
  </div> 

</div>
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web')?>/js/script.js"></script>

<script type="text/javascript">
  $(document).ready(function() {

    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');  
      
    $('[data-toggle="tooltip"]').tooltip();

    $(".icon03").click(function(){
      var url = $(this).attr("data-popover");
      $(".share_facebook_2").attr('data-url','http://www.facebook.com/sharer.php?u='+url);
      $(".share_twitter_2").attr('data-url','https://twitter.com/share'+url);
      $(".share_linkedin_2").attr('data-url','https://www.linkedin.com/cws/share?url='+url);
      $(".share_google_2").attr('data-url','https://plus.google.com/share?url='+url);
      $('[data-toggle="popover"]').popover({
          html: true, 
          content: function() {
              return $('#popover-content').html();
          }
      });
    });
  });
</script>