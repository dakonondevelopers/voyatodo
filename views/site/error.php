<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Voy A Todo | Error';
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Un error ocurrio mientras el servidor web estaba procesando tu requerimiento.
    </p>
    <p>
        Por favor contactanos si crees que es un error del servidor. Gracias.
    </p>

</div>
