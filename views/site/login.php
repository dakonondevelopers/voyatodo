<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Voy A Todo | Login';

?>
<div id="container-login">
  <!-- Start Home Page Slider -->
  <section id="login-pages"> 
    <!-- Carousel -->
    <div id="main-slide0" class="carousel00 slide">
      <div class="item01">
        <div class="slider-content0">
          <div class="col-md-12 text-center">
            <h2 class="animated2"> <span>Comienza Ahora!</span> </h2>
            <h3 class="animated3"> <span>publica y gestiona tus eventos en minutos</span> </h3>
            <div class="col-md-4 hidden-xs"> </div>
            <div class="col-md-4">
              <div class="logi-box">
                <h3>INGRESAR</h3>
                <?php $form = ActiveForm::begin([
                  'method' => 'post',
                  'id' => 'contact-form',
                  'enableClientValidation' => true,
                  'enableAjaxValidation' => true,
                  'class'=> 'contact-form',
                  ]);
                ?>
                  <div class="form-group">
                    <div class="controls">
                      <label>E-MAIL</label>    
                      <?= $form->field($model, "username")->input("email", ['placeholder' => 'johnsmith@mymail.com', 'class' => 'email requiredField'])->label(false); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="controls">
                      <label>CONTRASE&Ntilde;A</label>
                      <?= $form->field($model, "password")->input("password", ['placeholder' => '•••••••••••••••••••••', 'class' => 'email'])->label(false); ?>
                    </div>
                  </div>
                  <button type="submit" id="submit" class="btn-system01">INGRESAR</button>
                  <div id="success" style="color:#34495e;"></div>
                  <p class="link-button0"><a href="<?php echo Yii::getAlias('@web') ?>/site/recuperar">&iquest;Olvidaste tu contrase&ntilde;a?</a></p>
                  <p class="text-list"> Ingresa con tu red social favorita </p>
                  <div class="social-auth-links text-center text-social"> 
                    <?= yii\authclient\widgets\AuthChoice::widget([
                     'baseAuthUrl' => ['site/auth'],
                     'popupMode' => false,
                     ]) ?> 
                 </div>
                  <p class="title-n">&iquest;ERES NUEVO?</p>
                  <p><span>Reg&iacute;strate y obt&eacute;n tu cuenta gratis
                    y empieza a crear y gestionar eventos.</span></p>
                  <div class="re-link"> <a href="<?php echo Yii::getAlias('@web') ?>/site/registro">REGISTRARME</a> </div>
              </div>
              <div class="descarga-box">
                <p>Descarga la app:</p>
                <p class="animated5"><a href="#" class="app">app</a> &nbsp; <a href="#" class="an">an</a> </p>
              </div>
            </div>
            <div class="col-md-4 hidden-xs"> </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- /carousel --> 
  </section>
  <div class="clearfix"></div>
</div> 
<script type="text/javascript">
  $(document).ready(function() {  
      $(".btn-system01").click(function(){
      $("#contact-form").submit();
    });
  });
</script>