<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;
use app\models\VTTconfig;

$this->title = 'Voy A Todo | Evento';

?>

<div id="container">
  <section id="inner01">
    <div id="main-slide" class="carousel00 slide slide_galery" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
        <div class="slider-content">
          <div class="col-md-12 text-center">
            <h3 class="animated3"> <span>estado de tu evento:</span> </h3>
            <p class="animated4"><a class="eventos public" >PUBLICADO</a> <a class="eventos save" >GUARDAR</a> </p>
          </div>
        </div>
      </div>
    </div>
  </section>


  <!-- Comienza la primera opción del menú -->

  <div class="menu-modal summary-modal dashboard-modal scrollbar" id="opcion1">
    
    <div class="separator"></div>
    
    <div class="row">
      <div class="col-md-7 col-sm-6 col-xs-6">
        <h4>RESUMEN DEL EVENTO</h4>
      </div>
      <div class="col-md-5">
        <a class="btn btn-left">DESCARGAR REPORTE</a>
      </div>
    </div>
    <div class="row">
      <?php 
        if(!empty($tickets))
        {
          $total_1=0;
          $comison = VTTconfig::find()->one();
          $visits=0;
          $buys=0;
          foreach ($tickets as $tick) 
          {
            $query = $buy->find()->where("fkticket =:pkticket", [":pkticket" => $tick->pkticket])->all();
            $queryBuy = $buy->find()
              ->where("fkticket =:pkticket", [":pkticket" => $tick->pkticket])
              ->groupBy('fkuser')
              ->all();
            $buys += count($queryBuy);
            $cont=0;
            foreach ($query as $buy) 
              $cont++;
            $total_1 += $cont*$tick->ticket_value;
            $visits += $cont;
          }
          $rest = $total_1*($comison->config_comision1/100);
          $total = $total_1 - $rest;
        }
        else
        {
          $total_1=0;
          $comison = VTTconfig::find()->one();
          $visits=0;
          $buys=0;
          $total=0;
          $rest=0;
        }
      ?>
      <div class="col-md-6 summary-images">
        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/pie.png">
          <p>0</p>
          <p>VISITAS</p>
        </div>
        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/buyers.png">
          <p><?php echo $buys; ?></p>
          <p>COMPRAS</p>
        </div>
      </div>
      <div class="col-md-6 summary-images">
        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/basket.png">
          <p><?php echo number_format($total); ?> COP</p>
          <p>DINERO RECIBIDO</p>
        </div>

        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/assistants.png">
          <p><?php echo $visits; ?></p>
          <p>ASISTENTES</p>
        </div>
      </div>
    </div>
    <div class="separator"></div>
    <h4 class="align_left">RESUMEN DE INGRESOS</h4>
    <table class="table">
      <tr>
        <td><p>BOLETA</p></td>
        <td><p>VALOR</p></td>
        <td><p>VENDIDAS</p></td>
      </tr>
      <?php 
        if(!empty($tickets))
        {
          foreach ($tickets as $tick) 
          {?>
            <tr>
              <td><?php echo $tick->ticket_name; ?></td>
              <?php
                if($tick->fktipetickect == 1)
                {
                  echo "<td>GRATIS</td>";
                }
                else
                {
                  echo "<td>".number_format($tick->ticket_value)."</td>";
                }
                $query = $buy->find()->where("fkticket =:pkticket", [":pkticket" => $tick->pkticket])->all();
                $cont=0;
                foreach ($query as $buy) 
                  $cont++;
                echo "<td>".$cont."</td>";
              ?>
            </tr>
          <?php
          }
        }
      ?>
      <tr>
        <td>TOTAL</td>
        <td><?php echo number_format($total_1); ?> COP</td>
      </tr>
      <tr>
        <td><p>COMISI&Oacute;N (<?php echo round($comison->config_comision1,2)." %"; ?>)</p></td>
        <td><p><?php echo number_format($rest); ?> COP</p></td>
      </tr>
      <tr class="bluish">
        <td class="ttl_blue">TOTAL A RECIBIR</td>
        <td class="ttl_blue"><?php echo number_format($total); ?> COP</td>
      </tr>
    </table>
  </div>

  <!-- Comienza la segunda opción del menú -->

  <form id="save-form" action="<?php echo Yii::getAlias('@web') ?>/account/guardar" method="post" enctype="multipart/form-data"></form>

  <?php $form = ActiveForm::begin([
      'method' => 'post',
      'id' => 'form-create',
      'enableClientValidation' => true,
      'enableAjaxValidation' => true,
      'options' => ['enctype' => 'multipart/form-data'],
      'class'=> 'contact-form',

    ]);
  ?>
    <input type="hidden"  name="FormCreateEvent[status]" value="<?php echo false; ?>">
    <input type="hidden"  name="FormCreateEvent[fkstatus]" value="<?php echo $fkstatus; ?>">
    <input type="hidden"  name="FormCreateEvent[eventid]" value="<?php echo $event->fkstatus; ?>">

    <div class="menu-modal information-modal summary-modal dashboard-modal scrollbar" id="opcion2">
      
      <div class="separator"></div>
      
      <h3>INFORMACI&Oacute;N DEL EVENTO</h3>
      <label for="nombre_evento">
        NOMBRE DEL EVENTO
      </label>
      <div class="input-group">
        <?= $form->field($model, "event_name")->input("text", ['placeholder' => '0/80', 'class' => 'form-control event-name', 'value' => $event->event_name])->label(false) ?>
        <span class="input-group-addon">
          <span class="cont_name">0/80</span>
        </span>
      </div>
      <div class="separator"></div>
      <div class="row">
        <h3>FECHA Y HORA</h3>
        <div class="col-md-6 col-sm-6 col-xs-6">
          <label class="lblnegro">FECHA INICO</label>
          <div class="input-group">
            <?php $model->event_stardate = $event->event_stardate; ?>
            <?= $form->field($model, 'event_stardate')->widget(
              DatePicker::className(), [
                'inline' => false,
                'language' => 'es', 
                'clientOptions' => [
                'autoclose' => true,
                'format' => 'dd-mm-yyyy'
                ]
              ],['class' => 'form-control no-left-border','id' => 'inicio'])->label(false);
            ?>
          </div> 
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 div_fecha">
          <div class="input-group clockpicker" data-donetext="Listo">
            <?php $model->event_starthour = $event->event_starthour; ?>
            <?= $form->field($model, "event_starthour")->input("text", ['class' => 'form-control event-starthour'])->label(false) ?>
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-time"></span>
            </span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-6">
          <label class="lblnegro">FECHA FINAL</label>
          <div class="input-group">
            <?php $model->event_enddate = $event->event_enddate; ?>
            <?= $form->field($model, 'event_enddate')->widget(
              DatePicker::className(), [
                'inline' => false,
                'language' => 'es', 
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'dd-mm-yyyy',
                    'value' =>  $event->event_enddate
                ]
              ],['class' => 'form-control no-left-border','id' => 'inicio'])->label(false);
            ?>
          </div> 
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 div_fecha">
          <div class="input-group clockpicker" data-donetext="Listo">
            <?php $model->event_endhour = $event->event_endhour; ?>
            <?= $form->field($model, "event_endhour")->input("text", ['class' => 'form-control event-endhour'])->label(false) ?>
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-time"></span>
            </span>
          </div>
        </div>                                        
      </div>

      <div class="separator"></div>

      <div class="row top">
        <div class="form-group" >
          <label class="col-sm-3 col-xs-3 lblnegro">PA&Iacute;S:</label>
          <div class="col-sm-9 col-xs-9">
            <?php 
              $listData2=ArrayHelper::map($countries,'pkcountry','country_name');
              $model->event_country = $event->getFkcity0()->one()->getFkdepartament0()->one()->getFkcountry0()->one()->pkcountry;
            ?>
            <?= $form->field($model, 'event_country')->dropDownList($listData2, 
              [
                'prompt'=>'Seleccione País',
                'class' => 'form-control cmb-country',
                'onchange'=>'
                  $.get( "'.Url::toRoute('account/getcities').'", { id: $(this).val() } )
                      .done(function( data ) {
                          $( "#'.Html::getInputId($model, 'event_city').'" ).html( data );
                      }
                  );'
              ])->label(false); ?>
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 col-xs-3 lblnegro">CIUDAD:</label>
           <div class="col-sm-9 col-xs-9">
              <?php 
                $model->event_city = $event->getFkcity0()->one()->pkcity;
              ?>
              <?= $form->field($model, 'event_city')->dropDownList([],
                ['prompt'=>'Seleccione Ciudad',
                'class' => 'form-control cmb-city'])->label(false); ?>
          </div>
        </div>
        <div class="form-group top">
          <label class="col-sm-3 col-xs-3 lblnegro">LUGAR:</label>
           <div class="col-sm-9 col-xs-9">
              <?= $form->field($model, "event_place")->input("text", ['class' => 'form-control event-place','value' => $event->event_place])->label(false) ?>
          </div>
        </div>
        <div class="form-group top">
          <label class="col-sm-3 col-xs-3 lblnegro">DIRECCI&Oacute;N:</label>
           <div class="col-sm-9 col-xs-9">
              <?= $form->field($model, "event_address")->input("text", ['class' => 'form-control text-address','value' => $event->event_address])->label(false) ?>
          </div>
        </div>
      </div>
      <div class="col-lg-2 input-group search">
        <button type="button" id="search" class="btn btn-sm btn-success btn-block">Buscar</button>
      </div>
      <div class="input-group">
        <div class="map_canvas1" style="height:200px;"></div>
      </div>
      <?= $form->field($model, "event_lat")->input("hidden", ['class' => 'form-control event_lat','value' => $event->event_lat])->label(false) ?>
      <?= $form->field($model, "event_log")->input("hidden", ['class' => 'form-control event_log', 'value' => $event->event_log])->label(false) ?> 

      <div class="separator"></div>

      <h3>CATEGOR&Iacute;AS DEL EVENTO</h3>
      <div class="row">
        <div class="form-group top">
          <label class="col-sm-3 col-xs-3 lblnegro">PRINCIPAL</label>
           <div class="col-sm-9 col-xs-9">
              <?php 
                $listData=ArrayHelper::map($categories,'pkcategory','category_name'); 
                $model->event_category_principal = $event->getFkcategory10()->one()->pkcategory;
              ?>
              <?= $form->field($model, 'event_category_principal')->dropDownList($listData, 
                [
                  'prompt'=>'Seleccione Categoría',
                  'class' => 'form-control',
                  'onchange'=>'
                    $.get( "'.Url::toRoute('account/getcategories').'", { id: $(this).val() } )
                        .done(function( data ) {
                            $( "#'.Html::getInputId($model, 'event_category_secundary').'" ).html( data );
                        }
                    );
              '])->label(false); ?>
           </div>
        </div>
        <div class="form-group" >
          <label class="col-sm-3 col-xs-3 lblnegro">SECUNDARIA</label>
           <div class="col-sm-9 col-xs-9">
              <?php 
                $model->event_category_secundary = $event->getFkcategory20()->one()->pkcategory;
              ?>
              <?= $form->field($model, 'event_category_secundary')->dropDownList([],
               ['prompt'=>'Seleccione categoría',
               'class' => 'form-control'])->label(false); ?>               
           </div>
        </div>
      </div>

      <div class="separator"></div>

      <h3>DETALLES DEL EVENTO</h3>
      <div class="row top">
        <div class="form-group">
          <label class="col-sm-3 col-md-12">RESUMEN DEL EVENTO</label>
          <div class="col-sm-9 col-md-12">
            <?= $form->field($model, "event_review")->input("text", ['placeholder' => '0/140','class' => 'form-control no-right-border text-review', 'value' => $event->event_review])->label(false)?>
            <span class="input-group-addon">
              <span class="cont_review">0/140</span>
            </span>
            <p class="description_social">Resume tu Evento en 140 car&aacute;cteres o menos.</p>
          </div>     
        </div>
        <div class="form-group">
          <label class="col-md-12 top">DESCRIPCI&Oacute;N DEL EVENTO</label>
          <div class="col-md-12">
            <?= $form->field($model, 'event_description')->textArea(['class' => 'text-area text-description', 'value' => $event->event_description])->label(false) ?>
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-12 top">SELECCIONA EL TIPO DE EVENTO</label>
          <div class="container-fluid">
            <div class="input-group radios">
            <?php
              $tickets = $event->getVtTtickets()->all();
              if(empty($tickets))
              {
                ?>
                <div class="col-md-4">
                  <input type="radio" name="FormCreateEvent[typeticket_name]" value="1" class="inputradio type_tic"> <span class="inputfalso"> <label class="lbl_radio">GRATIS</label> </span> <br>          
                </div>
                <div class="col-md-4">
                  <input type="radio" name="FormCreateEvent[typeticket_name]" value="2" class="inputradio type_tic"> <span class="inputfalso"> <label class="lbl_radio">PAGO</label> </span> <br>
                </div>
                <div class="col-md-4">
                  <input type="radio" name="FormCreateEvent[typeticket_name]" checked value="4" class="inputradio type_tic"> <span class="inputfalso"> <label class="lbl_radio">LIBRE</label> </span> <br>
                </div>
                <div class="col-md-6 col-md-offset-3">
                  <input type="radio" name="FormCreateEvent[typeticket_name]" value="3" class="inputradio type_tic"> <span class="inputfalso"> <label class="lbl_radio">CROWFUNDING</label> </span> <br>
                </div>
                <?php
              }
              else
              {
                ?>
                <div class="col-md-4">
                  <input type="radio" <?php echo $event->getVtTtickets()->one()->fktipetickect = 1 ? "checked" : ""; ?> name="FormCreateEvent[typeticket_name]" value="1" class="inputradio type_tic"> <span class="inputfalso"> <label class="lbl_radio">GRATIS</label> </span> <br>          
                </div>
                <div class="col-md-4">
                  <input type="radio" <?php echo $event->getVtTtickets()->one()->fktipetickect = 2 ? "checked" : ""; ?> name="FormCreateEvent[typeticket_name]" value="2" class="inputradio type_tic"> <span class="inputfalso"> <label class="lbl_radio">PAGO</label> </span> <br>
                </div>
                <div class="col-md-4">
                  <input type="radio" name="FormCreateEvent[typeticket_name]" value="4" class="inputradio type_tic"> <span class="inputfalso"> <label class="lbl_radio">LIBRE</label> </span> <br>
                </div>
                <div class="col-md-6 col-md-offset-3">
                  <input type="radio" <?php echo $event->getVtTtickets()->one()->fktipetickect = 3 ? "checked" : ""; ?> name="FormCreateEvent[typeticket_name]" value="3" class="inputradio type_tic"> <span class="inputfalso"> <label class="lbl_radio">CROWFUNDING</label> </span> <br>
                </div>
                <?php
              }
            ?>
            </div>
          </div>
          <div class="hidden options-crowfunding"> 
            <div class="col-md-6 col-sm-4 col-xs-4 top">
              <label for="nombre_evento">
                META FINANCIERA
              </label>
            </div>
            <div class="col-md-6 col-sm-4 col-xs-4 top">
              <div class="input-group">
                <div class="input-group-addon input-group-addon0">$</div>
                <?= $form->field($model, "event_crowfunding")->input("text", ['class' => 'form-control meta_financiera align_right', 'value' => $event->event_crowfunding])->label(false)?>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group top">
          <label class="col-md-12">URL DEL EVENTO</label>
          <div class="col-md-12">
            <span class="input-group-addon" id="basic-addon1">http://voyatodo.com/evento/v/</span> 
             <?= $form->field($model, "event_url")->input("text", ['class' => 'form-control text-url', 'value' => $event->event_url])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-12 top">VIDEO DE YOUTUBE O VIMEO</label>
          <div class="col-md-12">
            <?= $form->field($model, 'event_linkvideo')->textArea(['value' => $event->event_linkvideo,'class' => 'text-area text-video','placeholder' => 'Pega aquí tu código de YouTube o Vimeo:'])->label(false) ?>
          </div>
        </div>
        <div class="input-group search">
          <button type="button" id="search-video" class="btn btn-sm btn-block top_">Buscar</button>
        </div>
      </div><!--row-->  
      <div class="row">
        <div class="form-group">
          <label class="col-md-12">GALER&Iacute;A DE IM&Aacute;GENES</label>
          <div class="col-md-12">
            <div class="cargar top_menos btn-block"><label class="lbl_btn">SUBIR IM&Aacute;GENES</label>
              <?= $form->field($model, "event_galery")->fileInput(['multiple' => true, 'class' => 'eventos archivos upload-img', 'accept' => 'image/*'])->label(false) ?>
            </div>
            <p class="description_social">El tama&ntilde;o de la imágenes debe ser superior a 200px y M&aacute;ximo 3</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="form-group top">
          <label for="nombre_evento" class="col-md-12">T&Eacute;RMINOS Y CONDICIONES</label>
          <div class="col-md-12">
            <?= $form->field($model, 'event_terms')->textArea(['class' => 'text-area text-terms','placeholder' => 'Agrega aquí los términos y condiciones del servicio','value' => $event->event_terms])->label(false) ?>
          </div>
        </div>
      </div><!--row end-->
      <div class="row">
        <div class="form-group">
          <label for="cat_principal" class="col-md-12 top">ESTADO</label>
          <div class="col-md-12">
            <?php 
              $listState_Ticket= ['0' => 'ABIERTO', '1' => 'PRIVADO']; 
              $model->event_visible = $event->event_visible;
            ?>
            <?= $form->field($model, 'event_visible')->dropDownList($listState_Ticket, 
              [
                'prompt'=>'',
                'class' => 'form-control'
              ])->label(false); 
            ?>
            <p class="description">
              Al poner un evento privado, s&oacute;lo acceder&aacute;s con la URL
            </p>
          </div>
        </div>
      </div><!--row end-->
      
      <div class="separator"></div>

      <h3>FORMULARIO DE REGISTRO</h3>

      <?php 
        foreach ($formdinamic as $value) 
        {
          ?>
          <div class="input-group">
            <input type="text" class="form-control form_regist" name="generic[]" value="<?php echo $value->formeventgeneric; ?>" readonly style="border-right: 0px !important;">
          </div>  
          <hr/>
          <?php
        }
      ?>
      <div class="input-group divciudad">
        <input type="text" id="ciudad" class="form-control form_regist" name="generic[]" value="AGREGA OTRO CAMPO" style="border-right: 0px !important;">
        <span class="input-group-btn">
          <a class="btn btn-default btn_delete" style="height: 108%;"><span class="glyphicon glyphicon-trash"></span></a>
        </span>
      </div>
      <hr class="divciudad"/>
      <div id="divotro" class="input-group">
        <input type="text" id="otro" class="form-control form_regist" name="generic[]" value="CIUDAD">
      </div>
      <div class="input-group">
        <div class="col-md-8 col-sm-8 col-xs-8  col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
        <a class="btn cargar btn-block" id="btnagregar_campo">AGREGRAR CAMPO</a>
        </div>
      </div>

      <div class="separator"></div>

      <h3>ENVIAR MAIL DEL EVENTO A LOS USUARIOS</h3>
      <div class="row top">
        <div class="form-group">
          <label for="nombre_evento" class="col-md-12">TITULO DEL EMAIL</label>
          <div class="col-md-12">
            <input name="text_email_send" type="text" class="form-control no-right-border text-email-send" placeholder="0/140">
            <p class="description_social">Describe tu Evento en 140 car&aacute;cteres o menos.s</p>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-12 top">DESCRIPCI&Oacute;N DEL EMAIL</label>
          <div class="col-md-12">
            <textarea class="text-area text-description-email" name="text_description_email"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label for="cat_principal" class="col-md-12">EMAIL DE RESPUESTA</label>
          <div class="col-md-12">
            <input name="text_email_send" type="email" class="form-control no-right-border text-email-respuesta" placeholder="nombre@correo.com">
            <p class="description_social">Aqu&iacute; se enviar&aacute; respuesta de tus destinatarios</p>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-12 top">SUBIR IM&Aacute;GEN DE CABECERA</label>
          <div class="col-md-12">
            <div class="cargar top_menos btn-block"><label class="lbl_btn">SUBIR IM&Aacute;GEN</label>
              <input name="event_imagemail" type="file" class="eventos archivos" accept="image/*">
            </div>
            <p class="description">El tama&ntilde;o de la im&aacute;gen debe ser de 600px</p>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-12 top">A&Ntilde;ADIR DESTINATARIOS</label>
          <div class="col-md-12">
            <div class="cargar top_menos btn-block"><label class="lbl_btn">CARGAR DESTINARIOS</label>
              <input name="event_filesend" type="file" class="eventos archivos">
            </div>
            <p class="description">Suba un archivo de Excel o CVS con los correos en una sola columna descendente</p>
          </div>
        </div>      
      </div><!--row end-->
      <div class="row top">
        <div class="col-md-6 col-sm-6 col-xs-6">
           <button type="button" class="btn btn-block btn-yellow">PREVIZUALIZAR</button>
        </div>    
        <div class="col-md-6 col-sm-6 col-xs-6">
           <button type="button" class="btn btn-block btn_green">ENVIAR EMAIL</button>
        </div>
      </div><!--row end-->
      <div class="separator"></div>
    </div><!--information-modal-->

    <!-- Comienza la tercera opción del menú -->

    <div class="separator"></div>

    <div class="menu-modal design-modal summary-modal dashboard-modal scrollbar"  id="opcion3">
      
      <div class="separator"></div>
      
      <h3>IM&Aacute;GEN DE BANNER</h3>
      <div class="row top">
        <div class="col-md-12">
          <div class="form-group">
            <label for="nombre_evento">CARGA TU PROPIA IM&Aacute;GEN</label>
            <div class="cargar top_menos btn-block"><label class="lbl_btn">SUBIR IM&Aacute;GEN</label>
              <?= $form->field($model, "event_image")->fileInput(['class' => 'eventos archivos upload-banner', 'accept' => 'image/*'])->label(false) ?>
            </div>
            <p class="description_social">El tama&ntilde;o del banner debe ser superior a 1400 x 600 px</p>
          </div>
        </div>
      </div><!--row end-->

      <div class="separator"></div>

      <h3>OPACIDAD</h3>
      <div class="row top">
        <div class="form-group">
          <label for="nombre_evento" class="col-md-12">CAPA OSCURA</label>
          <div class="col-md-12">
            <?= $form->field($model, "event_opacityimage")->input("range", ['min' => '0' ,'max' => '1','step' => '0.1','value' => '1','id' => 'opacity-banner'])->label(false) ?>
          </div>
        </div>
      </div><!--row end-->
    </div><!--diseño-modal-->

    <!-- Comienza la cuarta opción del menú -->

    <div class="menu-modal tickets-modal summary-modal dashboard-modal tickets_principal scrollbar"  id="opcion4">

      <div class="separator"></div>

      <h3>TUS ENTRADAS</h3>
      <label for="nombre_evento">
        CARGA TU PROPIA ENTRADA
      </label>
      <div class="col-md-12 top">
        <a class="btn btn-yellow btn-block btn_create_ticket">CREAR ENTRADA NUEVA</a>
      </div>
      <p class="description_social">
        Crea nuevas entradas, edita o borra Entradas existentes
      </p>
      <div id="goal_complete" style="color:#A94452;"></div>

      <input type="hidden"  name="FormCreateEvent[tickets]" class="json_tickets" value="">

      <div class="separator"></div>

      <h3>ENTRADAS CREADAS</h3>

      <div class="row div_tickets">
        <?php 
          if(!empty($tickets))
          {
            foreach ($tickets as $tick) 
            {
            ?>
            <div class="col-md-8 div_banco top div_update_ticket" name="<?php echo $tick->pkticket; ?>" id="<?php echo $tick->pkticket; ?>">
              <label class="lbl_bnc">
                <?php echo $tick->ticket_name; ?>
                <br/>
                <span class="ttl_blue">
                  <?php echo $tick->getFktipetickect0()->one()->typeticket_name; ?>
                </span>
              </label>
            </div>
            <?php
            }
          }
        ?>
        <div class="new_ticket"></div>
      </div>
    </div>

    <div class="menu-modal tickets-modal summary-modal dashboard-modal create_tickets hidden scrollbar"  id="opcion-1">

      <div class="separator"></div>

      <div class="row">
        <div class="col-md-7 col-sm-6 col-xs-6">
          <h4>DETALLE DEL TICKET</h4>
        </div>
        <div class="col-md-5">
          <a class="btn btn_delete_ticket"> ELIMINAR </a>
        </div>
      </div>
      <label for="nombre_evento">
        NOMBRE DEL TICKET
      </label>
      <div class="input-group">
        <input name="FormCreateEvent[ticket_name]" type="text" class="form-control ticket-name align_right" />
        <span class="input-group-addon">
          <span class="cont_ticket-name">0/35</span>
        </span>
      </div>
      <label for="nombre_evento">
        DESCRIPCI&Oacute;N
      </label>
      <div class="input-group">
        <input name="FormCreateEvent[ticket_description]" type="text" class="form-control ticket-description align_right"/>
        <span class="input-group-addon">
          <span class="cont_ticket-description">0/50</span>
        </span>
      </div>

      <div class="separator"></div>

      <h3>PRECIO DEL TICKET</h3>
      <div class="hidden options-pay">
        <div class="row top">
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento">
              VALOR DE ENTRADA
            </label>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <div class="input-group-addon input-group-addon0">$</div>
              <input name="FormCreateEvent[ticket_value]" type="number" class="form-control ticket-value align_right" placeholder="30000" />
            </div>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento" class="lbl_tickets">
              COMISI&Oacute;N
            </label>
            <p>
              Esta es la comisi&oacute;n que cobra Voy A Todo por el manejo de los eventos. 
            </p>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <input type="text" class="form-control ticket-comision ttl_blue" readonly="true"/>
            </div>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento" class="lbl_tickets">
              RECIBIR&Aacute;S
            </label>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <div class="input-group-addon input-group-addon0 ttl_blue">$</div>
              <input name="FormCreateEvent[ticket_comision]" type="number" class="form-control ticket-pay-user" placeholder="28700"/>
            </div>
          </div>
        </div>
      </div>
      <div class="hidden options-crowfunding">
        <div class="row top">
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento">
              VALOR DE BOLETA
            </label>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <div class="input-group-addon input-group-addon0">$</div>
              <input name="FormCreateEvent[ticket_value]" type="number" class="form-control ticket_value_crw align_right" placeholder="000" />
            </div>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento" class="lbl_tickets">
              DESCUENTO BOLETA
            </label>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <input type="text" class="form-control discount-ticket ttl_blue" readonly="true"/>
            </div>
          </div>

          <div class="col-md-12 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <div class="col-md-12">
                <input type="range" max="100" step="0.1" id="increase_discount">
              </div>
              <p class="description_social">
                Puedes escoger el valor de descuento de tu boleta.
              </p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento" class="lbl_tickets">
              COMISI&Oacute;N VAT
            </label>
            <p>
              Esta es la comisi&oacute;n que cobra Voy A Todo por el manejo de los eventos. 
            </p>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <input type="text" class="form-control ticket-comision ttl_blue" readonly="true"/>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento" class="lbl_tickets">
              VAS A RECIBIR
            </label>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <div class="input-group-addon input-group-addon0 ttl_blue">$</div>
              <input name="FormCreateEvent[ticket_comision]" type="number" class="form-control ticket_pay_crw" placeholder="000" />
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento" class="lbl_tickets">
              PUNTO DE EQUILIBRIO
            </label>
            <p class="description_social">
              Cantidad de boletas para cubrir la meta financiera
            </p>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <input name="qty_crowfunding" type="text" class="ttl_blue form-control qty_crowfunding" placeholder="0" readonly="true" />
            </div>
          </div>
        </div><!--row-->
        <div class="row top">
          <div class="col-md-12 col-sm-4 col-xs-4 top">
            <label for="nombre_evento">
              DESCRIBE EL INCENTIVO
            </label>
          </div>
          <div class="col-md-12 col-sm-4 col-xs-4">
            <input name="FormCreateEvent[ticket_insentive]" type="text" class="form-control ticket-insentive" placeholder="Camiseta y Gorra autografiada" />
          </div>
          <div class="col-md-12 col-sm-4 col-xs-4 top">
            <label for="nombre_evento">
              FECHA DE FINALIZACI&Oacute;N
            </label>
          </div>
          <div class="col-md-7 col-sm-4 col-xs-4">
            <div class="input-group">
              <?= $form->field($model, 'ticket_enddate')->widget(
                DatePicker::className(), [
                  'inline' => false,
                  'language' => 'es', 
                  'clientOptions' => [
                  'autoclose' => true,
                  'format' => 'mm-dd-yyyy'
                  ]
                ],['class' => 'form-control no-left-border','id' => 'inicio'])->label(false);
              ?>
            </div>
          </div>
          <div class="col-md-12 col-sm-4 col-xs-4">
            <label for="nombre_evento">
              FECHA LIMITE PARA CUMPLIR LA META
            </label>
          </div>
          <div class="col-md-12 col-sm-4 col-xs-4">
            <label class="ticket_limit_date" style="color:#A94452;font-size: 15px;text-align: center;"></label>
          </div>
        </div>
      </div>
      <div class="row top">
        <div class="col-md-12 col-sm-4 col-xs-4">
          <label for="nombre_evento">
            CANTIDAD
          </label>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
          <div class="input-group">
            <input name="FormCreateEvent[ticket_qty]" type="number" class="form-control ticket-qty" placeholder="100" />
          </div>
        </div>
        <div class="col-md-8 col-sm-4 col-xs-4">
          <input class="inputradio out_limit" type="checkbox"><span class="inputfalso"> <label class="lbl_radio">SIN L&Iacute;MITE</label></span>
        </div>
      </div>
      <div class="row top">
        <div class="col-md-4 col-sm-4 col-xs-4">
          <label for="nombre_evento">
            ENTRADAS RECLAMADAS
          </label>
        </div>
        <div class="entrance_tic">
          <div class="col-md-4 col-sm-4 col-xs-4">
            <input type="radio" name="FormCreateEvent[ticket_seeclaim]" value="0" checked class="inputradio"> 
            <span class="inputfalso"/> <label class="lbl_radio">OCULTAR</label> </span>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-4">
            <input type="radio" name="FormCreateEvent[ticket_seeclaim]" value="1" class="inputradio"> 
            <span class="inputfalso"/> <label class="lbl_radio">MOSTRAR</label> </span>
          </div>
        </div>
        <div class="col-md-12 col-sm-4 col-xs-4">
          <p class="description_social">Ocultar o Mostrar las entradas reclamadas para el evento. </p>
        </div>
      </div>
      
      <div class="separator"></div>

      <h3>DISPONIBLE AL P&Uacute;BLICO</h3>
      <div class="row top">
        <div class="col-md-12 col-sm-4 col-xs-4">
          <label for="nombre_evento">
            DESDE
          </label>
        </div>
        <div class="col-md-6 col-sm-4 col-xs-4">
          <?= $form->field($model, 'ticket_start')->widget(
            DatePicker::className(), [
              'inline' => false,
              'language' => 'es', 
              'clientOptions' => [
              'autoclose' => true,
              'format' => 'mm-dd-yyyy'
              ]
            ],['class' => 'form-control no-left-border'])->label(false);
          ?>
        </div>
        <div class="col-md-6 col-sm-4 col-xs-4">
          <input class="inputradio start_date" type="checkbox">
          <span class="inputfalso"> 
            <label class="lbl_radio">INMEDIATAMENTE</label> 
          </span>
        </div>
        <div class="col-md-12 col-sm-4 col-xs-4">
          <label for="nombre_evento">
            HASTA
          </label>
        </div>
        <div class="col-md-6 col-sm-4 col-xs-4">
          <?= $form->field($model, 'ticket_finish')->widget(
            DatePicker::className(), [
              'inline' => false,
              'language' => 'es', 
              'clientOptions' => [
              'autoclose' => true,
              'format' => 'mm-dd-yyyy'
              ]
            ],['class' => 'form-control no-left-border'])->label(false);
          ?>
        </div>
        <div class="col-md-5 col-sm-4 col-xs-4">
          <input class="inputradio end_date" type="checkbox">
          <span class="inputfalso"> 
            <label class="lbl_radio">D&Iacute;A DEL EVENTO</label> 
          </span>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 top">
          <a class="btn btn-yellow btn-block btn_save_ticket hidden">GUARDAR</a>
          <a class="btn btn-yellow btn-block btn_update_ticket hidden">ACTUALIZAR</a>        
        </div>
        <div id="error_tickets" style="color:#A94452;"></div>
      </div>
    </div>

    <!-- Comienza la quinta opción del menú -->

    <div class="menu-modal bank-modal summary-modal dashboard-modal bank_acount scrollbar"  id="opcion5">
      
      <div class="separator"></div>

      <h4 class="h4_left">A&Ntilde;ADIR CUENTA PARA RECIBIR PAGOS</h4>
      <div class="row div_bank">
        <?php 
          if(!empty($userbank))
          {
            foreach ($userbank as $bank) 
            {
            ?>
            <div class="col-md-8 div_banco top div_update_acount" name="<?php echo $bank->pkuserbank; ?>" id="<?php echo $bank->pkuserbank; ?>">
              <label class="lbl_bnc"><?php echo $bank->userbank_tipeacount.' - '. $bank->getFkbank0()->one()->bank_name ?>  
                <br/><span class="ttl_blue"><?php echo $bank->userbank_identification; ?></span>
              </label>
            </div>
            <?php
            }
          }
          else
          {
            ?>
            <div class="col-md-12 top">
              <a class="btn btn-yellow btn-block btn_pay">A&Ntilde;ADIR CUENTA</a>
            </div>
            <?php
          }
        ?>
        <div class="alert alert-danger hidden message" role="alert">
          <strong>No se pudo procesar tu requerimiento</strong>
        </div>
      </div>
      
      <div class="separator"></div>

      <h4 class="h4_left">M&Eacute;TODOS DE PAGO PUBLICIDAD </h4>
      <div class="row div_cards">
        <?php 
          if(!empty($creditcard))
          {
            foreach ($creditcard as $card) 
            {
            ?>
              <div class="col-md-8 div_banco top div_update_card" name="<?php echo $card->pkcreditcard; ?>" id="<?php echo $card->pkcreditcard; ?>">
                <label class="lbl_bnc"><?php echo $card->creditcard_type; ?><br/>
                <span class="ttl_blue">
                  <?php echo "##########".substr($card->creditcard_numbercard, -4); ?>
                </span></label>
              </div>
            <?php
            }
          }
        ?>
        <div id="new"></div>  
        <div class="col-md-12 top">
          <a class="btn btn-yellow btn-block btn_target">A&Ntilde;ADIR TARJETA</a>
        </div>
        <div class="alert alert-danger hidden messagec" role="alert">
          <strong>Tu tarjeta no se guardo</strong>
        </div>
      </div>

      <div class="separator top"></div>

      <h4 class="h4_left">PAGO DE CR&Eacute;DITO ACTUAL</h4>
      <div class="row div_pays">  
        <span class="col-md-12 ttl_blue">0 COP <br/></span><span class="col-md-12"> Pagar con:</span>
        <?php 
          if(!empty($creditcard))
          {
            foreach ($creditcard as $card) 
            {
            ?>
              <div class="col-md-8 div_banco top div_update_card" name="<?php echo $card->pkcreditcard; ?>" id="<?php echo $card->pkcreditcard; ?>">
                <label class="lbl_bnc"><?php echo $card->creditcard_type; ?><br/>
                <span class="ttl_blue">
                  <?php echo "##########".substr($card->creditcard_numbercard, -4); ?>
                </span></label>
              </div>
            <?php
            }
          }
        ?>
        <div id="new1"></div>
      </div><!--row end-->
    </div>

    <div class="menu-modal bank-modal summary-modal dashboard-modal edit_account hidden scrollbar"  id="opcion-2">

      <div class="separator"></div>

      <div class="row">
        <div class="col-md-7 col-sm-6 col-xs-6">
          <h4>EDITAR CUENTA PARA RECIBIR PAGOS</h4>
        </div>
        <div class="col-md-5">
          <a class="btn btn_green btn_delete_bank"> ELIMINAR </a>
        </div>
      </div>
      <p class="description_social" style="margin-top: 40px; margin-bottom: 50px;">
        VoyATodo enviar&aacute; los pagos recibidos en la compra de tus 
        productos vendidos durante la semana. Podr&aacute;s verla en tu 
        extracto como VOYATODO SAS. Recuerda que la
        transferencia tiene un costo de 15.000 COP 
      </p>
      <div class="row">
        <div class="form-group">
          <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">BANCO</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?php $listBank=ArrayHelper::map($listbanks,'pkbank','bank_name'); ?>
            <?= $form->field($model, 'fkbank')->dropDownList($listBank, 
            [
              'prompt'=>'',
              'class' => 'form-control bank-name'
            ])->label(false); ?>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-3 col-sm-3 col-xs-3">NOMBRE TITULAR</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?= $form->field($model, 'userbank_titularname')->input('text', ['class' => 'form-control bank-titular'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-3 col-sm-3 col-xs-3">C&Eacute;DULA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
             <?= $form->field($model, 'userbank_identification')->input('number', ['class' => 'form-control bank-identification'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-3 col-sm-3 col-xs-3">N° DE CUENTA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?= $form->field($model, 'userbank_numberacount')->input('number', ['class' => 'form-control bank-numberacount'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-3 col-sm-3 col-xs-3">TIPO DE CUENTA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?php $listAcount= ['Corriente' => 'Corriente', 'Crédito' => 'Crédito', 'Ahorro' => 'Ahorro']; ?>
            <?= $form->field($model, 'userbank_tipeacount')->dropDownList($listAcount, 
            [
            'prompt'=>'',
            'class' => 'form-control bank-type'
             ])->label(false); ?>
          </div>
        </div>
      </div><!--row end-->
      <div class="row">
        <a class="btn btn-yellow btn-block create_acount hidden">CREAR CUENTA</a>
        <a class="btn btn-yellow btn-block update_acount hidden">ACTUALIZAR CUENTA</a>
      </div><!--row end-->
    </div><!--modal end-->

    <div class="menu-modal bank-modal summary-modal dashboard-modal edit_target hidden scrollbar"  id="opcion-3">
        
      <div class="separator"></div>

      <h4>EDITAR TARJETA DE CR&Eacute;DITO PARA PAGOS</h4>
      <p class="description_social" style="margin-top: 40px; margin-bottom: 50px;">
        VoyATodo recibir&aacute; los pagos de la pauta publicitaria y <br />
        se cargar&aacute;n a tu tarjeta de cr&eacute;dito. Podr&aacute;s verla en tu <br />
        extracto como VOYATODO SAS, s&oacute;lo se cobrar&aacute; el valor <br />
        que hayas avalado en tu banner publicitario
      </p>
      <div class="row">
        <div class="form-group">
          <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">NOMBRE</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?= $form->field($model, 'creditcard_titularname')->input('text', ['class' => 'form-control creditcard-name'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">NÂ° TARJETA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?= $form->field($model, 'creditcard_numbercard')->input('number', ['class' => 'form-control creditcard-number'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">TIPO DE TARJETA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?php $listType= ['MASTERCARD' => 'MASTERCARD', 'VISA' => 'VISA', 'AMERICAN EXPRESS' => 'AMERICAN EXPRESS','DINNERS' => 'DINNERS']; ?>
            <?= $form->field($model, 'creditcard_type')->dropDownList($listType, 
             [
              'prompt'=>'',
              'class' => 'form-control creditcard-type'
            ])->label(false); ?>
          </div>
        </div> 
      </div><!--row end-->
      <div class="row">
        <a class="btn btn-yellow btn-block create_target hidden">CREAR TARJETA</a>
        <a class="btn btn-yellow btn-block update_target hidden">ACTUALIZAR TARJETA</a>
      </div>
    </div><!--modal end-->

    <!-- Comienza la sexta opción del menú -->

    <div class="menu-modal share-modal summary-modal dashboard-modal scrollbar"  id="opcion6">

      <div class="separator"></div>

      <h3>COMPARTIR POR REDES SOCIALES</h3>
      <p class="description_social">
        Ahora puedes invitar a tus amigos, seguidores y
        contactos a trav&eacute;s de las redes sociales
      </p>
      <span>
        <a href="" class="text-facebook ico red_margin share_facebook"> <i class="fa fa-facebook"></i> </a>
        <a href="https://twitter.com/share"class="text-twitter ico red_margin share_twitter"> <i class="fa fa-twitter"></i> </a>
        <a href="" class="text-linkedin ico red_margin share_linkedin"> <i class="fa fa-linkedin"></i> </a>
        <a href="" class="text-google ico red_margin share_google"> <i class="fa fa-google-plus"></i> </a>
      </span>

      <div class="separator"></div>

      <h3>COMPARTIR POR E-MAIL</h3>
      <p class="description_social">
        Copia y pega el siguiente enlace en tu correo para <br>
        que tus invitados registren el evento:
      </p>
      <div class="col-md-12">
        <?= $form->field($model, 'event_url')->textArea(['class' => 'text-area', 'readonly' => true,'id' => 'txt_url','value' => "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']])->label(false) ?>
      </div>
      <div class="col-md-12 top">
        <a class="btn btn-yellow btn-block btn-url" data-clipboard-target="#txt_url">COPIAR ENLACE</a>
      </div>
    </div>

    <!-- Comienza la séptima opción del menú -->

    <div class="menu-modal sponsor-modal summary-modal dashboard-modal sponsor_principal scrollbar"  id="opcion7">

      <div class="separator"></div>

      <h3>PATROCINADOR DEL EVENTO</h3>
      <label for="nombre_evento">
        ACTIVA PARA CONSEGUIR PATROCINADOR
      </label>
      <div class="row">
        <div class="input-group">
          <div class="col-md-5">
            <input type="radio" name="FormCreateEvent[event_sponsorship]" value="0" <?php echo $event->event_sponsorship = 0 ? "checked" : ""; ?> class="inputradio"> <span class="inputfalso"/> <label class="lbl_radio">PRIVADO</label></span> <br>
          </div>
          <div class="col-md-5">
            <input type="radio" name="FormCreateEvent[event_sponsorship]" value="1" <?php echo $event->event_sponsorship = 1 ? "checked" : ""; ?> class="inputradio"> <span class="inputfalso"/> <label class="lbl_radio">ACTIVAR</label> </span>
          </div>
        </div>
      </div>
      <p class="description_social">
        Una vez activo debes esperar que un patrocinador
        se ponga en contacto contigo.
      </p>

      <div class="separator"></div>

      <h3>PATROCINADORES INTERESADOS</h3>

      <div class="row">
        <?php 
          if(!empty($proposals))
          {
            foreach ($proposals as $proposal) 
            {
            ?>
            <div class="col-md-8 div_banco top div_sponsor_chat" name="<?php echo $proposal->pkproposal; ?>" id="<?php echo $proposal->pkproposal; ?>">
              <label class="lbl_bnc"><?php echo $proposal->getFkuser0()->one()->username ?>  
                <br/><span class="ttl_blue">Patrocinador</span>
              </label>
            </div>
            <?php
            }
          }
        ?>
      </div>
    </div>

    <div class="menu-modal sponsor-modal summary-modal dashboard-modal chat_sponsor hidden scrollbar"  id="opcion-4">

      <div class="separator"></div>

      <h3>PROPUESTA PATROCINIO</h3>
      <div class="form-group">
        <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">ESPECIE</label>
        <div class="col-md-9 col-sm-9 col-xs-9">
          <?= $form->field($model, 'proposal_contributions')->input('text', ['class' => 'form-control align_right ttl_blue val_contributions','readonly' => true])->label(false) ?>
        </div>
      </div>
      <div class="form-group">
        <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">DINERO</label>
        <div class="col-md-9 col-sm-9 col-xs-9">
          <?= $form->field($model, 'proposal_total1')->input('number',['class' => 'form-control align_right ttl_blue val_money', 'readonly' => true])->label(false) ?>
        </div>
      </div>
      <div class="col-md-12 top">
        <a class="btn btn-yellow btn-block btn_accept_proposal">ACEPTAR PROPUESTA</a>
        <input type="hidden"  id="pkevent" value="<?php echo $event->pkevent; ?>">
      </div>
      <p class="description_social", style="text-align: center">
        Una vez aceptas la propuesta se cargar&aacute; la informaci&oacute;n
        del anunciante a tu Evento
      </p>
      <div class="form-group">
        <label class="col-md-12 top"><?php echo $event->event_name; ?></label>
        <div class="col-md-12">
          <div class="area-message">
            <div class="message_new"></div>
          </div>
        </div>
      </div>
      <textarea class="form-control align_left text-input-message" autofocus=""></textarea>
      <div class="col-md-12 top">
        <a class="btn btn-yellow btn-block send_message">ENVIAR MENSAJE</a>
      </div>
      <p class="description_social" style="margin-top:-10px;">
        Todos los mensajes ser&aacute;n aprobados previamente por Voy A Todo, antes de ser reflejados.
      </p>
    </div>

    <!-- Comienza la octava opción del menú -->

    <div class="menu-modal asistent-modal summary-modal dashboard-modal mw100 scrollbar"  id="opcion8">

      <div class="separator"></div>

      <h3>LISTA ASISTENTES</h3>
      <div class="row">
        <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
          <p class="help-block top">BUSCAR POR NOMBRE, APELLIDO O E-MAIL</p>
          <div class="input-group search">
            <?= $form->field($model, "token_asistent")->input("text", ['class' => 'form-control text-address top_menos'])->label(false) ?>
          </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
          <a id="search_asistent" class="btn cargar_  hidden-sm hidden-xs top_50">BUSCAR</a>
          <a id="search_asistent" class="btn cargar_  btn-block hidden-md hidden-lg top_10">BUSCAR</a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pull-right">
          <a id="download_asistent" class="btn cargar_  hidden-sm hidden-xs top_50">DESCARGAR REPORTE</a>
          <a id="download_asistent" class="btn cargar_  btn-block hidden-md hidden-lg top_10">DESCARGAR REPORTE</a>
        </div>
      </div><!--row-->
      <div class="table-resposive">
        <table class="table table-condensed" id="example">
          <thead>
            <tr>
              <th>C&Oacute;DIGO</th>
              <?php
                $count = 0; 
                if(!empty($formdinamic))
                {
                  foreach ($formdinamic as $val) 
                  {
                    ?>
                    <th><?php echo $val->formeventgeneric; ?></th>
                    <?php
                    $count++;
                  }
                }
              ?>
              <th>TIPO ENTREGA</th>
              <th>PRECIO</th>
              <th>FECHA REGISTRO</th>
            </tr>
          </thead>
          <tbody class="tbody">
            <tr>
              <?php
                if(!empty($asist))
                {
                  $cont = count($asist);
                  var_dump($asist);
                  for ($i=0; $i < $cont; $i++) 
                  {
                    ?>
                    <td><?php echo $asist[$i]['pkbuy']; ?></td>
                    <?php
                    /*
                      foreach ($formdinamic as $val) 
                      {
                        if($asist[$i]["formeventgeneric"] == $val->formeventgeneric)
                        {
                          ?>
                          <td><?php echo $asist[$i]["formgenericresponse_response"]; ?></td>
                          <?php
                        }
                      }
                    */
                    ?>
                    <td>
                      <?php echo $event->getVtTtickets()->one()->getFktipetickect0()->one()->typeticket_name; ?>
                    </td>
                    <td><?php echo $asist[$i]['buy_value']; ?></td>
                    <td><?php echo $asist[$i]['buy_date']; ?></td>
                    </tr>
                    <?php
                  } 
                }
              ?>
          </tbody>
        </table>
      </div>
    </div>

    <!-- Comienza la novena opción del menú -->

    <div class="menu-modal access-modal summary-modal dashboard-modal scrollbar"  id="opcion9">

      <div class="separator"></div>

      <h3>VALIDAR ASISTENTE</h3>
      <div class="row">
        <div class="col-md-12">
          <p class="description_social">
            Para registrar el ingreso de tus invitados al evento debes
            solicitarles el c&oacute;digo que recibieron en el e-mail, en el cual
            se encuentra un código PIN, ingr&eacute;salo a continuaci&oacute;n:
          </p>
        </div>
      </div><!--/row-->
      <div class="row top">
        <div class="form-group">
          <div class="col-sm-8 col-xs-12">
            <input type="text" class="form-control txt_pin">
          </div>
          <div class="col-sm-4 col-xs-12 ">
            <a id="valite_PIN" class="btn cargar_ btn-block">VALIDAR</a>
          </div>
        </div>
      </div><!--/row-->
      <div class="row top txt_success hidden">
        <span class="col-sm-12 col-xs-12 txt_green">
          El c&oacute;digo ha sido validado con &eacute;xito.
        </span>
      </div>
      <div class="row top txt_error hidden">
        <span class="col-sm-12 col-xs-12 txt_fucsia">
          El c&oacute;digo ingresado no es v&aacute;lido.
        </span>
      </div>
    </div>

    <!-- Comienza la decima opción del menú -->

    <div class="menu-modal publicity-modal summary-modal dashboard-modal publicity_principal scrollbar"  id="opcion10">

      <div class="separator"></div>

      <h3>PUBLICIDAD</h3>
      <div class="row top">
        <div class="form-group">
          <div class="col-md-12">
            <label>DESTACA TU EVENTO A&Ntilde;ADIENDO PAUTA</label>
            <a class="btn btn-yellow btn-block btn_publicity">CREAR PAUTA</a>
            <p class="help-block">Tu evento en los destacados de Voyatodo.com</p>
          </div>
        </div>
      </div>

      <div class="separator"></div>

      <h3 class="top">CAMPA&Ntilde;AS CREADAS</h3>
      <div class="row new_publicity">
        <?php 
          if(!empty($publicities))
          {
            foreach ($publicities as $publicity) 
            {
            ?>
              <div class="col-md-8 div_banco top div_update_publicity" name="<?php echo $publicity->pkpublicity; ?>">
                <label class="lbl_bnc">
                  <?php echo $publicity->publicity_name ?><br/>
                  <span class="ttl_blue"><?php echo $publicity->publicity_value.' de '.number_format($publicity->getVtTbuys()->one()->buy_value).' COP'; ?>
                  </span>
                </label>
              </div>
            <?php
            }
          }
        ?>
      </div><!--/row-->
    </div>

    <div class="menu-modal publicity-modal summary-modal dashboard-modal create_publicity hidden">

      <div class="separator"></div>

      <h3>DETALLE DE PAUTA</h3>

      <div class="row top">
        <div class="form-group">
          <label class="col-sm-12 col-md-12">NOMBRE DE LA CAMPA&Ntilde;A</label>
          <div class="col-sm-12 col-md-12">
            <input type="text" class="form-control align_right name_advertiser" placeholder ='0/45'>
          </div>     
        </div>
      </div>

      <div class="separator"></div>

      <h3>CALCULADOR DE LA PAUTA</h3>

      <div class="row top">
        <div class="form-group">
          <label class="col-sm-6 col-xs-12 lblnegro lblxs">ESCOGER MEDIO DE PAGO</label>
           <div class="col-sm-6 col-xs-12">
            <?php
              $list = array();
              foreach ($creditcard as $card) 
              {
                $card->creditcard_type .= ' ' . substr($card->creditcard_numbercard, -4);
                $list[] = $card;
              }
              $lisTargets=ArrayHelper::map($list,'pkcreditcard','creditcard_type'); 
            ?>
              <?= $form->field($model, 'userbank_tipeacount')->dropDownList($lisTargets, 
              [
                'class' => 'form-control tipeacount_advertiser'
              ])->label(false); ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-6 col-xs-12 lblnegro lblxs">VALOR DIARIO DE LA PAUTA</label>
          <div class="col-sm-6 col-xs-12">
            <input type="text" class="form-control no-right-border ttl_blue advertising_comision">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-6 col-xs-12 lblnegro lblxs">VALOR A PAGAR TOTAL</label>
          <div class="col-sm-6 col-xs-12">
            <input type="number" class="form-control no-right-border ttl_blue advertising_total">
          </div>
        </div>
        <div class="col-md-12 top">
          <a class="btn btn-yellow btn-block btn_new_publicity hidden">COMPRAR PAUTA</a>
          <a class="btn btn-yellow btn-block btn_update_publicity hidden">ACTUALIZAR PAUTA</a>
        </div>
      </div><!--/row-->      
    </div>
  <?php $form->end() ?>

  <!-- Contenido de la página -->
  <section id="photo" class="dashboard-row">
    <img id="img-banner" class="responsive-image" style="opacity: <?php echo $event->event_opacityimage;?>;" src="<?php echo Yii::getAlias('@web').$event->event_image; ?>"/>
    <div class="shadow" ></div>
    <!-- Menu -->
    <div class="col-xs-2 col-md-2 col-sm-1 dashboard-column">
      <div class="dashboard-content col-sm-12">
        <div class="col-md-12 col-xs-0 col-md-offset-0 dashboard-element" id="dashboard-summary">
          <a class="img_click" name="summary-" style="cursor: pointer">
            <img class="img_summary-" src="<?php echo Yii::getAlias('@web') ?>/images/summary-gray.png">
            <p class="hidden-sm hidden-xs">DASHBOARD</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-information">
          <a class="img_click" name="information-" style="cursor: pointer">
            <img class="img_information-" src="<?php echo Yii::getAlias('@web') ?>/images/information-gray.png">
            <p class="hidden-sm hidden-xs">INFORMACI&Oacute;N</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-design">
          <a class="img_click" name="design-" style="cursor: pointer">
            <img class="img_design-" src="<?php echo Yii::getAlias('@web') ?>/images/design-gray.png">
            <p class="hidden-sm hidden-xs">DISE&Ntilde;O</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-tickets">
          <a class="img_click menu_tickets" name="tickets-" style="cursor: pointer">
            <img class="img_tickets-" src="<?php echo Yii::getAlias('@web') ?>/images/tickets-gray.png">
            <p class="hidden-sm hidden-xs">ENTRADAS</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-bank">
          <a class="img_click" name="bank-" style="cursor: pointer">
            <img class="img_bank-" src="<?php echo Yii::getAlias('@web') ?>/images/bank-gray.png">
            <p class="hidden-sm hidden-xs">BANCO</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 col-sm-offset-0  dashboard-element" id="dashboard-share">
          <a class="img_click" name="share-" style="cursor: pointer">
            <img class="img_share-" src="<?php echo Yii::getAlias('@web') ?>/images/share-gray.png">
            <p class="hidden-sm hidden-xs">COMPARTIR</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 col-sm-offset-1 dashboard-element" id="dashboard-sponsor">
          <a class="img_click" name="sponsor-" style="cursor: pointer">
            <img class="img_sponsor-" src="<?php echo Yii::getAlias('@web') ?>/images/sponsor-gray.png">
            <p class="hidden-sm hidden-xs">PATROCINADOR</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-asistent">
          <a class="img_click" name="asistent-" style="cursor: pointer">
            <img class="img_asistent-" src="<?php echo Yii::getAlias('@web') ?>/images/asistent-gray.png">
            <p class="hidden-sm hidden-xs">ASISTENTES</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-access">
          <a class="img_click" name="access-" style="cursor: pointer">
            <img class="img_access-" src="<?php echo Yii::getAlias('@web') ?>/images/access-gray.png">
            <p class="hidden-sm hidden-xs">ACCESO</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-publicity">
          <a class="img_click" name="publicity-" style="cursor: pointer">
            <img class="img_publicity-" src="<?php echo Yii::getAlias('@web') ?>/images/publicity-gray.png">
            <p class="hidden-sm hidden-xs">PUBLICIDAD</p>
          </a>
        </div>
      </div>
    </div>
    <!-- End Menu -->

    <div class="contenido-evento col-md-10">
      <h1 class="titulo" style="color: white"> <?php echo $event->event_name ; ?> </h1>
      <div class="detalles">
        <div class="fecha">
          <i class="fa fa-calendar"></i>
          <label class="text_date"> 
            <?php
              $days = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
              $months = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
              echo $days[date('w',strtotime($event->event_stardate))]." ".date('d',strtotime($event->event_stardate)).
                  " de ".$months[date(('n'),strtotime($event->event_stardate))]. " del ".
                    date('Y',strtotime($event->event_stardate)) ;
            ?>
          </label>
          <br>
          <i class="fa fa-calendar"></i>
          <label class="text_date_finish"> 
            <?php
              $days = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
              $months = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
              echo $days[date('w',strtotime($event->event_enddate))]." ".date('d',strtotime($event->event_enddate)).
                  " de ".$months[date(('n'),strtotime($event->event_enddate))]. " del ".
                    date('Y',strtotime($event->event_enddate)) ;
            ?>
          </label>
        </div>
        <div class="hora">
          <i class="fa fa-clock-o"></i>
          <label class="text_hour"> 
            <?php
              echo date('H:i',strtotime($event->event_starthour));
            ?>
          </label>
          <br>
          <i class="fa fa-clock-o"></i>
          <label class="text_hour_finish">
            <?php
              echo date('H:i',strtotime($event->event_endhour));
            ?>
          </label>
        </div>
      </div>
      <div class="ubicacion">
        <i class="fa fa-map-marker"></i>
        <label class="lbl_address"> 
          <?php echo $event->event_address.", " ; ?> 
        </label>
        <label class="lbl_city">
          <?php echo $event->getFkcity0()->one()->city_name.", "; ?>
        </label>
        <label class="lbl_country">
          <?php echo $event->getFkcity0()->one()->getFkdepartament0()->one()->getFkcountry0()->one()->country_name ; ?>
        </label>
        <br>
        <label class="lbl_place"> 
          <?php echo $event->event_place ; ?>  
        </label>
      </div>
    </div>
  </section>

  <div class="section destacados" style="background:#f2f2f2;">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="blog-d">
            <p>COMPARTIR POR REDES SOCIALES &nbsp; &nbsp;
              <span>
                <a href="" class="text-facebook ico share_facebook"> <i class="fa fa-facebook"></i> </a>
                <a href="https://twitter.com/share" class="text-twitter ico share_twitter"> <i class="fa fa-twitter"></i> </a>
                <a href="" class="text-linkedin ico share_linkedin"> <i class="fa fa-linkedin"></i> </a>
                <a href="" class="text-google share_google"> <i class="fa fa-google-plus"></i> </a>
              </span>
            </p>
            <h3 class="review txt_gray"> <?php echo $event->event_review ; ?> </h3>
            <?php
              if (strrpos($event->event_linkvideo, "www.youtube.com") !== false)
              {
                $links = explode("v=", $event->event_linkvideo);
                $token = $links[1];
                ?>
                <div class="video vy">
                  <iframe class="text-youtube"  id="ytplayer" type="text/html" width="200" height="100%"
                    src="https://www.youtube.com/embed/<?php echo $token; ?>" frameborder="0" allowfullscreen>
                  </iframe>
                </div>
                <div class="video vm hidden">
                  <iframe  class="text-vimeo" src="https://player.vimeo.com/video/25224442" width="200" height="100%" 
                    webkitallowfullscreen mozallowfullscreen allowfullscreen>
                  </iframe>
                </div>
                <?php
              }
              else
                if(strrpos($event->event_linkvideo, "vimeo.com") !== false)
                {
                  $links = explode("/", $event->event_linkvideo);
                  $token = $links[3];
                  ?>
                   <div class="video vm">
                      <iframe  class="text-vimeo" src="https://player.vimeo.com/video/<?php echo $token; ?>" width="200" height="100%" 
                        webkitallowfullscreen mozallowfullscreen allowfullscreen>
                      </iframe>
                    </div>
                    <div class="video vy hidden">
                      <iframe class="text-youtube"  id="ytplayer" type="text/html" width="200" height="100%"
                        src="https://www.youtube.com/embed/5NV6Rdv1a3I" frameborder="0" allowfullscreen>
                      </iframe>
                    </div>
                  <?php
                }
            ?>
            <div class="blog-text">
              <p class="text_description">
                <?php echo $event->event_description; ?>
              </p>
              <div class="row">
                <?php
                  if(!empty($galery))
                  {?>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                      <?php
                      $cont = count($galery);
                      ?>
                      <ol class="carousel-indicators divli">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <?php
                        for ($i=1; $i < $cont; $i++) 
                        { 
                          ?>
                          <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" ></li>
                          <?php
                        }
                        ?>
                      </ol>
                      <div class="carousel-inner divimg" role="listbox">
                        <?php
                        for ($j=0; $j < $cont; $j++) 
                        {
                          if($j == 0)
                          {
                            ?>
                            <div class="item active">
                              <img src="<?php echo Yii::getAlias('@web').$galery[$j]['image_path']; ?>" alt="galery_event" style="width: 100%">
                            </div>
                            <?php
                          }
                          else
                          {
                            ?>
                            <div class="item">
                              <img src="<?php echo Yii::getAlias('@web').$galery[$j]['image_path']; ?>" alt="galery_event" style="width: 100%">
                            </div>
                            <?php
                          }
                          ?>
                          <?php
                        }
                        ?>
                      </div>
                      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>                  
                    <?php
                  }
                  else
                  {
                    ?>
                    <div class="galeria hidden">
                      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators divli"></ol>
                        <div class="carousel-inner divimg" role="listbox"></div>
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>                  
                    </div>
                    <?php
                  }
                ?>
                <div class="clearfix"></div>
                <br />
                <div class="clearfix"></div>
                <div class="col-md-12"> 
                  <div class="map_canvas" style="height:350px; margin-top: 60px;"></div>
                </div>
                <?php 
                  if(!empty($event_proposals))
                  {
                  ?>
                    <div class="row top">
                      <div class="col-md-12"> 
                        <h5 style="margin-left: 20px;">PATROCINADO POR</h5>
                      </div>
                      <?php
                      foreach ($event_proposals as $prp) 
                      {
                      ?>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                          <a target="_blank" href="<?php echo $prp->getFkproposaldetails0()->one()->proposaldetails_url ?>">
                            <img  class="img-responsive-sponsor" src="<?php echo Yii::getAlias('@web').$prp->getFkproposaldetails0()->one()->proposaldetails_image ?>"/>                   
                          </a>
                        </div> 
                      <?php
                      }?>
                    </div>
                  <?php
                  }
                ?>
                <div class="clearfix"></div>
                <br />
                <div class="col-md-12">
                  <h5>T&Eacute;RMINOS Y CONDICIONES</h5>
                  <p class="text_terms">
                    <?php echo $event->event_terms; ?>
                  </p>
                  <br />
                </div>
              </div><!--row-->
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="blog-d-right">
            <ul class="blog-d-right00">
              <div id="space_ticket_free"></div>
              <div id="space_ticket_pay"></div>
              <div id="space_ticket_crowfundig"></div>
              <?php
                $tickets = $event->getVtTtickets()->all();
                if(empty($tickets))
                  echo '<h1 align="center" class="ttl_blue">EVENTO DE ENTRADA LIBRE</h1>';
                else
                {
                  $contribuit = 0;
                  $porcentain = 0;
                  if(!empty($event->event_crowfunding))
                  {
                    foreach ($tickets as $tick) 
                    {
                      $query = $buy->find()->where("fkticket =:pkticket", [":pkticket" => $tick->pkticket])->all();
                      $sum=0;
                      foreach ($query as $buy) 
                        $sum += $buy->getFkticket0()->one()->ticket_comision;
                      $porcentain += ($sum/$event->event_crowfunding)*100;
                      $contribuit += $sum;
                    }
                  }
                  foreach ($tickets as $tick) 
                  {
                    $val_random = rand();
                    $start = date('Y-m-d',strtotime($tick->ticket_start));
                    $end = date('Y-m-d',strtotime($tick->ticket_finish));
                    $finish = date('Y-m-d',strtotime($tick->ticket_enddate));
                    $currentDate = date('Y-m-d');
                    if ($start <= $currentDate && $end >= $currentDate)
                    {
                      $qty = $tick->ticket_qty;
                      $items = "";
                      if($tick->fktipetickect == 1)
                      { 
                        if(is_null($qty))
                        {
                          $items .= "<option>0</option>";
                          for($i=0; $i<5; $i++)
                            $items .= "<option>".($i+1)."</option>";
                        }
                        else
                          if($qty == 0)
                            $items .= "<option>AGOTADA</option>";
                          else
                            if($qty != 0 && $qty != 1 && $qty != 2 && $qty != 3 && $qty != 4)
                            {
                              $items .= "<option>0</option>";
                              for($i=0; $i<5; $i++)
                                $items .= "<option>".($i+1)."</option>";
                            }
                            else
                            {
                              $items .= "<option>0</option>";
                              for($i=0; $i<$qty; $i++)
                                $items .= "<option>".($i+1)."</option>";
                            }
                        ?>
                        <li>
                          <div class="pull-left"> 
                            <h3>GRATIS</h3>
                          </div>
                          <div class="pull-right">
                            <select class="buy_free buy_free<?php echo $val_random ?>" name="<?php echo $val_random; ?>" data-ticket="<?php echo $tick->pkticket; ?>"> <?php echo $items; ?> </select>
                          </div>
                          <div class="clearfix"></div>
                          <h4>  <?php echo $tick->ticket_name; ?> </h4>
                          <p> <?php echo $tick->ticket_description; ?> </p>
                          <div class="clearfix"></div>
                          <div class="row clear-list">
                            <div class="col-md-6">
                              <span> 
                                <?php
                                  if($tick->ticket_seeclaim == 1) 
                                    echo "0 de ".$qty;
                                  else
                                     echo "0 ";
                                ?> 
                              </span>Compradas
                            </div>
                            <div class="col-md-6 pull-left0">
                              <span> <?php echo $tick->ticket_finish; ?> </span>
                              Fecha l&iacute;mite
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </li>
                        <?php
                      }
                      else
                        if($tick->fktipetickect == 2)
                        {
                          if(is_null($qty))
                          {
                            $items .= "<option>0</option>";
                            for($i=0; $i<5; $i++)
                              $items .= "<option>".($i+1)."</option>";
                          }
                          else
                            if($qty == 0)
                              $items .= "<option>AGOTADA</option>";
                            else
                              if($qty != 0 && $qty != 1 && $qty != 2 && $qty != 3 && $qty != 4)
                              {
                                $items .= "<option>0</option>";
                                for($i=0; $i<5; $i++)
                                  $items .= "<option>".($i+1)."</option>";
                              }
                              else
                              {
                                $items .= "<option>0</option>";
                                for($i=0; $i<$qty; $i++)
                                  $items .= "<option>".($i+1)."</option>";
                              }
                          ?>
                          <li>
                            <div class="pull-left">
                              <h3> <?php echo number_format( $tick->ticket_value); ?> COP
                                <span>Despu&eacute;s de <?php echo number_format($tick->ticket_value); ?> COP</span>
                              </h3>
                            </div>
                            <div class="pull-right">
                              <select class="buy_pay buy_pay<?php echo $val_random ?>" name="<?php echo $val_random; ?>" data-ticket="<?php echo $tick->pkticket; ?>"> <?php echo $items; ?> </select>
                            </div>
                            <div class="clearfix"></div>
                            <h4> <?php echo $tick->ticket_name; ?> </h4>
                            <p> <?php echo $tick->ticket_description; ?> </p>
                            <div class="clearfix"></div>
                            <div class="row clear-list">
                              <div class="col-md-6">
                                <span> 
                                  <?php
                                  if($tick->ticket_seeclaim == 1) 
                                    echo "0 de ".$qty;
                                  else
                                     echo "0 ";
                                  ?>
                                </span>Compradas
                              </div>
                              <div class="col-md-6 pull-left0">
                                <span> <?php echo $tick->ticket_finish; ?> </span>
                                Fecha l&iacute;mite
                              </div>
                              <div class="clearfix"></div>
                            </div>
                          </li>
                          <?php
                        }
                        else
                          if($tick->fktipetickect == 3 && $finish > $currentDate)
                          {
                            if(is_null($qty))
                            {
                              $items .= "<option>0</option>";
                              for($i=0; $i<5; $i++)
                                $items .= "<option>".($i+1)."</option>";
                            }
                            else
                              if($qty == 0)
                                $items .= "<option>AGOTADA</option>";
                              else
                                if($qty != 0 && $qty != 1 && $qty != 2 && $qty != 3 && $qty != 4)
                                {
                                  $items .= "<option>0</option>";
                                  for($i=0; $i<5; $i++)
                                    $items .= "<option>".($i+1)."</option>";
                                }
                                else
                                {
                                  $items .= "<option>0</option>";
                                  for($i=0; $i<$qty; $i++)
                                    $items .= "<option>".($i+1)."</option>";
                                }
                            ?>
                            <li>
                              <div class="pull-left">
                                <h3> 
                                  <span class="cb">META FINANCIERA</span>
                                  <span class="ttl_blue" style="font-size: 20px !important;">
                                    <?php echo number_format($event->event_crowfunding)." "; ?>COP
                                  </span>
                                  <span class="cb top">VALOR DE ENTRADA</span>
                                  <?php echo number_format( $tick->ticket_value); ?> COP
                                </h3>
                              </div>
                              <div class="pull-right">
                                <select class="buy_crowfunding buy_crowfunding<?php echo $val_random ?>" name="<?php echo $val_random; ?>" data-ticket="<?php echo $tick->pkticket; ?>"> <?php echo $items; ?> </select>
                              </div>
                              <div class="clearfix"></div>
                              <h4> <?php echo $tick->ticket_name; ?> </h4>
                              <p> <?php echo $tick->ticket_description; ?> </p>
                              <h4>INCENTIVO</h4>
                              <p> <?php echo $tick->ticket_insentive; ?> </p>
                              <div class="team-member modern">
                                <div class="progress">
                                  <div class="progress-bar progress-bar-primary" data-progress-animation="<?php echo $porcentain; ?>%" data-appear-animation-delay="400">
                                    <span class="percentage"><?php echo round($porcentain,2); ?>%</span>
                                  </div>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="row clear-list">
                                <div class="col-md-4">
                                  <span><?php echo round($porcentain,2); ?>%</span>Financiado
                                </div>
                                <div class="col-md-4">
                                  <span><?php echo number_format($contribuit); ?></span>Contribuido
                                </div>
                                <div class="col-md-4">
                                  <span>
                                    <?php
                                      $date1=$tick->ticket_enddate;
                                      $segundos=strtotime($date1) - strtotime(date('Y-m-d'));
                                      $days=intval($segundos/60/60/24);
                                      $days=floor($days);
                                      echo $days;
                                    ?>
                                  </span>Días más
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </li>
                            <?php
                          }
                    }
                  }
                }
              ?>
            </ul>
          </div>
          <div class="opor">
            <h5 class="txt_gray">ORGANIZADO POR</h5>
            <div class="blog-d-right">
              <div class="blog-d-right-w">
                <h5>
                  <?php echo $event->getFkuser0()->one()->username. ' '.$event->getFkuser0()->one()->last_name; ?>
                </h5>
                <div style="background-size:100% 100%;background-image:url('<?php echo Yii::getAlias('@web').$event->getFkuser0()->one()->user_image; ?>');background-repeat: no-repeat;" class="blog-d-right-w-img img_p"></div>
                <h6>Email</h6>
                <a target="_blank" href="mailto:<?php echo $event->getFkuser0()->one()->email;?>">
                  <?php echo $event->getFkuser0()->one()->email;?>
                </a> 
                <br />
                <br />
                <div class="blog-d-right-w-img">
                  <a target="_blank" href="<?php echo $event->getFkuser0()->one()->user_facebook;?>" class="text-facebook"> 
                    <i class="fa fa-facebook"></i> 
                  </a>
                  <a target="_blank" href="<?php echo $event->getFkuser0()->one()->user_twitter;?>" class="text-twitter"> 
                    <i class="fa fa-twitter"></i> 
                  </a>
                  <a target="_blank" href="<?php echo $event->getFkuser0()->one()->user_youtube;?>" class="text-you"> 
                    <i class="fa fa-youtube-play"></i> 
                  </a>
                  <a target="_blank" href="<?php echo $event->getFkuser0()->one()->user_google;?>" class="text-google"> 
                    <i class="fa fa-google-plus"></i> 
                  </a>
                 </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <!-- End Team Members -->
    </div>
    <!-- .container -->
  </div>
</div>

<?php
  if($message == 1)
  { ?>
    <script>
    swal({
      title: "Oh! no",
      text: "Tu evento no se pudo publicar. Revisa la información que ingresaste.",
      type: "error"
    });
    </script>
  <?php
  }
  else
    if($message == 2)
    {
    ?>
    <script>
      swal("Muy bien", "Tu evento ha sido publicado", "success")
    </script>
    <?php
    }
    else
      if($message == 3)
      { ?>
        <script>
        swal({
          title: "Oh! no",
          text: "Tu evento no se pudo guardar. Revisa la información que ingresaste.",
          type: "error"
        });
        </script>
      <?php
      }
      else
        if($message == 4)
        {
        ?>
        <script>
          swal("Muy bien", "Tu evento ha sido guardado", "success")
        </script>
        <?php
        }
?>

<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/bootstrap-clockpicker.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/script_maps.js"></script>

<script>
  $(document).ready(function(){
    new Clipboard('.btn-url');

    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

    $(".public").click(function(){
      $("#form-create").submit();
    });

    $('.save').click(function(){
      var form = $("#form-create").clone();
      $("#save-form").submit();
    });
  });

  $('.clockpicker').clockpicker({
    afterDone: function() {
      $(".text_hour").html($(".event-starthour").val());
      $(".text_hour_finish").html($(".event-endhour").val());
    }
  });
</script>