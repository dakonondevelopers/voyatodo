<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Voy A Todo | Evento';

?>

<div id="container-fluid">
  <section id="inner01">
    <div id="main-slide" class="carousel00 slide slide_galery" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">       
      </div>
    </div>
  </section>

  <!-- Contenido de la página -->
  <section id="photo" class="dashboard-row" style="margin-top:-115px;">
    <img class="responsive-image" style="opacity: <?php echo $event->event_opacityimage; ?>" src="<?php echo Yii::getAlias('@web').$event->event_image; ?>"/>
    <div class="shadow" ></div>
    <div class="contenido-evento col-md-10">
      <h1 style="color: white"> <?php echo $event->event_name ; ?> </h1>
      <input type="hidden"  id="pkevent" value="<?php echo $event->pkevent; ?>">
      <div class="detalles">
        <div class="fecha">
          <i class="fa fa-calendar"></i>
          <label> 
            <?php
              $days = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
              $months = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
              echo $days[date('w',strtotime($event->event_stardate))]." ".date('d',strtotime($event->event_stardate)).
                  " de ".$months[date(('n'),strtotime($event->event_stardate))]. " del ".
                    date('Y',strtotime($event->event_stardate)) ;
            ?>
          </label>
          <br>
          <i class="fa fa-calendar"></i>
          <label> 
            <?php
              $days = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
              $months = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
              echo $days[date('w',strtotime($event->event_enddate))]." ".date('d',strtotime($event->event_enddate)).
                  " de ".$months[date(('n'),strtotime($event->event_enddate))]. " del ".
                    date('Y',strtotime($event->event_enddate)) ;
            ?>
          </label>
        </div>
        <div class="hora">
          <i class="fa fa-clock-o"></i>
          <label> 
            <?php
              echo date('H:i',strtotime($event->event_starthour));
            ?>
          </label>
          <br>
          <i class="fa fa-clock-o"></i>
          <label>
            <?php
              echo date('H:i',strtotime($event->event_endhour));
            ?>
          </label>
        </div>
      </div>
      <div class="ubicacion">
        <i class="fa fa-map-marker"></i>
        <label> 
          <?php echo $event->event_address.", " ; ?> 
        </label>
        <label>
          <?php echo $event->getFkcity0()->one()->city_name.", "; ?>
        </label>
        <label>
          <?php echo $event->getFkcity0()->one()->getFkdepartament0()->one()->getFkcountry0()->one()->country_name ; ?>
        </label>
        <br>
        <label> 
           <?php echo $event->event_place ; ?>  
        </label>
      </div>
    </div>
  </section>

  <div id="container-fluid">
  <section id="inner01">
    <div id="main-slide" class="carousel00 slide slide_galery" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">       
        <div class="slider-content calendar">
          <div class="col-md-4 col-lg-2 col-sm-6 col-xs-12 col-md-offset-0  top">
            <?php 
              if($event->fkstatus == 1)
              {
                ?>
                 <a class="btn btn_sm btn_show_event" name="<?php echo $event->pkevent; ?>"> <i class="fa fa-unlock" aria-hidden="true"></i> DESBLOQUEAR EVENTO </a>
                <?php
              }
              else
              {
                ?>
                <a class="btn btn_sm btn_hidden_event" name="<?php echo $event->pkevent; ?>"> <i class="fa fa-lock" aria-hidden="true"></i> BLOQUEAR EVENTO </a>
                <?php
              }
            ?>
          </div>
        </div>   
      </div>
    </div>
  </section>

  <div class="section destacados" style="background:#f2f2f2;">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="blog-d">
            <p>COMPARTIR POR REDES SOCIALES &nbsp; &nbsp;
              <span>
                <a href="" class="text-facebook ico share_facebook"> <i class="fa fa-facebook"></i> </a>
                <a href="https://twitter.com/share" class="text-twitter ico share_twitter"> <i class="fa fa-twitter"></i> </a>
                <a href="" class="text-linkedin ico share_linkedin"> <i class="fa fa-linkedin"></i> </a>
                <a href="" class="text-google share_google"> <i class="fa fa-google-plus"></i> </a>
              </span>
            </p>
            <h3 class="review txt_gray"> <?php echo $event->event_review ; ?> </h3>
            <?php
              if (strrpos($event->event_linkvideo, "www.youtube.com") !== false)
              {
                $links = explode("v=", $event->event_linkvideo);
                $token = $links[1];
                ?>
                <div class="video">
                  <iframe class="text-youtube"  id="ytplayer" type="text/html" width="200" height="100%"
                    src="https://www.youtube.com/embed/<?php echo $token; ?>" frameborder="0" allowfullscreen>
                  </iframe>
                </div>
                <?php
              }
              else
                if(strrpos($event->event_linkvideo, "vimeo.com") !== false)
                {
                  $links = explode("/", $event->event_linkvideo);
                  $token = $links[3];
                  ?>
                   <div class="video">
                      <iframe  class="text-vimeo" src="https://player.vimeo.com/video/<?php echo $token; ?>" width="200" height="100%" 
                        webkitallowfullscreen mozallowfullscreen allowfullscreen>
                      </iframe>
                    </div>
                  <?php
                }
            ?>
            <div class="blog-text">
              <p>
                <?php echo $event->event_description; ?>
              </p>
              <div class="row">
                <?php
                  if(!empty($galery))
                  {?>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                      <?php
                      $cont = count($galery);
                      ?>
                      <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <?php
                        for ($i=1; $i < $cont; $i++) 
                        { 
                          ?>
                          <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" ></li>
                          <?php
                        }
                        ?>
                      </ol>
                      <div class="carousel-inner" role="listbox">
                        <?php
                        for ($j=0; $j < $cont; $j++) 
                        {
                          if($j == 0)
                          {
                            ?>
                            <div class="item active">
                              <img src="<?php echo Yii::getAlias('@web').$galery[$j]['image_path']; ?>" alt="galery_event" style="width: 100%">
                            </div>
                            <?php
                          }
                          else
                          {
                            ?>
                            <div class="item">
                              <img src="<?php echo Yii::getAlias('@web').$galery[$j]['image_path']; ?>" alt="galery_event" style="width: 100%">
                            </div>
                            <?php
                          }
                          ?>
                          <?php
                        }
                        ?>
                      </div>
                      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>                  
                    <?php
                  }
                ?>
                <div class="clearfix"></div>
                <br />
                <div class="clearfix"></div>
                <div class="col-md-12"> 
                  <div class="map_canvas" style="height:350px; margin-top: 60px;"></div>
                </div>
                <?php 
                  if(!empty($event_proposals))
                  {
                  ?>
                    <div class="row top">
                      <div class="col-md-12"> 
                        <h5 class="txt_gray" style="margin-left: 20px !important;">PATROCINADO POR</h5>
                      </div>
                      <?php
                      foreach ($event_proposals as $prp) 
                      {
                      ?>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                          <a target="_blank" href="<?php echo $prp->getFkproposaldetails0()->one()->proposaldetails_url ?>">
                            <img  class="img-responsive-sponsor" src="<?php echo Yii::getAlias('@web').$prp->getFkproposaldetails0()->one()->proposaldetails_image ?>"/>                   
                          </a>
                        </div> 
                      <?php
                      }?>
                    </div>
                  <?php
                  }
                ?>
                <div class="clearfix"></div>
                <br />
                <div class="col-md-12">
                  <h5 class="txt_gray">T&Eacute;RMINOS Y CONDICIONES</h5>
                  <p> <?php echo $event->event_terms; ?> </p>
                  <br />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="blog-d-right">
            <ul class="blog-d-right00">
              <?php
                $tickets = $event->getVtTtickets()->all();
                if(empty($tickets))
                  echo '<h1 align="center" class="ttl_blue">EVENTO DE ENTRADA LIBRE</h1>';
                else
                {
                  $contribuit = 0;
                  $porcentain = 0;
                  if(!empty($event->event_crowfunding))
                  {
                    foreach ($tickets as $tick) 
                    {
                      $query = $buy->find()->where("fkticket =:pkticket", [":pkticket" => $tick->pkticket])->all();
                      $sum=0;
                      foreach ($query as $buy) 
                        $sum += $buy->getFkticket0()->one()->ticket_comision;
                      $porcentain += ($sum/$event->event_crowfunding)*100;
                      $contribuit += $sum;
                    }
                  }
                  foreach ($tickets as $tick) 
                  {
                    $val_random = rand();
                    $start = date('Y-m-d',strtotime($tick->ticket_start));
                    $end = date('Y-m-d',strtotime($tick->ticket_finish));
                    $finish = date('Y-m-d',strtotime($tick->ticket_enddate));
                    $currentDate = date('Y-m-d');
                    if ($start <= $currentDate && $end >= $currentDate)
                    {
                      $qty = $tick->ticket_qty;
                      $items = "";
                      if($tick->fktipetickect == 1)
                      { 
                        if(is_null($qty))
                        {
                          $items .= "<option>0</option>";
                          for($i=0; $i<5; $i++)
                            $items .= "<option>".($i+1)."</option>";
                        }
                        else
                          if($qty == 0)
                            $items .= "<option>AGOTADA</option>";
                          else
                            if($qty != 0 && $qty != 1 && $qty != 2 && $qty != 3 && $qty != 4)
                            {
                              $items .= "<option>0</option>";
                              for($i=0; $i<5; $i++)
                                $items .= "<option>".($i+1)."</option>";
                            }
                            else
                            {
                              $items .= "<option>0</option>";
                              for($i=0; $i<$qty; $i++)
                                $items .= "<option>".($i+1)."</option>";
                            }
                        ?>
                        <li>
                          <div class="pull-left"> 
                            <h3>GRATIS</h3>
                          </div>
                          <div class="pull-right">
                            <select class="buy_free buy_free<?php echo $val_random ?>" name="<?php echo $val_random; ?>" data-ticket="<?php echo $tick->pkticket; ?>"> <?php echo $items; ?> </select>
                          </div>
                          <div class="clearfix"></div>
                          <h4>  <?php echo $tick->ticket_name; ?> </h4>
                          <p> <?php echo $tick->ticket_description; ?> </p>
                          <div class="clearfix"></div>
                          <div class="row clear-list">
                            <div class="col-md-6">
                              <span> 
                                <?php
                                  if($tick->ticket_seeclaim == 1) 
                                    echo "0 de ".$qty;
                                  else
                                     echo "0 ";
                                ?> 
                              </span>Compradas
                            </div>
                            <div class="col-md-6 pull-left0">
                              <span> <?php echo $tick->ticket_finish; ?> </span>
                              Fecha l&iacute;mite
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </li>
                        <?php
                      }
                      else
                        if($tick->fktipetickect == 2)
                        {
                          if(is_null($qty))
                          {
                            $items .= "<option>0</option>";
                            for($i=0; $i<5; $i++)
                              $items .= "<option>".($i+1)."</option>";
                          }
                          else
                            if($qty == 0)
                              $items .= "<option>AGOTADA</option>";
                            else
                              if($qty != 0 && $qty != 1 && $qty != 2 && $qty != 3 && $qty != 4)
                              {
                                $items .= "<option>0</option>";
                                for($i=0; $i<5; $i++)
                                  $items .= "<option>".($i+1)."</option>";
                              }
                              else
                              {
                                $items .= "<option>0</option>";
                                for($i=0; $i<$qty; $i++)
                                  $items .= "<option>".($i+1)."</option>";
                              }
                          ?>
                          <li>
                            <div class="pull-left">
                              <h3> <?php echo number_format( $tick->ticket_value); ?> COP
                                <span>Despu&eacute;s de <?php echo number_format($tick->ticket_value); ?> COP</span>
                              </h3>
                            </div>
                            <div class="pull-right">
                              <select class="buy_pay buy_pay<?php echo $val_random ?>" name="<?php echo $val_random; ?>" data-ticket="<?php echo $tick->pkticket; ?>"> <?php echo $items; ?> </select>
                            </div>
                            <div class="clearfix"></div>
                            <h4> <?php echo $tick->ticket_name; ?> </h4>
                            <p> <?php echo $tick->ticket_description; ?> </p>
                            <div class="clearfix"></div>
                            <div class="row clear-list">
                              <div class="col-md-6">
                                <span> 
                                  <?php
                                  if($tick->ticket_seeclaim == 1) 
                                    echo "0 de ".$qty;
                                  else
                                     echo "0 ";
                                  ?>
                                </span>Compradas
                              </div>
                              <div class="col-md-6 pull-left0">
                                <span> <?php echo $tick->ticket_finish; ?> </span>
                                Fecha l&iacute;mite
                              </div>
                              <div class="clearfix"></div>
                            </div>
                          </li>
                          <?php
                        }
                        else
                          if($tick->fktipetickect == 3 && $finish > $currentDate)
                          {
                            if(is_null($qty))
                            {
                              $items .= "<option>0</option>";
                              for($i=0; $i<5; $i++)
                                $items .= "<option>".($i+1)."</option>";
                            }
                            else
                              if($qty == 0)
                                $items .= "<option>AGOTADA</option>";
                              else
                                if($qty != 0 && $qty != 1 && $qty != 2 && $qty != 3 && $qty != 4)
                                {
                                  $items .= "<option>0</option>";
                                  for($i=0; $i<5; $i++)
                                    $items .= "<option>".($i+1)."</option>";
                                }
                                else
                                {
                                  $items .= "<option>0</option>";
                                  for($i=0; $i<$qty; $i++)
                                    $items .= "<option>".($i+1)."</option>";
                                }
                            ?>
                            <li>
                              <div class="pull-left">
                                <h3> 
                                  <span class="cb">META FINANCIERA</span>
                                  <span class="ttl_blue" style="font-size: 20px !important;">
                                    <?php echo number_format($event->event_crowfunding)." "; ?>COP
                                  </span>
                                  <span class="cb top">VALOR DE ENTRADA</span>
                                  <?php echo number_format( $tick->ticket_value); ?> COP
                                </h3>
                              </div>
                              <div class="pull-right">
                                <select class="buy_crowfunding buy_crowfunding<?php echo $val_random ?>" name="<?php echo $val_random; ?>" data-ticket="<?php echo $tick->pkticket; ?>"> <?php echo $items; ?> </select>
                              </div>
                              <div class="clearfix"></div>
                              <h4> <?php echo $tick->ticket_name; ?> </h4>
                              <p> <?php echo $tick->ticket_description; ?> </p>
                              <h4>INCENTIVO</h4>
                              <p> <?php echo $tick->ticket_insentive; ?> </p>
                              <div class="team-member modern">
                                <div class="progress">
                                  <div class="progress-bar progress-bar-primary" data-progress-animation="<?php echo $porcentain; ?>%" data-appear-animation-delay="400">
                                    <span class="percentage"><?php echo round($porcentain,2); ?>%</span>
                                  </div>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="row clear-list">
                                <div class="col-md-4">
                                  <span><?php echo round($porcentain,2); ?>%</span>Financiado
                                </div>
                                <div class="col-md-4">
                                  <span><?php echo number_format($contribuit); ?></span>Contribuido
                                </div>
                                <div class="col-md-4">
                                  <span>
                                    <?php
                                      $date1=$tick->ticket_enddate;
                                      $segundos=strtotime($date1) - strtotime(date('Y-m-d'));
                                      $days=intval($segundos/60/60/24);
                                      $days=floor($days);
                                      echo $days;
                                    ?>
                                  </span>D&iacute;as m&aacute;s
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </li>
                            <?php
                          }
                    }
                  }
                }
              ?>
            </ul>
          </div>
          <div class="opor">
            <h5>ORGANIZADO POR</h5>
            <div class="blog-d-right">
              <div class="blog-d-right-w">
                <h5>
                  <?php echo $event->getFkuser0()->one()->username. ' '.$event->getFkuser0()->one()->last_name; ?>
                </h5>
                <div style="background-size:100% 100%;background-image:url('<?php echo Yii::getAlias('@web').$event->getFkuser0()->one()->user_image; ?>');background-repeat: no-repeat;" class="blog-d-right-w-img img_p"></div>
                <h6>Email</h6>
                <a target="_blank" href="mailto:<?php echo $event->getFkuser0()->one()->email;?>">
                  <?php echo $event->getFkuser0()->one()->email;?>
                </a> 
                <br />
                <br />
                <div class="blog-d-right-w-img">
                  <a target="_blank" href="<?php echo $event->getFkuser0()->one()->user_facebook;?>" class="text-facebook"> 
                    <i class="fa fa-facebook"></i> 
                  </a>
                  <a target="_blank" href="<?php echo $event->getFkuser0()->one()->user_twitter;?>" class="text-twitter"> 
                    <i class="fa fa-twitter"></i> 
                  </a>
                  <a target="_blank" href="<?php echo $event->getFkuser0()->one()->user_youtube;?>" class="text-you"> 
                    <i class="fa fa-youtube-play"></i> 
                  </a>
                  <a target="_blank" href="<?php echo $event->getFkuser0()->one()->user_google;?>" class="text-google"> 
                    <i class="fa fa-google-plus"></i> 
                  </a>
                 </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <!-- End Team Members -->
    </div>
    <!-- .container -->
  </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js" type="text/javascript"></script>

<script>

  !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

  function load_map() {
    var myLatlng = new google.maps.LatLng(<?php echo $event->event_lat; ?>, <?php echo $event->event_log; ?>);
    var myOptions = {
        zoom: 16,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    map = new google.maps.Map($(".map_canvas").get(0), myOptions);
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: ''
    });
  }
  $('.carousel').carousel();
  load_map();
</script>