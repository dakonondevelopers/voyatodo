<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Voy A Todo | Evento';

Yii::$app->view->registerJsFile(Yii::getAlias('@web')."/js/jquery.validate.js",['position' => yii\web\View::POS_HEAD]);
  $this->head();   

?>

<div id="container">
  <section id="inner01">
    <div id="main-slide" class="carousel00 slide slide_galery" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
        <div class="slider-content">
          <div class="col-md-4 col-md-offset-2 col-xs-12 pull-left">
            <label style="font-size: 15px;">A&ntilde;ade al calendario:</label>
            <a id="btn-cal" class="icons_calendar"><i class="fa fa-windows"></i></a>
            <a id="btn-cal" class="icons_calendar"><i class="fa fa-google"></i></a>
            <a id="btn-cal" class="icons_calendar"><i class="fa fa-apple"></i></a>
            <a id="btn-cal" class="icons_calendar"><i class="fa fa-yahoo"></i></a>
          </div>
          <div class="col-md-2 col-xs-12 col-md-offset-3 top">
            <a class="btn btn-block btn_buy" data-toggle="modal" id="btn_valtickets" data-target="#modal-form-tickets">COMPRAR AHORA</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Comienza la primera opción del menú -->

  <div class="menu-modal summary-modal dashboard-modal scrollbar" id="opcion1">
    <div class="separator">
    </div>
    <div class="row">
      <div class="col-md-7 col-sm-6 col-xs-6">
        <h4>RESUMEN DEL EVENTO</h4>
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-6 summary-images">
        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/pie.png">
          <p>0</p>
          <p>VISITAS</p>
        </div>

        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/buyers.png">
          <p>12</p>
          <p>COMPRAS</p>
        </div>
      </div>
      <div class="col-md-6 summary-images">
        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/basket.png">
          <p>200.000</p>
          <p>DINERO RECIBIDO</p>
        </div>

        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/assistants.png">
          <p>200</p>
          <p>ASISTENTES</p>
        </div>
      </div>
    </div>
    <div class="separator"></div>
    <h4 class="align_left">RESUMEN DE VISITAS Y REGISTROS</h4>
    <table class="table">
      <tr>
        <td><p>PERIODO</p></td>
        <td><p>VISITANTES</p></td>
        <td><p>CLICS</p></td>
      </tr>
      <tr>
        <td>Hoy</td>
        <td>10</td>
        <td>9</td>
      </tr>
      <tr>
        <td>Ayer</td>
        <td>6</td>
        <td>4</td>
      </tr>
      <tr>
        <td>Esta semana</td>
        <td>20</td>
        <td>10</td>
      </tr>
      <tr>
        <td>Semana pasada</td>
        <td>19</td>
        <td>7</td>
      </tr>
      <tr>
        <td>Mes pasado</td>
        <td>1</td>
        <td>6</td>
      </tr>
      <tr class="bluish">
        <td class="ttl_blue">TOTAL</td>
        <td class="ttl_blue">56</td>
        <td class="ttl_blue">36</td>
      </tr>
    </table>
    <div class="separator"></div>
    <h4 class="align_left">RESUMEN DE CAMPA&Ntilde;A</h4>
    <table class="table">
      <tr>
        <td><p>BANNER</p></td>
        <td><p>IMPRESIONES</p></td>
        <td><p>CLICS</p></td>
      </tr>
      <tr>
        <td>PREFERENCIAL</td>
        <td>300</td>
        <td>36</td>
      </tr>
      <tr>
        <td>VALOR IMPRESI&Oacute;N</td>
        <td>140 COP</td>
      </tr>
      <tr class="bluish">
        <td class="ttl_blue">TOTAL A PAGAR</td>
        <td class="ttl_blue">42000 COP</td>
      </tr>
    </table>
  </div>

  <!-- Comienza la segunda opción del menú -->


  <?php $form = ActiveForm::begin([
      'method' => 'post',
      'id' => 'form-sponsor',
      'options' => ['enctype' => 'multipart/form-data'],
    ]);
  ?>

    <div class="menu-modal bank-modal summary-modal dashboard-modal bank_acount scrollbar" id="opcion2">

      <div class="separator"></div>

      <h4 class="h4_left">M&Eacute;TODOS DE PAGO </h4>

      <div class="row div_cards">
        <?php 
          if(!empty($creditcard))
          {
            foreach ($creditcard as $card) 
            {
            ?>
              <div class="col-md-8 div_banco top div_update_target" name="<?php echo $card->pkcreditcard; ?>" id="<?php echo $card->pkcreditcard; ?>">
                <label class="lbl_bnc"><?php echo $card->creditcard_type; ?><br/>
                <span class="ttl_blue">
                  <?php echo "##########".substr($card->creditcard_numbercard, -4); ?>
                </span></label>
              </div>
            <?php
            }
          }
        ?>
        <div id="new"></div>
      </div>

      <div class="separator"></div>

        <h4>EDITAR TARJETA DE CR&Eacute;DITO PARA PAGOS</h4>
        <p class="description_social" style="margin-top: 40px; margin-bottom: 50px;">
          VoyATodo recibir&aacute; los pagos de la pauta publicitaria y <br />
          se cargar&aacute;n a tu tarjeta de cr&eacute;dito. Podr&aacute;s verla en tu <br />
          extracto como VOYATODO SAS, s&oacute;lo se cobrar&aacute; el valor <br />
          que hayas avalado en tu banner publicitario
        </p>

        <div class="row">
          <div class="form-group">
            <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">NOMBRE</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
               <?= $form->field($model, 'creditcard_titularname')->input('text', ['class' => 'form-control intittularname'])->label(false) ?>
            </div>
          </div>
          <div class="form-group">
            <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">N° TARJETA</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
               <?= $form->field($model, 'creditcard_numbercard')->input('text', ['class' => 'form-control innumbercard'])->label(false) ?>
            </div>
          </div>
          <div class="form-group">
            <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">TIPO DE TARJETA</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <?php $listType= ['MASTERCARD' => 'MASTERCARD', 'VISA' => 'VISA', 'AMERICAN EXPRESS' => 'AMERICAN EXPRESS','DINNERS' => 'DINNERS']; ?>
              <?= $form->field($model, 'creditcard_type')->dropDownList($listType, 
               [
                'prompt'=>'',
                'class' => 'form-control intype'
              ])->label(false); ?>
            </div>
          </div> 
        </div><!--row end-->
        <div class="row">
          <a class="btn btn-yellow btn-block set_target_sponsor">A&Ntilde;ADIR TARJETA</a>
        </div>
        <div class="alert alert-danger hidden message" role="alert">
          <strong>Tu tarjeta no se guardo</strong>
        </div>

      <div class="separator"></div>

      <h4 class="h4_left">PAGO DE CRÉDITO ACTUAL</h4>

      <div class="row div_pays">  
        <?php 
          if(!empty($creditcard))
          {
            foreach ($creditcard as $card) 
            {
            ?>
              <div class="col-md-8 div_banco top div_update_target" name="<?php echo $card->pkcreditcard; ?>" id="<?php echo $card->pkcreditcard; ?>">
                <label class="lbl_bnc"><?php echo $card->creditcard_type; ?><br/>
                <span class="ttl_blue">
                  <?php echo "##########".substr($card->creditcard_numbercard, -4); ?>
                </span></label>
              </div>
            <?php
            }
          }
        ?>
        <div id="new1"></div>
         
      </div><!--row end-->
    </div>

    <div class="menu-modal bank-modal summary-modal dashboard-modal edit_target hidden scrollbar"  id="opcion-1">
        
        <div class="separator"></div>

        <h4>EDITAR TARJETA DE CR&Eacute;DITO PARA PAGOS</h4>
        <p class="description_social" style="margin-top: 40px; margin-bottom: 50px;">
          VoyATodo recibir&aacute; los pagos de la pauta publicitaria y <br />
          se cargar&aacute;n a tu tarjeta de cr&eacute;dito. Podr&aacute;s verla en tu <br />
          extracto como VOYATODO SAS, s&oacute;lo se cobrar&aacute; el valor <br />
          que hayas avalado en tu banner publicitario
        </p>

        <div class="row">
          <div class="form-group">
            <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">NOMBRE</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
               <?= $form->field($model, 'creditcard_titularname')->input('text', ['class' => 'form-control titularname'])->label(false) ?>
            </div>
          </div>
          <div class="form-group">
            <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">N° TARJETA</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
               <?= $form->field($model, 'creditcard_numbercard')->input('number', ['class' => 'form-control target'])->label(false) ?>
            </div>
          </div>
          <div class="form-group">
            <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">TIPO DE TARJETA</label>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <?php $listType= ['MASTERCARD' => 'MASTERCARD', 'VISA' => 'VISA', 'AMERICAN EXPRESS' => 'AMERICAN EXPRESS']; ?>
              <?= $form->field($model, 'creditcard_type')->dropDownList($listType, 
               [
                'prompt'=>'',
                'class' => 'form-control type'
              ])->label(false); ?>
            </div>
          </div> 
        </div><!--row end-->
        <div class="row">
          <a class="btn btn-yellow btn-block update_target">ACTUALIZAR TARJETA</a>
        </div>
        <div class="row" id="response">
          <label></label>
        </div>
    </div><!--modal end-->

    <!-- Comienza la tercera opción del menú -->

    <div class="menu-modal share-modal summary-modal dashboard-modal scrollbar" id="opcion3">

      <div class="separator"></div>

      <h3>COMPARTIR POR REDES SOCIALES</h3>
      <p class="description_social">
        Ahora puedes invitar a tus amigos, seguidores y <br>
        contactos a trav&eacute;s de las redes sociales
      </p>
      <span>
        <a href="" class="text-facebook ico red_margin share_facebook"> <i class="fa fa-facebook"></i> </a>
        <a href="https://twitter.com/share"class="text-twitter ico red_margin share_twitter"> <i class="fa fa-twitter"></i> </a>
        <a href="" class="text-linkedin ico red_margin share_linkedin"> <i class="fa fa-linkedin"></i> </a>
        <a href="" class="text-google ico red_margin share_google"> <i class="fa fa-google-plus"></i> </a>
      </span>

      <div class="separator"></div>

      <h3>COMPARTIR POR E-MAIL</h3>
      <p class="description_social">
        Copia y pega el siguiente enlace en tu correo para <br>
        que tus invitados registren el evento:
      </p>
      <div class="col-md-12">
        <?= $form->field($model, 'event_url')->textArea(['class' => 'text-area', 'readonly' => true,'id' => 'txt_url','value' => "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']])->label(false) ?>
      </div>
      <div class="col-md-12 top">
        <a class="btn btn-yellow btn-block btn-url" data-clipboard-target="#txt_url">COPIAR ENLACE</a>
      </div>
    </div>

    <!-- Comienza la cuarta opción del menú -->

    <div class="menu-modal sponsor-modal summary-modal dashboard-modal scrollbar" id="opcion4">

      <div class="separator"></div>

      <h3>PROPUESTA PATROCINIO</h3>
      <div class="row">
        <input type="hidden"  name="FormSponsor[pkcreditcard]" class="pkcreditcard" value="">
        <?php 
          if(empty($proposals))
          {
          ?>
            <div class="form-group top">
              <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">ESPECIE</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
                <?= $form->field($model, 'proposal_contributions')->input('text', ['class' => 'form-control align_right ttl_blue val_contributions','placeholder' => ' - '])->label(false) ?>
              </div>
            </div>
            <div class="form-group">
              <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">DINERO</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
                <?= $form->field($model, 'proposal_neto1')->input('number',['class' => 'form-control align_right ttl_blue val_money', 'placeholder' => '180000'])->label(false) ?>
              </div>
            </div>
            <div class="form-group">
              <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">COMISI&Oacute;N</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
                <?= $form->field($model, 'creditcard_numbercard')->input('number',['class' => 'form-control align_right ttl_blue val_comision', 'readonly' => true])->label(false) ?>
                <p class="help-block top__10">Esta es la comisión que cobra Voy A Todo por el manejo de los eventos y el proveedor de pagos.</p>
              </div>
            </div>
            <div class="form-group">
              <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">TOTAL ENVIADO</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
                <?= $form->field($model, 'proposal_total1')->input('number', ['class' => 'form-control align_right ttl_blue val_total', 'placeholder' => '157000','readonly' => true])->label(false) ?>
              </div>
            </div>
          <?php
          }
          else
          {
          ?>
            <div class="form-group">
              <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">ESPECIE</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
                <?= $form->field($model, 'proposal_contributions')->input('text', ['class' => 'form-control align_right ttl_blue val_contributions','value' => $proposals->proposal_contributions])->label(false) ?>
              </div>
            </div>
            <div class="form-group">
              <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">DINERO</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
                <?= $form->field($model, 'proposal_neto1')->input('number',['class' => 'form-control align_right ttl_blue val_money', 'value' => $proposals->proposal_neto1])->label(false) ?>
              </div>
            </div>
            <div class="form-group">
              <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">COMISI&Oacute;N</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
                <?= $form->field($model, 'creditcard_numbercard')->input('number',['class' => 'form-control align_right ttl_blue val_comision', 'readonly' => true])->label(false) ?>
              </div>
            </div>
            <div class="form-group">
              <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">TOTAL ENVIADO</label>
              <div class="col-md-9 col-sm-9 col-xs-9">
                <?= $form->field($model, 'proposal_total1')->input('number', ['class' => 'form-control align_right ttl_blue val_total', 'value' => $proposals->proposal_total1,'readonly' => true])->label(false) ?>
              </div>
            </div>
            <?php
              if($proposals->fkstatus == '5')
              {
              ?>
                <p class="description_social">Tu propuesta fue aceptada por el creador del evento</p>
                <div class="row top">
                  <div class="form-group">
                    <label class="col-md-12">CARGAR IM&Aacute;GEN</label>
                    <div class="col-md-12">
                      <div class="cargar top_menos btn-block"><label class="lbl_btn">SUBIR IM&Aacute;GEN</label>
                        <?= $form->field($model, "sponsor_banner")->fileInput(['class' => 'eventos archivos', 'accept' => 'image/*'])->label(false) ?>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3 top">URL:</label>
                    <div class="col-md-9 col-sm-9 col-xs-9 top">
                       <?= $form->field($model, 'sponsor_url')->input('text', ['class' => 'form-control intittularname'])->label(false) ?>
                    </div>
                  </div>
                </div>
                <p class="description_social">Esta URL sirve como redirecci&oacute;n</p>
                <div class="col-md-12 top">
                  <a class="btn btn-blue btn-block save_proposal">GUARDAR</a>
                </div>
              <?php
              } 
          }
        ?>
      </div><!--row end-->
      <div class="row">
        <a class="btn btn-yellow btn-block send_proposal">ENVIAR PROPUESTA</a>
        <p class="description_social">
          Una vez el cliente acepta la propuesta se cargar&aacute; tu <br>
          banner y empezará a funcionar tu pauta.
          <input type="hidden"  id="pkevent" value="<?php echo $event->pkevent; ?>">
        </p>
      </div>
      <div class="form-group">
        <label class="col-md-12 top"><?php echo $event->event_name; ?></label>
        <?php
          if(empty($messages))
          {
          ?>
            <div class="col-md-12">
              <div class="area-message">
                <div class="message_new"></div>
              </div>
            </div>
          <?php
          }
          else
          {
          ?>
            <div class="col-md-12">
              <div class="area-message">
                <?php
                  foreach ($messages as $msg) 
                  {
                    if($msg->message_senduser == 2)
                    {
                    ?>
                      <div class="col-md-8 div_message1">
                        <?php echo $msg->message_message; ?>
                      </div>
                    <?php
                    }
                    else
                    {
                    ?>
                      <div class="col-md-8 div_message2">
                        <?php echo $msg->message_message; ?>
                      </div>
                    <?php
                    }
                  }
                ?>
                <div class="message_new"></div>
              </div>
            </div>
          <?php  
          } 
        ?>
      </div>
      <textarea class="form-control align_left text-input-message" autofocus=""></textarea>
      <div class="col-md-12 top">
        <a class="btn btn-yellow btn-block send_message">ENVIAR MENSAJE</a>
      </div>
      <p class="description_social" style="margin-top:-10px;">
        Todos los mensajes ser&aacute;n aprobados previamente por Voy A Todo, antes de ser reflejados.
      </p>
    </div>

  <?php $form->end() ?>

  <!-- Contenido de la página -->
  <section id="photo" class="dashboard-row">
    <img class="responsive-image" style="opacity: <?php echo $event->event_opacityimage; ?>" src="<?php echo Yii::getAlias('@web').$event->event_image; ?>"/>
    <div class="shadow" ></div>

    <!-- Menu -->
    <div class="col-xs-2 col-md-2 col-sm-1 dashboard-column">
      <div class="dashboard-content col-sm-12">
        <div class="col-md-12 col-xs-0 col-md-offset-0 dashboard-element" id="dashboard-summary">
          <a class="img_click" name="summary-" style="cursor: pointer">
            <img class="img_summary-" src="<?php echo Yii::getAlias('@web') ?>/images/summary-gray.png">
            <p class="hidden-sm hidden-xs">DASHBOARD</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-bank">
          <a class="img_click" name="bank-" style="cursor: pointer">
            <img class="img_bank-" src="<?php echo Yii::getAlias('@web') ?>/images/bank-gray.png">
            <p class="hidden-sm hidden-xs">BANCO</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 col-sm-offset-0  dashboard-element" id="dashboard-share">
          <a class="img_click" name="share-" style="cursor: pointer">
            <img class="img_share-" src="<?php echo Yii::getAlias('@web') ?>/images/share-gray.png">
            <p class="hidden-sm hidden-xs">COMPARTIR</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 col-sm-offset-1 dashboard-element" id="dashboard-sponsor">
          <a class="img_click" id="click_sponsor" name="sponsor-" style="cursor: pointer">
            <img class="img_sponsor-" src="<?php echo Yii::getAlias('@web') ?>/images/sponsor-gray.png">
            <p class="hidden-sm hidden-xs">PATROCINADOR</p>
          </a>
        </div>
      </div>
    </div>
    <!-- End Menu -->

    <div class="contenido-evento col-md-10">
      <h1 style="color: white"> <?php echo $event->event_name ; ?> </h1>
      <div class="detalles">
        <div class="fecha">
          <i class="fa fa-calendar"></i>
          <label> 
            <?php
              $days = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
              $months = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
              echo $days[date('w',strtotime($event->event_stardate))]." ".date('d',strtotime($event->event_stardate)).
                  " de ".$months[date(('n'),strtotime($event->event_stardate))]. " del ".
                    date('Y',strtotime($event->event_stardate)) ;
            ?>
          </label>
          <br>
          <i class="fa fa-calendar"></i>
          <label> 
            <?php
              $days = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
              $months = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
              echo $days[date('w',strtotime($event->event_enddate))]." ".date('d',strtotime($event->event_enddate)).
                  " de ".$months[date(('n'),strtotime($event->event_enddate))]. " del ".
                    date('Y',strtotime($event->event_enddate)) ;
            ?>
          </label>
        </div>
        <div class="hora">
          <i class="fa fa-clock-o"></i>
          <label> 
            <?php
              echo date('H:i',strtotime($event->event_starthour));
            ?>
          </label>
          <br>
          <i class="fa fa-clock-o"></i>
          <label>
            <?php
              echo date('H:i',strtotime($event->event_endhour));
            ?>
          </label>
        </div>
      </div>
      <div class="ubicacion">
        <i class="fa fa-map-marker"></i>
        <label> 
          <?php echo $event->event_address.", " ; ?> 
        </label>
        <label>
          <?php echo $event->getFkcity0()->one()->city_name.", "; ?>
        </label>
        <label>
          <?php echo $event->getFkcity0()->one()->getFkdepartament0()->one()->getFkcountry0()->one()->country_name ; ?>
        </label>
        <br>
        <label> 
          <?php echo $event->event_place ; ?>  
        </label>
      </div>
    </div>
  </section>

  <div class="section destacados" style="background:#f2f2f2;">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="blog-d">
            <p>COMPARTIR POR REDES SOCIALES &nbsp; &nbsp;
              <span>
                <a href="" class="text-facebook ico share_facebook"> <i class="fa fa-facebook"></i> </a>
                <a href="https://twitter.com/share" class="text-twitter ico share_twitter"> <i class="fa fa-twitter"></i> </a>
                <a href="" class="text-linkedin ico share_linkedin"> <i class="fa fa-linkedin"></i> </a>
                <a href="" class="text-google share_google"> <i class="fa fa-google-plus"></i> </a>
              </span>
            </p>
            <h3 class="review"> <?php echo $event->event_review ; ?> </h3>
            <?php
              if (strrpos($event->event_linkvideo, "www.youtube.com") !== false)
              {
                $links = explode("v=", $event->event_linkvideo);
                $token = $links[1];
                ?>
                <div class="video">
                  <iframe class="text-youtube"  id="ytplayer" type="text/html" width="200" height="100%"
                    src="https://www.youtube.com/embed/<?php echo $token; ?>" frameborder="0" allowfullscreen>
                  </iframe>
                </div>
                <?php
              }
              else
                if(strrpos($event->event_linkvideo, "vimeo.com") !== false)
                {
                  $links = explode("/", $event->event_linkvideo);
                  $token = $links[3];
                  ?>
                   <div class="video">
                      <iframe  class="text-vimeo" src="https://player.vimeo.com/video/<?php echo $token; ?>" width="200" height="100%" 
                        webkitallowfullscreen mozallowfullscreen allowfullscreen>
                      </iframe>
                    </div>
                  <?php
                }
            ?>
            <div class="blog-text">
              <p>
                <?php echo $event->event_description; ?>
              </p>
              <div class="row">
                <?php
                  if(!empty($galery))
                  {?>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                      <?php
                      $cont = count($galery);
                      ?>
                      <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <?php
                        for ($i=1; $i < $cont; $i++) 
                        { 
                          ?>
                          <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" ></li>
                          <?php
                        }
                        ?>
                      </ol>
                      <div class="carousel-inner" role="listbox">
                        <?php
                        for ($j=0; $j < $cont; $j++) 
                        {
                          if($j == 0)
                          {
                            ?>
                            <div class="item active">
                              <img src="<?php echo Yii::getAlias('@web').$galery[$j]['image_path']; ?>" alt="galery_event" style="width: 100%">
                            </div>
                            <?php
                          }
                          else
                          {
                            ?>
                            <div class="item">
                              <img src="<?php echo Yii::getAlias('@web').$galery[$j]['image_path']; ?>" alt="galery_event" style="width: 100%">
                            </div>
                            <?php
                          }
                          ?>
                          <?php
                        }
                        ?>
                      </div>
                      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>                  
                    <?php
                  }
                ?>
                <div class="clearfix"></div>
                <br />
                <div class="clearfix"></div>
                <div class="col-md-12"> 
                  <div class="map_canvas" style="height:350px; margin-top: 60px;"></div>
                </div>
                <div class="clearfix"></div>
                <br />
                <?php 
                  if(!empty($event_proposals))
                  {
                  ?>
                    <div class="row top">
                      <div class="col-md-12"> 
                        <h5 style="margin-left: 20px;">PATROCINADO POR</h5>
                      </div>
                      <?php
                      foreach ($event_proposals as $prp) 
                      {
                      ?>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                          <a target="_blank" href="<?php echo $prp->getFkproposaldetails0()->one()->proposaldetails_url ?>">
                            <img  class="img-responsive-sponsor" src="<?php echo Yii::getAlias('@web').$prp->getFkproposaldetails0()->one()->proposaldetails_image ?>"/>                   
                          </a>
                        </div> 
                      <?php
                      }?>
                    </div>
                  <?php
                  }
                ?>
                <div class="col-md-12 top">
                  <h5>T&Eacute;RMINOS Y CONDICIONES</h5>
                  <p> <?php echo $event->event_terms; ?> </p>
                  <br />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="blog-d-right">
            <ul class="blog-d-right00">
              <?php
                $tickets = $event->getVtTtickets()->all();
                if(empty($tickets))
                  echo '<h1 align="center" class="ttl_blue">EVENTO DE ENTRADA LIBRE</h1>';
                else
                {
                  $contribuit = 0;
                  $porcentain = 0;
                  if(!empty($event->event_crowfunding))
                  {
                    foreach ($tickets as $tick) 
                    {
                      $query = $buy->find()->where("fkticket =:pkticket", [":pkticket" => $tick->pkticket])->all();
                      $sum=0;
                      foreach ($query as $buy) 
                        $sum += $buy->getFkticket0()->one()->ticket_comision;
                      $porcentain += ($sum/$event->event_crowfunding)*100;
                      $contribuit += $sum;
                    }
                  }
                  foreach ($tickets as $tick) 
                  {
                    $val_random = rand();
                    $start = date('Y-m-d',strtotime($tick->ticket_start));
                    $end = date('Y-m-d',strtotime($tick->ticket_finish));
                    $finish = date('Y-m-d',strtotime($tick->ticket_enddate));
                    $currentDate = date('Y-m-d');
                    if ($start <= $currentDate && $end >= $currentDate)
                    {
                      $qty = $tick->ticket_qty;
                      $items = "";
                      if($tick->fktipetickect == 1)
                      { 
                        if(is_null($qty))
                        {
                          $items .= "<option>0</option>";
                          for($i=0; $i<5; $i++)
                            $items .= "<option>".($i+1)."</option>";
                        }
                        else
                          if($qty == 0)
                            $items .= "<option>AGOTADA</option>";
                          else
                            if($qty != 0 && $qty != 1 && $qty != 2 && $qty != 3 && $qty != 4)
                            {
                              $items .= "<option>0</option>";
                              for($i=0; $i<5; $i++)
                                $items .= "<option>".($i+1)."</option>";
                            }
                            else
                            {
                              $items .= "<option>0</option>";
                              for($i=0; $i<$qty; $i++)
                                $items .= "<option>".($i+1)."</option>";
                            }
                        ?>
                        <li>
                          <div class="pull-left"> 
                            <h3>GRATIS</h3>
                          </div>
                          <div class="pull-right">
                            <select class="buy_free buy_free<?php echo $val_random ?>" name="<?php echo $val_random; ?>" data-ticket="<?php echo $tick->pkticket; ?>"> <?php echo $items; ?> </select>
                          </div>
                          <div class="clearfix"></div>
                          <h4>  <?php echo $tick->ticket_name; ?> </h4>
                          <p> <?php echo $tick->ticket_description; ?> </p>
                          <div class="clearfix"></div>
                          <div class="row clear-list">
                            <div class="col-md-6">
                              <span> 
                                <?php
                                  if($tick->ticket_seeclaim == 1) 
                                    echo "0 de ".$qty;
                                  else
                                     echo "0 ";
                                ?> 
                              </span>Compradas
                            </div>
                            <div class="col-md-6 pull-left0">
                              <span> <?php echo $tick->ticket_finish; ?> </span>
                              Fecha l&iacute;mite
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </li>
                        <?php
                      }
                      else
                        if($tick->fktipetickect == 2)
                        {
                          if(is_null($qty))
                          {
                            $items .= "<option>0</option>";
                            for($i=0; $i<5; $i++)
                              $items .= "<option>".($i+1)."</option>";
                          }
                          else
                            if($qty == 0)
                              $items .= "<option>AGOTADA</option>";
                            else
                              if($qty != 0 && $qty != 1 && $qty != 2 && $qty != 3 && $qty != 4)
                              {
                                $items .= "<option>0</option>";
                                for($i=0; $i<5; $i++)
                                  $items .= "<option>".($i+1)."</option>";
                              }
                              else
                              {
                                $items .= "<option>0</option>";
                                for($i=0; $i<$qty; $i++)
                                  $items .= "<option>".($i+1)."</option>";
                              }
                          ?>
                          <li>
                            <div class="pull-left">
                              <h3> <?php echo number_format( $tick->ticket_value); ?> COP
                                <span>Despu&eacute;s de <?php echo number_format($tick->ticket_value); ?> COP</span>
                              </h3>
                            </div>
                            <div class="pull-right">
                              <select class="buy_pay buy_pay<?php echo $val_random ?>" name="<?php echo $val_random; ?>" data-ticket="<?php echo $tick->pkticket; ?>"> <?php echo $items; ?> </select>
                            </div>
                            <div class="clearfix"></div>
                            <h4> <?php echo $tick->ticket_name; ?> </h4>
                            <p> <?php echo $tick->ticket_description; ?> </p>
                            <div class="clearfix"></div>
                            <div class="row clear-list">
                              <div class="col-md-6">
                                <span> 
                                  <?php
                                  if($tick->ticket_seeclaim == 1) 
                                    echo "0 de ".$qty;
                                  else
                                     echo "0 ";
                                  ?>
                                </span>Compradas
                              </div>
                              <div class="col-md-6 pull-left0">
                                <span> <?php echo $tick->ticket_finish; ?> </span>
                                Fecha l&iacute;mite
                              </div>
                              <div class="clearfix"></div>
                            </div>
                          </li>
                          <?php
                        }
                        else
                          if($tick->fktipetickect == 3 && $finish > $currentDate)
                          {
                            if(is_null($qty))
                            {
                              $items .= "<option>0</option>";
                              for($i=0; $i<5; $i++)
                                $items .= "<option>".($i+1)."</option>";
                            }
                            else
                              if($qty == 0)
                                $items .= "<option>AGOTADA</option>";
                              else
                                if($qty != 0 && $qty != 1 && $qty != 2 && $qty != 3 && $qty != 4)
                                {
                                  $items .= "<option>0</option>";
                                  for($i=0; $i<5; $i++)
                                    $items .= "<option>".($i+1)."</option>";
                                }
                                else
                                {
                                  $items .= "<option>0</option>";
                                  for($i=0; $i<$qty; $i++)
                                    $items .= "<option>".($i+1)."</option>";
                                }
                            ?>
                            <li>
                              <div class="pull-left">
                                <h3> 
                                  <span class="cb">META FINANCIERA</span>
                                  <span class="ttl_blue" style="font-size: 20px !important;">
                                    <?php echo number_format($event->event_crowfunding)." "; ?>COP
                                  </span>
                                  <span class="cb top">VALOR DE ENTRADA</span>
                                  <?php echo number_format( $tick->ticket_value); ?> COP
                                </h3>
                              </div>
                              <div class="pull-right">
                                <select class="buy_crowfunding buy_crowfunding<?php echo $val_random ?>" name="<?php echo $val_random; ?>" data-ticket="<?php echo $tick->pkticket; ?>"> <?php echo $items; ?> </select>
                              </div>
                              <div class="clearfix"></div>
                              <h4> <?php echo $tick->ticket_name; ?> </h4>
                              <p> <?php echo $tick->ticket_description; ?> </p>
                              <h4>INCENTIVO</h4>
                              <p> <?php echo $tick->ticket_insentive; ?> </p>
                              <div class="team-member modern">
                                <div class="progress">
                                  <div class="progress-bar progress-bar-primary" data-progress-animation="<?php echo $porcentain; ?>%" data-appear-animation-delay="400">
                                    <span class="percentage"><?php echo round($porcentain,2); ?>%</span>
                                  </div>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="row clear-list">
                                <div class="col-md-4">
                                  <span><?php echo round($porcentain,2); ?>%</span>Financiado
                                </div>
                                <div class="col-md-4">
                                  <span><?php echo number_format($contribuit); ?></span>Contribuido
                                </div>
                                <div class="col-md-4">
                                  <span>
                                    <?php
                                      $date1=$tick->ticket_enddate;
                                      $segundos=strtotime($date1) - strtotime(date('Y-m-d'));
                                      $days=intval($segundos/60/60/24);
                                      $days=floor($days);
                                      echo $days;
                                    ?>
                                  </span>Días más
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </li>
                            <?php
                          }
                    }
                  }
                }
              ?>
            </ul>
          </div>
          <div class="opor">
            <h5>ORGANIZADO POR</h5>
            <div class="blog-d-right">
              <div class="blog-d-right-w">
                <h5>
                  <?php echo $event->getFkuser0()->one()->username. ' '.$event->getFkuser0()->one()->last_name; ?>
                </h5>
                <div style="background-size:100% 100%;background-image:url('<?php echo Yii::getAlias('@web').$event->getFkuser0()->one()->user_image; ?>');background-repeat: no-repeat;" class="blog-d-right-w-img img_p"></div>
                <h6>Email</h6>
                <a target="_blank" href="mailto:<?php echo $event->getFkuser0()->one()->email;?>">
                  <?php echo $event->getFkuser0()->one()->email;?>
                </a> 
                <br />
                <br />
                <div class="blog-d-right-w-img">
                  <a target="_blank" href="<?php echo $event->getFkuser0()->one()->user_facebook;?>" class="text-facebook"> 
                    <i class="fa fa-facebook"></i> 
                  </a>
                  <a target="_blank" href="<?php echo $event->getFkuser0()->one()->user_twitter;?>" class="text-twitter"> 
                    <i class="fa fa-twitter"></i> 
                  </a>
                  <a target="_blank" href="<?php echo $event->getFkuser0()->one()->user_youtube;?>" class="text-you"> 
                    <i class="fa fa-youtube-play"></i> 
                  </a>
                  <a target="_blank" href="<?php echo $event->getFkuser0()->one()->user_google;?>" class="text-google"> 
                    <i class="fa fa-google-plus"></i> 
                  </a>
                 </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <!-- End Team Members -->
    </div>
    <!-- .container -->
  </div>

  <!-- Modal tickets -->
  <div class="logi-box box_modal modal fade" id="modal-form-tickets" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <h3>ENTRADAS</h3>
        <div class="blog-d-right">
          <ul class="blog-d-right00">
            <div class="list_tickets"></div>
          </ul>
        </div>
        <div class="form-group">
          <h5 align="center"><b>Valor total a pagar</h5>
          <h1 align="center" class="ttl_blue total_pay"></h1>
        </div>
        <div class="row btn_buy_hidden hidden">
          <div class="col-md-8 col-md-offset-2">
            <a class="btn btn-block btn_buy" id="btn_comprar" data-toggle="modal" data-target="#modal-form-dinamic">SIGUIENTE</a>                
          </div>
        </div>
      </div>           
    </div>
  </div>

  <!-- Modal formulario dinámico -->
  <div class="logi-box box_modal modal fade" id="modal-form-dinamic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <h3>REGISTRARME AL EVENTO</h3>
        <div class="content_form"></div>
      </div>           
    </div>
  </div>

</div>

<script src="https://maps.googleapis.com/maps/api/js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/script_buy.js"></script>

<script>
  $(document).on("click",".save_proposal",function(){
    $("#form-sponsor").submit();
  });
  $(document).ready(function() { 
    
    new Clipboard('.btn-url');

    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

    function load_map() {
      var myLatlng = new google.maps.LatLng(<?php echo $event->event_lat; ?>, <?php echo $event->event_log; ?>);
      var myOptions = {
          zoom: 16,
          center: myLatlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
      };
      map = new google.maps.Map($(".map_canvas").get(0), myOptions);
      var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: ''
      });
    }
    load_map();
  });
</script>