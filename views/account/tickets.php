<?php

$this->title = 'Voy A Todo | Mis Tickets';

?>
  
  <section id="inner01">
    <div id="main-slide" class="carousel00 slide slide_galery" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
        <div class="slider-content">
          <div class="col-md-12 text-center">
            <h4 class="animated3" style="font-size: 22px !important;color: #FFF !important;"> Esta es tu lista de Tickets! </h4>
            <h3 class="animated3"> PRUEBALA TAMBI&Eacute;N EN TU SMARTPHONE </h3>
            <p class="animated4" style="margin-top: 10px;"><a href="<?php echo Yii::getAlias('@web') ?>/account/" class="eventos public" >BUSCAR EVENTO</a></p>
          </div>
        </div>
      </div>
    </div>
  </section>  
  <div class="section destacados">
    <div class="container container-update"> 
      <div class="row">
        <?php
            if(empty($mytickets))
            { ?>
            <div class="big-title text-center">
              <h1>A&Uacute;N NO HAY <span>entradas</span></h1>
              <p class="text-center">Busca el evento al que quieres asistir y aparecer&aacute; aqu&iacute;.</p>
            </div>
            <img class="img-face-sad" src="<?php echo Yii::getAlias('@web')?>/images/face-sad.png" alt="face-sad">
          <?php  
            }
            else
            { 
          ?>
            <div class="big-title text-center">
              <h1>mis <span>tickets</span></h1>
              <p class="text-center">¿Est&iacute;s listo para la diversi&oacute;n?</p>
            </div>
            <?php
              foreach ($mytickets as $ticket) 
              {
              ?>
                <div class="col-lg-3 col-md-4 col-sm-6">
                  <div class="team-member modern">
                    <a href="<?php echo Yii::getAlias('@web').'/evento/v/'.$ticket->getFkticket0()->one()->getFkevent0()->one()->event_url; ?>"> 
                      <div class="member-photo">
                        <?php
                          if($ticket->getFkticket0()->one()->fktipetickect == 1)
                          { ?>
                            <div class="member-name color01"><span>gratis</span> </div>
                          <?php
                          }
                          else
                            if($ticket->getFkticket0()->one()->fktipetickect == 2)
                            { ?>
                              <div class="member-name color02"><span>pago</span> </div>
                            <?php
                            }
                            else
                            { ?>
                              <div class="member-name color03"><span>Preventa</span> </div>
                            <?php
                            }
                        ?>
                        <img alt="" src="<?php echo Yii::getAlias('@web').$ticket->getFkticket0()->one()->getFkevent0()->one()->event_image; ?>" />
                      </div>
                    </a>
                    <div class="member-info">
                      <div class="post-date">
                        <?php
                          date_default_timezone_set('America/Bogota');
                          echo date('d M, Y. H:i',strtotime($ticket->getFkticket0()->one()->getFkevent0()->one()->event_stardate.$ticket->getFkticket0()->one()->getFkevent0()->one()->event_starthour));
                        ?>
                      </div>

                      <h2><a href="<?php echo Yii::getAlias('@web').'/evento/v/'.$ticket->getFkticket0()->one()->getFkevent0()->one()->event_url; ?>"> <?php echo $ticket->getFkticket0()->one()->getFkevent0()->one()->event_name; ?></a></h2>
                      <div class="post-date"><?php echo $ticket->getFkticket0()->one()->getFkevent0()->one()->event_address; ?> </div>
                    </div>  
                    <div class="member-socail"> <a class="icon05" href="#"></a> <a class="icon04" data-toggle="tooltip" title="Previsualizar" data-placement="bottom" href="<?php echo Yii::getAlias('@web').'/evento/v/'.$ticket->getFkticket0()->one()->getFkevent0()->one()->event_url; ?>"></a> <a class="icon03" data-toggle="tooltip" title="Compartir" data-placement="bottom" href="#"></a> <a class="link-button" href="<?php echo Yii::getAlias('@web').'/evento/v/'.$ticket->getFkticket0()->one()->getFkevent0()->one()->event_url; ?>">ver m&aacute;s</a> </div>
                  </div>
                </div>
              <?php
              }
            }
        ?>
      </div>
      <!-- End Team Members --> 
    </div>
    <!-- .container --> 
  </div>
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>