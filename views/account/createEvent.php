<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;

$this->title = 'Voy A Todo | Crear Evento';

?>

<div id="container">
  <section id="inner01">
    <div id="main-slide" class="carousel00 slide slide_galery" data-ride="carousel">
      <div class="item active"> <img class="img-responsive1" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
        <div class="slider-content">
          <div class="col-md-12 textEvent">
            <h3 class="animated3"> <span>estado de tu evento:</span> </h3>
            <p class="animated4"><a class="eventos public" >PUBLICAR</a> <a class="eventos save" >GUARDAR</a> </p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Comienza la primera opción del menú -->

  <div class="menu-modal summary-modal dashboard-modal scrollbar" id="opcion1">
    <div class="separator">
    </div>
    <div class="row">
      <div class="col-md-7 col-sm-6 col-xs-6">
        <h4>RESUMEN DEL EVENTO</h4>
      </div>
      <div class="col-md-5">
        <a class="btn btn-left">DESCARGAR REPORTE</a>
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-6 summary-images">
        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/pie.png">
          <p>0</p>
          <p>VISITAS</p>
        </div>

        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/buyers.png">
          <p>0</p>
          <p>COMPRAS</p>
        </div>
      </div>
      <div class="col-md-6 summary-images">
        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/basket.png">
          <p>0</p>
          <p>DINERO RECIBIDO</p>
        </div>

        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/assistants.png">
          <p>0</p>
          <p>ASISTENTES</p>
        </div>
      </div>
    </div>
    <div class="separator"></div>
    <h4 class="align_left">RESUMEN DE INGRESOS</h4>
    <table class="table">
      <tr>
        <td><p>BOLETA</p></td>
        <td><p>VALOR</p></td>
        <td><p>VENDIDAS</p></td>
      </tr>
      <tr>
        <td>------</td>
        <td>------</td>
        <td>------</td>
      </tr>
      <tr>
        <td>TOTAL</td>
        <td>0 COP</td>
      </tr>
      <tr>
        <td><p>COMISI&Oacute;N (4.5%)</p></td>
        <td><p>0 COP</p></td>
      </tr>
      <tr class="bluish">
        <td class="ttl_blue">TOTAL A RECIBIR</td>
        <td class="ttl_blue">0 COP</td>
      </tr>
    </table>
  </div>

  <!-- Comienza la segunda opción del menú -->

  <form id="save-form" action="<?php echo Yii::getAlias('@web') ?>/account/guardar" method="post" enctype="multipart/form-data"></form>

  <?php $form = ActiveForm::begin([
      'method' => 'post',
      'id' => 'form-create',
      'enableClientValidation' => true,
      'enableAjaxValidation' => true,
      'options' => ['enctype' => 'multipart/form-data'],
      'class'=> 'contact-form',
    ]);
  ?>
    <input type="hidden"  name="FormCreateEvent[status]" value="<?php echo $new; ?>">
    <input type="hidden"  name="FormCreateEvent[fkstatus]" value="<?php echo $fkstatus; ?>">
    <input type="hidden"  name="FormCreateEvent[eventid]" value="<?php echo $eventid; ?>">

    <div class="menu-modal information-modal summary-modal dashboard-modal scrollbar" id="opcion2">
      
      <div class="separator"></div>
      
      <h3>INFORMACI&Oacute;N DEL EVENTO</h3>
      <label for="nombre_evento">
        NOMBRE DEL EVENTO
      </label>
      <div class="input-group">
        <?= $form->field($model, "event_name")->input("text", ['class' => 'form-control event-name', 'placeholder'=>'Mi primer evento'])->label(false) ?>
        <span class="input-group-addon">
          <span class="cont_name">0/80</span>
        </span>
      </div>
      <div class="separator"></div>
      <div class="row">
        <h3>FECHA Y HORA</h3>
        <div class="col-md-6 col-sm-6 col-xs-6">
          <label class="lblnegro">FECHA INICO</label>
          <div class="input-group">
            <?= $form->field($model, 'event_stardate')->widget(
              DatePicker::className(), [
                'inline' => false,
                'language' => 'es', 
                'clientOptions' => [
                'autoclose' => true,
                'format' => 'dd-mm-yyyy'
                ]
              ],['class' => 'form-control no-left-border','id' => 'inicio'])->label(false);
            ?>
          </div> 
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 div_fecha">
          <label class="lblnegro">HORA INICIO</label>
          <div class="input-group clockpicker" data-donetext="Listo">
            <?= $form->field($model, "event_starthour")->input("text", ['class' => 'form-control event-starthour'])->label(false) ?>
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-time"></span>
            </span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-6">
          <label class="lblnegro">FECHA FINAL</label>
          <div class="input-group">
              <?= $form->field($model, 'event_enddate')->widget(
                DatePicker::className(), [
                  'inline' => false,
                  'language' => 'es', 
                  'clientOptions' => [
                      'autoclose' => true,
                      'format' => 'dd-mm-yyyy'
                  ]
                ],['class' => 'form-control no-left-border','id' => 'inicio'])->label(false);
              ?>
          </div> 
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 div_fecha">
          <label class="lblnegro">HORA FINAL</label>
          <div class="input-group clockpicker" data-donetext="Listo">
            <?= $form->field($model, "event_endhour")->input("text", ['class' => 'form-control event-endhour'])->label(false) ?>
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-time"></span>
            </span>
          </div>
        </div>                                        
      </div>

      <div class="separator"></div>

      <div class="row top">
        <div class="form-group" >
          <label class="col-sm-3 col-xs-3 lblnegro">PA&Iacute;S:</label>
          <div class="col-sm-9 col-xs-9">
            <?php $listData2=ArrayHelper::map($countries,'pkcountry','country_name'); ?>
            <?= $form->field($model, 'event_country')->dropDownList($listData2, 
              [
                'prompt'=>'Seleccione País',
                'class' => 'form-control cmb-country',
                'onchange'=>'
                  $.get( "'.Url::toRoute('account/getcities').'", { id: $(this).val() } )
                      .done(function( data ) {
                          $( "#'.Html::getInputId($model, 'event_city').'" ).html( data );
                      }
                  );'
              ])->label(false); ?>
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 col-xs-3 lblnegro">CIUDAD:</label>
           <div class="col-sm-9 col-xs-9">
              <?= $form->field($model, 'event_city')->dropDownList([],
                ['prompt'=>'Seleccione Ciudad','class' => 'form-control cmb-city'])->label(false); ?>
          </div>
        </div>
        <div class="form-group top">
          <label class="col-sm-3 col-xs-3 lblnegro">LUGAR:</label>
           <div class="col-sm-9 col-xs-9">
              <?= $form->field($model, "event_place")->input("text", ['class' => 'form-control event-place', 'placeholder' => 'Museo'])->label(false) ?>
          </div>
        </div>
        <div class="form-group top">
          <label class="col-sm-3 col-xs-3 lblnegro">DIRECCI&Oacute;N:</label>
           <div class="col-sm-9 col-xs-9">
              <?= $form->field($model, "event_address")->input("text", ['class' => 'form-control text-address','id' => 'datepicker', 'placeholder' => 'Cra 54 A # 112 C 09'])->label(false) ?>
          </div>
        </div>
      </div>
      <div class="col-lg-2 input-group search">
        <button type="button" id="search" class="btn btn-sm btn-success btn-block">Buscar</button>
      </div>
      
      <div class="input-group">
        <label>Ubicación del evento</label>
        <div class="map_canvas1" style="height:200px;"></div>
      </div>
      <?= $form->field($model, "event_lat")->input("hidden", ['class' => 'form-control event_lat'])->label(false) ?>
      <?= $form->field($model, "event_log")->input("hidden", ['class' => 'form-control event_log'])->label(false) ?> 

      <div class="separator"></div>

      <h3>CATEGOR&Iacute;AS DEL EVENTO</h3>
      <div class="row">
        <div class="form-group top">
          <label class="col-sm-3 col-xs-3 lblnegro">PRINCIPAL</label>
           <div class="col-sm-9 col-xs-9">
              <?php $listData=ArrayHelper::map($categories,'pkcategory','category_name'); ?>
              <?= $form->field($model, 'event_category_principal')->dropDownList($listData, 
                [
                  'prompt'=>'Seleccione Categoría',
                  'class' => 'form-control',
                  'onchange'=>'
                    $.get( "'.Url::toRoute('account/getcategories').'", { id: $(this).val() } )
                        .done(function( data ) {
                            $( "#'.Html::getInputId($model, 'event_category_secundary').'" ).html( data );
                        }
                    );
              '])->label(false); ?>
           </div>
        </div>
        <div class="form-group" >
          <label class="col-sm-3 col-xs-3 lblnegro">SECUNDARIA</label>
           <div class="col-sm-9 col-xs-9">
              <?= $form->field($model, 'event_category_secundary')->dropDownList([],
              ['prompt'=>'Seleccione categoría',
               'class' => 'form-control'])->label(false); ?>               
           </div>
        </div>
      </div>

      <div class="separator"></div>

      <h3>DETALLES DEL EVENTO</h3>
      <div class="row top">
        <div class="form-group">
          <label class="col-sm-3 col-md-12">RESUMEN DEL EVENTO</label>
          <div class="col-sm-9 col-md-12">
            <?= $form->field($model, "event_review")->input("text", ['class' => 'form-control no-right-border text-review', 'placeholder'=>'Este es el resumen de mi evento.'])->label(false)?>
            <span class="input-group-addon">
              <span class="cont_review">0/140</span>
            </span>
          <p class="description_social">Resume tu Evento en 140 car&aacute;cteres o menos.</p>
          </div>     
        </div>
        <div class="form-group">
          <label class="col-md-12 top">DESCRIPCI&Oacute;N DEL EVENTO</label>
          <div class="col-md-12">
            <?= $form->field($model, 'event_description')->textArea(['class' => 'text-area text-description','placeholder'=>'Esta es la descripción de mi primer evento.'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-12 top">SELECCIONA EL TIPO DE EVENTO</label>
          <div class="container-fluid">
            <div class="input-group radios">
              <div class="col-md-4">
                <input type="radio" name="FormCreateEvent[typeticket_name]" value="1" class="inputradio type_tic" checked> <span class="inputfalso"> <label class="lbl_radio">GRATIS</label> </span> <br>          
              </div>
              <div class="col-md-4">
                <input type="radio" name="FormCreateEvent[typeticket_name]" value="2" class="inputradio type_tic"> <span class="inputfalso"> <label class="lbl_radio">PAGO</label> </span> <br>
              </div>
              <div class="col-md-4">
                <input type="radio" name="FormCreateEvent[typeticket_name]" value="4" class="inputradio type_tic"> <span class="inputfalso"> <label class="lbl_radio">LIBRE</label> </span> <br>
              </div>
              <div class="col-md-6 col-md-offset-3">
                <input type="radio" name="FormCreateEvent[typeticket_name]" value="3" class="inputradio type_tic"> <span class="inputfalso"> <label class="lbl_radio">PREVENTA</label> </span> <br>
              </div>
            </div>
          </div>
          <div class="hidden options-crowfunding"> 
            <div class="col-md-6 col-sm-4 col-xs-4 top">
              <label for="nombre_evento">
                META FINANCIERA
              </label>
            </div>
            <div class="col-md-6 col-sm-4 col-xs-4 top">
              <div class="input-group">
                <div class="input-group-addon input-group-addon0">$</div>
                <?= $form->field($model, "event_crowfunding")->input("text", ['class' => 'form-control meta_financiera align_right', 'placeholder' => '300000'])->label(false)?>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group top">
          <label class="col-md-12">URL DEL EVENTO</label>
          <div class="col-md-12">
            <span class="input-group-addon" id="basic-addon1">http://voyatodo.com/evento/v/</span> 
             <?= $form->field($model, "event_url")->input("text", ['class' => 'form-control text-url'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-12 top">VIDEO DE YOUTUBE O VIMEO</label>
          <div class="col-md-12">
            <?= $form->field($model, 'event_linkvideo')->textArea(['class' => 'text-area text-video','placeholder' => 'Pega aquí tu link de YouTube o Vimeo:'])->label(false) ?>
          </div>
        </div>
        <div class="input-group search">
          <button type="button" id="search-video" class="btn btn-sm btn-block top_">Buscar</button>
        </div>
      </div><!--row-->  
      <div class="row">
        <div class="form-group">
          <label class="col-md-12">GALER&Iacute;A DE IM&Aacute;GENES</label>
          <div class="col-md-12">
            <div class="cargar top_menos btn-block"><label class="lbl_btn">SUBIR IM&Aacute;GENES</label>
              <?= $form->field($model, "event_galery[]")->fileInput(['multiple' => true, 'accept' => 'image/*','class' => 'eventos archivos upload-img'])->label(false) ?>
            </div>
            <p class="description_social top__10">
              El tama&ntilde;o de las im&aacute;genes debe ser superior a 200px
              y M&aacute;ximo 3 
            </p>
          </div>
        </div>
        <div class="tumbnails-content"></div>
      </div>
      <div class="row">
        <div class="form-group top">
          <label for="nombre_evento" class="col-md-12">T&Eacute;RMINOS Y CONDICIONES</label>
          <div class="col-md-12">
            <?= $form->field($model, 'event_terms')->textArea(['class' => 'text-area text-terms','placeholder' => 'Agrega aquí los términos y condiciones del servicio'])->label(false) ?>
          </div>
        </div>
      </div><!--row end-->
      <div class="row">
        <div class="form-group">
          <label for="cat_principal" class="col-md-12 top">ESTADO</label>
          <div class="col-md-12">
            <?php $listState_Ticket= ['0' => 'ABIERTO', '1' => 'PRIVADO']; ?>
            <?= $form->field($model, 'event_visible')->dropDownList($listState_Ticket, 
              [
                'prompt'=>'',
                'class' => 'form-control'
           ])->label(false); ?>
            <p class="description">
              Al poner un evento privado, s&oacute;lo acceder&aacute;s con la URL
            </p>
          </div>
        </div>
      </div><!--row end-->
      
      <div class="separator"></div>

      <h3>FORMULARIO DE REGISTRO</h3>
      <p class="help-block to">
        Agrega la información que crees pertinente al momento que los usuarios se registren a tu evento.
      </p>
      <div class="input-group top">
        <input type="text" id="name" class="form-control form_regist" name="generic[]" value="NOMBRE" readonly style="border-right: 0px !important;">
      </div>  
      <hr/>
      <div class="input-group">
        <input type="text" id="last_name" class="form-control form_regist" name="generic[]" value="APELLIDO" readonly style="border-right: 0px !important;">
      </div>
      <hr/>
      <div class="input-group">
        <input type="text" id="email" class="form-control form_regist" name="generic[]" value="E-MAIL" readonly style="border-right: 0px !important;">
      </div>
      <hr/>
      <div class="input-group">
        <input type="text" id="telefono" class="form-control form_regist" name="generic[]" value="TEL&Eacute;FONO" readonly style="border-right: 0px !important;">
      </div>
      <hr/>
      <div class="input-group divciudad">
        <input type="text" id="ciudad" class="form-control form_regist" name="generic[]" value="CIUDAD" style="border-right: 0px !important;">
        <span class="input-group-btn">
          <a class="btn btn-default btn_delete" style="height: 108%;"><span class="glyphicon glyphicon-trash"></span></a>
        </span>
      </div>
      <hr class="divciudad"/>
      <div id="divotro" class="input-group">
        <input type="text" id="otro" class="form-control form_regist" name="generic[]" value="CIUDAD">
      </div>
      <div class="input-group">
        <div class="col-md-8 col-sm-8 col-xs-8  col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
        <a class="btn cargar btn-block" id="btnagregar_campo">AGREGRAR CAMPO</a>
        </div>
      </div>

      <div class="separator"></div>

      <h3>ENVIAR MAIL DEL EVENTO A LOS USUARIOS</h3>

      <div class="form-group">
        <label class="col-md-12 top">ENVIAR CORREOS</label>
        <div class="row">
          <div class="input-group radios">
            <div class="col-md-4">
              <input type="radio" name="response" value="0" class="inputradio type_response" checked> <span class="inputfalso"> <label class="lbl_radio">DESACTIVAR</label> </span> <br>          
            </div>
            <div class="col-md-4">
              <input type="radio" name="response" value="1" class="inputradio type_response"> <span class="inputfalso"> <label class="lbl_radio">ACTIVAR</label> </span> <br>
            </div>
          </div>
        </div>
      </div>

      <div class="hidden opt_mail">
        <?php $form = ActiveForm::begin([
          'id' => 'mail-form',
          'method' => 'post',
          'enableClientValidation' => true,
          'enableAjaxValidation' => true,
          'options' => ['enctype' => 'multipart/form-data'],
        ]); ?>
          <?= $form->field($modelMail, "event_name")->input("hidden", ['id' => 'mail_event_name'])->label(false) ?>
          <?= $form->field($modelMail, "event_stardate")->input("hidden", ['id' => 'mail_event_stardate'])->label(false) ?>
          <?= $form->field($modelMail, "event_place")->input("hidden", ['id' => 'mail_event_place'])->label(false) ?>
          <?= $form->field($modelMail, "event_starthour")->input("hidden", ['id' => 'mail_event_starthour'])->label(false) ?>
          <?= $form->field($modelMail, "event_url")->input("hidden", ['id' => 'mail_event_url'])->label(false) ?>
          <div class="row top">
            <div class="form-group">
              <label for="nombre_evento" class="col-md-12">TITULO DEL EMAIL</label>
              <div class="col-md-12">
                <?= $form->field($modelMail, 'subject')->input("text", ['placeholder'=>'Descripción de tu email', 'class' => 'form-control text-email-send'])->label(false); ?>
                <span class="input-group-addon">
                  <span class="cont_email">0/140</span>
                </span>
                <p class="description_social">Describe tu correo en 140 car&aacute;cteres o menos.</p>
              </div>
            </div>
            <div class="form-group">
              <label for="nombre_evento" class="col-md-12 top">DESCRIPCI&Oacute;N DEL EMAIL</label>
              <div class="col-md-12">
                <?= $form->field($modelMail, 'message')->textarea(['placeholder' => 'Texto del email', 'class' => 'text-area text-description-email', ])->label(false); ?>
              </div>
            </div>
            <div class="form-group">
              <label for="cat_principal" class="col-md-12">EMAIL DE RESPUESTA</label>
              <div class="col-md-12">
                <?= $form->field($modelMail, 'response')->input("email", ['placeholder'=>'nombre@correo.com', 'class' => 'form-control no-right-border text-email-response'])->label(false); ?>
                <p class="description_social">Aqu&iacute; se enviar&aacute; respuesta de tus destinatarios</p>
              </div>
            </div>
            <div class="form-group">
              <label for="nombre_evento" class="col-md-12 top">SUBIR IM&Aacute;GEN DE CABECERA</label>
              <div class="col-md-12">
                <div class="cargar top_menos btn-block"><label class="lbl_btn">SUBIR IM&Aacute;GEN</label>
                <?= $form->field($modelMail, "image")->fileInput(['class' => 'eventos archivos upload_email', 'accept' => 'image/*'])->label(false); ?>
                </div>
                <p class="description">El tama&ntilde;o de la im&aacute;gen debe ser de 600px</p>
              </div>
            </div>
            <div class="form-group">
              <label for="nombre_evento" class="col-md-12 top">A&Ntilde;ADIR DESTINATARIOS</label>
              <p class="description" style="color: #00C4FA !important;">
                Puedes subir un archivo de Excel o CVS con los correos en una sola columna descendente o agregar los correos separados por punto y coma (;)</p>
              <div class="col-md-12 top">
                <div class="cargar top_menos btn-block"><label class="lbl_btn">CARGAR DESTINARIOS</label>
                  <?= $form->field($modelMail, "file")->fileInput(['class' => 'eventos archivos'])->label(false); ?>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <textarea class="text-area" name="text_upload_email"></textarea>
              </div>
            </div>      
          </div><!--row end-->
          <div class="row top">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <a type="button" class="btn btn-block btn-yellow" data-toggle="modal" id="preview_email" data-target="#modal-preview-email">PREVIZUALIZAR</a>
            </div>    
            <div class="col-md-6 col-sm-6 col-xs-6">
               <a type="button" class="btn btn-block btn_green btn_send_email">ENVIAR EMAIL</a>
            </div>
          </div><!--row end-->
          <div class="separator"></div>
        <?php ActiveForm::end(); ?>
      </div>

    </div><!--information-modal-->

    <!-- Comienza la tercera opción del menú -->

    <div class="separator"></div>

    <div class="menu-modal design-modal summary-modal dashboard-modal scrollbar"  id="opcion3">
      
      <div class="separator"></div>
      
      <h3>IM&Aacute;GEN DE BANNER</h3>
      <div class="row top">
        <div class="col-md-12">
          <div class="form-group">
            <label for="nombre_evento">CARGA TU PROPIA IM&Aacute;GEN</label>
            <div class="cargar top_menos btn-block"><label class="lbl_btn">SUBIR IM&Aacute;GEN</label>
              <?= $form->field($model, "event_image")->fileInput(['class' => 'eventos archivos upload-banner', 'accept' => 'image/*'])->label(false) ?>
            </div>
            <p class="description_social">El tama&ntilde;o del banner debe ser superior a 1400 x 600 px</p>
          </div>
        </div>
      </div><!--row end-->

      <div class="separator"></div>

      <h3>OPACIDAD</h3>
      <div class="row top">
        <div class="form-group">
          <label for="nombre_evento" class="col-md-12">CAPA OSCURA</label>
          <div class="col-md-12">
            <?= $form->field($model, "event_opacityimage")->input("range", ['min' => '0' ,'max' => '1','step' => '0.1','value' => '0','id' => 'opacity-banner'])->label(false) ?>
          </div>
        </div>
      </div><!--row end-->
    </div><!--diseño-modal-->

    <!-- Comienza la cuarta opción del menú -->

    <div class="menu-modal tickets-modal summary-modal dashboard-modal tickets_principal scrollbar"  id="opcion5">

      <div class="separator"></div>

      <h3>TUS ENTRADAS</h3>
      <label for="nombre_evento">
        CARGA TU PROPIA ENTRADA
      </label>
      <div class="col-md-12 top">
        <a class="btn btn-yellow btn-block btn_create_ticket">CREAR ENTRADA NUEVA</a>
      </div>
      <p class="description_social">
        Crea nuevas entradas, edita o borra Entradas existentes
      </p>
      <div id="goal_complete" style="color:#A94452;"></div>

      <input type="hidden"  name="FormCreateEvent[tickets]" class="json_tickets" value="">

      <div class="separator"></div>

      <h3>ENTRADAS CREADAS</h3>

      <div class="row div_tickets">
        <?php 
          if(!empty($tickets))
          {
            foreach ($tickets as $tick) 
            {
            ?>
            <div class="col-md-8 div_banco top div_update_ticket" name="<?php echo $tick->pkticket; ?>" id="<?php echo $tick->pkticket; ?>">
              <label class="lbl_bnc">
                <?php echo $tick->ticket_name; ?>
                <br/>
                <span class="ttl_blue">
                  <?php echo $tick->getFktipetickect0()->one()->typeticket_name; ?>
                </span>
              </label>
            </div>
            <?php
            }
          }
        ?>
        <div class="new_ticket"></div>
      </div>
    </div>

    <div class="menu-modal tickets-modal summary-modal dashboard-modal create_tickets hidden scrollbar"  id="opcion-1">

      <div class="separator"></div>

      <div class="row">
        <div class="col-md-7 col-sm-6 col-xs-6">
          <h4>DETALLE DEL TICKET</h4>
        </div>
        <div class="col-md-5">
          <a class="btn btn_delete_ticket"> ELIMINAR </a>
        </div>
      </div>
      <div class="input-group top">
        <label for="nombre_evento"> NOMBRE DEL TICKET</label>
        <input name="FormCreateEvent[ticket_name]" type="text" class="form-control ticket-name align_right" placeholder="Entrada general" />
        <span class="input-group-addon">
          <span class="cont_ticket-name">0/35</span>
        </span>
      </div>
      <div class="input-group top">
        <label for="nombre_evento"> DESCRIPCI&Oacute;N </label>
        <input name="FormCreateEvent[ticket_description]" type="text" class="form-control ticket-description align_right" placeholder="Entrada general" />
        <span class="input-group-addon">
          <span class="cont_ticket-description">0/50</span>
        </span>
      </div>

      <div class="separator"></div>

      <h3>PRECIO DEL TICKET</h3>
      <div class="hidden options-pay">
        <div class="row top">
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento">
              VALOR DE ENTRADA
            </label>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <div class="input-group-addon input-group-addon0">$</div>
              <input name="FormCreateEvent[ticket_value]" type="number" class="form-control ticket-value align_right" placeholder="30000" />
            </div>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento" class="lbl_tickets">
              COMISI&Oacute;N
            </label>        
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <input type="text" class="form-control ticket-comision ttl_blue" readonly="true"/>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <p>
              Esta es la comisi&oacute;n que cobra Voy A Todo por el manejo de los eventos y el proveedor de pagos.
            </p>
          </div>

          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento" class="lbl_tickets">
              RECIBIR&Aacute;S
            </label>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <div class="input-group-addon input-group-addon0 ttl_blue">$</div>
              <input name="FormCreateEvent[ticket_comision]" type="number" class="form-control ticket-pay-user" placeholder="28700"/>
            </div>
          </div>
        </div>
      </div>
      <div class="hidden options-crowfunding">
        <div class="row top">
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento">
              VALOR DE BOLETA
            </label>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <div class="input-group-addon input-group-addon0">$</div>
              <input name="FormCreateEvent[ticket_value]" type="number" class="form-control ticket_value_crw align_right" placeholder="000" />
            </div>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento" class="lbl_tickets">
              DESCUENTO BOLETA
            </label>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <input type="text" class="form-control discount-ticket ttl_blue" readonly="true"/>
            </div>
          </div>

          <div class="col-md-12 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <div class="col-md-12">
                <input type="range" max="100" step="0.1" id="increase_discount">
              </div>
              <p class="description_social">
                Puedes escoger el valor de descuento de tu boleta.
              </p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento" class="lbl_tickets">
              COMISI&Oacute;N VAT
            </label>
            <p>
              Esta es la comisi&oacute;n que cobra Voy A Todo por el manejo de los eventos. 
            </p>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <input type="text" class="form-control ticket-comision ttl_blue" readonly="true"/>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento" class="lbl_tickets">
              VAS A RECIBIR
            </label>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <div class="input-group-addon input-group-addon0 ttl_blue">$</div>
              <input name="FormCreateEvent[ticket_comision]" type="number" class="form-control ticket_pay_crw" placeholder="000" />
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 top">
            <label for="nombre_evento" class="lbl_tickets">
              PUNTO DE EQUILIBRIO
            </label>
            <p class="description_social">
              Cantidad de boletas para cubrir la meta financiera
            </p>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 top">
            <div class="input-group">
              <input name="qty_crowfunding" type="text" class="ttl_blue form-control qty_crowfunding" placeholder="0" readonly="true" />
            </div>
          </div>
        </div><!--row-->
        <div class="row top">
          <div class="col-md-12 col-sm-4 col-xs-4 top">
            <label for="nombre_evento">
              DESCRIBE EL INCENTIVO
            </label>
          </div>
          <div class="col-md-12 col-sm-4 col-xs-4">
            <input name="FormCreateEvent[ticket_insentive]" type="text" class="form-control ticket-insentive" placeholder="Camiseta y Gorra autografiada" />
          </div>
          <div class="col-md-12 col-sm-4 col-xs-4 top">
            <label for="nombre_evento">
              FECHA DE FINALIZACI&Oacute;N
            </label>
          </div>
          <div class="col-md-7 col-sm-4 col-xs-4">
            <div class="input-group">
              <?= $form->field($model, 'ticket_enddate')->widget(
                DatePicker::className(), [
                  'inline' => false,
                  'language' => 'es', 
                  'clientOptions' => [
                  'autoclose' => true,
                  'format' => 'mm-dd-yyyy'
                  ]
                ],['class' => 'form-control no-left-border','id' => 'inicio'])->label(false);
              ?>
            </div>
          </div>
          <div class="col-md-12 col-sm-4 col-xs-4">
            <label for="nombre_evento">
              FECHA LIMITE PARA CUMPLIR LA META
            </label>
          </div>
          <div class="col-md-12 col-sm-4 col-xs-4">
            <label class="ticket_limit_date" style="color:#A94452;font-size: 15px;text-align: center;"></label>
          </div>
        </div>
      </div>
      <div class="row top">
        <div class="col-md-12 col-sm-4 col-xs-4">
          <label for="nombre_evento">
            CANTIDAD
          </label>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
          <div class="input-group">
            <input name="FormCreateEvent[ticket_qty]" type="number" class="form-control ticket-qty" placeholder="100" />
          </div>
        </div>
        <div class="col-md-8 col-sm-4 col-xs-4">
          <input class="inputradio out_limit" type="checkbox"><span class="inputfalso"> <label class="lbl_radio">SIN L&Iacute;MITE</label></span>
        </div>
      </div>
      <div class="row top">
        <div class="col-md-4 col-sm-4 col-xs-4">
          <label for="nombre_evento">
            ENTRADAS RECLAMADAS
          </label>
        </div>
        <div class="entrance_tic">
          <div class="col-md-4 col-sm-4 col-xs-4">
            <input type="radio" name="FormCreateEvent[ticket_seeclaim]" value="0" checked class="inputradio"> 
            <span class="inputfalso"/> <label class="lbl_radio">OCULTAR</label> </span>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-4">
            <input type="radio" name="FormCreateEvent[ticket_seeclaim]" value="1" class="inputradio"> 
            <span class="inputfalso"/> <label class="lbl_radio">MOSTRAR</label> </span>
          </div>
        </div>
        <div class="col-md-12 col-sm-4 col-xs-4">
          <p class="description_social">Ocultar o Mostrar las entradas reclamadas para el evento. </p>
        </div>
      </div>
      
      <div class="separator"></div>

      <h3>DISPONIBLE AL P&Uacute;BLICO</h3>
      <div class="row top">
        <div class="col-md-12 col-sm-4 col-xs-4">
          <label for="nombre_evento">
            DESDE
          </label>
        </div>
        <div class="col-md-6 col-sm-4 col-xs-4">
          <?= $form->field($model, 'ticket_start')->widget(
            DatePicker::className(), [
              'inline' => false,
              'language' => 'es', 
              'clientOptions' => [
              'autoclose' => true,
              'format' => 'yyyy-mm-dd'
              ]
            ],['class' => 'form-control no-left-border '])->label(false);
          ?>
        </div>
        <div class="col-md-6 col-sm-4 col-xs-4">
          <input class="inputradio start_date" type="checkbox">
          <span class="inputfalso"> 
            <label class="lbl_radio">INMEDIATAMENTE</label> 
          </span>
        </div>
        <div class="col-md-12 col-sm-4 col-xs-4">
          <label for="nombre_evento">
            HASTA
          </label>
        </div>
        <div class="col-md-6 col-sm-4 col-xs-4">
          <?= $form->field($model, 'ticket_finish')->widget(
            DatePicker::className(), [
              'inline' => false,
              'language' => 'es', 
              'clientOptions' => [
              'autoclose' => true,
              'format' => 'yyyy-mm-dd'
              ]
            ],['class' => 'form-control no-left-border'])->label(false);
          ?>
        </div>
        <div class="col-md-5 col-sm-4 col-xs-4">
          <input class="inputradio end_date" type="checkbox">
          <span class="inputfalso"> 
            <label class="lbl_radio">D&Iacute;A DEL EVENTO</label> 
          </span>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 top">
          <a class="btn btn-yellow btn-block btn_save_ticket hidden">GUARDAR</a>
          <a class="btn btn-yellow btn-block btn_update_ticket hidden">ACTUALIZAR</a>        
        </div>
        <div><center><div id="error_tickets" style="color:#A94452;"></div></center></div>
      </div>
    </div>

    <!-- Comienza la quinta opción del menú -->

    <div class="menu-modal bank-modal summary-modal dashboard-modal bank_acount scrollbar"  id="opcion4">
      
      <div class="separator"></div>

      <h4 class="h4_left">A&Ntilde;ADIR CUENTA PARA RECIBIR PAGOS</h4>
      <div class="row div_bank">
        <?php 
          if(!empty($userbank))
          {
            foreach ($userbank as $bank) 
            {
            ?>
            <div class="col-md-8 div_banco top div_update_acount" name="<?php echo $bank->pkuserbank; ?>" id="<?php echo $bank->pkuserbank; ?>">
              <label class="lbl_bnc"><?php echo $bank->userbank_tipeacount.' - '. $bank->getFkbank0()->one()->bank_name ?>  
                <br/><span class="ttl_blue"><?php echo $bank->userbank_identification; ?></span>
              </label>
            </div>
            <?php
            }
          }
          else
          {
            ?>
            <div class="col-md-12 top">
              <a class="btn btn-yellow btn-block btn_pay">A&Ntilde;ADIR CUENTA</a>
            </div>
            <?php
          }
        ?>
        <div class="alert alert-danger hidden message" role="alert">
          <strong>No se pudo procesar tu requerimiento</strong>
        </div>
      </div>
      
      <div class="separator"></div>

      <h4 class="h4_left">M&Eacute;TODOS DE PAGO PUBLICIDAD </h4>
      <div class="row div_cards">
        <?php 
          if(!empty($creditcard))
          {
            foreach ($creditcard as $card) 
            {
            ?>
              <div class="col-md-8 div_banco top div_update_card" name="<?php echo $card->pkcreditcard; ?>" id="<?php echo $card->pkcreditcard; ?>">
                <label class="lbl_bnc"><?php echo $card->creditcard_type; ?><br/>
                <span class="ttl_blue">
                  <?php echo "##########".substr($card->creditcard_numbercard, -4); ?>
                </span></label>
              </div>
            <?php
            }
          }
        ?>
        <div id="new"></div>  
        <div class="col-md-12 top">
          <a class="btn btn-yellow btn-block btn_target">A&Ntilde;ADIR TARJETA</a>
        </div>
        <div class="alert alert-danger hidden messagec" role="alert">
          <strong>Tu tarjeta no se guardo</strong>
        </div>
      </div>

      <div class="separator top"></div>

      <h4 class="h4_left">PAGO DE CR&Eacute;DITO ACTUAL</h4>
      <div class="row div_pays">  
        <span class="col-md-12 ttl_blue">0 COP <br/></span><span class="col-md-12"> Pagar con:</span>
        <?php 
          if(!empty($creditcard))
          {
            foreach ($creditcard as $card) 
            {
            ?>
              <div class="col-md-8 div_banco top div_update_card" name="<?php echo $card->pkcreditcard; ?>" id="<?php echo $card->pkcreditcard; ?>">
                <label class="lbl_bnc"><?php echo $card->creditcard_type; ?><br/>
                <span class="ttl_blue">
                  <?php echo "##########".substr($card->creditcard_numbercard, -4); ?>
                </span></label>
              </div>
            <?php
            }
          }
        ?>
        <div id="new1"></div>
      </div><!--row end-->
    </div>

    <div class="menu-modal bank-modal summary-modal dashboard-modal edit_account hidden scrollbar"  id="opcion-2">

      <div class="separator"></div>

      <div class="row">
        <div class="col-md-7 col-sm-6 col-xs-6">
          <h4>EDITAR CUENTA PARA RECIBIR PAGOS</h4>
        </div>
        <div class="col-md-5">
          <a class="btn btn_green btn_delete_bank"> ELIMINAR </a>
        </div>
      </div>
      <p class="description_social" style="margin-top: 40px; margin-bottom: 50px;">
        VoyATodo enviar&aacute; los pagos recibidos en la compra de tus 
        productos vendidos durante la semana. Podr&aacute;s verla en tu 
        extracto como VOYATODO SAS. Recuerda que la
        transferencia tiene un costo de 15.000 COP 
      </p>
      <div class="row">
        <div class="form-group">
          <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">BANCO</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?php $listBank=ArrayHelper::map($listbanks,'pkbank','bank_name'); ?>
            <?= $form->field($model, 'fkbank')->dropDownList($listBank, 
            [
              'prompt'=>'',
              'class' => 'form-control bank-name'
            ])->label(false); ?>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-3 col-sm-3 col-xs-3">NOMBRE TITULAR</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?= $form->field($model, 'userbank_titularname')->input('text', ['class' => 'form-control bank-titular'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-3 col-sm-3 col-xs-3">C&Eacute;DULA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
             <?= $form->field($model, 'userbank_identification')->input('number', ['class' => 'form-control bank-identification'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-3 col-sm-3 col-xs-3">N° DE CUENTA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?= $form->field($model, 'userbank_numberacount')->input('number', ['class' => 'form-control bank-numberacount'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-3 col-sm-3 col-xs-3">TIPO DE CUENTA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?php $listAcount= ['Corriente' => 'Corriente', 'Crédito' => 'Crédito', 'Ahorro' => 'Ahorro']; ?>
            <?= $form->field($model, 'userbank_tipeacount')->dropDownList($listAcount, 
            [
            'prompt'=>'',
            'class' => 'form-control bank-type'
             ])->label(false); ?>
          </div>
        </div>
      </div><!--row end-->
      <div class="row">
        <a class="btn btn-yellow btn-block create_acount hidden">CREAR CUENTA</a>
        <a class="btn btn-yellow btn-block update_acount hidden">ACTUALIZAR CUENTA</a>
      </div><!--row end-->
    </div><!--modal end-->

    <div class="menu-modal bank-modal summary-modal dashboard-modal edit_target hidden scrollbar"  id="opcion-3">
        
      <div class="separator"></div>

      <h4>EDITAR TARJETA DE CR&Eacute;DITO PARA PAGOS</h4>
      <p class="description_social" style="margin-top: 40px; margin-bottom: 50px;">
        VoyATodo recibir&aacute; los pagos de la pauta publicitaria y <br />
        se cargar&aacute;n a tu tarjeta de cr&eacute;dito. Podr&aacute;s verla en tu <br />
        extracto como VOYATODO SAS, s&oacute;lo se cobrar&aacute; el valor <br />
        que hayas avalado en tu banner publicitario
      </p>
      <div class="row">
        <div class="form-group">
          <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">NOMBRE</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?= $form->field($model, 'creditcard_titularname')->input('text', ['class' => 'form-control creditcard-name'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">NÂ° TARJETA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?= $form->field($model, 'creditcard_numbercard')->input('number', ['class' => 'form-control creditcard-number'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">TIPO DE TARJETA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?php $listType= ['MASTERCARD' => 'MASTERCARD', 'VISA' => 'VISA', 'AMERICAN EXPRESS' => 'AMERICAN EXPRESS','DINNERS' => 'DINNERS']; ?>
            <?= $form->field($model, 'creditcard_type')->dropDownList($listType, 
             [
              'prompt'=>'',
              'class' => 'form-control creditcard-type'
            ])->label(false); ?>
          </div>
        </div> 
      </div><!--row end-->
      <div class="row">
        <a class="btn btn-yellow btn-block create_target hidden">CREAR TARJETA</a>
        <a class="btn btn-yellow btn-block update_target hidden">ACTUALIZAR TARJETA</a>
      </div>
    </div><!--modal end-->

    <!-- Comienza la sexta opción del menú -->

    <div class="menu-modal share-modal summary-modal dashboard-modal scrollbar"  id="opcion6">

      <div class="separator"></div>

      <h3>COMPARTIR POR REDES SOCIALES</h3>
      <p class="description_social">
        Ahora puedes invitar a tus amigos, seguidores y
        contactos a trav&eacute;s de las redes sociales
      </p>
      <span>
        <a class="text-facebook red_margin no_share"> <i class="fa fa-facebook"></i> </a>
        <a class="text-twitter red_margin no_share"> <i class="fa fa-twitter"></i> </a>
        <a class="text-linkedin red_margin no_share"> <i class="fa fa-linkedin"></i> </a>
        <a class="text-google red_margin no_share"> <i class="fa fa-google-plus"></i> </a>
      </span>

      <div class="separator"></div>

      <h3>COMPARTIR POR E-MAIL</h3>
      <p class="description_social">
        Copia y pega el siguiente enlace en tu correo para <br>
        que tus invitados registren el evento:
      </p>
      <div class="col-md-12">
        <?= $form->field($model, 'event_url')->textArea(['class' => 'text-area', 'readonly' => true,'id' => 'txt_url'])->label(false) ?>
      </div>
      <div class="col-md-12 top">
        <a class="btn btn-yellow btn-block btn-url" data-clipboard-target="#txt_url">COPIAR ENLACE</a>
      </div>
    </div>

    <!-- Comienza la séptima opción del menú -->

    <div class="menu-modal sponsor-modal summary-modal dashboard-modal scrollbar"  id="opcion7">

      <div class="separator"></div>

      <h3>PATROCINADOR DEL EVENTO</h3>
      <label for="nombre_evento">
        ACTIVA PARA CONSEGUIR PATROCINADOR
      </label>
      <div class="row">
        <div class="input-group">
          <div class="col-md-5">
            <input type="radio" name="FormCreateEvent[event_sponsorship]" value="0" class="inputradio" checked=""> <span class="inputfalso"/> <label class="lbl_radio">PRIVADO</label></span> <br>
          </div>
          <div class="col-md-5">
            <input type="radio" name="FormCreateEvent[event_sponsorship]" value="1" class="inputradio"> <span class="inputfalso"/> <label class="lbl_radio">ACTIVAR</label> </span>
          </div>
        </div>
      </div>
      <p class="description_social">
        Una vez activo debes esperar que un patrocinador
        se ponga en contacto contigo.
      </p>

      <div class="separator"></div>

      <h3>PATROCINADORES INTERESADOS</h3>
    </div>

    <!-- Comienza la octava opción del menú -->
    <div class="menu-modal asistent-modal summary-modal dashboard-modal mw100 scrollbar"  id="opcion8">

      <div class="separator"></div>

      <h3>LISTA ASISTENTES</h3>
        <div class="row">
          <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
            <p class="help-block top">BUSCAR POR NOMBRE, APELLIDO O E-MAIL</p>
            <div class="input-group search">
              <?= $form->field($model, "token_asistent")->input("text", ['class' => 'form-control text-address top_menos'])->label(false) ?>
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <a id="search_asistent" class="btn cargar_  hidden-sm hidden-xs top_50">BUSCAR</a>
            <a id="search_asistent" class="btn cargar_  btn-block hidden-md hidden-lg top_10">BUSCAR</a>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pull-right">
            <a id="download_asistent" class="btn cargar_  hidden-sm hidden-xs top_50">DESCARGAR REPORTE</a>
            <a id="download_asistent" class="btn cargar_  btn-block hidden-md hidden-lg top_10">DESCARGAR REPORTE</a>
          </div>
        </div><!--row-->
        
          <div class="table-resposive">
            <table class="table table-condensed">
              <thead>
                <tr>
                  <th>C&Oacute;DIGO</th>
                  <th>NOMBRE</th>
                  <th>APELLIDO</th>
                  <th>EMAIL</th>
                  <th>TIPO ENTREGA</th>
                  <th>PRECIO</th>
                  <th>FECHA REGISTRO</th>
                  <th>ESTADO</th>
                </tr>
              </thead>
              <tbody class="tbody">
                <td>---</td>
                <td>---</td>
                <td>---</td>
                <td>---</td>
                <td>---</td>
                <td>---</td>
                <td>---</td>
                <td>---</td>
              </tbody>
            </table>
          </div>
    </div>

    <!-- Comienza la novena opción del menú -->

    <div class="menu-modal access-modal summary-modal dashboard-modal scrollbar"  id="opcion4">

      <div class="separator"></div>

      <h3>VALIDAR ASISTENTE</h3>
      <div class="row">
        <div class="col-md-12">
          <p class="description_social">
            Para registrar el ingreso de tus invitados al evento debes
            solicitarles el c&oacute;digo que recibieron en el e-mail, en el cual
            se encuentra un c&oacute;digo PIN, ingr&eacute;salo a continuaci&oacute;n:
          </p>
        </div>
      </div><!--/row-->
      <div class="row top">
        <div class="form-group">
          <div class="col-sm-8 col-xs-12">
            <input type="text" class="form-control txt_pin">
          </div>
           <div class="col-sm-4 col-xs-12 ">
              <a class="btn cargar_ btn-block">VALIDAR</a>
          </div>
        </div>
      </div><!--/row-->
    </div>

    <!-- Comienza la decima opción del menú -->

    <div class="menu-modal publicity-modal summary-modal dashboard-modal publicity_principal scrollbar"  id="opcion10">

      <div class="separator"></div>

      <h3>PUBLICIDAD</h3>
      <div class="row top">
        <div class="form-group">
          <div class="col-md-12">
            <label>DESTACA TU EVENTO A&Ntilde;ADIENDO PAUTA</label>
            <a class="btn btn-yellow btn-block">CREAR PAUTA</a>
            <p class="help-block">Tu evento en los destacados de Voyatodo.com</p>
          </div>
        </div>
      </div>

      <div class="separator"></div>

      <h3 class="top">CAMPA&Ntilde;AS CREADAS</h3>
      <div class="row top">
        <div class="form-group">
          <div class="col-md-12">
            <label>UNA VEZ PUBLIQUES TU EVENTO ESTA OPCI&Oacute;N SE HABILITAR&Aacute;</label>
          </div>
        </div>
      </div>
    </div>
  <?php $form->end() ?>

  <!-- Modal preview email -->

  <div class="modal fade" id="modal-preview-email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">AS&Iacute; SE VERA TU EMAIL</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <img id="img_email" src="">
            </div>
            <div class="col-md-12">
              <h1>Invitaci&oacute;n al Evento</h1>
              <h2 class="titulo">[NOMBRE EVENTO]</h2>
            </div>
            <div class="col-md-12 top"> 
              Fecha: <span class="text_date"></span><br>
              Lugar: <span class="lbl_address"></span><br>
              Hora: <span class="text_hour"></span><br>
            </div>
            <div class="col-md-12 top"> 
              <span id="md_text"></span>
            </div>
            <div class="col-md-12 top">
              <span id="md_description"></span>
            </div>
            <div class="col-md-12 top">
              Te puedes comunicar al correo: <span id="md_response">tucorreo@email.com</span>
            </div>
            <div class="col-md-12">
              <a class="btn btn-block btn_url_email" style="background: #f3d71a; color: #4B0082 !important;">VER DETALLES DEL EVENTO</a>
              <div id="information_url" style="color:#A94452;"></div>
            </div>
            <div class="col-md-12">
              <div class="divmess">No te vayas a quedar por fuera! entra en los detalles del Evento y adquiere tu entrada al Evento.</div>
            </div>
            <div class="col-md-12">
              <hr/>
            </div>
            <div class="col-md-12">
              <h6 align="center" style="font-style: italic;">Copyright &copy; 2016, VOY A TODO. All rights reserved.</h6>
              <h6 align="center" class="top">¿Tienes problemas?<br/>
                info@voyatodo.com</h6>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <a type="button" class="btn btn-default" data-dismiss="modal">Cerrar</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Contenido de la página -->
  <section id="photo" class="dashboard-row">
    <img id="img-banner" class="responsive-image" />
    <div class="shadow" ></div>

    <!-- Menu -->
    <div class="col-xs-2 col-md-2 col-sm-1 dashboard-column">
      <div class="dashboard-content col-sm-12">
        <div class="col-md-12 col-xs-0 col-md-offset-0 dashboard-element" id="dashboard-summary">
          <a class="img_click" name="summary-" style="cursor: pointer">
            <img class="img_summary-" src="<?php echo Yii::getAlias('@web') ?>/images/summary-gray.png">
            <p class="hidden-sm hidden-xs">DASHBOARD</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-information">
          <a class="img_click" name="information-" style="cursor: pointer">
            <img class="img_information-" src="<?php echo Yii::getAlias('@web') ?>/images/information-gray.png">
            <p class="hidden-sm hidden-xs">INFORMACI&Oacute;N</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-design">
          <a class="img_click" name="design-" style="cursor: pointer">
            <img class="img_design-" src="<?php echo Yii::getAlias('@web') ?>/images/design-gray.png">
            <p class="hidden-sm hidden-xs">DISE&Ntilde;O</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-tickets">
          <a class="img_click menu_tickets" name="tickets-" style="cursor: pointer">
            <img class="img_tickets-" src="<?php echo Yii::getAlias('@web') ?>/images/tickets-gray.png">
            <p class="hidden-sm hidden-xs">ENTRADAS</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-bank">
          <a class="img_click menu_banks" name="bank-" style="cursor: pointer">
            <img class="img_bank-" src="<?php echo Yii::getAlias('@web') ?>/images/bank-gray.png">
            <p class="hidden-sm hidden-xs">BANCO</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 col-sm-offset-0  dashboard-element" id="dashboard-share">
          <a class="img_click" name="share-" style="cursor: pointer">
            <img class="img_share-" src="<?php echo Yii::getAlias('@web') ?>/images/share-gray.png">
            <p class="hidden-sm hidden-xs">COMPARTIR</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 col-sm-offset-1 dashboard-element" id="dashboard-sponsor">
          <a class="img_click" name="sponsor-" style="cursor: pointer">
            <img class="img_sponsor-" src="<?php echo Yii::getAlias('@web') ?>/images/sponsor-gray.png">
            <p class="hidden-sm hidden-xs" style="margin-left: -18px;">PATROCINADOR</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-asistent">
          <a class="img_click" name="asistent-" style="cursor: pointer">
            <img class="img_asistent-" src="<?php echo Yii::getAlias('@web') ?>/images/asistent-gray.png">
            <p class="hidden-sm hidden-xs">ASISTENTES</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-access">
          <a class="img_click" name="access-" style="cursor: pointer">
            <img class="img_access-" src="<?php echo Yii::getAlias('@web') ?>/images/access-gray.png">
            <p class="hidden-sm hidden-xs">ACCESO</p>
          </a>
        </div>
        <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-publicity">
          <a class="img_click" name="publicity-" style="cursor: pointer">
            <img class="img_publicity-" src="<?php echo Yii::getAlias('@web') ?>/images/publicity-gray.png">
            <p class="hidden-sm hidden-xs">PUBLICIDAD</p>
          </a>
        </div>
      </div>
    </div>
    <!-- End Menu -->

    <div class="contenido-evento col-md-10">
      <h1 class="titulo">Nombre de tu evento </h1>
      <div class="detalles">
        <div class="fecha">
          <i class="fa fa-calendar"></i>
          <label class="text_date"> S&aacute;bado 31 de Diciembre de 2016 </label>
          <br>
          <i class="fa fa-calendar"></i>
          <label class="text_date_finish"> Domingo 01 de Enero de 2017 </label>
        </div>

        <div class="hora">
          <i class="fa fa-clock-o"></i>
          <label class="text_hour"> 07:00 </label>
          <br>
          <i class="fa fa-clock-o"></i>
          <label class="text_hour_finish"> 17:00 </label>
        </div>
      </div>
      <div class="ubicacion">
        <i class="fa fa-map-marker"></i>
        <label class="lbl_address"> 
          Calle 00 # 00 - 00, 
        </label>
        <label class="lbl_city">
          Medell&iacute;n,
        </label>
        <label class="lbl_country">
          Colombia
        </label>
        <br>
        <label class="lbl_place"> 
          Lugar 
        </label>
      </div>
    </div>
  </section>

  <div class="section destacados" style="background:#f2f2f2;">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="blog-d">
            <p>COMPARTIR POR REDES SOCIALES &nbsp; &nbsp;
              <span>
                <a class="text-facebook no_share"> <i class="fa fa-facebook"></i> </a>
                <a class="text-twitter no_share"> <i class="fa fa-twitter"></i> </a>
                <a class="text-linkedin no_share"> <i class="fa fa-linkedin"></i> </a>
                <a class="text-google no_share"> <i class="fa fa-google-plus"></i> </a>
              </span>
            </p>
            <h3 class="review txt_gray"> Resumen de tu evento </h3>
            <div class="video vy hidden">
              <iframe class="text-youtube"  id="ytplayer" type="text/html" width="200" height="100%"
                src="https://www.youtube.com/embed/5NV6Rdv1a3I" frameborder="0" allowfullscreen>
              </iframe>
            </div>
            <div class="video vm hidden">
              <iframe  class="text-vimeo" src="https://player.vimeo.com/video/25224442" width="200" height="100%" 
                webkitallowfullscreen mozallowfullscreen allowfullscreen>
              </iframe>
            </div>
            <div class="blog-text">
              <p class="text_description">
                En este lugar coloca la descripci&oacute;n de tu evento.
              </p>
              
              <div class="row">
                <div class="galeria hidden">
                  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators divli"></ol>
                    <div class="carousel-inner divimg" role="listbox"></div>
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>                  
                </div>
                <div class="clearfix"></div>
                <br />
                <div class="clearfix"></div>
                <div class="col-md-12"> 
                  <h5 class="txt_gray">UBICACIÓN DEL EVENTO</h5>
                  <div class="map_canvas" style="height:350px;"></div>
                </div>
                <div class="clearfix"></div>
                <br />
                <div class="col-md-12">
                  <h5 class="txt_gray">T&Eacute;RMINOS Y CONDICIONES</h5>
                  <p class="text_terms">En esta parte agrega los t&eacute;rminos y condiciones de tu evento</p>
                  <br />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="blog-d-right">
            <ul class="blog-d-right00 space_tickets">
              <div id="space_ticket_free"></div>
              <div id="space_ticket_pay"></div>
              <div id="space_ticket_crowfundig"></div>
            </ul>
          </div>
          <div class="opor">
            <h5 class="txt_gray">ORGANIZADO POR</h5>
            <div class="blog-d-right">
              <div class="blog-d-right-w">
                <h5><?php echo Yii::$app->user->identity->username.' '.Yii::$app->user->identity->last_name ;?> </h5>
                <div style="background-size:100% 100%;background-image:url('<?php echo Yii::getAlias('@web').Yii::$app->user->identity->user_image; ?>');background-repeat: no-repeat;" class="blog-d-right-w-img img_p"></div>
                <h6>Email</h6>
                <a target="_blank" href="mailto:<?php echo Yii::$app->user->identity->email;?>"><?php echo Yii::$app->user->identity->email;?></a> <br />
                <br />
                <div class="blog-d-right-w-img">
                  <a target="_blank" href="<?php echo Yii::$app->user->identity->user_facebook;?>" class="text-facebook"> 
                    <i class="fa fa-facebook"></i> 
                  </a>
                  <a target="_blank" href="<?php echo Yii::$app->user->identity->user_twitter;?>" class="text-twitter"> 
                    <i class="fa fa-twitter"></i> 
                  </a>
                  <a target="_blank" href="<?php echo Yii::$app->user->identity->user_youtube;?>" class="text-you"> 
                    <i class="fa fa-youtube-play"></i> 
                  </a>
                  <a target="_blank" href="<?php echo Yii::$app->user->identity->user_google;?>" class="text-google"> 
                    <i class="fa fa-google-plus"></i> 
                  </a>
                 </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <!-- End Team Members -->
    </div>
    <!-- .container -->
  </div>
</div>

<?php
  if($message == 1)
  { ?>
    <script>
    swal({
      title: "Oh! no",
      text: "Tu evento no se pudo publicar. Revisa la información que ingresaste.",
      type: "error"
    });
    </script>
  <?php
  }
  else
    if($message == 2)
    {
    ?>
    <script>
      swal("Muy bien", "Tu evento ha sido publicado", "success")
    </script>
    <?php
    }
    else
      if($message == 3)
      { ?>
        <script>
        swal({
          title: "Oh! no",
          text: "Tu evento no se pudo guardar. Revisa la información que ingresaste.",
          type: "error"
        });
        </script>
      <?php
      }
      else
        if($message == 4)
        {
        ?>
        <script>
          swal("Muy bien", "Tu evento ha sido guardado", "success")
        </script>
        <?php
        }
        else
          if($message == 5)
          {
          ?>
          <script>
            swal({
              title: "Oh! no",
              text: "Debe seleccionar un archivo o ingresar un destinatario.",
              type: "error"
            });
          </script>
          <?php
          }
        else
          if($message == 6)
          {
          ?>
          <script>
            swal("Muy bien", "Tu correo fue enviado", "success")
          </script>
          <?php
          }
?>

<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/bootstrap-clockpicker.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/script_maps.js"></script>

<script>
  $(document).ready(function(){
    new Clipboard('.btn-url');
    $(".public").click(function(){
      $("#form-create").submit();
    });

    $(".btn_send_email").click(function(){
      $("#mail-form").submit();
    });

    $('.save').click(function(){
      var form = $("#form-create").clone();
      $("#save-form").submit();
    });
    $(".trash").click(function(){
      $(".imgtrash").remove();
    });
  });

  $('.clockpicker').clockpicker({
    afterDone: function() {
      $(".text_hour").html($(".event-starthour").val());
      $(".text_hour_finish").html($(".event-endhour").val());
    }
  });
</script>