<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


$this->title = 'Voy A Todo | Contacto';

?>

<!-- Contenido de la página -->

<style>
#contact{
    margin-top: 70px !important;
}
</style>

<div class="section destacados">
  <div class="container container-update"> 
    <div class="row">
      <div class="text-center">
        <?php $form = ActiveForm::begin([
          'action' => Yii::getAlias('@web') . "/site/contact",
          'id'=> "contact",
          ]); ?>
          <h2 class="ttlwhite">CONTACTANOS</h2>
          <h3 class="ttlwhite">Desde aqu&iacute; puedes enviarnos mensajes</h3>
        </div> 

        <div class="col-md-6 col-md-offset-3">
          <div class="logi-box logicontact">
            <h3>CONTACTO</h3>
            <div class="form-group">
              <div class="controls top">
                <label>NOMBRE</label>    
                <?= $form->field($model, "name")->input("text", ['placeholder' => 'Ingrese su nombre', 'class' => 'form-control'])->label(false) ?>
              </div>
            </div>
            <div class="form-group">
              <div class="controls">
                <label>CORREO</label>
                <?= $form->field($model, "email")->input("email", ['placeholder' => 'jsonde@hotmail.com', 'class' => 'form-control event-name'])->label(false) ?>
              </div>
            </div>
            <div class="form-group">
              <div class="controls">
                <label>MENSAJE</label>    
                <?= $form->field($model, "body")->input("textarea", ['placeholder' => 'Ingrese su mensaje', 'class' => 'form-control event-name'])->label(false) ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12 top">
                <center>
                  <a id="message" class="btn btn-yellow btn-block send_message" >ENVIAR MENSAJE</a>
                </center>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <!-- End Team Members --> 
    </div>
    <!-- .container --> 
  </div>

  <?php
    if($message == 1){ ?>
      <script>
      swal("Oh! no", "Tu mensaje no fue enviado", "error");
      </script>
    <?php }
    if($message == 2){?>
      <script>
        swal("Muy bien", "Mail recibido exitosamente", "success");
      </script>
  <?php }?>

  <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

  <script>
    $(document).ready(function(){
      $("#message").click(function(){
        $("#contact").submit();
      });
    });
  </script>