<?php

$this->title = 'Voy A Todo | Favoritos';

?>
  
  <section id="inner01">
    <div id="main-slide" class="carousel00 slide slide_galery" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
        <div class="slider-content">
          <div class="col-md-12 text-center">
            <h2 class="animated3"> Esta es la lista de tus Eventos Favoritos </h2>
            <h3 class="animated3"> PRUEBALA TAMBI&Eacute;N EN TU SMARTPHONE </h3>
          </div>
        </div>
      </div>
    </div>
  </section>  
  <div class="section destacados">
    <div class="container container-update"> 
      <div class="row">
        <?php
          if(empty($favorites))
          { ?>
          <div class="big-title text-center">
            <h1>A&Uacute;N NO TIENES <span>eventos favoritos</span></h1>
          </div>
          <img class="img-face-sad" src="<?php echo Yii::getAlias('@web')?>/images/face-sad.png" alt="face-sad">
          <?php  
          }
          else
          { ?>
            <div class="big-title text-center">
              <h1>lista de tus <span>eventos favoritos</span></h1>
            </div>
            <?php
            foreach ($favorites as $event) 
            {
              ?>
              <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="team-member modern">
                  <a href="<?php echo Yii::getAlias('@web').'/evento/v/'.$event->getFkevent0()->one()->event_url ?>"> 
                    <div class="member-photo imgevent" style="background-image: url('<?php echo Yii::getAlias('@web').$event->getFkevent0()->one()->event_image; ?>');background-size: 100% 100%;">
                      <?php
                        if($event->getFkevent0()->one()->getVtTtickets()->one())
                        {
                          ?>
                            <div class="member-name color0<?php echo $event->getFkevent0()->one()->getVtTtickets()->one()->getFktipetickect0()->one()->pktypetickect; ?>">
                              <span><?php echo $event->getFkevent0()->one()->getVtTtickets()->one()->getFktipetickect0()->one()->typeticket_name; ?></span> 
                            </div>
                          <?php
                        }
                        else
                        {
                          ?>
                            <div class="member-name color04">
                              <span>LIBRE</span> 
                            </div>
                          <?php
                        }
                      ?>
                    </div>
                  </a>
                  <div class="member-info">
                    <div class="post-date">
                      <?php
                        date_default_timezone_set('America/Bogota');
                        echo date('d M, Y. H:i',strtotime($event->getFkevent0()->one()->event_stardate.$event->getFkevent0()->one()->event_starthour));
                      ?>
                    </div>
                    <h2><a href="<?php echo Yii::getAlias('@web').'/evento/v/'.$event->getFkevent0()->one()->event_url; ?>"> <?php echo $event->getFkevent0()->one()->event_name; ?></a></h2>
                    <div class="post-date"><?php echo $event->getFkevent0()->one()->event_address; ?> </div>
                  </div>  
                  <div class="member-socail"> 
                    <?php
                      $icon = $event->getFkevent0()->one()->getFkcategory10()->one()->category_pathicon;
                      $name = $event->getFkevent0()->one()->getFkcategory10()->one()->category_name;
                    ?> 
                    <a class="icon05" style="background: url('<?php echo Yii::getAlias('@web').$icon; ?>') center no-repeat;" data-toggle="tooltip" title="<?php echo $name; ?>" data-placement="bottom"></a>
                    <a class="icon04 btn_favorites <?php echo 'btn_favorites'.$event->getFkevent0()->one()->pkevent; ?>" data-id="<?php echo $event->getFkevent0()->one()->pkevent; ?>" data-toggle="tooltip" title="Favoritos" data-placement="bottom" style="background: url(../images/icon-favorite-blue.png) center no-repeat;"></a> 
                    <a class="icon03" data-toggle="tooltip" title="Compartir" data-placement="bottom"></a> 
                    <a class="link-button" href="<?php echo Yii::getAlias('@web').'/evento/v/'.$event->getFkevent0()->one()->event_url; ?>">ver m&aacute;s</a> 
                  </div>
                </div>
              </div>
            <?php
            }
          }
        ?>
      </div>
      <!-- End Team Members --> 
    </div>
    <!-- .container --> 
  </div>
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>