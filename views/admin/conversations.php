<?php

$this->title = 'Voy A Todo | Conversaciones';
use app\models\VTTmessage;

?>
  
<section id="inner01">
  <div id="main-slide" class="carousel00 slide slide_galery" data-ride="carousel">
    <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
      <div class="slider-content">
        <div class="col-md-12 text-center">
          <h2 class="animated3" style="margin-top: 0px"> Esta es toda la lista de Mensajes</h2>
          <h3 class="animated3"> <span>PRUEBALA TAMBI&Eacute;N EN TU SMARTPHONE</span> </h3>
        </div>
      </div>
    </div>
  </div>
</section>  
<div class="section destacados">
  <div class="container container-update"> 
    <div class="row">
      <?php
          if(empty($messages))
          { ?>
          <div class="big-title text-center">
            <h1>A&Uacute;N NO HAY MENSAJES DE <span>PATROCINADORES Y/O USUARIOS</span></h1>
          </div>
          <img class="img-face-sad" src="<?php echo Yii::getAlias('@web')?>/images/face-sad.png" alt="face-sad">
          <?php  
          }
          else
          { 
          ?>
            <h4 class="align_left">MENSAJES</h4>
            <div class="new_table table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <td><p>EVENTO</p></td>
                    <td><p>USUARIO</p></td>
                    <td><p>PATROCINADOR</p></td>
                    <td><p>MENSAJES PENDIENTES</p></td>
                    <td></td>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $cont = 0;
                  foreach ($messages as $message) 
                  {
                  ?>
                    <tr>
                      <td>
                        <?php 
                          echo $message->getFkproposal0()->one()->getFkevent0()->one()->event_name;
                        ?>
                      </td>
                      <td>
                        <?php 
                          $user = $message->getFkproposal0()->one()->getFkevent0()->one()->getFkuser0()->one();
                          echo $user->username.' '.$user->last_name;
                        ?>
                      </td>
                      <td>
                        <?php 
                          $sponsor = $message->getFkproposal0()->one()->getFkuser0()->one();
                          echo $sponsor->username.' '.$sponsor->last_name;
                        ?>
                      </td>
                      <td>
                        <center class="number_center">
                          <?php 
                            $msg = VTTmessage::find()
                              ->where("fkstatus =:fkstatus", [":fkstatus" => 6])
                              ->andWhere("fkproposal =:fkproposal", [":fkproposal" => $message->fkproposal])
                              ->all(); 
                            echo count($msg);
                          ?>
                        </center>
                      </td>
                      <td><a class="btn_ok watch_messages" data-name-event="<?php echo $message->getFkproposal0()->one()->getFkevent0()->one()->event_name;?>" name="<?php echo $message->fkproposal;?>" data-name-user="<?php $user = $message->getFkproposal0()->one()->getFkevent0()->one()->getFkuser0()->one();
                          echo $user->username.' '.$user->last_name;?>" data-name-sponsor="<?php $sponsor = $message->getFkproposal0()->one()->getFkuser0()->one();
                          echo $sponsor->username.' '.$sponsor->last_name;?>">VER</a></td>
                    </tr>
                    <?php
                    $cont += count($msg);
                  }?>
                  <tr class="bluish">
                    <td class="ttl_blue">TOTAL DE MENSAJES</td>
                    <td class="ttl_blue">
                      <?php 
                        echo $cont;
                      ?>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div> 
          <?php
          }
      ?>
    </div>
    <div class="form-group new_chat"></div>
    <!-- End Team Members --> 
  </div>
  <!-- .container --> 
</div>

<!-- Modal -->
<div class="modal fade" id="modal-edit-message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar mensaje</h4>
      </div>
      <div class="modal-body content_message">
      </div>
      <div class="modal-footer">
        <a class="btn_chat btn_modal_save">Guardar</a>
      </div>
    </div>
  </div>
</div>
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>