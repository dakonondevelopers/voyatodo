<?php

$this->title = 'Voy A Todo | Comisiones';

?>
  
  <section id="inner01">
    <div id="main-slide" class="carousel00 slide slide_galery" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
        <div class="slider-content">
          <div class="col-md-12 text-center">
            <h2 class="animated3" style="margin-top: 8px !important;"> Desde aqu&iacute; puedes hacer el manejo de los par&aacute;metros de Voy A Todo </h2>
            <h3 class="animated3"> <span>PRUEBALA TAMBI&Eacute;N EN TU SMARTPHONE</span> </h3>
          </div>
        </div>
      </div>
    </div>
  </section>  

  <div id="main-slide0" class="carousel00 slide">
    <div class="item01">
      <div class="slider-content0">
        <div class="col-md-12 text-center">
          <div class="col-md-4 hidden-xs"> </div>
          <div class="col-md-4">
            <div class="logi-box">
              <h3>PAR&Aacute;METROS</h3>
              <?php 
                foreach ($comisions as $value) 
                {
                  ?>
                  <h4>COMISIONES</h4>
                  <p>Valores en porcentaje </p> 
                  <form>
                    <div class="form-group">
                      <div class="controls">
                        <label>BOLETAS COMPRADAS</label>
                        <input type="number" value="<?php echo $value['config_comision1']; ?>" class="email requiredField val_1">   
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="controls">
                        <label>PRODUCTOS COMPRADOS</label>
                        <input type="number" value="<?php echo $value['config_comision4']; ?>" class="email requiredField val_6">   
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="controls">
                        <label>PATROCINIO</label> 
                        <input type="number" value="<?php echo $value['config_comision2']; ?>" class="email requiredField val_2">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="controls">
                        <label>PUBLICIDAD</label> 
                        <input type="number" value="<?php echo $value['config_comision3']; ?>" class="email requiredField val_3">
                      </div>
                    </div>
                    <h4>CROWFUNDING</h4>
                    <p>D&iacute;as hasta la &uacute;ltima recolecci&oacute;n </p>
                    <div class="form-group">
                      <div class="controls">
                        <label>D&Iacute;AS</label> 
                        <input type="number" value="<?php echo $value['config_days']; ?>" class="email requiredField val_4">
                      </div>
                    </div>
                    <p>Valor m&iacute;nimo de descuento en porcentaje</p>
                    <div class="form-group">
                      <div class="controls">
                        <label>VALOR</label> 
                        <input type="number" value="<?php echo $value['config_discount_crowfunding']; ?>" class="email requiredField val_5">
                      </div>
                    </div>
                    <a type="submit" id="submit" class="btn-system01 btn_save_comisions">GUARDAR</a>
                  </form>
                  <?php
                } 
              ?>
            </div>
          </div>
          <div class="col-md-4 hidden-xs"> </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>

<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>