<?php

$this->title = 'Voy A Todo | Usuarios';

?>
  
  <section id="inner01"> 
    <!-- Carousel -->
    <div id="main-slide" class="carousel00 ver-evento slide" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
        <div class="slider-content">
          
          <div class="container-fluid">
            <div class="col-md-12 text-center">
              <h2 style="margin-top: 0px !important;">Esta es la lista de todos los usuarios</h2>
              <h3 class="help-block">ENCUENTRA A LOS USUARIOS EN EL BUSCADOR</h3>
              <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 col-lg-offset-1 top_10">
                <select class="form-control slc_rol">
                  <option value="">Seleccione tipo de usuario</option>
                  <option value="1">Administrador</option>
                  <option value="2">Patrocinador</option>
                  <option value="3">Usuario</option>
                  <option value="4">Proveedor</option>
                  <option value="5">Todos</option>
                </select>
              </div>
              <div class="col-lg-1 col-md-1 col-sm-6 col-xs-6 top_10">
                <a class="btn btn-sm xsbtn btn-block visible-xs visible-md visible-lg btn_filter_users">Buscar</a>
                <a class="btn cargar_ visible-sm btn_filter_users">Buscar</a>
              </div>
            </div>
          </div><!--row-->

        </div>
      </div>
    </div>
    <!-- /carousel --> 
  </section>
  <div class="section destacados">
    <div class="container container-update"> 
      <div class="row">
        <?php
            if(empty($users))
            { ?>
            <div class="big-title text-center">
              <h1>A&Uacute;N NO HAY <span> USUARIOS</span></h1>
            </div>
            <img class="img-face-sad" src="<?php echo Yii::getAlias('@web')?>/images/face-sad.png" alt="face-sad">
            <?php  
            }
            else
            {
            ?>
              <h4 class="align_left">USUARIOS</h4>
              <div class="table-responsive new_table">
                <table class="table">
                  <thead>
                    <tr>
                      <td><p>NOMBRES</p></td>
                      <td><p>APELLIDOS</p></td>
                      <td><p>EMAIL</p></td>
                      <td><p>TEL&Eacute;FONO 1</p></td>
                      <td><p>TEL&Eacute;FONO 2</p></td>
                      <td><p>TIPO DE USUARIO</p></td>
                      <td><p>CALIFICACI&Oacute;N DEL USUARIO</p></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $cont = 0;
                    $tipo = '';
                    foreach ($users as $user) 
                    {
                    ?>
                      <tr>
                        <td><?php echo $user->username; ?></td>
                        <td><?php echo $user->last_name; ?></td>
                        <td><?php echo $user->email; ?></td>
                        <td><?php echo $user->user_phone1; ?></td>
                        <td><?php echo $user->user_phone2; ?></td>
                        <?php
                            if ($user->rol == 1)
                              $tipo = 'Administrador';
                            else 
                              if ($user->rol == 2)
                                $tipo = 'Patrocinador';
                              else
                                if($user->rol == 3)
                                  $tipo = 'Usuario';
                                else
                                  if($user->rol == 4)
                                    $tipo = 'Proveedor';
                        ?>
                        <td><?php echo $tipo; ?></td>
                        <td style="text-align: center;"><?php echo $user->user_calification; ?></td>
                      </tr>
                    <?php
                      $cont++;
                    }?>
                    <tr class="bluish">
                      <td class="ttl_blue">TOTAL DE USUARIOS</td>
                      <td class="ttl_blue"><?php echo $cont; ?></td>
                    </tr>
                  </tbody>
                </table>
              </div> 
            <?php
            }
        ?>
      </div>
      <!-- End Team Members --> 
    </div>
    <!-- .container --> 
  </div>
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>