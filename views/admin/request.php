<?php

$this->title = 'Voy A Todo | Solicitudes';

?>
  
  <section id="inner01">
    <div id="main-slide" class="carousel00 slide slide_galery" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
        <div class="slider-content">
          <div class="col-md-12 text-center" style="margin-top: -20px;">
            <h2 class="animated3" style="font-size: 22px !important;color: #FFF !important;"> Esta es toda la lista de Solicitudes </h2>
            <h3 class="animated3"> <span>PRUEBALA TAMBI&Eacute;N EN TU SMARTPHONE</span> </h3>
          </div>
        </div>
      </div>
    </div>
  </section>  
  <div class="section destacados">
    <div class="container container-update"> 
      <div class="row">
        <?php
            if(empty($request))
            { ?>
            <div class="big-title text-center">
              <h1>A&Uacute;N NO HAY <span>SOLICITUDES</span></h1>
            </div>
            <img class="img-face-sad" src="<?php echo Yii::getAlias('@web')?>/images/face-sad.png" alt="face-sad">
            <?php  
            }
            else
            {
            ?>
              <h4 class="align_left">SOLICITUDES</h4>
              <div class="table-responsive new_table">
                <table class="table">
                  <thead>
                    <tr>
                      <td><p>NOMBRES</p></td>
                      <td><p>APELLIDOS</p></td>
                      <td><p>EMAIL</p></td>
                      <td><p>TEL&Eacute;FONO 1</p></td>
                      <td><p>TEL&Eacute;FONO 2</p></td>
                      <td><p>TIPO DE USUARIO</p></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $cont = 0;
                    $tipo = '';
                    foreach ($request as $user) 
                    {
                    ?>

                      <tr>
                        <td><?php echo $user->username; ?></td>
                        <td><?php echo $user->last_name; ?></td>
                        <td><?php echo $user->email; ?></td>
                        <td><?php echo $user->user_phone1; ?></td>
                        <td><?php echo $user->user_phone2; ?></td>
                        <?php 
                          if ($user->rol == 2)
                            $tipo = 'Patrocinador';
                          else
                            if($user->rol == 4)
                              $tipo = 'Proveedor';
                            else
                              if($user->rol == 3)
                                $tipo = 'Empresa';
                        ?>
                        <td><?php echo $tipo; ?></td>
                        <td><a class="btn_ok accept" name="<?php echo $user->id; ?>">ACEPTAR</a></td>
                        <td><a class="btn_ok refuse" name="<?php echo $user->id; ?>">RECHAZAR</a></td>
                      </tr>
                    <?php
                      $cont++;
                    }?>
                    <tr class="bluish">
                      <td class="ttl_blue">TOTAL DE SOLICITUDES</td>
                      <td class="ttl_blue"><?php echo $cont; ?></td>
                    </tr>
                  </tbody>
                </table>
              </div> 
            <?php
            }
        ?>
      </div>
      <!-- End Team Members --> 
    </div>
    <!-- .container --> 
  </div>
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>