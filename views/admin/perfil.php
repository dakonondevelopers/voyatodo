<?php
  
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\BaseHtml;

$this->title = 'Voy A Todo | Mi Perfil';

?>

  <section id="inner01">
    <div id="main-slide" class="carousel00 slide slide_galery" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02 heigtht_img" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
        <div class="slider-content">
          <?php $form = ActiveForm::begin([
                'method' => 'post',
                'id' => 'form-create',
                'enableClientValidation' => true,
                'enableAjaxValidation' => true,
                'options' => ['enctype' => 'multipart/form-data'],
                'class'=> 'contact-form',
              ]);
            ?>
          <div class="col-md-12 text-center hitop">
            <div class="ttlperfil"><?php echo Yii::$app->user->identity->username." ".Yii::$app->user->identity->last_name;?></div>                                 
        </div>
        <div class="col-lg-2 col-lg-offset-5 col-sm-2 col-sm-offset-5 col-xs-12">
          <center> 
            <div  class="fa_circle field_photo" style=""></div>    
          </center>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
          <label for="files" data-role="button" class="btn btn_green top" data-inline="true" data-mini="true" data-corners="false">Cambiar foto</label>
          <input type='file' accept="image/*" class="change_photo" style="margin-top: -26px;">
        </div>
        <div class="col-lg-6 col-lg-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
          <input id="input-21d" value="4" type="number" readonly="true" class="rating" min=0 max=5 step=1 data-size="xs">          
        </div>
        <?php $form->end() ?> 
      </div>
    </div>
  </section>  
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
  <div class="container-fluid">
    <div class="col-md-12 col-sm-12 big-title text-center">
      <h1 class="">DATOS PERSONALES</h1> <p class="help-block">Información básica de la cuenta</p>
    </div>

    <div class="col-md-4 col-xs-12 col-md-offset-1">

      <div class="col-md-12 col-xs-12 datos">
        <div class="separator">
          DATOS PERSONALES DEL USUARIO
        </div>
      </div>

      <form class="form-horizontal">
         <div class="form-group">
           <label class="col-md-4 control-label">NOMBRE</label>
           <div class="col-md-8">
             <input type="text" class="form-control">
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <label class="col-md-4 control-label">APELLEDO</label>
           <div class="col-md-8">
             <input type="text" class="form-control">
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <label class="col-md-4 control-label">E-MAIL</label>
           <div class="col-md-8">
             <input type="text" class="form-control">
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <label class="col-md-4 control-label">TELÉFONO</label>
           <div class="col-md-8">
             <input type="text" class="form-control">
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <label class="col-md-4 control-label">EMPRESA</label>
           <div class="col-md-8">
             <input type="text" class="form-control">
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <label class="col-md-4 control-label">WEB</label>
           <div class="col-md-8">
             <input type="text" class="form-control">
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <label class="col-md-4 control-label">CONTRASEÑA</label>
           <div class="col-md-8">
             <input type="text" class="form-control">
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <label class="col-md-4 control-label">REPETIR CONTRASEÑA</label>
           <div class="col-md-8">
             <input type="text" class="form-control">
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <div class="col-md-12">
             <a href="#" class="btn btn_sm btn-block">GUARDAR INFORMACI&Oacute;N</a>
           </div>
         </div><!--form-group-->
      </form><!--FORM-->      
    </div>

    <div class="col-md-4 col-md-offset-1">

      <div class="col-md-12 col-xs-12 datos">
        <div class="separator">
          EDITAR CUENTA PARA RECIBIR PAGOS       
        </div>
        <p>VoyATodo enviará los pagos recibidos en la compra de tus productos vendidos durante la semana. Podrás verla en tu extracto 
        como VOYATODO SAS. Recuerda que la transferencia tiene un costo de 15.000 COP
      </p>
      </div>

      

      <form class="form-horizontal">
        <div class="form-group">
           <div class="col-md-12">
             
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <label class="col-md-4 control-label">BANCO</label>
           <div class="col-md-8">
             <input type="text" class="form-control">
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <label class="col-md-4 control-label">NOMBRE TITULAR</label>
           <div class="col-md-8">
             <input type="text" class="form-control">
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <label class="col-md-4 control-label">CEDULA</label>
           <div class="col-md-8">
             <input type="text" class="form-control">
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <label class="col-md-4 control-label">N° CUENTA</label>
           <div class="col-md-8">
             <input type="text" class="form-control">
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <label class="col-md-4 control-label">TIPO CUENTA</label>
           <div class="col-md-8">
             <input type="text" class="form-control">
           </div>
         </div><!--form-group-->
         <div class="form-group">
           <div class="col-md-12">
             <a href="#" class="btn btn_sm btn-block">ACTUALIZAR CUENTA</a>
           </div>
         </div><!--form-group-->
      </form><!--form-->
    </div>
  </div><!--container-->

<script src="<?php echo Yii::getAlias('@web') ?>/js/star-rating.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(".change_photo").change(function(event) {
      $.each(event.target.files, function(index, file) {
        var reader = new FileReader();
          reader.onload = function(event) {  
          object = {};
          object.filename = file.name;
          object.data = event.target.result;
          $(".field_photo").attr("style","background: url(" + object.data + ") center no-repeat; background-size: auto 100%");
        };  
        reader.readAsDataURL(file);
      });
    });
</script>