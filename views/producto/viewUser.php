<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Voy A Todo | Producto/Servicio';

?>


<div id="container">
  <!-- Start Home Page Slider -->
  <section id="inner01"> 
    <!-- Carousel -->
    <div id="main-slide" class="carousel00 ver-evento slide" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
        <div class="slider-content">
          <div class="container-fluid">
            <div class="col-md-12 text-center">
              <h2 style="margin-top: 0px !important;">¿Qu&eacute; necesitas para tu evento?</h2>
              <h3 class="help-block">ENCUENTRA LO QUE NECESITAS EN EL BUSCADOR</h3>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 col-lg-offset-1 top_10">
              <select class="form-control">
                <?php
                echo "<option>Seleccione Categoría </option>";
                foreach ($categories as $category) 
                {
                  echo "<option value=".$category->pkcategoryproduct.">".$category->categoryproduct_name."</option>";
                }
                ?>
              </select>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 top_10">
              <select class="form-control">
                <option>FILTRAR POR</option>
              </select>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 top_10">
              <input type="text" placeholder="CIUDAD" class="form-control">
            </div>
            <div class="col-lg-1 col-md-1 col-sm-6 col-xs-12 top_10">
              <a class="btn btn-sm xsbtn btn-block visible-xs visible-md visible-lg btn_search_products">Buscar</a>
              <a class="btn cargar_ visible-sm btn_search_products">Buscar</a>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-lg-offset-1 top_10">
              <a class="btn cargar_ btn-block hidden-xs" data-toggle="modal" data-target="#modal_information"><i class="fa fa-shopping-basket">
              </i> Mi carrito</a><div class="div_car hidden-xs">0</div>
              <a class="btn cargar_ btn-block top hidden-sm hidden-md hidden-lg" data-toggle="modal" data-target="#modal_information"><i class="fa fa-shopping-basket"></i>  Mi carrito</a><div class="div_car hidden-sm hidden-md hidden-lg">0</div>
            </div>
          </div><!--row-->

        </div>
      </div>
    </div>
    <!-- /carousel --> 
  </section>
  <!-- End Home Page Slider -->

  <!-- Modal información de compra -->
  <div class="logi-box box_modal modal fade" id="modal_information" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <h3>PRODUCTOS/SERVICIOS A COMPRAR</h3>
        <div class="blog-d-right">
          <ul class="blog-d-right00">
            <div class="list_products"></div>
          </ul>
        </div>
        <div class="form-group">
          <h5 align="center"><b>Valor total a pagar</h5>
          <h1 align="center" class="ttl_blue total_pay"></h1>
        </div>
        <div class="row">
          <div class="col-md-5">
            <a class="btn cargar_ btn-block btn_next_product" data-toggle="modal" data-target="#modal_buy">SIGUIENTE</a>
          </div>
        </div>
      </div>           
    </div>
  </div>

  <!-- Modal formulario de compra -->
  <div class="logi-box box_modal modal fade" id="modal_buy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <h3>INFORMACI&Oacute;N DE LA COMPRA</h3>
        <form class="body_modal" action="/voyatodo/web/site/registro" method="post">
          <div class="form-group">
            <div class="controls">
              <label>NOMBRE</label>    
              <div class="form-group field-formuserregister-username required">
                <input type="text" id="formuserregister-username" class="email requiredField" name="FormUserRegister[username]" placeholder="Jhon">
                <div class="help-block"></div>
              </div>               
            </div>
          </div>
          <div class="form-group">
            <div class="controls">
              <label>APELLIDO</label>
              <div class="form-group field-formuserregister-last_name required">
                <input type="text" id="formuserregister-last_name" class="email requiredField" name="FormUserRegister[last_name]" placeholder="Smith">
                <div class="help-block"></div>
              </div>               
            </div>
          </div>
          <div class="form-group">
            <div class="controls">
              <label>E-MAIL</label>    
              <div class="form-group field-formuserregister-email required">
                <input type="email" id="formuserregister-email" class="email requiredField" name="FormUserRegister[email]" placeholder="johnsmith@mymail.com">
                <div class="help-block"></div>
              </div>                
            </div>
          </div>
          <div class="form-group">
            <div class="controls">
              <label>TELÉFONO</label>
              <div class="form-group required">
                <input type="tel" id="" class="email" name="" placeholder="312 333 4455">
                <div class="help-block"></div>
              </div>                
            </div>
          </div>
          <div class="form-group">
            <div class="controls">
              <label>CIUDAD</label>
              <div class="form-group required">
                <input type="text" id="" class="email" name="" placeholder="Bogotá">
                <div class="help-block"></div>
              </div>                
            </div>
          </div>
          <div class="form-group">
            <h5 align="center"><b>Valor total a pagar</h5>
            <h1 align="center" class="ttl_blue">200.000 COP</h1>
          </div>

          <div class="row">
            <div class="col-md-5 col-md-offset-1">
              <img src="<?php echo Yii::getAlias('@web') ?>/images/pay.png">
            </div>
            <div class="col-md-5">
              <a class="btn cargar_ btn-block btn_pay_product" data-toggle="modal" data-target="#modal_calification">PAGAR</a>
            </div>
          </div>
        </form>   
      </div>           
    </div>
  </div>

  <!-- Modal calificación -->
  <div class="logi-box box_modal modal fade" id="modal_calification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <h2>Un &uacute;ltimo paso!</h2>
        <p>Esta calificaci&oacute;n puede ayudar a otros usuarios a tomar la decisi&oacute;n de compra</p>          
        <h3 class="ttl_blue">CALIFICAR PROVEEDOR</h3>            
        <input id="input-21d" value="2" type="number" class="rating" min=0 max=5 step=1 data-size="sm">
        <hr>
        <a class="eventos btn_create">CALIFICAR</a>
        <p>23 Calificaciones</p>          
      </div>
    </div>
  </div> 

  <div class="container-fluid" style="background:#f2f2f2;">
    <div class="col-md-12 product-description-row">
      <div class="row top">
        <div class="col-md-12">
          <h2><?php echo $product->product_name; ?></h2>
          <h3 class="txt_review"><?php echo $product->product_review; ?></h3>
          <a class="category_product" href="<?php echo Yii::getAlias('@web').'/producto/categoria/'.$product->getFkcategory0()->one()->categoryproduct_name; ?>"><?php echo $product->getFkcategory0()->one()->categoryproduct_name; ?></a>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php
            if(!empty($galery))
            {?>
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <?php
                $cont = count($galery);
                ?>
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <?php
                  for ($i=1; $i < $cont; $i++) 
                  { 
                    ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" ></li>
                    <?php
                  }
                  ?>
                </ol>
                <div class="carousel-inner" role="listbox">
                  <?php
                  for ($j=0; $j < $cont; $j++) 
                  {
                    if($j == 0)
                    {
                      ?>
                      <div class="item active">
                        <img src="<?php echo Yii::getAlias('@web').$galery[$j]['image_path']; ?>" alt="galery_event" style="width: 100%">
                      </div>
                      <?php
                    }
                    else
                    {
                      ?>
                      <div class="item">
                        <img src="<?php echo Yii::getAlias('@web').$galery[$j]['image_path']; ?>" alt="galery_event" style="width: 100%">
                      </div>
                      <?php
                    }
                    ?>
                    <?php
                  }
                  ?>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>                  
              <?php
            }
          ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <h4>DESCRIPCI&Oacute;N</h4>
          <p><?php echo $product->product_description; ?></p>
          <div class="row payment-details top">
            <div class="col-md-6 col-sm-6">
              <div class="price">
                <h2 class="ttl_blue"> $ <?php echo number_format($product->product_value); ?> COP</h2>
              </div>
            </div>
            <div class="col-md-4 col-sm-4 top_10">
              <div class="search-input">
                <select class="form-control qty_product">
                  <?php
                  $options = '';
                  if($product->product_qty == '')
                    $options .= '<option>SIN LÍMITE </option>';
                  else 
                  {
                    for ($i=0; $i < $product->product_qty; $i++) 
                      $options .= '<option>'.$i.'</option>';
                  }
                  echo $options; 
                  ?>
                </select>
              </div>
            </div>
          </div><!--row-->
          <div class="row top">
            <div class="col-md-6 col-sm-6">
              <div class="addcart">
                <a class="eventos carrito" name="<?php echo $product->pkproduct;?>">AGREGAR AL CARRITO</a>
              </div>
            </div>
            <div class="col-md-4 col-sm-4 top_10">
              <div class="search-input">
                <select class="form-control date_product">
                  <?php
                    $dates = '';
                    $availability = $product->getVtTavailabilities()->all();
                    foreach ($availability as $date)
                      $dates .= '<option>'.$date->availability_date.'</option>';
                    echo $dates; 
                  ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row top">
            <div class="col-md-6 col-sm-6">
              <div class="buynow">
                <?php 
                if(\Yii::$app->user->isGuest)
                {
                  ?>
                  <a href="<?php echo Yii::getAlias('@web') ?>/site/login" class="eventos">COMPRAR AHORA</a>
                  <?php
                }
                else
                {
                  ?>
                  <a class="eventos btn_buy_product" data-toggle="modal" data-target="#modal_information">COMPRAR AHORA</a>
                  <?php
                }
                ?>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 top_10">
              <img src="<?php echo Yii::getAlias('@web') ?>/images/pay.png">
            </div>
          </div><!--row-->
          <div class="row top">
            <div class="col-xs-12 col-sm-12 visible-xs visible-sm">
              <h3>INFORMACI&Oacute;N DEL VENDEDOR</h3>
              <div class="col-md-2">
                <p>
                  Calificaci&oacute;n del Vendedor:
                </p>
              </div>
              <div class="col-md-10">
                <?php 
                  $calification=$product->getFkuser0()->one()->getVtTusercalifications()->all();
                  $sum=0;
                  $cont=0;
                  $total=0;
                  foreach ($calification as $value) 
                  {
                    $sum+=$value->usercalification;
                    $cont++;
                  }
                  if($cont != 0)
                    $total = round($sum/$cont);
                ?>
                <input id="input-21d" value="<?php echo $total; ?>" type="number" readonly="true" class="rating" min=0 max=5 step=1 data-size="xs"> 
              </div>
              <div class="col-md-2">
                <p>Ventas efectuadas:</p>
              </div>
              <div class="col-md-10">
                <p class="ttl_blue">0</p>
              </div>
              <div class="col-md-2">
                <p>
                  Tiempo en Voy a Todo: 
                </p>
              </div>
              <div class="col-md-10">
                <p class="ttl_blue">
                  <?php
                    $date1=$product->getFkuser0()->one()->user_date;
                    $segundos=strtotime($date1) - strtotime(date('Y-m-d'));
                    $days=intval($segundos/60/60/24);
                    $days=abs(floor($days));
                    echo $days." días.";
                  ?>
                </p>
              </div>
            </div>
            <div class="col-md-12 col-sm-12 top_10">
              <h3>T&Eacute;RMINOS Y CONDICIONES</h3>
              <p><?php echo $product->product_terms; ?></p><br />
            </div>
          </div><!--row-->
        </div><!--col-->
        <div class="container-fluid top hidden-sm hidden-xs">
          <div class="col-md-12 col-sm-12">
            <h3>INFORMACI&Oacute;N DEL VENDEDOR</h3>
            <div class="col-md-3" style="text-align:right;">
              <p>
                Calificaci&oacute;n del Vendedor:
              </p>
            </div>
            <?php 
              $calification=$product->getFkuser0()->one()->getVtTusercalifications()->all();
              $sum=0;
              $cont=0;
              $total=0;
              foreach ($calification as $value) 
              {
                $sum+=$value->usercalification;
                $cont++;
              }
              if($cont != 0)
                $total = round($sum/$cont);
            ?>
            <input id="input-21d" value="<?php echo $total; ?>" type="number" readonly="true" class="rating" min=0 max=5 step=1 data-size="xs"> 
            <div class="col-md-3" style="text-align:right;">
              <p>Ventas efectuadas:</p>
            </div>
            <div class="col-md-9">
              <p class="ttl_blue">0</p>
            </div>
            <div class="col-md-3" style="text-align:right;">
              <p>
                Tiempo en Voy a Todo: 
              </p>
            </div>
            <div class="col-md-9">
              <p class="ttl_blue">
                <?php
                  $date1=$product->getFkuser0()->one()->user_date;
                  $segundos=strtotime($date1) - strtotime(date('Y-m-d'));
                  $days=intval($segundos/60/60/24);
                  $days=abs(floor($days));
                  echo $days." días.";
                ?>
              </p>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 top_10">
            <h3>T&Eacute;RMINOS Y CONDICIONES</h3>
            <p><?php echo $product->product_terms; ?></p><br />
          </div>
        </div><!--container-fluid-->
      </div>
    </div><!--row-->        
    <!-- End Team Members --> 
  </div>
</div>

<div class="section purchase crowdfunding">
  <div class="container"> 
    <div class="section-video-content text-center"> 
      <!-- Start Animations Text -->
      <h3 class="h31">CREA TU PROPIO <span>EVENTO</span></h3>
      <p class="help-block">Voy A Todo te d&aacute; las herramientas necesarias para que puedas hacer toda<br />
       la gesti&oacute;n de tu eventos en su s&oacute;lo lugar</p>
       <!-- End Animations Text -->
       <div class="eventos-list eventos-list00"> <a class="eventos" href="<?php echo Yii::getAlias('@web') ?>/account/evento">crear mi evento</a> </div>
     </div>
   </div>
   <!-- .container --> 
 </div>
</div>

<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/script_buy.js"></script>
<script src="<?php echo Yii::getAlias('@web') ?>/js/star-rating.min.js" type="text/javascript"></script>


<script>
  $(document).ready(function () {

    $('.carousel').carousel();

    $('.btn_create').click(function(){
      alert($('#input-21d').val());
    });        
  });
</script>