<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\VTTusercalification;
use app\models\VTTpublicity;

$this->title = 'Voy A Todo | Producto/Servicio';

?>

<!-- Full Body Container -->
<div id="container">
  
  <!-- Start Home Page Slider -->
  <section id="inner01"> 
    <!-- Carousel -->
    <div id="main-slide" class="carousel00 ver-evento slide" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
        <div class="slider-content">
          <div class="col-md-12 text-center">
            <h3 class="animated3"> <span>ESTADO DEL NUEVO PRODUCTO:</span></h3>
            <p class="animated4">
              <a class="eventos btn_save_product">GUARDAR</a> 
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- /carousel --> 
  </section>
  <!-- End Home Page Slider -->

  <!-- Comienza la primera opción del menú -->

  <div class="menu-modal summary-modal dashboard-modal scrollbar" id="opcion1">

    <div class="separator"></div>

    <div class="row">
      <div class="col-md-7 col-sm-6 col-xs-6">
        <h4>RESUMEN DEL PRODUCTO</h4>
      </div>
    </div>
      
    <div class="row">
      <div class="col-md-6 summary-images">
        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/pie.png">
          <p>0</p>
          <p>VISITAS</p>
        </div>

        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/buyers.png">
          <p>12</p>
          <p>COMPRAS</p>
        </div>
      </div>
      <div class="col-md-6 summary-images">
        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/basket.png">
          <p>200.000</p>
          <p>DINERO RECIBIDO</p>
        </div>

        <div>
          <img src="<?php echo Yii::getAlias('@web') ?>/images/assistants.png">
          <p>200</p>
          <p>ASISTENTES</p>
        </div>
      </div>
    </div>

    <div class="separator"></div>
    
    <h4 class="align_left">RESUMEN DE INGRESOS</h4>
    <table class="table">
      <tr>
        <td><p>PRODUCTO</p></td>
        <td><p>VALOR</p></td>
        <td><p>VENDIDAS</p></td>
      </tr>
      <tr>
        <td>-------</td>
        <td>-------</td>
        <td>-------</td>
      </tr>
      <tr>
        <td>--------</td>
        <td>--------</td>
        <td>--------</td>
      </tr>
      <tr>
        <td>TOTAL</td>
        <td>--------</td>
      </tr>
      <tr>
        <td><p>COMISI&Oacute;N</p></td>
        <td><p>--------</p></td>
      </tr>
      <tr class="bluish">
        <td class="ttl_blue">TOTAL A RECIBIR</td>
        <td class="ttl_blue">--------</td>
        <td></td>
      </tr>
    </table>
    <div class="col-md-12 top">
      <a class="btn btn-yellow btn-block">VER HISTORIAL DE VENTAS</a>
    </div>
  </div>

  	<!-- Comienza la segunda opción del menú -->

  <?php $form = ActiveForm::begin([
      'method' => 'post',
      'id' => 'form-advertiser',
      'enableClientValidation' => true,
      'enableAjaxValidation' => true,
      'options' => ['enctype' => 'multipart/form-data'],
    ]);
  ?>
  	<div class="menu-modal information-modal summary-modal dashboard-modal scrollbar" id="opcion2">
      
    	<div class="separator"></div>
      
  		<h3>TUS PRODUCTOS/SERVICIOS </h3>
  		<label for="nombre_evento top">
  			NO PIERDAS TIEMPO Y DINERO
  		</label>
  		<div class="col-md-12 top">
  			<a class="btn btn-yellow btn-block btn_new_product">CREAR PRODUCTO/SERVICIO NUEVO</a>
  		</div>
  		<p class="description_social" style="margin-top: 40px; margin-bottom: 20px;">
  			Crea nuevos productos, edita o borra productos existentes.    
  		</p>

      <div class="separator"></div>

    	<h3>PRODUCTOS CREADOS</h3>

    	<div class="new_product">
      	<?php 
          if(!empty($products))
          {
            foreach ($products as $product) 
            {
            ?>
    					<div class="col-md-8 div_banco top div_update_product" name="<?php echo $product->pkproduct; ?>">
    						<label class="lbl_bnc"><?php echo $product->product_name; ?><br/>
    							<span class="ttl_blue"><?php echo $product->product_value; ?> COP</span>
    						</label>
    					</div>
            <?php
            }
          }
        ?>      		
    	</div>
    </div>

    <div class="menu-modal summary-modal dashboard-modal information-product hidden scrollbar" id="opcion-1">

  	  <div class="separator"></div>

  		<div class="row">
  	    <div class="col-md-7 col-sm-6 col-xs-6">
  	      <h4>INFO. DEL PRODUCTO/SERVICIO</h4>
  	    </div>
  	    <div class="col-md-5">
  	      <a class="btn btn_delete_ticket delete_product"> ELIMINAR </a>
  	    </div>
      </div>
  		<label for="nombre_evento">
  			NOMBRE DEL PRODUCTO/SERVICIO
  		</label>
  		<div class="input-group">
  			<?= $form->field($model, "product_name")->input("text", ['placeholder' => '0/80', 'class' => 'form-control product-name','value' => $product->product_name])->label(false) ?>
        <span class="input-group-addon">
          <span id="cont_name">0/80</span>
        </span>
      </div>
      <label for="nombre_evento">
        RESUMEN DEL PRODUCTO/SERVICIO
      </label>
      <div class="input-group">
        <?= $form->field($model, "product_review")->input("text", ['placeholder' => '0/140', 'class' => 'form-control product-review','value' => $product->product_review])->label(false) ?>
        <span class="input-group-addon">
          <span id="cont_review">0/140</span>
        </span>
      </div>
  		<p class="description_social" style="margin-top: 30px; margin-bottom: 10px;">
  			Resume tu producto en en 140 car&aacute;cteres o menos.   
  		</p>
      <div class="form-group">
        <label class="col-md-12 top">DESCRIPCI&Oacute;N DEL PRODUCTO/SERVICIO</label>
        <div class="col-md-12">
          <?= $form->field($model, 'product_description')->textArea(['class' => 'text-area text-description','value' => $product->product_description])->label(false) ?>
        </div>
      </div>
    	<div class="row">
      	<div class="form-group">
          <label class="col-md-12">IM&Aacute;GEN DEL PRODUCTO/SERVICIO</label>
          <div class="col-md-12">
	          <div class="cargar top_menos btn-block"><label class="lbl_btn">SUBIR IM&Aacute;GEN</label>
	            <?= $form->field($model, "product_image")->fileInput(['accept' => 'image/*','class' => 'eventos archivos upload_img'])->label(false) ?>
	          </div>
            <p class="description_social">
	            El tama&ntilde;o de la im&aacute;gen debe ser superior a 600px
            </p>

        	</div>
      	</div>
        <div class="item col-lg-4 col-sm-4 col-md-4 top20">
          <div class="product">
            <div class="image">
              <div class="quickview">
                <div class="centro">
                  <i class="fa fa-trash-o fa-lg trash"></i>
                </div>
              </div>
              <center>
                <img src="<?php echo Yii::getAlias('@web') ?>/images/system_imgs/1644fee0dd1c9a7ec942524fb8b2e33f650618eb9c0cbd2efagalery_1.jpg" class="imgtrash">
              </center>
            </div>
          </div><!--/product-->
        </div><!--/item-->
    	</div>
      

    	<div class="separator"></div>

    	<h3>CATEGOR&Iacute;A DEL PRODUCTO</h3>
    	<div class="form-group top">
        <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3 top">PRINCIPAL</label>
        <div class="col-md-9 col-sm-9 col-xs-9 top">
          <?php $listData=ArrayHelper::map($categories,'pkcategoryproduct','categoryproduct_name'); ?>
            <?= $form->field($model, 'fkcategory')->dropDownList($listData, 
              [
                'prompt'=>'Seleccione Categoría',
                'class' => 'form-control',
            	])->label(false); ?>
        </div>
      </div> 
      <div class="form-group">
        <label for="nombre_evento" class="col-md-12">T&Eacute;RMINOS Y CONDICIONES</label>
        <div class="col-md-12">
          <?= $form->field($model, 'product_terms')->textArea(['class' => 'text-area text-terms','placeholder' => 'Agrega aquí los términos y condiciones del servicio','value' => $product->product_terms])->label(false) ?>
        </div>
      </div>

      <div class="separator"></div>

    	<h3>PRECIO DEL PRODUCTO/SERVICIO</h3>
    	<div class="row top">
        <div class="col-md-6 col-sm-4 col-xs-4 top">
          <label for="nombre_evento">
            VALOR A COBRAR
          </label>
        </div>
        <div class="col-md-6 col-sm-4 col-xs-4 top">
          <div class="input-group">
            <div class="input-group-addon input-group-addon0">$</div>
            <input type="number" class="form-control product-value align_right" placeholder="300000" />
          </div>
        </div>
        <div class="col-md-6 col-sm-4 col-xs-4 top">
          <label for="nombre_evento" class="lbl_tickets">
            COMISI&Oacute;N
          </label>
        </div>
        <div class="col-md-6 col-sm-4 col-xs-4 top">
          <div class="input-group">
            <input type="text" class="form-control product-comision ttl_blue" readonly="true"/>
          </div>
        </div>
        <div class="col-md-6 col-sm-4 col-xs-4 top">
          <label for="nombre_evento" class="lbl_tickets">
            RECIBIR&Aacute;S
          </label>
        </div>
        <div class="col-md-6 col-sm-4 col-xs-4 top">
          <div class="input-group">
            <div class="input-group-addon input-group-addon0 ttl_blue">$</div>
            <input name="FormAdvertiser[product_value]" type="text" class="form-control product-pay-user" placeholder="28700" value="<?php echo $product->product_value; ?>"/>
          </div>
        </div>
      </div>
      <div class="row top">
        <div class="col-md-12 col-sm-4 col-xs-4">
          <label for="nombre_evento">
            CANTIDAD
          </label>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
          <div class="input-group">
            <input name="FormAdvertiser[product_qty]" type="number" class="form-control product-qty" placeholder="100" value="<?php echo $product->product_qty; ?>"/>
          </div>
        </div>
        <div class="col-md-8 col-sm-4 col-xs-4">
          <input class="inputradio out_limit" type="checkbox"><span class="inputfalso"> <label class="lbl_radio">SIN L&Iacute;MITE</label></span>
        </div>
	    </div>

	    <div class="separator"></div>

      <h3>FECHAS DISPONIBLES</h3>
      <div id="calendar"></div>
      <p>Haz click sobre las fechas que quieras tener disponible el producto</p>
      <input type="hidden"  name="FormAdvertiser[dates]" class="json_dates" value="">
    </div>

    <!-- Comienza la tercera opción del menú -->

    <div class="menu-modal sale-modal summary-modal dashboard-modal mw100 scrollbar" id="opcion3">

      <div class="separator"></div>

      <h3>LISTA COMPRADORES</h3>
      <div class="row">
        <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
          <p class="help-block top">BUSCAR POR NOMBRE, APELLIDO O E-MAIL</p>
          <div class="input-group search">
            <input type="text" class="form-control text-address top_menos">
          </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
          <a id="search_asistent" class="btn cargar_  hidden-sm hidden-xs top_50">BUSCAR</a>
          <a id="search_asistent" class="btn cargar_  btn-block hidden-md hidden-lg top_10">BUSCAR</a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-md-offset-2 ">
          <a id="download_asistent" class="btn cargar_  hidden-sm hidden-xs top_50">DESCARGAR REPORTE</a>
          <a id="download_asistent" class="btn cargar_  btn-block hidden-md hidden-lg top_10">DESCARGAR REPORTE</a>
        </div>
      </div><!--row-->
      <div class="table-resposive">
        <table class="table table-condensed">
          <thead>
            <tr>
              <th>NOMBRE</th>
              <th>APELLIDO</th>
              <th>E-MAIL</th>
              <th>PRODUCTO</th>
              <th>PRECIO</th>
              <th>CANTIDAD</th>
              <th>FECHA COMPRA</th>
              <th>ESTADO</th>
            </tr>
          </thead>
          <tbody class="tbody">
            <tr>
              <td>------</td>
              <td>------</td>
              <td>------</td>
              <td>------</td>
              <td>------</td>
              <td>------</td>
              <td>------</td>
              <td>------</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <!-- Comienza la cuarta opción del menú -->

    <div class="menu-modal bank-modal summary-modal dashboard-modal bank_acount scrollbar" id="opcion4">
      
      <div class="separator"></div>

      <h4 class="h4_left">A&Ntilde;ADIR CUENTA PARA RECIBIR PAGOS</h4>
      <div class="row div_bank">
        <?php 
          if(!empty($userbank))
          {
            foreach ($userbank as $bank) 
            {
            ?>
            <div class="col-md-8 div_banco top div_update_acount" name="<?php echo $bank->pkuserbank; ?>" id="<?php echo $bank->pkuserbank; ?>">
              <label class="lbl_bnc"><?php echo $bank->userbank_tipeacount.' - '. $bank->getFkbank0()->one()->bank_name ?>  
                <br/><span class="ttl_blue"><?php echo $bank->userbank_identification; ?></span>
              </label>
            </div>
            <?php
            }
          }
          else
          {
            ?>
            <div class="col-md-12 top">
              <a class="btn btn-yellow btn-block btn_pay">A&Ntilde;ADIR CUENTA</a>
            </div>
            <?php
          }
        ?>
        <div class="alert alert-danger hidden message" role="alert">
          <strong>No se pudo procesar tu requerimiento</strong>
        </div>
      </div>
      
      <div class="separator"></div>

      <h4 class="h4_left">M&Eacute;TODOS DE PAGO PUBLICIDAD </h4>
      <div class="row div_cards">
        <?php 
          if(!empty($creditcard))
          {
            foreach ($creditcard as $card) 
            {
            ?>
              <div class="col-md-8 div_banco top div_update_card" name="<?php echo $card->pkcreditcard; ?>" id="<?php echo $card->pkcreditcard; ?>">
                <label class="lbl_bnc"><?php echo $card->creditcard_type; ?><br/>
                <span class="ttl_blue">
                  <?php echo "##########".substr($card->creditcard_numbercard, -4); ?>
                </span></label>
              </div>
            <?php
            }
          }
        ?>
        <div id="new"></div>  
        <div class="col-md-12 top">
          <a class="btn btn-yellow btn-block btn_target">A&Ntilde;ADIR TARJETA</a>
        </div>
        <div class="alert alert-danger hidden messagec" role="alert">
          <strong>Tu tarjeta no se guardo</strong>
        </div>
      </div>

      <div class="separator top"></div>

      <h4 class="h4_left">PAGO DE CR&Eacute;DITO ACTUAL</h4>
      <div class="row div_pays">  
        <span class="col-md-12 ttl_blue">0 COP <br/></span><span class="col-md-12"> Pagar con:</span>
        <?php 
          if(!empty($creditcard))
          {
            foreach ($creditcard as $card) 
            {
            ?>
              <div class="col-md-8 div_banco top div_update_card" name="<?php echo $card->pkcreditcard; ?>" id="<?php echo $card->pkcreditcard; ?>">
                <label class="lbl_bnc"><?php echo $card->creditcard_type; ?><br/>
                <span class="ttl_blue">
                  <?php echo "##########".substr($card->creditcard_numbercard, -4); ?>
                </span></label>
              </div>
            <?php
            }
          }
        ?>
        <div id="new1"></div>
      </div><!--row end-->
    </div>

    <div class="menu-modal bank-modal summary-modal dashboard-modal edit_account hidden scrollbar" id="opcion-2">

      <div class="separator"></div>

      <div class="row">
        <div class="col-md-7 col-sm-6 col-xs-6">
          <h4>EDITAR CUENTA PARA RECIBIR PAGOS</h4>
        </div>
        <div class="col-md-5">
          <a class="btn btn_green btn_delete_bank"> ELIMINAR </a>
        </div>
      </div>
      <p class="description_social" style="margin-top: 40px; margin-bottom: 50px;">
        VoyATodo enviar&aacute; los pagos recibidos en la compra de tus 
        productos vendidos durante la semana. Podr&aacute;s verla en tu 
        extracto como VOYATODO SAS. Recuerda que la
        transferencia tiene un costo de 15.000 COP 
      </p>
      <div class="row">
        <div class="form-group">
          <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">BANCO</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?php $listBank=ArrayHelper::map($listbanks,'pkbank','bank_name'); ?>
            <?= $form->field($model, 'fkbank')->dropDownList($listBank, 
            [
              'prompt'=>'',
              'class' => 'form-control bank-name'
            ])->label(false); ?>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-3 col-sm-3 col-xs-3">NOMBRE TITULAR</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?= $form->field($model, 'userbank_titularname')->input('text', ['class' => 'form-control bank-titular'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-3 col-sm-3 col-xs-3">C&Eacute;DULA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
             <?= $form->field($model, 'userbank_identification')->input('number', ['class' => 'form-control bank-identification'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-3 col-sm-3 col-xs-3">N° DE CUENTA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?= $form->field($model, 'userbank_numberacount')->input('number', ['class' => 'form-control bank-numberacount'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="nombre_evento" class="col-md-3 col-sm-3 col-xs-3">TIPO DE CUENTA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?php $listAcount= ['Corriente' => 'Corriente', 'Crédito' => 'Crédito', 'Ahorro' => 'Ahorro']; ?>
            <?= $form->field($model, 'userbank_tipeacount')->dropDownList($listAcount, 
            [
            'prompt'=>'',
            'class' => 'form-control bank-type'
             ])->label(false); ?>
          </div>
        </div>
      </div><!--row end-->
      <div class="row">
        <a class="btn btn-yellow btn-block create_acount hidden">CREAR CUENTA</a>
        <a class="btn btn-yellow btn-block update_acount hidden">ACTUALIZAR CUENTA</a>
      </div><!--row end-->
    </div><!--modal end-->

    <div class="menu-modal bank-modal summary-modal dashboard-modal edit_target hidden scrollbar" id="opcion-3">
        
      <div class="separator"></div>

      <h4>EDITAR TARJETA DE CR&Eacute;DITO PARA PAGOS</h4>
      <p class="description_social" style="margin-top: 40px; margin-bottom: 50px;">
        VoyATodo recibir&aacute; los pagos de la pauta publicitaria y <br />
        se cargar&aacute;n a tu tarjeta de cr&eacute;dito. Podr&aacute;s verla en tu <br />
        extracto como VOYATODO SAS, s&oacute;lo se cobrar&aacute; el valor <br />
        que hayas avalado en tu banner publicitario
      </p>
      <div class="row">
        <div class="form-group">
          <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">NOMBRE</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?= $form->field($model, 'creditcard_titularname')->input('text', ['class' => 'form-control creditcard-name'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">N° TARJETA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?= $form->field($model, 'creditcard_numbercard')->input('number', ['class' => 'form-control creditcard-number'])->label(false) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="cat_principal" class="col-md-3 col-sm-3 col-xs-3">TIPO DE TARJETA</label>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <?php $listType= ['MASTERCARD' => 'MASTERCARD', 'VISA' => 'VISA', 'AMERICAN EXPRESS' => 'AMERICAN EXPRESS']; ?>
            <?= $form->field($model, 'creditcard_type')->dropDownList($listType, 
             [
              'prompt'=>'',
              'class' => 'form-control creditcard-type'
            ])->label(false); ?>
          </div>
        </div> 
      </div><!--row end-->
      <div class="row">
        <a class="btn btn-yellow btn-block create_target hidden">CREAR TARJETA</a>
        <a class="btn btn-yellow btn-block update_target hidden">ACTUALIZAR TARJETA</a>
      </div>
    </div><!--modal end-->

    <!-- Comienza la quinta opción del menú -->

    <div class="menu-modal share-modal summary-modal dashboard-modal scrollbar" id="opcion5">

      <div class="separator"></div>

      <h3>COMPARTIR POR REDES SOCIALES</h3>
      <p class="description_social">
        Ahora puedes invitar a tus amigos, seguidores y <br>
        contactos a trav&eacute;s de las redes sociales
      </p>
      <span>
        <a href="" class="text-facebook ico red_margin share_facebook"> <i class="fa fa-facebook"></i> </a>
        <a href="https://twitter.com/share"class="text-twitter ico red_margin share_twitter"> <i class="fa fa-twitter"></i> </a>
        <a href="" class="text-linkedin ico red_margin share_linkedin"> <i class="fa fa-linkedin"></i> </a>
        <a href="" class="text-google ico red_margin share_google"> <i class="fa fa-google-plus"></i> </a>
      </span>

      <div class="separator"></div>

      <h3>COMPARTIR POR E-MAIL</h3>
      <p class="description_social">
        Copia y pega el siguiente enlace en tu correo para <br>
        que tus invitados registren el evento:
      </p>
      <div class="col-md-12">
        <?= $form->field($model, 'product_url')->textArea(['class' => 'text-area', 'readonly' => true,'id' => 'txt_url', 'value' => "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']])->label(false) ?>
      </div>
      <div class="col-md-12 top">
        <a class="btn btn-yellow btn-block btn-url" data-clipboard-target="#txt_url">COPIAR ENLACE</a>
      </div>
    </div>

    <!-- Comienza la sexta opción del menú -->

    <div class="menu-modal publicity-modal summary-modal dashboard-modal publicity_principal scrollbar"  id="opcion6">

      <div class="separator"></div>

      <h3>PUBLICIDAD</h3>
      <div class="row top">
        <div class="form-group">
          <div class="col-md-12">
            <label>DESTACA TU EVENTO A&Ntilde;ADIENDO PAUTA</label>
            <a class="btn btn-yellow btn-block btn_publicity">CREAR PAUTA</a>
            <p class="description_social" style="text-align: center">
              Tu producto/servicio en los destacados de la tienda Voyatodo.com
            </p>
          </div>
        </div>
      </div>

      <div class="separator"></div>

      <h3 class="top">CAMPA&Ntilde;AS CREADAS</h3>
      <div class="row new_publicity">
        <?php
          foreach ($products as $value) 
          {
            $publicities = VTTpublicity::find()
                -> where("fkproduct =:fkproduct", [":fkproduct" => $value->pkproduct])
                ->all();
            if(!empty($publicities))
            {
              foreach ($publicities as $publicity) 
              {
              ?>
                <div class="col-md-8 div_banco top div_update_publicity" name="<?php echo $publicity->pkpublicity; ?>">
                  <label class="lbl_bnc">
                    <?php echo $publicity->publicity_name ?><br/>
                    <span class="ttl_blue"><?php echo $publicity->publicity_value.' de '.number_format($publicity->getVtTbuys()->one()->buy_value).' COP'; ?>
                    </span>
                  </label>
                </div>
              <?php
              }
            }
          } 
        ?>
      </div><!--/row-->
    </div>

    <div class="menu-modal publicity-modal summary-modal dashboard-modal create_publicity hidden scrollbar" id="opcion-4">

      <div class="separator"></div>

      <h3>DETALLE DE PAUTA</h3>

      <div class="row top">
        <div class="form-group">
          <label class="col-sm-12 col-md-12">NOMBRE DE LA CAMPA&Ntilde;A</label>
          <div class="col-sm-12 col-md-12">
            <input type="text" class="form-control align_right name_advertiser" placeholder ='0/45'>
          </div>     
        </div>
      </div>

      <div class="separator"></div>

      <h3>CALCULADOR DE LA PAUTA</h3>

      <div class="row top">
        <div class="form-group">
          <label class="col-sm-6 col-xs-12 lblnegro lblxs">ESCOGER MEDIO DE PAGO</label>
           <div class="col-sm-6 col-xs-12">
            <?php
              $list = array();
              foreach ($creditcard as $card) 
              {
                $card->creditcard_type .= ' ' . substr($card->creditcard_numbercard, -4);
                $list[] = $card;
              }
              $lisTargets=ArrayHelper::map($list,'pkcreditcard','creditcard_type'); 
            ?>
              <?= $form->field($model, 'userbank_tipeacount')->dropDownList($lisTargets, 
              [
                'class' => 'form-control tipeacount_advertiser'
              ])->label(false); ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-6 col-xs-12 lblnegro lblxs">VALOR DIARIO DE LA PAUTA</label>
          <div class="col-sm-6 col-xs-12">
            <input type="text" class="form-control no-right-border ttl_blue advertising_comision">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-6 col-xs-12 lblnegro lblxs">VALOR A PAGAR TOTAL</label>
          <div class="col-sm-6 col-xs-12">
            <input type="number" class="form-control no-right-border ttl_blue advertising_total">
          </div>
        </div>
        <div class="col-md-12 top">
          <a class="btn btn-yellow btn-block btn_new_publicity hidden">COMPRAR PAUTA</a>
          <a class="btn btn-yellow btn-block btn_update_publicity hidden">ACTUALIZAR PAUTA</a>
        </div>
      </div><!--/row-->      
    </div>

  <?php $form->end() ?>
  
  <div style="background:#f2f2f2;">
    <div class="container-fluid">
      <div class="row dashboard-row">
    		<!-- Menu -->
        <div class="col-xs-2 col-md-1 col-sm-1 dashboard-column">
          <div class="dashboard-content col-sm-12 ">
            <div class="col-md-12 col-xs-0 col-md-offset-0 dashboard-element" id="dashboard-summary">
              <a class="img_click" name="summary-" style="cursor: pointer">
                <img class="img_summary-" src="<?php echo Yii::getAlias('@web') ?>/images/summary-gray.png">
                <p class="hidden-sm hidden-xs">DASHBOARD</p>
              </a>
            </div>
            <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-information">
              <a class="img_click" name="information-" style="cursor: pointer">
                <img class="img_information-" src="<?php echo Yii::getAlias('@web') ?>/images/information-gray.png">
                <p class="hidden-sm hidden-xs">INFORMACI&Oacute;N</p>
              </a>
            </div>
            <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-sale">
              <a class="img_click" name="asistent-" style="cursor: pointer">
                <img class="img_asistent-" src="<?php echo Yii::getAlias('@web') ?>/images/asistent-gray.png">
                <p class="hidden-sm hidden-xs">VENTAS</p>
              </a>
            </div>
            <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-bank">
              <a class="img_click" name="bank-" style="cursor: pointer">
                <img class="img_bank-" src="<?php echo Yii::getAlias('@web') ?>/images/bank-gray.png">
                <p class="hidden-sm hidden-xs">BANCO</p>
              </a>
            </div>
            <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-share">
              <a class="img_click" name="share-" style="cursor: pointer">
                <img class="img_share-" src="<?php echo Yii::getAlias('@web') ?>/images/share-gray.png">
                <p class="hidden-sm hidden-xs">COMPARTIR</p>
              </a>
            </div>
            <div class="col-md-12 col-xs-0 dashboard-element" id="dashboard-publicity">
              <a class="img_click" name="publicity-" style="cursor: pointer">
                <img class="img_publicity-" src="<?php echo Yii::getAlias('@web') ?>/images/publicity-gray.png">
                <p class="hidden-sm hidden-xs">PUBLICIDAD</p>
              </a>
            </div>
          </div>
        </div>
        <!-- End Menu -->
        <div class="col-xs-10 product-description-row">
          <div class="row">
            <div class="col-xs-12">
              <h2 class="txt_name"><?php echo $product->product_name; ?></h2>
              <h3 class="txt_review"><?php echo $product->product_review; ?></h3>
              <a class="category_product" href="<?php echo Yii::getAlias('@web').'/producto/categoria/'.$product->getFkcategory0()->one()->categoryproduct_name; ?>"><?php echo $product->getFkcategory0()->one()->categoryproduct_name; ?></a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5 col-sm-12">
            	<?php
                if(!empty($galery))
                {?>
                  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <?php
                    $cont = count($galery);
                    ?>
                    <ol class="carousel-indicators">
                      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                      <?php
                      for ($i=1; $i < $cont; $i++) 
                      { 
                        ?>
                        <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" ></li>
                        <?php
                      }
                      ?>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                      <?php
                      for ($j=0; $j < $cont; $j++) 
                      {
                        if($j == 0)
                        {
                          ?>
                          <div class="item active">
                            <img src="<?php echo Yii::getAlias('@web').$galery[$j]['image_path']; ?>" alt="galery_event" style="width: 100%">
                          </div>
                          <?php
                        }
                        else
                        {
                          ?>
                          <div class="item">
                            <img src="<?php echo Yii::getAlias('@web').$galery[$j]['image_path']; ?>" alt="galery_event" style="width: 100%">
                          </div>
                          <?php
                        }
                        ?>
                        <?php
                      }
                      ?>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>                  
                  <?php
                }
              ?>
            </div>
            <div class="col-md-7 col-sm-12">
              <h4>DESCRIPCI&Oacute;N</h4>
              <p class="txt_description"> <?php echo $product->product_description; ?> </p>
              <div class="row payment-details top">
    		        <div class="col-md-6 col-sm-6">
    		    	    <div class="price">
    		            <h2 class="ttl_blue"> $ <?php echo ceil($product->product_value); ?> COP </h2>
    		          </div>
    		        </div>
    		        <div class="col-md-2 col-sm-6 top_10">
    		    	    <div class="search-input qty_product">
    		            <select class="form-control">
  		                <?php
                        $options = '';
                        if($product->product_qty == '')
                          $options .= '<option>SIN LÍMITE </option>';
                        else 
                        {
                          for ($i=0; $i < $product->product_qty; $i++) 
                            $options .= '<option>'.$i.'</option>';
                        }
                        echo $options; 
                      ?>
  		              </select>
    		          </div>
    		        </div>
  	   		    </div><!--row-->
      		    <div class="row top">
                <div class="col-md-6 col-sm-6">
                  <div class="addcart">
                    <a class="eventos carrito">AGREGAR AL CARRITO</a>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 top_10">
                  <div class="search-input">
                    <select class="form-control">
                      <?php
                        $dates = '';
                        $availability = $product->getVtTavailabilities()->all();
                        foreach ($availability as $date)
                          $dates .= '<option>'.$date->availability_date.'</option>';
                      echo $dates; 
                      ?>
                    </select>
                  </div>
                </div>
              </div>
      		    <div class="row top">
    		        <div class="col-md-6 col-sm-6">
    		 	        <div class="buynow">
    		            <a class="eventos">COMPRAR AHORA</a>
    		          </div>
    		        </div>
    		        <div class="col-md-6 col-sm-6 top_10">
    		          <img src="<?php echo Yii::getAlias('@web') ?>/images/pay.png">
    		        </div>
      		    </div><!--row-->
            </div>
          </div>
      		<div class="row">
            <div class="col-md-6 col-sm-12">
              <h3>INFORMACI&Oacute;N DEL VENDEDOR</h3>
              <div class="col-md-5" style="text-align:right;">
                <p>
                  Calificaci&oacute;n del Vendedor:
                </p>
              </div>
              <div class="col-md-7">
                <?php 
                  $calification=VTTusercalification::find()->where("fkuser =:user", [":user" => Yii::$app->user->identity->id])->all();
                  $sum=0;
                  $cont=0;
                  $total=0;
                  foreach ($calification as $value) 
                  {
                    $sum+=$value->usercalification;
                    $cont++;
                  }
                  if($cont != 0)
                    $total = round($sum/$cont);
                ?>
                <input id="input-21d" value="<?php echo $total; ?>" type="number" readonly="true" class="rating" min=0 max=5 step=1 data-size="xs"> 
              </div>
              <div class="col-md-5" style="text-align:right;">
                <p>Ventas efectuadas:</p>
              </div>
              <div class="col-md-7">
                <p class="ttl_blue">0</p>
              </div>
              <div class="col-md-5" style="text-align:right;">
                <p>
                  Tiempo en Voy a Todo: 
                </p>
              </div>
              <div class="col-md-7">
                <p class="ttl_blue">
                  <?php
                    $date1=Yii::$app->user->identity->user_date;
                    $segundos=strtotime($date1) - strtotime(date('Y-m-d'));
                    $days=intval($segundos/60/60/24);
                    $days=abs(floor($days));
                    echo $days." días.";
                  ?>
                </p>
              </div>
              <div class="col-md-6"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo Yii::getAlias('@web') ?>/js/star-rating.min.js" type="text/javascript"></script>

<script>
	$(document).ready(function(){

    new Clipboard('.btn-url');

    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

		$(".btn_save_product").click(function(){
			$("#form-advertiser").submit();
		});
    $(".trash").click(function(){
      $(".imgtrash").remove();
    });
	});
</script>