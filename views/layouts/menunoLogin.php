<!-- Start  Logo & Naviagtion  -->
<header class="clearfix"> 
    
    <!-- Start  Logo & Naviagtion  -->
    <div class="navbar navbar-default navbar-top">
      <div class="container-fluid">
        <div class="navbar-header"> 
          <!-- Stat Toggle Nav Link For Mobiles -->
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <i class="fa fa-bars"></i> </button>
          <!-- End Toggle Nav Link For Mobiles --> 
          <a class="navbar-brand" href="<?php echo Yii::getAlias('@web') ?>/site/"> <img alt="" src="<?php echo Yii::getAlias('@web') ?>/images/logo.png"> </a> </div>
        <div class="navbar-collapse collapse"> 
          <!-- Stat Search -->
          <div class="search-side"> <a class="show-search"><i class="fa fa-search"></i></a>
            <div class="search-form">
              <form autocomplete="off" role="search" method="get" class="searchform" action="#">
                <input type="text" value="" name="s" id="s" placeholder="Search the site...">
              </form>
            </div>
          </div>
          <!-- End Search --> 
          <!-- Start Navigation List -->
          <ul class="nav navbar-nav navbar-right">
            <li> <a class="active" href="<?php echo Yii::getAlias('@web') ?>/site/">Inicio</a></li>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/site/tienda">Tienda</a> </li>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/site/patrocinador">Patrocinador</a> </li>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/site/proveedor">Proveedor</a> </li>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/site/contact">Contacto</a></li>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/site/login">Inicia Sesi&oacute;n</a> </li>
          </ul>
          <!-- End Navigation List --> 
        </div>
      </div>
      
      <!-- Mobile Menu Start -->
      <ul class="wpb-mobile-menu">
        <li> <a class="active" href="<?php echo Yii::getAlias('@web') ?>/site/">Inicio</a>
        </li>
        <li><a href="<?php echo Yii::getAlias('@web') ?>/site/tienda">Tienda</a> </li>
        <li><a href="<?php echo Yii::getAlias('@web') ?>/site/patrocinador">Patrocinador</a> </li>
        <li><a href="<?php echo Yii::getAlias('@web') ?>/site/proveedor">Proveedor</a> </li>
        <li><a href="#">Contacto</a></li>
        <li><a href="<?php echo Yii::getAlias('@web') ?>/site/login">Inicia Sesion</a> </li>
      </ul>
      <!-- Mobile Menu End --> 
      
    </div>
    <!-- End Header Logo & Naviagtion --> 
    
  </header>