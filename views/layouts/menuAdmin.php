<!-- Start  Logo & Naviagtion  -->
  <header class="clearfix"> 
    <div class="navbar navbar-default navbar-top">
      <div class="container-fluid">
        <div class="navbar-header"> 
          <!-- Stat Toggle Nav Link For Mobiles -->
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <i class="fa fa-bars"></i> </button>
          <!-- End Toggle Nav Link For Mobiles --> 
          <a class="navbar-brand" href="<?php echo Yii::getAlias('@web') ?>/account/"> <img alt="" src="<?php echo Yii::getAlias('@web') ?>/images/logo.png"> </a> </div>
        <div class="navbar-collapse collapse"> 
          <!-- Stat Search -->
          <div class="search-side"> <a class="show-search"><i class="fa fa-search"></i></a>
            <div class="search-form">
              <form autocomplete="off" role="search" method="post" class="searchform" action="#">
                <input type="text" value="" name="s" id="s" placeholder="Search the site...">
              </form>
            </div>
          </div>
          <!-- End Search --> 
          <!-- Start Navigation List -->
          <ul class="nav navbar-nav navbar-right">
            <li> <a class="active" href="<?php echo Yii::getAlias('@web') ?>/admin/">Inicio</a></li>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/admin/solicitudes">Solicitudes</a> </li>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/admin/conversaciones">Conversaciones</a> </li>
            <li>
              <a href="#">Finanzas</a>
              <ul class="ul-menu">
                <li><a href="<?php echo Yii::getAlias('@web') ?>/admin/parametros"> Par&aacute;metros </a> </li>              
              </ul>
            </li>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/admin/usuarios">Usuarios</a> </li>
            <li>
              <a href="#"> 
                <?php echo Yii::$app->user->identity->username." ".Yii::$app->user->identity->last_name;
                ?>
              </a>
              <ul class="ul-menu">             
                <li><a href="<?php echo Yii::getAlias('@web') ?>/site/logout"> Cerrar sesi&oacute;n </a> </li>              
              </ul>
            </li> 
          </ul>
          <!-- End Navigation List --> 
        </div>
      </div>
    </div>
  </header>
      
      <!-- Mobile Menu Start -->
      <ul class="wpb-mobile-menu">
        <li> <a class="active" href="<?php echo Yii::getAlias('@web') ?>/admin/">INICIO</a>
        </li>
        <li> <a href="<?php echo Yii::getAlias('@web') ?>/admin/solicitudes">SOLICITUDES</a>
        </li>
        <li> <a href="<?php echo Yii::getAlias('@web') ?>/admin/conversaciones">CONVERSACIONES</a>
        </li>
        <li>
          <a href="#">Finanzas</a>
          <ul class="ul-menu">
            <li><a href="<?php echo Yii::getAlias('@web') ?>/admin/comisiones"> Comisiones </a> </li>              
          </ul>
        </li>
        <li> <a href="<?php echo Yii::getAlias('@web') ?>/admin/usuarios">USUARIOS</a>
        </li>
        <li> 
          <a href="blog.html">  <li>
            <?php echo Yii::$app->user->identity->username." ".Yii::$app->user->identity->last_name;
            ?>
          </a>
          <ul>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/admin/perfil"> Mi Perfil </a> </li>              
            <li><a href="<?php echo Yii::getAlias('@web') ?>/site/logout"> Cerrar sesi&oacute;n </a> </li>              
          </ul>
        </li>
      </ul>
      <!-- Mobile Menu End --> 