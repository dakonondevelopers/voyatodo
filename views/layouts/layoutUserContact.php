<?php 
	$this->beginPage(); 
?>

<!doctype html>
<html lang="es">
<head>

<!-- Define Charset -->
<meta charset="utf-8">

<!-- Responsive Metatag -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- Page Title, Description, Image and Author here-->
<meta property="og:title" content="TITULO AQUÍ" />
<meta property="og:description" content="DESCRIPCIÓN DEL CONTENIDO AQUÍ" />
<meta property="og:image" content="RUTA DE LA IMAGEN QUE SE VA A MOSTRAR AQUÍ" />
<meta name="author" content="Voy A Todo INC">

<link rel="shortcut icon" href="<?php echo Yii::getAlias('@web') ?>/favicon.jpg" type="image/x-icon">

<!-- Bootstrap CSS  -->
<link rel="stylesheet" href="<?php echo Yii::getAlias('@web') ?>/asset/css/bootstrap.min.css" type="text/css" media="screen">

<!-- Font Awesome CSS -->
<link rel="stylesheet" href="https://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" type="text/css" media="screen">

<!-- Slicknav -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/slicknav.css" media="screen">

<!-- Margo CSS Styles  -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/style.css" media="screen">

<!-- Responsive CSS Styles  -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/responsive.css" media="screen">

<!-- Css3 Transitions Styles  -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/animate.css" media="screen">

<!-- jQuery UI -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/asset/css/jquery-ui.min.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/sweetalert.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/animate.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="<?php echo Yii::getAlias('@web') ?>/css/star-rating.min.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/datepicker3.css" media="screen">

<!-- Color CSS Styles  -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/colors/red.css" title="red" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/colors/jade.css" title="jade" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/colors/green.css" title="green" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/colors/blue.css" title="blue" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/colors/beige.css" title="beige" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/colors/cyan.css" title="cyan" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/colors/orange.css" title="orange" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/colors/peach.css" title="peach" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/colors/pink.css" title="pink" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/colors/purple.css" title="purple" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/colors/sky-blue.css" title="sky-blue" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?>/css/colors/yellow.css" title="yellow" media="screen" />

<!-- Margo JS  -->
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/jquery-2.1.4.min.js"></script>


<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/jquery.migrate.js"></script>

<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/modernizrr.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/asset/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/nivo-lightbox.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/jquery.appear.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/count-to.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/jquery.textillate.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/jquery.lettering.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/jquery.easypiechart.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/jquery.parallax.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/mediaelement-and-player.js"></script>
<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/jquery.slicknav.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/sweetalert.min.js"></script>
<script src="<?php echo Yii::getAlias('@web') ?>/js/clipboard.min.js"></script>

<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

<title><?php echo isset($this->title) ? $this->title : Yii::app()->name; ?></title>

</head>

<body class="login-bg body-category">
<?php $this->beginBody() ?>
	<?php include('menuUser.php') ?>
	<?php echo $content ?>
  	<?php include('footer.php') ?>
	<script type="text/javascript" src="<?php echo Yii::getAlias('@web') ?>/js/script_account.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>