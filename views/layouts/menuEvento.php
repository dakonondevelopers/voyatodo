<!-- Start  Logo & Naviagtion  -->
  <header class="clearfix headerEvent" > 
    <div class="navbar navbar-default navbar-top">
      <div class="container-fluid">
        <div class="navbar-header"> 
          <!-- Stat Toggle Nav Link For Mobiles -->
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <i class="fa fa-bars"></i> </button>
          <!-- End Toggle Nav Link For Mobiles --> 
          <a class="navbar-brand" href="<?php echo Yii::getAlias('@web') ?>/account/"> <img alt="" src="<?php echo Yii::getAlias('@web') ?>/images/logo.png"> </a> </div>
      </div>
    </div>
  </header>
      