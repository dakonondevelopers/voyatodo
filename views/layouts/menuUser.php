<!-- Start  Logo & Naviagtion  -->
  <header class="clearfix"> 
    <div class="navbar navbar-default navbar-top">
      <div class="container-fluid">
        <div class="navbar-header"> 
          <!-- Stat Toggle Nav Link For Mobiles -->
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <i class="fa fa-bars"></i> </button>
          <!-- End Toggle Nav Link For Mobiles --> 
          <a class="navbar-brand" href="<?php echo Yii::getAlias('@web') ?>/account/"> <img alt="" src="<?php echo Yii::getAlias('@web') ?>/images/logo.png"> </a> </div>
        <div class="navbar-collapse collapse mn"> 
          <!-- Stat Search -->
          <div class="search-side"> <a class="show-search"><i class="fa fa-search"></i></a>
            <div class="search-form">
              <form autocomplete="off" role="search" method="post" class="searchform" action="#">
                <input type="text" value="" name="s" id="s" placeholder="Search the site...">
              </form>
            </div>
          </div>
          <!-- End Search --> 
          <!-- Start Navigation List -->
          <ul class="nav navbar-nav navbar-right">
            <li> <a class="active" href="<?php echo Yii::getAlias('@web') ?>/account/">Inicio</a></li>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/account/tienda">Tienda</a> </li>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/account/miseventos">Mis eventos</a> </li>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/account/mistickets">Mis entradas</a></li>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/account/contact">Contacto</a> </li>
            <li>
              <a href="#"> 
                <?php echo Yii::$app->user->identity->username." ".Yii::$app->user->identity->last_name;
                ?>
              </a>
              <ul class="ul-menu">
                <li><a href="<?php echo Yii::getAlias('@web') ?>/account/evento?open=true"> Crear Evento </a> </li>              
                <li><a href="<?php echo Yii::getAlias('@web') ?>/account/perfil"> Mi Perfil </a> </li> 
                <li><a href="<?php echo Yii::getAlias('@web') ?>/account/favoritos"> Favoritos </a> </li>
                <li><a href="<?php echo Yii::getAlias('@web') ?>/site/logout"> Cerrar sesi&oacute;n </a> </li>              
              </ul>
            </li>
            <?php 
              if(!is_null(Yii::$app->user->identity->user_image))
              {
                ?>
                <li>
                  <div class="fa-circle-menu" style="background: url(<?php echo Yii::getAlias('@web').Yii::$app->user->identity->user_image; ?>) center no-repeat;background-size: auto 100%">
                  </div>
                </li>
                <?php            
              }
              else
              {
                ?>
                <li><div class="fa-circle-menu"></div></li>
                <?php 
              }
            ?>
          </ul>
          <!-- End Navigation List --> 
        </div>
      </div>
    </div>
  </header>
      
      <!-- Mobile Menu Start -->
      <ul class="wpb-mobile-menu">
        <li> <a class="active" href="<?php echo Yii::getAlias('@web') ?>/account">INICIO</a>
        </li>
        <li> <a href="<?php echo Yii::getAlias('@web') ?>/account/tienda">TIENDA</a>
        </li>
        <li> <a href="<?php echo Yii::getAlias('@web') ?>/account/miseventos">MIS EVENTOS</a>
        </li>
        <li> <a href="<?php echo Yii::getAlias('@web') ?>/account/mistickets">MIS TICKETS</a>
        </li>
        <li> <a href="#">CONTACTO</a>
        </li>
        <li> 
          <a href="blog.html">  <li>
            <?php echo Yii::$app->user->identity->username." ".Yii::$app->user->identity->last_name;
            ?>
          </a>
          <ul>
            <li><a href="<?php echo Yii::getAlias('@web') ?>/account/evento"> Crear Evento </a> </li>              
            <li><a href="<?php echo Yii::getAlias('@web') ?>/account/perfil"> Mi Perfil </a> </li>   
            <li><a href="<?php echo Yii::getAlias('@web') ?>/account/favoritos"> Favoritos </a> </li>           
            <li><a href="<?php echo Yii::getAlias('@web') ?>/site/logout"> Cerrar sesi&oacute;n </a> </li>              
          </ul>
        </li>
      </ul>
      <!-- Mobile Menu End --> 