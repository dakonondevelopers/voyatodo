<?php
  
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\BaseHtml;

$this->title = 'Voy A Todo | Mi Perfil';

?>

<section id="inner01">
  <div id="main-slide" class="carousel00 slide slide_galery" data-ride="carousel">
    <div class="item active"> <img class="img-responsive02 heigtht_img" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
      <div class="slider-content">
        <?php $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'form-update',
            'enableClientValidation' => true,
            'enableAjaxValidation' => true,
            'options' => ['enctype' => 'multipart/form-data'],
            'class'=> 'contact-form',
          ]);
        ?>
          <div class="col-md-12 text-center hitop">
            <div class="ttlperfil"><?php echo Yii::$app->user->identity->username." ".Yii::$app->user->identity->last_name;?></div>                                 
          </div>
          <div class="col-lg-2 col-lg-offset-5 col-sm-2 col-sm-offset-5 col-xs-12">
            <center>
              <?php 
                if(!is_null($information->user_image))
                {
                  ?>
                    <div class="fa_circle field_photo" style="background: url(<?php echo Yii::getAlias('@web').$information->user_image; ?>) center no-repeat;background-size: auto 100%">
                    </div>
                  <?php            
                }
                else
                {
                  ?>
                  <div class="fa_circle field_photo"></div>
                  <?php 
                }
              ?> 
            </center>
          </div>
          <div class="col-lg-2 col-sm-3 col-xs-12">
            <label for="files" data-role="button" class="btn btn_green top" data-inline="true" data-mini="true" data-corners="false">Cambiar foto</label>
            <?= $form->field($model, "user_photo")->fileInput(['class' => 'change_photo', 'accept' => 'image/*', 'style' => 'margin-top: -35px; margin-left: 40px;'])->label(false) ?>
          </div>
          <div class="col-lg-6 col-lg-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
            <?php 
              $calification=$information->getVtTusercalifications()->all();
              $sum=0;
              $cont=0;
              $total=0;
              foreach ($calification as $value) 
              {
                $sum+=$value->usercalification;
                $cont++;
              }
              if($cont != 0)
                $total = round($sum/$cont);
            ?>
            <input id="input-21d" value="<?php echo $total; ?>" type="number" readonly="true" class="rating" min=0 max=5 step=1 data-size="xs">          
          </div>
    </div>
  </div>
</section>

<div class="container-fluid">
  <div class="col-md-12 col-sm-12 big-title text-center">
    <h1 class="">DATOS PERSONALES</h1> <p class="help-block">Informaci&oacute;n b&aacute;sica de la cuenta</p>
  </div>

  <div class="col-md-3 col-xs-12">
    <div class="panel-group" id="accordion">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
              CAMBIAR CONTRASE&Ntilde;A
            </a>
          </h4>
        </div>
        <div id="collapse1" class="panel-collapse collapse in">
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-12 control-label">CONTRASE&Ntilde;A</label>
              <div class="col-md-12">
                <input type="password" class="form-control password" placeholder="•••••••••••••••••••••">
                <div id="error1" style="color:#A94452;"></div>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-12 control-label">CONFIRMAR CONTRASE&Ntilde;A</label>
              <div class="col-md-12">
                <input type="password" class="form-control password_repeat" placeholder="•••••••••••••••••••••">
                <div id="error2" style="color:#A94452;"></div>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <a class="btn btn_sm btn-block save_password">ACTUALIZAR CONTRASE&Ntilde;A</a>
              </div>
            </div><!--form-group-->
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
              A&Ntilde;ADIR REDES SOCIALES
            </a>
          </h4>
        </div>
        <div id="collapse2" class="panel-collapse collapse">
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-12 control-label">FACEBOOK</label>
              <div class="col-md-12">
                <input type="text" value="<?php echo $information->user_facebook; ?>" class="form-control txt_facebook">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-12 control-label">TWITTER</label>
              <div class="col-md-12">
                <input type="text" value="<?php echo $information->user_twitter; ?>" class="form-control txt_twitter">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-12 control-label">GOOGLE+</label>
              <div class="col-md-12">
                <input type="text" value="<?php echo $information->user_google; ?>" class="form-control txt_google">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-12 control-label">YOUTUBE</label>
              <div class="col-md-12">
                <input type="text" value="<?php echo $information->user_youtube; ?>" class="form-control txt_youtube">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <div class="col-md-12">
                <div id="error3" style="color:#A94452;"></div>
                <a class="btn btn_sm btn-block save_social">GUARDAR</a>
              </div>
            </div><!--form-group-->
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-4 col-xs-12">

    <div class="col-md-12 col-xs-12 datos">
      <div class="separator">
        DATOS PERSONALES DEL USUARIO
      </div>
    </div>

    <div class="form-horizontal">
       <div class="form-group">
         <label class="col-md-4 control-label">NOMBRE</label>
         <div class="col-md-8">
          <?= $form->field($model, "username")->input("text", ['value' => $information->username, 'class' => 'form-control'])->label(false) ?>
         </div>
       </div><!--form-group-->
       <div class="form-group">
         <label class="col-md-4 control-label">APELLIDO</label>
         <div class="col-md-8">
          <?= $form->field($model, "last_name")->input("text", ['value' => $information->last_name, 'class' => 'form-control'])->label(false) ?>
         </div>
       </div><!--form-group-->
       <div class="form-group">
         <label class="col-md-4 control-label">E-MAIL</label>
         <div class="col-md-8">
            <input readonly="true" value="<?php echo $information->email; ?>" class = "form-control">
         </div>
       </div><!--form-group-->
       <div class="form-group">
         <label class="col-md-4 control-label">TEL&Eacute;FONO 1</label>
          <div class="col-md-8">
            <?= $form->field($model, "user_phone1")->input("text", ['value' => $information->user_phone1, 'class' => 'form-control'])->label(false); ?>
          </div>
       </div><!--form-group-->
       <div class="form-group">
         <label class="col-md-4 control-label">TEL&Eacute;FONO 2</label>
          <div class="col-md-8">
            <?= $form->field($model, "user_phone2")->input("text", ['value' => $information->user_phone2, 'class' => 'form-control'])->label(false); ?>
          </div>
       </div><!--form-group-->
       <div class="form-group">
         <div class="col-md-12">
            <a class="btn btn_sm btn-block save_information">GUARDAR INFORMACI&Oacute;N</a>
         </div>
       </div><!--form-group-->
    </div><!--FORM-->      
    <?php $form->end() ?> 
  </div>

  <div class="col-md-4 col-xs-12 col-md-offset-1">

    <div class="col-md-12 col-xs-12 datos">
      <div class="separator">
        EVENTOS QUE EST&Aacute;S PATROCINANDO      
      </div>
    </div>
    <div class="form-horizontal">
      <?php
        foreach ($proposals as $val) 
        {
          ?>
          <div class="form-group">
            <label class="col-md-4 control-label">NOMBRE EVENTO</label>
            <div class="col-md-8">
              <input type="text" class="form-control" readonly="true" value="<?php echo $val->getFkevent0()->one()->event_name; ?>">
            </div>
          </div><!--form-group-->
          <div class="form-group">
            <label class="col-md-4 control-label">TOTAL EN ESPECIE</label>
            <div class="col-md-8">
             <input type="text" class="form-control" readonly="true" value="<?php echo $val->proposal_total1; ?>">
            </div>
          </div><!--form-group-->
          <div class="form-group">
            <label class="col-md-4 control-label">INCENTIVO</label>
           <div class="col-md-8">
             <input type="text" class="form-control" readonly="true" value="<?php echo $val->proposal_contributions; ?>">
           </div>
          </div><!--form-group-->
          <?php
        } 
      ?>
    </div><!--form-->
  </div>
</div><!--container-->

<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

<script src="<?php echo Yii::getAlias('@web') ?>/js/star-rating.min.js" type="text/javascript"></script>

<?php
  if($message == 1)
  { ?>
    <script>
    swal({
      title: "Oh! no",
      text: "Hubo un error al procesar tu requerimiento. Revisa la información que ingresaste.",
      type: "error"
    });
    </script>
  <?php
  }
  else
    if($message == 2)
    {
    ?>
    <script>
      swal("Muy bien", "Tus datos fueron actualizados correctamente", "success")
    </script>
    <?php
    }
?>

<script type="text/javascript">
  $(".change_photo").change(function(event) {
    $.each(event.target.files, function(index, file) {
      var reader = new FileReader();
        reader.onload = function(event) {  
        object = {};
        object.filename = file.name;
        object.data = event.target.result;
        $(".field_photo").attr("style","background: url(" + object.data + ") center no-repeat; background-size: auto 100%");
      };  
      reader.readAsDataURL(file);
    });
  });
  $(document).ready(function(){
    $(".save_information").click(function(){
      $("#form-update").submit();
    });
  });
</script>