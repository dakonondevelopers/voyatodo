<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Voy A Todo | Conversaciones';

?>

  <!-- Contenido de la página -->
  
  <section id="inner01">
    <div id="main-slide" class="carousel00 slide slide_galery" data-ride="carousel">
      <div class="item active"> <img class="img-responsive02" src="<?php echo Yii::getAlias('@web') ?>/images/inner-banner01.jpg" alt="slider">
        <div class="slider-content">
          <div class="col-md-12 text-center">
            <h2 class="animated3" style="margin-top: 0px"> Esta es la lista de Conversaciones! </h2>
            <h3 class="animated3"> <span>AQU&Iacute; PUEDES REVISAR TODAS LAS CONVERSACIONES</span> </h3>
          </div>
        </div>
      </div>
    </div>
  </section> 

  <div class="section destacados">
    <div class="container container-update"> 
      <div class="row">
        <?php
            if(empty($messages))
            { ?>
            <div class="big-title text-center">
              <h1>A&Uacute;N NO HAY <span>conversaciones</span></h1>
            </div>
            <img class="img-face-sad" src="<?php echo Yii::getAlias('@web')?>/images/face-sad.png" alt="face-sad">
          <?php  
            }
            else
            { 
          ?>
            <div class="big-title text-center">
              <h1><span>conversaciones</span></h1>
              <p class="text-center">Desde aqu&iacute; puedes ver las conversaciones</p>
            </div>
            <?php
              foreach ($messages as $msg) 
              {
                ?>
                <div class="col-lg-3 col-md-4 col-sm-6">
                  <div class="team-member modern">
                    <div class="member-photo member-photo-sponsor">
                      <img alt="" src="<?php echo Yii::getAlias('@web').$msg->getFkproposal0()->one()->getFkevent0()->one()->event_image; ?>" />
                    </div>
                    <div class="member-info">
                      <h2><a href="<?php echo Yii::getAlias('@web').'/evento/v/'.$msg->getFkproposal0()->one()->getFkevent0()->one()->event_url; ?>"> <?php echo $msg->getFkproposal0()->one()->getFkevent0()->one()->event_name; ?></a></h2>
                    </div>
                    <div class="member-socail"> 
                      <a class="link-button-message" href="<?php echo Yii::getAlias('@web').'/evento/v/'.$msg->getFkproposal0()->one()->getFkevent0()->one()->event_url; ?>?openchat=true"> ver </a>
                    </div>
                  </div>
                </div>
          <?php
              }
            }
        ?>
      </div>
      <!-- End Team Members --> 
    </div>
    <!-- .container --> 
  </div>
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>