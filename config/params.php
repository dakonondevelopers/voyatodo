<?php

return [
    'adminEmail' => 'noreply@voyatodo.com',
	'title' => 'Voy a Todo',
	'salt' => '$6$rounds=5000$usesomesillystringforsalt$',
];
