<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tbuy".
 *
 * @property string $pkbuy
 * @property string $buy_date
 * @property string $buy_code
 * @property string $buy_value
 * @property string $buy_signature
 * @property string $buy_numberpay
 * @property string $buy_status
 * @property string $fkticket
 * @property string $fkuser
 * @property string $fkpublicity
 *
 * @property VtTpublicity $fkpublicity0
 * @property VtTticket $fkticket0
 * @property VtTuser $fkuser0
 */
class VTTbuy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tbuy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['buy_date'], 'safe'],
            [['buy_value', 'fkticket', 'fkuser', 'fkpublicity'], 'integer'],
            [['fkuser'], 'required'],
            [['buy_code'], 'string', 'max' => 10],
            [['buy_signature'], 'string', 'max' => 200],
            [['buy_numberpay'], 'string', 'max' => 100],
            [['buy_status'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkbuy' => 'Pkbuy',
            'buy_date' => 'Buy Date',
            'buy_code' => 'Buy Code',
            'buy_value' => 'Buy Value',
            'buy_signature' => 'Buy Signature',
            'buy_numberpay' => 'Buy Numberpay',
            'buy_status' => 'Buy Status',
            'fkticket' => 'Fkticket',
            'fkuser' => 'Fkuser',
            'fkpublicity' => 'Fkpublicity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkpublicity0()
    {
        return $this->hasOne(VTTpublicity::className(), ['pkpublicity' => 'fkpublicity']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkticket0()
    {
        return $this->hasOne(VTTticket::className(), ['pkticket' => 'fkticket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(Users::className(), ['id' => 'fkuser']);
    }
}
