<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tbank".
 *
 * @property string $pkbank
 * @property string $bank_name
 *
 * @property VtTuserbank[] $vtTuserbanks
 */
class VTTbank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tbank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkbank' => 'Pkbank',
            'bank_name' => 'Bank Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTuserbanks()
    {
        return $this->hasMany(VTTuserbank::className(), ['fkbank' => 'pkbank']);
    }
}
