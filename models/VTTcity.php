<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "VT_Tcity".
 *
 * @property string $pkcity
 * @property string $city_name
 * @property string $fkdepartament
 *
 * @property VTTdepartament $fkdepartament0
 * @property VTTevent[] $vTTevents
 */
class VTTcity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tcity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkdepartament'], 'required'],
            [['fkdepartament'], 'integer'],
            [['city_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkcity' => 'Pkcity',
            'city_name' => 'City Name',
            'fkdepartament' => 'Fkdepartament',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkdepartament0()
    {
        return $this->hasOne(VTTdepartament::className(), ['pkdepartament' => 'fkdepartament']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVTTevents()
    {
        return $this->hasMany(VTTevent::className(), ['fkcity' => 'pkcity']);
    }
}
