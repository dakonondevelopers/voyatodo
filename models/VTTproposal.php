<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tproposal".
 *
 * @property integer $pkproposal
 * @property string $fkuser
 * @property string $fkevent
 * @property string $fkproposaldetails
 * @property integer $proposal_type
 * @property integer $proposal_neto1
 * @property integer $proposal_total1
 * @property integer $fkstatus
 * @property string $proposal_contributions
 *
 * @property VtTmessage[] $vtTmessages
 * @property VtTevent $fkevent0
 * @property VtTuser $fkuser0
 * @property VtTstatus $fkstatus0
 * @property VtTproposaldetails $fkproposaldetails0
 */
class VTTproposal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tproposal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkuser', 'fkevent', 'proposal_type', 'fkstatus'], 'required'],
            [['fkuser', 'fkevent', 'fkproposaldetails', 'proposal_type', 'proposal_neto1', 'proposal_total1', 'fkstatus'], 'integer'],
            [['proposal_contributions'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkproposal' => 'Pkproposal',
            'fkuser' => 'Fkuser',
            'fkevent' => 'Fkevent',
            'fkproposaldetails' => 'Fkproposaldetails',
            'proposal_type' => 'Proposal Type',
            'proposal_neto1' => 'Proposal Neto1',
            'proposal_total1' => 'Proposal Total1',
            'fkstatus' => 'Fkstatus',
            'proposal_contributions' => 'Proposal Contributions',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTmessages()
    {
        return $this->hasMany(VTTmessage::className(), ['fkproposal' => 'pkproposal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkevent0()
    {
        return $this->hasOne(Event::className(), ['pkevent' => 'fkevent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(Users::className(), ['id' => 'fkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkstatus0()
    {
        return $this->hasOne(VTTstatus::className(), ['pkstatus' => 'fkstatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkproposaldetails0()
    {
        return $this->hasOne(VTTproposaldetails::className(), ['pkproposaldetails' => 'fkproposaldetails']);
    }
}
