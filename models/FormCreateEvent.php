<?php
 
namespace app\models;
use Yii;
use yii\base\Model;
use ReflectionClass;
use app\models\Event;
use yii\web\UploadedFile;


class FormCreateEvent extends Model{
 
    public $event_name;
    public $event_lat;
    public $event_log;
    public $event_stardate;
    public $event_enddate;
    public $event_starthour;
    public $event_endhour;
    public $event_url;
    public $event_image;
    public $event_review;
    public $event_description;
    public $event_place;
    public $event_address;
    public $event_linkvideo;
    public $event_opacityimage;
    public $event_sponsorship;
    public $event_galery;
    public $event_terms;
    public $event_crowfunding;
    public $fkstatus;
    public $event_country;
    public $event_city;
    public $event_category_principal;
    public $event_category_secundary;
    public $event_visible;
    public $fkbank;
    public $userbank_titularname;
    public $userbank_identification;
    public $userbank_numberacount;
    public $userbank_tipeacount;
    public $creditcard_type;
    public $creditcard_titularname;
    public $creditcard_numbercard;
    public $creditcard_year;
    public $ticket_name;
    public $ticket_description;
    public $ticket_value;
    public $ticket_comision;
    public $ticket_insentive;
    public $ticket_finish;
    public $ticket_seeclaim;
    public $ticket_start;
    public $ticket_enddate;
    public $ticket_days;
    public $ticket_qty;
    public $typeticket_name;
    public $token_asistent;
    public $new;
    public $eventid;
    public $tickets;
    public $proposal_total1;
    public $proposal_contributions;

    public function rules()
    {
        return [
             [['event_name', 
              'event_starthour', 
              'event_stardate', 
              'event_endhour', 
              'event_enddate', 
              'event_place', 
              'event_address', 
              'event_review', 
              'event_description', 
              'event_terms', 
              'event_country', 
              'event_city',
              'event_url',
              'event_visible',
              'event_sponsorship',
              'event_category_principal',
              'event_category_secundary'
             ], 'required', 'message' => 'Campo requerido'],
             ['event_stardate', 'validateDate_Start'],
             ['event_enddate', 'validateDate_End'],
             ['event_endhour', 'validateHour_End'],
             ['event_url', 'url_existe'],
             ['event_crowfunding', 'match', 'pattern' => "/^[0-9]*$/i", 'message' => 'Sólo se aceptan números'],
             ['event_review','match', 'pattern' => "/^.{5,140}$/", 'message' => 'Mínimo 5 y máximo 140 caracteres'],
            ['event_description', 'match', 'pattern' => "/^.{10,400}$/", 'message' => 'Mínimo 10 y máximo 400 caracteres'],
            ['event_linkvideo', 'string', 'max' => 255,'message' => 'Máximo 255 caracteres'],
            ['event_name', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['event_galery', 'file',
             'maxSize' => 2097152, //2 MB
             'tooBig' => 'El tamaño máximo permitido es 2MB',
             'extensions' => 'png, jpg, jpeg',
             'wrongExtension' => 'El archivo {file} no contiene una extensión permitida {extensions}',
             'maxFiles' => 3,
             'tooMany' => '' //'El máximo de archivos permitidos son {limit}'
            ],
            ['event_image', 'image',
             'maxSize' => 2097152, //2 MB
             'tooBig' => 'El tamaño máximo permitido es 2MB',
             'extensions' => 'png, jpg, jpeg',
             'maxFiles' => 1,
             'tooMany' => 'El máximo de archivos permitidos son {limit}' 
            ],
            ['ticket_qty', 'match', 'pattern' => "/^[1-100]*$/", 'message'=> 'Valor invalido'],
            ['ticket_name', 'match', 'pattern' => "/^.{5,35}$/", 'message' => 'Mínimo 5 y máximo 35 caracteres'],
            ['ticket_description', 'match', 'pattern' => "/^.{10,50}$/", 'message' => 'Mínimo 10 y máximo 50 caracteres']
        ];
    }

    public function getAtributes()
    {
        $class = new ReflectionClass($this);
        $names = [];
        foreach ($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            if (!$property->isStatic()) {
                $names[] = $property->getName();
            }
        }
        return $names;
    }

    public function validateDate_Start($attribute, $params)
    {
      $time = strtotime($this->event_stardate);
      $newformat = date('Y-m-d',$time);
      $currentDate = date('Y-m-d');
      if ($newformat < $currentDate)
      {
        $this->addError($attribute, "Ingresa una fecha mayor o igual a la actual");
      }
    }
    
    public function validateDate_End($attribute, $params)
    {
      $dateStart = strtotime($this->event_stardate);
      $start = date('Y-m-d',$dateStart);
      $dateEnd = strtotime($this->event_enddate);
      $end = date('Y-m-d',$dateEnd);
      $currentDate = date('Y-m-d');
      if ($end < $currentDate || $start > $end)
      {
        $this->addError($attribute, "Ingresa una fecha mayor a la actual y de la de inicio");
      }
    }

    public function validateHour_End($attribute, $params)
    {
      $dateStart = strtotime($this->event_stardate);
      $start = date('Y-m-d',$dateStart);
      $dateEnd = strtotime($this->event_enddate);
      $end = date('Y-m-d',$dateEnd);
      if ($start == $end)
      {
        $hourStart = strtotime($this->event_starthour);
        $startHour = date('G:i',$hourStart);
        $hourEnd = strtotime($this->event_endhour);
        $endHour = date('G:i',$hourEnd);
        if($endHour > $startHour)
          $this->addError($attribute, "Ingresa una hora mayor a la de inicio");
      }
    }

    public function url_existe($attribute, $params)
    {
      $table = Event::find()->where("event_url=:event_url", [":event_url" => $this->event_url]);
      if ($table->count() == 1)
      {
        $this->addError($attribute, "La url seleccionada ya existe");
      }
    }
}