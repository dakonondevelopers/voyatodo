<?php
 
namespace app\models;
use Yii;
use yii\base\Model;
use app\models\User;

class FormUserRegister extends Model{
 
    public $username;
    public $last_name;
    public $email;
    public $password;
    public $password_repeat;
    public $user_phone1;
    public $user_phone2;
    
    public function rules()
    {
        return [
            [['username', 'email','user_phone1'], 'required','message' => 'Campo requerido'],
            [['username' , 'last_name'], 'match', 'not' => true ,'pattern' => "/[^a-zA-Z_ -]/", 'message' => 'Sólo se aceptan letras'],
            [['username','email'], 'match', 'pattern' => "/^.{3,100}$/", 'message' => 'Mínimo 3 y máximo 100 caracteres'],
            ['email', 'email', 'message' => 'Formato no válido'],
            ['email', 'email_existe'],
        ];
    }

    public function email_existe($attribute, $params)
    {
        $table = Users::find()->where("email=:email", [":email" => $this->email]);
        if ($table->count() == 1)
            $this->addError($attribute, "El email seleccionado existe");
    }
 
}