<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tadvertising".
 *
 * @property string $pkadvertising
 * @property string $advertising_name
 * @property string $advertising_total
 * @property integer $fkcreditcard
 *
 * @property VtTcreditcard $fkcreditcard0
 */
class VTTadvertising extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tadvertising';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['advertising_total', 'fkcreditcard'], 'integer'],
            [['fkcreditcard'], 'required'],
            [['advertising_name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkadvertising' => 'Pkadvertising',
            'advertising_name' => 'Advertising Name',
            'advertising_total' => 'Advertising Total',
            'fkcreditcard' => 'Fkcreditcard',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcreditcard0()
    {
        return $this->hasOne(VTTcreditcard::className(), ['pkcreditcard' => 'fkcreditcard']);
    }
}
