<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tavailability".
 *
 * @property string $pkavailability
 * @property string $availability_date
 * @property string $availability_status
 * @property string $availability_code
 * @property string $fkproduct
 *
 * @property VtTproduct $fkproduct0
 */
class VTTavailability extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tavailability';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['availability_date'], 'safe'],
            [['fkproduct'], 'integer'],
            [['availability_status'], 'string', 'max' => 1],
            [['availability_code'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkavailability' => 'Pkavailability',
            'availability_date' => 'Availability Date',
            'availability_status' => 'Availability Status',
            'availability_code' => 'Availability Code',
            'fkproduct' => 'Fkproduct',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkproduct0()
    {
        return $this->hasOne(VTTproduct::className(), ['pkproduct' => 'fkproduct']);
    }
}
