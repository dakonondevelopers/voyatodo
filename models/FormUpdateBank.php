<?php
 
namespace app\models;
use Yii;
use yii\base\Model;

class FormUpdateBank extends Model{
 
    public $fkbank;
    public $userbank_titularname;
    public $userbank_identification;
    public $userbank_numberacount;
    public $userbank_tipeacount;
    
    public function rules()
    {
        return [
             ['fkbank', 'required', 'message' => 'Seleccione un banco'],
             ['userbank_tipeacount', 'required', 'message' => 'Seleccione tipo de cuenta'],
             [['userbank_titularname', 
              'userbank_identification', 
              'userbank_numberacount',
             ], 'required', 'message' => 'Campo requerido'],
             [['userbank_identification', 'userbank_numberacount'], 'match', 'pattern' => "/^[0-9]+$/i", 'message' => 'Sólo se aceptan números'],
             
        ];
    }
}