<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Contact extends Model
{
	public $name;
	public $email;
	public $body;
public function rules()
{
	
	
	
	return [

			[['name', 'email', 'body'],'required', 'message'=> 'Este campo es requerido'],
			[['body'], 'string', 'max'=>150,  'message'=> 'el mensaje debe contener menos de 150 caracteres '],
			[['name'], 'string', 'message'=> 'Debe ingresar un email valido'],
			[['email'], 'email', 'message'=> 'Debe ingresar un email valido'],
			
			
	];
}
public function attributeLabels()
{
	return [
			'name' => 'Nombre',
			'email' => 'Correo',
			'body' => 'Cuerpo del mensaje',

	];
}

}