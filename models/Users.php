<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tuser".
 *
 * @property string $id
 * @property string $username
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $user_phone1
 * @property string $user_phone2
 * @property string $authkey
 * @property string $accessToken
 * @property integer $activate
 * @property string $verification_code
 * @property integer $rol
 * @property string $user_date
 * @property string $user_image
 * @property string $user_facebook
 * @property string $user_twitter
 * @property string $user_google
 * @property string $user_youtube
 * @property integer $user_calification
 *
 * @property VtTbuy[] $vtTbuys
 * @property VtTcreditcard[] $vtTcreditcards
 * @property VtTevent[] $vtTevents
 * @property VtTfavorites[] $vtTfavorites
 * @property VtTformgenericresponse[] $vtTformgenericresponses
 * @property VtTmessage[] $vtTmessages
 * @property VtTproduct[] $vtTproducts
 * @property VtTproposal[] $vtTproposals
 * @property VtTrol $rol0
 * @property VtTuserbank[] $vtTuserbanks
 * @property VtTusercalification[] $vtTusercalifications
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tuser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password'], 'string'],
            [['activate', 'rol', 'user_calification'], 'integer'],
            [['rol'], 'required'],
            [['user_date'], 'safe'],
            [['username', 'email'], 'string', 'max' => 100],
            [['last_name', 'user_phone1', 'user_phone2'], 'string', 'max' => 45],
            [['authkey', 'accessToken', 'verification_code'], 'string', 'max' => 250],
            [['user_image', 'user_facebook', 'user_twitter', 'user_google', 'user_youtube'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'password' => 'Password',
            'user_phone1' => 'User Phone1',
            'user_phone2' => 'User Phone2',
            'authkey' => 'Authkey',
            'accessToken' => 'Access Token',
            'activate' => 'Activate',
            'verification_code' => 'Verification Code',
            'rol' => 'Rol',
            'user_date' => 'User Date',
            'user_image' => 'User Image',
            'user_facebook' => 'User Facebook',
            'user_twitter' => 'User Twitter',
            'user_google' => 'User Google',
            'user_youtube' => 'User Youtube',
            'user_calification' => 'User Calification',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTbuys()
    {
        return $this->hasMany(VtTbuy::className(), ['fkuser' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTcreditcards()
    {
        return $this->hasMany(VtTcreditcard::className(), ['fkuser' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTevents()
    {
        return $this->hasMany(VtTevent::className(), ['fkuser' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTfavorites()
    {
        return $this->hasMany(VtTfavorites::className(), ['fkuser' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTformgenericresponses()
    {
        return $this->hasMany(VtTformgenericresponse::className(), ['fkuser' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTmessages()
    {
        return $this->hasMany(VtTmessage::className(), ['fkuser' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTproducts()
    {
        return $this->hasMany(VtTproduct::className(), ['fkuser' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTproposals()
    {
        return $this->hasMany(VtTproposal::className(), ['fkuser' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRol0()
    {
        return $this->hasOne(VtTrol::className(), ['pkrol' => 'rol']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTuserbanks()
    {
        return $this->hasMany(VtTuserbank::className(), ['fkuser' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTusercalifications()
    {
        return $this->hasMany(VtTusercalification::className(), ['fkuser' => 'id']);
    }
}
