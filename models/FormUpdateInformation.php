<?php
 
namespace app\models;
use Yii;
use yii\base\Model;
use app\models\User;

class FormUpdateInformation extends Model{
 
    public $username;
    public $last_name;
    public $user_phone1;
    public $user_phone2;
    public $user_photo;
    public $fkbank;
    public $tipeacount;
    
    public function rules()
    {
        return [
            ['username', 'required', 'message' => 'Campo requerido'],
            [['username' , 'last_name'], 'match', 'not' => true ,'pattern' => "/[^a-zA-Z_ -]/", 'message' => 'Sólo se aceptan letras'],
            [['username','last_name'], 'match', 'pattern' => "/^.{3,100}$/", 'message' => 'Mínimo 3 y máximo 100 caracteres'],
            [['user_phone1', 'user_phone2'], 'match', 'pattern' => "/^[0-9]+$/i", 'message' => 'Sólo se aceptan números'],
            [['user_phone1', 'user_phone2'], 'match', 'pattern' => "/^.{5,45}$/", 'message' => 'Mínimo 5 y máximo 45 caracteres'],
            ['user_phone2', 'validate_Phone'],
            ['user_photo', 'image',
             'maxSize' => 2097152, //2 MB
             'tooBig' => 'El tamaño máximo permitido es 2MB',
             'extensions' => 'png, jpg, jpeg',
             'maxFiles' => 1,
             'tooMany' => 'El máximo de archivos permitidos son {limit}' 
            ]
        ];
    }

    public function validate_Phone($attribute, $params)
    {
        if($this->user_phone1 == $this->user_phone2)
            $this->addError($attribute, "Los télefonos son iguales");
    }
 
}