<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tevent".
 *
 * @property string $pkevent
 * @property string $event_name
 * @property string $event_lat
 * @property string $event_log
 * @property string $event_stardate
 * @property string $event_enddate
 * @property string $event_starthour
 * @property string $event_endhour
 * @property string $event_url
 * @property string $event_image
 * @property string $event_review
 * @property string $event_description
 * @property string $event_place
 * @property string $event_address
 * @property string $fkcity
 * @property string $fkuser
 * @property string $event_linkvideo
 * @property double $event_opacityimage
 * @property integer $event_sponsorship
 * @property string $event_terms
 * @property integer $event_visible
 * @property string $fkcategory1
 * @property string $fkcategory2
 * @property integer $fkstatus
 * @property string $event_crowfunding
 *
 * @property VtTcategory $fkcategory10
 * @property VtTcategory $fkcategory20
 * @property VtTcity $fkcity0
 * @property VtTuser $fkuser0
 * @property VtTstatus $fkstatus0
 * @property VtTfavorites[] $vtTfavorites
 * @property VtTformeventgeneric[] $vtTformeventgenerics
 * @property VtTimage[] $vtTimages
 * @property VtTproposal[] $vtTproposals
 * @property VtTpublicity[] $vtTpublicities
 * @property VtTticket[] $vtTtickets
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tevent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_stardate', 'event_enddate', 'event_starthour', 'event_endhour'], 'safe'],
            [['event_description', 'event_terms'], 'string'],
            [['event_description'], 'string', 'max' => 400],
            [['fkcity', 'fkuser', 'event_sponsorship', 'event_visible', 'fkcategory1', 'fkcategory2', 'fkstatus', 'event_crowfunding'], 'integer'],
            [['fkuser'], 'required'],
            [['event_opacityimage'], 'number'],
            [['event_name'], 'string', 'max' => 80],
            [['event_lat', 'event_log', 'event_url', 'event_image', 'event_place', 'event_address'], 'string', 'max' => 100],
            [['event_review'], 'string', 'max' => 140],
            [['event_linkvideo'], 'string', 'max' => 255],
            [['event_url'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkevent' => 'Pkevent',
            'event_name' => 'Event Name',
            'event_lat' => 'Event Lat',
            'event_log' => 'Event Log',
            'event_stardate' => 'Event Stardate',
            'event_enddate' => 'Event Enddate',
            'event_starthour' => 'Event Starthour',
            'event_endhour' => 'Event Endhour',
            'event_url' => 'Event Url',
            'event_image' => 'Event Image',
            'event_review' => 'Event Review',
            'event_description' => 'Event Description',
            'event_place' => 'Event Place',
            'event_address' => 'Event Address',
            'fkcity' => 'Fkcity',
            'fkuser' => 'Fkuser',
            'event_linkvideo' => 'Event Linkvideo',
            'event_opacityimage' => 'Event Opacityimage',
            'event_sponsorship' => 'Event Sponsorship',
            'event_terms' => 'Event Terms',
            'event_visible' => 'Event Visible',
            'fkcategory1' => 'Fkcategory1',
            'fkcategory2' => 'Fkcategory2',
            'fkstatus' => 'Fkstatus',
            'event_crowfunding' => 'Event Crowfunding',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcategory10()
    {
        return $this->hasOne(VTTcategory::className(), ['pkcategory' => 'fkcategory1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcategory20()
    {
        return $this->hasOne(VTTcategory::className(), ['pkcategory' => 'fkcategory2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcity0()
    {
        return $this->hasOne(VTTcity::className(), ['pkcity' => 'fkcity']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(Users::className(), ['id' => 'fkuser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkstatus0()
    {
        return $this->hasOne(VTTstatus::className(), ['pkstatus' => 'fkstatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTfavorites()
    {
        return $this->hasMany(VTTfavorites::className(), ['fkevent' => 'pkevent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTformeventgenerics()
    {
        return $this->hasMany(VTTformeventgeneric::className(), ['fkevent' => 'pkevent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTimages()
    {
        return $this->hasMany(VTTimage::className(), ['fkevent' => 'pkevent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTproposals()
    {
        return $this->hasMany(VTTproposal::className(), ['fkevent' => 'pkevent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTpublicities()
    {
        return $this->hasMany(VTTpublicity::className(), ['fkevent' => 'pkevent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTtickets()
    {
        return $this->hasMany(VTTticket::className(), ['fkevent' => 'pkevent']);
    }
}
