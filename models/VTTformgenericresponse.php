<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tformgenericresponse".
 *
 * @property string $pkformgenericresponse
 * @property string $formgenericresponse_response
 * @property string $formgenericresponse_code
 * @property integer $formgenericresponse_sequence
 * @property string $fkformeventgeneric
 * @property string $fkuser
 *
 * @property VtTuser $fkuser0
 */
class VTTformgenericresponse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tformgenericresponse';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['formgenericresponse_sequence', 'fkformeventgeneric', 'fkuser'], 'integer'],
            [['fkformeventgeneric', 'fkuser'], 'required'],
            [['formgenericresponse_response'], 'string', 'max' => 300],
            [['formgenericresponse_code'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkformgenericresponse' => 'Pkformgenericresponse',
            'formgenericresponse_response' => 'Formgenericresponse Response',
            'formgenericresponse_code' => 'Formgenericresponse Code',
            'formgenericresponse_sequence' => 'Formgenericresponse Sequence',
            'fkformeventgeneric' => 'Fkformeventgeneric',
            'fkuser' => 'Fkuser',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(Users::className(), ['id' => 'fkuser']);
    }
}
