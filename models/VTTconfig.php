<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tconfig".
 *
 * @property integer $pkconfig
 * @property string $config_comision1
 * @property string $config_comision2
 * @property string $config_comision3
 * @property string $config_comision4
 * @property string $config_days
 * @property string $config_discount_crowfunding
 */
class VTTconfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tconfig';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['config_comision1', 'config_comision2', 'config_comision3', 'config_comision4', 'config_discount_crowfunding'], 'number'],
            [['config_days'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkconfig' => 'Pkconfig',
            'config_comision1' => 'Config Comision1',
            'config_comision2' => 'Config Comision2',
            'config_comision3' => 'Config Comision3',
            'config_comision4' => 'Config Comision4',
            'config_days' => 'Config Days',
            'config_discount_crowfunding' => 'Config Discount Crowfunding',
        ];
    }
}
