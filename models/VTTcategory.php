<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tcategory".
 *
 * @property string $pkcategory
 * @property string $category_name
 * @property string $category_route
 * @property string $category_pathImage
 * @property string $category_pathicon
 *
 * @property VtTevent[] $vtTevents
 * @property VtTevent[] $vtTevents0
 */
class VTTcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pkcategory'], 'required'],
            [['pkcategory'], 'integer'],
            [['category_name', 'category_route', 'category_pathImage', 'category_pathicon'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkcategory' => 'Pkcategory',
            'category_name' => 'Category Name',
            'category_route' => 'Category Route',
            'category_pathImage' => 'Category Path Image',
            'category_pathicon' => 'Category Pathicon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTevents()
    {
        return $this->hasMany(Event::className(), ['fkcategory1' => 'pkcategory']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTevents0()
    {
        return $this->hasMany(Event::className(), ['fkcategory2' => 'pkcategory']);
    }
}
