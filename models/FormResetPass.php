<?php
 
namespace app\models;
use Yii;
use yii\base\Model;
use app\models\User;

class FormResetPass extends Model{
 
    public $password;
    public $password_repeat;
    
    public function rules()
    {
        return [
            [['password', 'password_repeat'], 'required', 'message' => 'Campo requerido'],
            ['password', 'match', 'pattern' => "/^.{6,16}$/", 'message' => 'Mínimo 6 y máximo 16 caracteres'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Las contraseñas no coinciden'],
        ];
    }
 
}