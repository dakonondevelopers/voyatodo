<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tproposaldetails".
 *
 * @property string $pkproposaldetails
 * @property string $proposaldetails_image
 * @property string $proposaldetails_url
 *
 * @property VtTproposal[] $vtTproposals
 */
class VTTproposaldetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tproposaldetails';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proposaldetails_image', 'proposaldetails_url'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkproposaldetails' => 'Pkproposaldetails',
            'proposaldetails_image' => 'Proposaldetails Image',
            'proposaldetails_url' => 'Proposaldetails Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTproposals()
    {
        return $this->hasMany(VtTproposal::className(), ['fkproposaldetails' => 'pkproposaldetails']);
    }
}
