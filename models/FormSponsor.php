<?php
 
namespace app\models;
use Yii;
use yii\base\Model;
use app\models\Event;
use yii\web\UploadedFile;


class FormSponsor extends Model{
 
  public $creditcard_type;
  public $creditcard_titularname;
  public $creditcard_numbercard;
  public $sponsor_banner;
  public $sponsor_url;
  public $event_url;
  public $proposal_neto1;
  public $proposal_total1;
  public $proposal_contributions;

  public function rules()
  {
    return [
        ['creditcard_titularname','match', 'pattern' => "/^.{5,100}$/", 'message' => 'Mínimo 5 y máximo 100 caracteres'],
        ['creditcard_titularname', 'match', 'not' => true ,'pattern' => "/[^a-zA-Z_ -]/", 'message' => 'Sólo se aceptan letras'],
        ['creditcard_numbercard', 'match', 'pattern' => "/^.{5,11}$/", 'message' => 'Mínimo 5 y máximo 11 caracteres'],
        ['creditcard_numbercard', 'match', 'pattern' => "/^[0-9]+$/i", 'message' => 'Sólo se aceptan números'],
        ['sponsor_banner', 'image',
         'maxSize' => 2097152, //2 MB
         'tooBig' => 'El tamaño máximo permitido es 2MB',
         'extensions' => 'png, jpg, jpeg',
         'wrongExtension' => 'El archivo {file} no contiene una extensión permitida {extensions}',
         'maxFiles' => 1,
         'tooMany' => 'El máximo de archivos permitidos son {limit}'
        ],
    ];

  }
}