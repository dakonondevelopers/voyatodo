<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class MailSend extends Model
{
    public $email;
    public $message;
    public $subject;
    public $file;
    public $image;
    public $response;
    public $event_stardate;
    public $event_starthour;
    public $event_name;
    public $event_place;
    public $event_url;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['message', 'subject'], 'required', 'message'=>'Complete este campo'],
            ['subject', 'match', 'pattern' => "/^.{1,140}$/", 'message' => 'Máximo 140 caracteres'],
            [['response','email'], 'email', 'message' => 'El correo ingresado no tiene un formato valido']
        ];
    }
}