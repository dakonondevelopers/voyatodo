<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tmessage".
 *
 * @property integer $pkmessage
 * @property string $message_message
 * @property string $message_date
 * @property string $message_senduser
 * @property integer $fkproposal
 * @property integer $fkstatus
 * @property string $fkuser
 *
 * @property VtTproposal $fkproposal0
 * @property VtTstatus $fkstatus0
 * @property VtTuser $fkuser0
 */
class VTTmessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tmessage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_message'], 'string'],
            [['message_date'], 'safe'],
            [['fkproposal', 'fkstatus', 'fkuser'], 'required'],
            [['fkproposal', 'fkstatus', 'fkuser'], 'integer'],
            [['message_senduser'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkmessage' => 'Pkmessage',
            'message_message' => 'Message Message',
            'message_date' => 'Message Date',
            'message_senduser' => 'Message Senduser',
            'fkproposal' => 'Fkproposal',
            'fkstatus' => 'Fkstatus',
            'fkuser' => 'Fkuser',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkproposal0()
    {
        return $this->hasOne(VTTproposal::className(), ['pkproposal' => 'fkproposal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkstatus0()
    {
        return $this->hasOne(VTTstatus::className(), ['pkstatus' => 'fkstatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(User::className(), ['id' => 'fkuser']);
    }
}
