<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tproduct".
 *
 * @property string $pkproduct
 * @property string $product_name
 * @property string $product_review
 * @property string $product_description
 * @property string $product_terms
 * @property integer $product_qty
 * @property string $product_value
 * @property integer $fkcategory
 * @property string $fkuser
 *
 * @property VtTavailability[] $vtTavailabilities
 * @property VtTimage[] $vtTimages
 * @property VtTcategoryproduct $fkcategory0
 * @property VtTuser $fkuser0
 */
class VTTproduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tproduct';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_description', 'product_terms'], 'string'],
            [['product_qty', 'fkcategory', 'fkuser'], 'integer'],
            [['product_value'], 'number'],
            [['product_name'], 'string', 'max' => 80],
            [['product_review'], 'string', 'max' => 140]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkproduct' => 'Pkproduct',
            'product_name' => 'Product Name',
            'product_review' => 'Product Review',
            'product_description' => 'Product Description',
            'product_terms' => 'Product Terms',
            'product_qty' => 'Product Qty',
            'product_value' => 'Product Value',
            'fkcategory' => 'Fkcategory',
            'fkuser' => 'Fkuser',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTavailabilities()
    {
        return $this->hasMany(VTTavailability::className(), ['fkproduct' => 'pkproduct']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTimages()
    {
        return $this->hasMany(VTTimage::className(), ['fkproduct' => 'pkproduct']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcategory0()
    {
        return $this->hasOne(VTTcategoryproduct::className(), ['pkcategoryproduct' => 'fkcategory']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(Users::className(), ['id' => 'fkuser']);
    }
}
