<?php
 
namespace app\models;
use Yii;
use yii\base\Model;
use \yii\web\UploadedFile;


class FormAdvertiser extends Model{
 
  public $product_name;
  public $product_review;
  public $product_description;
  public $product_image;
  public $product_terms;
  public $product_qty;
  public $product_value;
  public $fkcategory;
  public $creditcard_titularname;
  public $userbank_titularname;
  public $userbank_identification;
  public $userbank_numberacount;
  public $userbank_tipeacount;
  public $fkbank;
  public $creditcard_numbercard;
  public $creditcard_type;
  public $product_url;
  public $dates;
  public $recib;


  public function rules()
  {
    return [
        [['product_name',
          'product_review',
          'product_description',
          'product_terms',
          'product_value',
          'fkcategory'
          ],'required', 'message' => 'Campo requerido'],
        ['product_name','match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
        ['product_review', 'match', 'pattern' => "/^.{5,140}$/", 'message' => 'Mínimo 5 y máximo 140 caracteres'],
        [['product_terms','product_description'], 'string', 'min' => 10, 'message' => 'Mínimo 10 caracteres'],
        [['product_value','recib'], 'match', 'pattern' => "/^[0-9]*$/i", 'message' => 'Sólo se aceptan números'],
        ['product_image', 'image',
         'maxSize' => 2097152, //2 MB
         'tooBig' => 'El tamaño máximo permitido es 2MB',
         'extensions' => 'png, jpg, jpeg',
         'maxFiles' => 3,
         'tooMany' => 'El máximo de archivos permitidos son {limit}' 
        ]
    ];

  }
}