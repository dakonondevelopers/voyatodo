<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tuserbank".
 *
 * @property string $pkuserbank
 * @property string $userbank_tipeacount
 * @property string $userbank_numberacount
 * @property string $userbank_identification
 * @property string $userbank_titularname
 * @property string $fkuser
 * @property string $fkbank
 *
 * @property VtTbank $fkbank0
 * @property VtTuser $fkuser0
 */
class VTTuserbank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tuserbank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkuser', 'fkbank'], 'required'],
            [['fkuser', 'fkbank'], 'integer'],
            [['userbank_tipeacount', 'userbank_numberacount', 'userbank_identification', 'userbank_titularname'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkuserbank' => 'Pkuserbank',
            'userbank_tipeacount' => 'Userbank Tipeacount',
            'userbank_numberacount' => 'Userbank Numberacount',
            'userbank_identification' => 'Userbank Identification',
            'userbank_titularname' => 'Userbank Titularname',
            'fkuser' => 'Fkuser',
            'fkbank' => 'Fkbank',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkbank0()
    {
        return $this->hasOne(VTTbank::className(), ['pkbank' => 'fkbank']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(Users::className(), ['id' => 'fkuser']);
    }
}
