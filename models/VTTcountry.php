<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "VT_Tcountry".
 *
 * @property string $pkcountry
 * @property string $coutry_name
 *
 * @property VTTdepartament[] $vTTdepartaments
 */
class VTTcountry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tcountry';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pkcountry'], 'required'],
            [['pkcountry'], 'integer'],
            [['coutry_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkcountry' => 'Pkcountry',
            'coutry_name' => 'Coutry Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVTTdepartaments()
    {
        return $this->hasMany(VTTdepartament::className(), ['fkcountry' => 'pkcountry']);
    }
}
