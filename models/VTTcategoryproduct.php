<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tcategoryproduct".
 *
 * @property integer $pkcategoryproduct
 * @property string $categoryproduct_name
 *
 * @property VtTproduct[] $vtTproducts
 */
class VTTcategoryproduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tcategoryproduct';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryproduct_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkcategoryproduct' => 'Pkcategoryproduct',
            'categoryproduct_name' => 'Categoryproduct Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTproducts()
    {
        return $this->hasMany(VTTproduct::className(), ['fkcategory' => 'pkcategoryproduct']);
    }
}
