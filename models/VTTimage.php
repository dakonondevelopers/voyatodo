<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_timage".
 *
 * @property string $pkimage
 * @property string $image_path
 * @property string $image_date
 * @property string $fkevent
 * @property string $fkproduct
 *
 * @property VtTevent $fkevent0
 * @property VtTproduct $fkproduct0
 */
class VTTimage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_timage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_date'], 'safe'],
            [['fkevent', 'fkproduct'], 'integer'],
            [['image_path'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkimage' => 'Pkimage',
            'image_path' => 'Image Path',
            'image_date' => 'Image Date',
            'fkevent' => 'Fkevent',
            'fkproduct' => 'Fkproduct',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkevent0()
    {
        return $this->hasOne(Event::className(), ['pkevent' => 'fkevent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkproduct0()
    {
        return $this->hasOne(VTTproduct::className(), ['pkproduct' => 'fkproduct']);
    }
}
