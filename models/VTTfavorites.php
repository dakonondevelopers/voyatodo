<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tfavorites".
 *
 * @property string $pkfavorites
 * @property string $fkevent
 * @property string $fkuser
 *
 * @property VtTevent $fkevent0
 * @property VtTuser $fkuser0
 */
class VTTfavorites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tfavorites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkevent', 'fkuser'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkfavorites' => 'Pkfavorites',
            'fkevent' => 'Fkevent',
            'fkuser' => 'Fkuser',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkevent0()
    {
        return $this->hasOne(Event::className(), ['pkevent' => 'fkevent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(Users::className(), ['id' => 'fkuser']);
    }
}
