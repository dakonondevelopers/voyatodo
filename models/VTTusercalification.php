<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tusercalification".
 *
 * @property string $pkusercalification
 * @property integer $usercalification
 * @property string $fkuser
 */
class VtTusercalification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tusercalification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usercalification', 'fkuser'], 'integer'],
            [['fkuser'], 'exist', 'skipOnError' => true, 'targetClass' => VtTuser::className(), 'targetAttribute' => ['fkuser' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkusercalification' => 'Pkusercalification',
            'usercalification' => 'Usercalification',
            'fkuser' => 'Fkuser',
        ];
    }
}
