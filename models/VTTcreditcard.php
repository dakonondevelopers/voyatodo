<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tcreditcard".
 *
 * @property integer $pkcreditcard
 * @property string $creditcard_type
 * @property string $creditcard_titularname
 * @property integer $creditcard_numbercard
 * @property string $fkuser
 *
 * @property VtTadvertising[] $vtTadvertisings
 * @property VtTuser $fkuser0
 */
class VTTcreditcard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tcreditcard';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creditcard_numbercard', 'fkuser'], 'integer'],
            [['fkuser'], 'required'],
            [['creditcard_type', 'creditcard_titularname'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkcreditcard' => 'Pkcreditcard',
            'creditcard_type' => 'Creditcard Type',
            'creditcard_titularname' => 'Creditcard Titularname',
            'creditcard_numbercard' => 'Creditcard Numbercard',
            'fkuser' => 'Fkuser',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTadvertisings()
    {
        return $this->hasMany(VTTadvertising::className(), ['fkcreditcard' => 'pkcreditcard']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkuser0()
    {
        return $this->hasOne(Users::className(), ['id' => 'fkuser']);
    }
}
