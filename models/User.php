<?php

namespace app\models;
use Yii;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    
    public $id;
    public $username;
    public $last_name;
    public $email;
    public $password;
    public $authkey;
    public $accessToken;
    public $activate;
    public $verification_code;
    public $rol;
    public $user_phone1;
    public $user_phone2;
    public $user_date;
    public $user_image;
    public $user_facebook;
    public $user_twitter;
    public $user_google;
    public $user_youtube;
    public $user_calification;
    
    public static function isUserAdmin($id)
    {
        if (Users::findOne(['id' => $id, 'activate' => '1', 'rol' => 1]))
            return true;
        else
            return false;
    }

    public static function isUserSponsor($id)
    {
        if (Users::findOne(['id' => $id, 'activate' => '1', 'rol' => 2]))
            return true;
        else 
            return false;
    }

    public static function isUserCustomer($id)
    {
        if (Users::findOne(['id' => $id, 'activate' => '1', 'rol' => 3]))
            return true;
        else
            return false;
    }

    public static function isUserAdvertiser($id)
    {
        if (Users::findOne(['id' => $id, 'activate' => '1', 'rol' => 4]))
            return true;
        else
            return false;
    }

    public static function findIdentity($id)
    {
        $user = Users::find()
                ->where("activate=:activate", [":activate" => 1])
                ->andWhere("id=:id", ["id" => $id])
                ->one();
        return isset($user) ? new static($user) : null;
    }

    /**
     * @inheritdoc
     */
    
    /* Busca la identidad del usuario a través de su token de acceso */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $users = Users::find()
                ->where("activate=:activate", [":activate" => 1])
                ->andWhere("accessToken=:accessToken", [":accessToken" => $token])
                ->all();
        foreach ($users as $user) {
            if ($user->accessToken === $token) {
                return new static($user);
            }
        }
        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    
    /* Busca la identidad del usuario a través del username */
    public static function findByUsername($username)
    {
        $users = Users::find()
                ->where("activate=:activate", [":activate" => 1])
                ->andWhere("email=:username", [":username" => $username])
                ->all();
        foreach ($users as $user) {
            if (strcasecmp($user->email, $username) === 0) {
                return new static($user);
            }
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    
    /* Regresa el id del usuario */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    
    /* Regresa la clave de autenticación */
    public function getAuthKey()
    {
        return $this->authkey;
    }

    /**
     * @inheritdoc
     */
    
    /* Valida la clave de autenticación */
    public function validateAuthKey($authKey)
    {
        return $this->authkey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        /* Valida el password */
        if (crypt($password, $this->password) == $this->password)
        {
            return $password === $password;
        }
    }

    public static function validateRol()
    {
        if (User::isUserAdmin(Yii::$app->user->identity->id))
            return "/admin";
        if (User::isUserSponsor(Yii::$app->user->identity->id))
            return "/sponsor";
        if (User::isUserCustomer(Yii::$app->user->identity->id))
            return "/account";
        if (User::isUserAdvertiser(Yii::$app->user->identity->id))
            return "/advertiser";
    }
}