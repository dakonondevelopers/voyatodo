<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tformeventgeneric".
 *
 * @property string $pkformeventgeneric
 * @property string $formeventgeneric
 * @property string $fkevent
 *
 * @property VtTevent $fkevent0
 */
class VTTformeventgeneric extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tformeventgeneric';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkevent'], 'required'],
            [['fkevent'], 'integer'],
            [['formeventgeneric'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkformeventgeneric' => 'Pkformeventgeneric',
            'formeventgeneric' => 'Formeventgeneric',
            'fkevent' => 'Fkevent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkevent0()
    {
        return $this->hasOne(Event::className(), ['pkevent' => 'fkevent']);
    }
}
