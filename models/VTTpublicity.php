<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tpublicity".
 *
 * @property string $pkpublicity
 * @property string $publicity_name
 * @property string $publicity_value
 * @property string $fkevent
 * @property integer $fkcreditcard
 * @property string $fkproduct
 *
 * @property VtTbuy[] $vtTbuys
 * @property VtTcreditcard $fkcreditcard0
 * @property VtTevent $fkevent0
 * @property VtTproduct $fkproduct0
 */
class VTTpublicity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tpublicity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['publicity_value', 'fkevent', 'fkcreditcard', 'fkproduct'], 'integer'],
            [['publicity_name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkpublicity' => 'Pkpublicity',
            'publicity_name' => 'Publicity Name',
            'publicity_value' => 'Publicity Value',
            'fkevent' => 'Fkevent',
            'fkcreditcard' => 'Fkcreditcard',
            'fkproduct' => 'Fkproduct',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTbuys()
    {
        return $this->hasMany(VTTbuy::className(), ['fkpublicity' => 'pkpublicity']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcreditcard0()
    {
        return $this->hasOne(VTTcreditcard::className(), ['pkcreditcard' => 'fkcreditcard']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkevent0()
    {
        return $this->hasOne(Event::className(), ['pkevent' => 'fkevent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkproduct0()
    {
        return $this->hasOne(VTTproduct::className(), ['pkproduct' => 'fkproduct']);
    }
}
