<?php
 
namespace app\models;
use Yii;
use yii\base\Model;
use app\models\User;

class FormSponsorRegister extends Model{
 
    public $username;
    public $last_name;
    public $email;
    public $user_phone1;
    public $user_phone2;
    
    public function rules()
    {
        return [
            [['username', 'last_name', 'email', 'user_phone1','user_phone2'], 'required', 'message' => 'Campo requerido'],
            [['username' , 'last_name'], 'match', 'not' => true ,'pattern' => "/[^a-zA-Z_ -]/", 'message' => 'Sólo se aceptan letras'],
            [['username','last_name','email'], 'match', 'pattern' => "/^.{3,100}$/", 'message' => 'Mínimo 3 y máximo 100 caracteres'],
            ['email', 'email', 'message' => 'Formato no válido'],
            ['email', 'email_existe'],
            [['user_phone1', 'user_phone2'], 'match', 'pattern' => "/^[0-9]+$/i", 'message' => 'Sólo se aceptan números'],
            [['user_phone1', 'user_phone2'], 'match', 'pattern' => "/^.{5,45}$/", 'message' => 'Mínimo 5 y máximo 45 caracteres'],
            ['user_phone2', 'validate_Phone']
        ];
    }
    
    public function validate_Phone($attribute, $params)
    {
        if($this->user_phone1 == $this->user_phone2)
            $this->addError($attribute, "Los télefonos son iguales");
    }

    public function email_existe($attribute, $params)
    {
        $table = Users::find()->where("email=:email", [":email" => $this->email]);
        if ($table->count() == 1)
            $this->addError($attribute, "El email seleccionado existe");
    }
 
}