<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_tticket".
 *
 * @property string $pkticket
 * @property string $ticket_name
 * @property string $ticket_description
 * @property string $ticket_value
 * @property string $ticket_comision
 * @property string $ticket_insentive
 * @property string $ticket_enddate
 * @property integer $ticket_qty
 * @property integer $ticket_seeclaim
 * @property string $ticket_start
 * @property string $ticket_finish
 * @property string $ticket_code
 * @property string $fkevent
 * @property string $fktipetickect
 *
 * @property VtTbuy[] $vtTbuys
 * @property VtTevent $fkevent0
 * @property VtTypetickect $fktipetickect0
 */
class VTTticket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_value', 'ticket_comision', 'ticket_qty', 'ticket_seeclaim', 'fkevent', 'fktipetickect'], 'integer'],
            [['ticket_enddate', 'ticket_start', 'ticket_finish'], 'safe'],
            [['fktipetickect'], 'required'],
            [['ticket_name'], 'string', 'max' => 35],
            [['ticket_description'], 'string', 'max' => 50],
            [['ticket_insentive'], 'string', 'max' => 100],
            [['ticket_code'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkticket' => 'Pkticket',
            'ticket_name' => 'Ticket Name',
            'ticket_description' => 'Ticket Description',
            'ticket_value' => 'Ticket Value',
            'ticket_comision' => 'Ticket Comision',
            'ticket_insentive' => 'Ticket Insentive',
            'ticket_enddate' => 'Ticket Enddate',
            'ticket_qty' => 'Ticket Qty',
            'ticket_seeclaim' => 'Ticket Seeclaim',
            'ticket_start' => 'Ticket Start',
            'ticket_finish' => 'Ticket Finish',
            'ticket_code' => 'Ticket Code',
            'fkevent' => 'Fkevent',
            'fktipetickect' => 'Fktipetickect',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTbuys()
    {
        return $this->hasMany(VTTbuy::className(), ['fkticket' => 'pkticket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkevent0()
    {
        return $this->hasOne(Event::className(), ['pkevent' => 'fkevent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFktipetickect0()
    {
        return $this->hasOne(VTTypetickect::className(), ['pktypetickect' => 'fktipetickect']);
    }
}
