<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "VT_Tdepartament".
 *
 * @property string $pkdepartament
 * @property string $departament_name
 * @property string $fkcountry
 *
 * @property VTTcity[] $vTTcities
 * @property VTTcountry $fkcountry0
 */
class VTTdepartament extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_tdepartament';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkcountry'], 'required'],
            [['fkcountry'], 'integer'],
            [['departament_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkdepartament' => 'Pkdepartament',
            'departament_name' => 'Departament Name',
            'fkcountry' => 'Fkcountry',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVTTcities()
    {
        return $this->hasMany(VTTcity::className(), ['fkdepartament' => 'pkdepartament']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkcountry0()
    {
        return $this->hasOne(VTTcountry::className(), ['pkcountry' => 'fkcountry']);
    }
}
