<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vt_typetickect".
 *
 * @property string $pktypetickect
 * @property string $typeticket_name
 *
 * @property VtTticket[] $vtTtickets
 */
class VTTypetickect extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vt_typetickect';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['typeticket_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pktypetickect' => 'Pktypetickect',
            'typeticket_name' => 'Typeticket Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVtTtickets()
    {
        return $this->hasMany(VtTticket::className(), ['fktipetickect' => 'pktypetickect']);
    }
}
